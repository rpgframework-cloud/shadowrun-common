package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public interface IShadowrunCharacterController<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, X extends ASpell, C extends ShadowrunCharacter<S,V,?,X>>
   extends CharacterController<ShadowrunAttribute,C> {

	public C getModel();

	//-------------------------------------------------------------------
	public IAttributeController getAttributeController();

	//-------------------------------------------------------------------
	public ISkillController<S, V> getSkillController();

	//-------------------------------------------------------------------
	public IQualityController getQualityController();

	//-------------------------------------------------------------------
	public IAdeptPowerController getAdeptPowerController();

	//-------------------------------------------------------------------
	public ISpellController<X> getSpellController();

	//-------------------------------------------------------------------
	public IRitualController getRitualController();

	//-------------------------------------------------------------------
	public IComplexFormController getComplexFormController();

	//-------------------------------------------------------------------
	public IContactController getContactController();

	//-------------------------------------------------------------------
	public IMetamagicOrEchoController getMetamagicOrEchoController();

	//-------------------------------------------------------------------
	public SINController getSINController();

	//-------------------------------------------------------------------
	public ILifestyleController getLifestyleController();

	//-------------------------------------------------------------------
	public IPANController getPANController();

	//-------------------------------------------------------------------
	public IEquipmentController<?,?,?> getEquipmentController();

	//-------------------------------------------------------------------
	public IFocusController getFocusController();

	//-------------------------------------------------------------------
	public ICritterPowerController getCritterPowerController();

}
