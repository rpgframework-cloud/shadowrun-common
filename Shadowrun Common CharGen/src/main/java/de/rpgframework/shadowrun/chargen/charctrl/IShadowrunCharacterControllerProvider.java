package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.chargen.CharacterControllerProvider;

/**
 * @author prelle
 *
 */
public interface IShadowrunCharacterControllerProvider<C extends IShadowrunCharacterController> extends CharacterControllerProvider<C> {

}
