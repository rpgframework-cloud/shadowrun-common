package de.rpgframework.shadowrun.chargen.gen;

import java.util.List;
import java.util.Map;

import de.rpgframework.shadowrun.Priority;
import de.rpgframework.shadowrun.PriorityOption;
import de.rpgframework.shadowrun.PriorityType;
import de.rpgframework.shadowrun.ShadowrunAttribute;

/**
 * @author Stefan
 *
 */
public interface IPrioritySettings  {

	//-------------------------------------------------------------------
	public Map<ShadowrunAttribute, PerAttributePoints> perAttrib();
	public Map<PriorityType, Priority> priorities();
	
	//-------------------------------------------------------------------
	public void setPriorityOptions(PriorityType type, List<PriorityOption> options);

}
