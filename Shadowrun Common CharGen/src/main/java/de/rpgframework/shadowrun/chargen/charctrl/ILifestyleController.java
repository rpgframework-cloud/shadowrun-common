package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.Lifestyle;
import de.rpgframework.shadowrun.LifestyleQuality;

/**
 * @author prelle
 *
 */
public interface ILifestyleController<L extends Lifestyle> extends
	ComplexDataItemController<LifestyleQuality, L>,
	NumericalValueController<LifestyleQuality, L> {

	public int getLifestyleCost(L lifestyle);

	public Possible canBeSelected(L value);

}
