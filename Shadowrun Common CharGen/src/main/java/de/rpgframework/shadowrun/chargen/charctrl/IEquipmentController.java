package de.rpgframework.shadowrun.chargen.charctrl;

import java.util.List;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.Hook;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.shadowrun.items.AShadowrunItemEnhancement;

/**
 * @author prelle
 *
 */
public interface IEquipmentController<T extends PieceOfGear<?,?,?,?>, H extends Hook, E extends AShadowrunItemEnhancement> extends
	ComplexDataItemController<T, CarriedItem<T>>,
	NumericalValueController<T, CarriedItem<T>> {

	//-------------------------------------------------------------------
	//public List<T> getAvailable(CarryMode mode, ItemType...types);

	//-------------------------------------------------------------------
	/**
	 * Check if the user is allowed to select the item
	 * @param value  Item to select
	 * @param variant Variant to select
	 * @param decisions Decisions made
	 * @return Selection allowed or not
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public Possible canBeSelected(T value, String variant, CarryMode mode, Decision... decisions);

	//-------------------------------------------------------------------
	/**
	 * Add/Select the item using the given decisions
	 * @param value  Item to select
	 * @param variant Variant to select
	 * @param decisions Decisions made
	 * @return value instance of selected item
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public OperationResult<CarriedItem<T>> select(T value, String variant, CarryMode mode, Decision... decisions);


	//-------------------------------------------------------------------
	/**
	 * Get all items that are embeddable in the given object
	 */
	public List<T> getEmbeddableIn(CarriedItem<T> ref, H slot);

	//-------------------------------------------------------------------
	public Possible canBeEmbedded(CarriedItem<T> container, H slot, T value, String variant, Decision...decisions);

	//-------------------------------------------------------------------
	public OperationResult<CarriedItem<T>> embed(CarriedItem<T> container, H slot, T value, String variant, Decision...decisions);

	//-------------------------------------------------------------------
	public Possible canBeRemoved(CarriedItem<T> container, H slot, CarriedItem<T> toRemove);

	//-------------------------------------------------------------------
	public Possible removeEmbedded(CarriedItem<T> container, H slot, CarriedItem<T> toRemove);

	//-------------------------------------------------------------------
	public int getConvertedKarma();

	//-------------------------------------------------------------------
	public int getConversionRateKarma();

	//-------------------------------------------------------------------
	public boolean canIncreaseConversion();

	//-------------------------------------------------------------------
	public boolean increaseConversion();

	//-------------------------------------------------------------------
	public boolean canDecreaseConversion();

	//-------------------------------------------------------------------
	public boolean decreaseConversion();

	public boolean canChangeCount(CarriedItem<T> item, int newCount);

	public ComplexDataItemController<E, ItemEnhancementValue<E>> getItemEnhancementController(CarriedItem<T> toModify);

}
