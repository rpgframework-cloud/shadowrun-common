package de.rpgframework.shadowrun.chargen.gen;

import java.lang.System.Logger.Level;
import java.util.function.BiFunction;

import de.rpgframework.genericrpg.chargen.CharacterControllerImpl;
import de.rpgframework.shadowrun.Priority;
import de.rpgframework.shadowrun.PriorityTableEntry;
import de.rpgframework.shadowrun.PriorityType;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author Stefan Prelle
 *
 */
public class SumToTenPriorityTableController<M extends ShadowrunCharacter<?,?,?,?>,P extends IPrioritySettings> extends PriorityTableController<M,P> {

	protected int max = 10;

	//--------------------------------------------------------------------
	public SumToTenPriorityTableController(CharacterControllerImpl<ShadowrunAttribute,M> parent, Class<P> clazz, BiFunction<PriorityType,Priority,PriorityTableEntry> resolver) {
		super(parent, clazz, resolver);
	}

	//--------------------------------------------------------------------
	public void setPriority(PriorityType option, Priority prio) {
		P settings = super.getPrioritySettings();
		logger.log(Level.WARNING, "setPriority("+option+" = "+prio+")");

		int invested = Math.max(0,4-prio.ordinal());
		settings.priorities().put(option, prio);

		Priority oldPriority = null;
		PriorityType oldOption = null;
		for (PriorityType opt : PriorityType.values()) {
			if (option==opt)
				continue;
			Priority tmpPrio = getPriority(opt);
			int toAdd = Math.max(0,4-tmpPrio.ordinal());
			int allow = max- invested;
			if (toAdd<=allow) {
				// No change necessary
				invested += toAdd;
			} else {
				// Priority must be reduced
				oldPriority = tmpPrio;
				oldOption   = opt;
				settings.priorities().put(opt, Priority.values()[5-(allow+1)]);
				invested += allow;
			}
		}
		logger.log(Level.INFO, "Set "+option+" to "+prio+" and change "+oldOption+" to "+oldPriority);

		// If points remain, distribute them
		while (invested<max) {
			inner:
			for (int i=4; i>=0; i--) {
				PriorityType tmpType = PriorityType.values()[i];
				if (tmpType==option)
					continue;
				Priority tmpPrio = getPriority(tmpType);
				if (tmpPrio.ordinal()>0) {
					Priority setTo = Priority.values()[tmpPrio.ordinal()-1];
					logger.log(Level.INFO, "Priority points left. Set "+tmpType+" from "+settings.priorities().get(tmpType)+" to "+setTo);
					settings.priorities().put(tmpType, setTo);
					invested++;
					break inner;
				}
			}
		}

		parent.runProcessors();
	}

}
