package de.rpgframework.shadowrun.chargen.charctrl;

import java.util.List;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;

public interface ISkillController<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>> extends 
	PartialController<S>, 
	ComplexDataItemController<S, V>, 
	NumericalValueController<S, V> {

	//-------------------------------------------------------------------
	/**
	 * Get a list of all skills - selected or not
	 */
	public List<V> getAll();
	
	public List<SkillSpecialization<S>> getAvailableSpecializations(V skillVal);

	public Possible canSelectSpecialization(V skillVal, SkillSpecialization<S> spec, boolean expertise);

	public Possible canDeselectSpecialization(V skillVal, SkillSpecializationValue<S> spec);

	public OperationResult<SkillSpecializationValue<S>> select(V skillVal, SkillSpecialization<S> spec, boolean expertise);

	public boolean deselect(V skillVal, SkillSpecializationValue<S> spec);

}