package de.rpgframework.shadowrun.chargen.gen;

public class PerAttributePoints extends APerValuePoints3 {
	public PerAttributePoints() {}
	public PerAttributePoints(int p1, int p2, int p3) {
		this.points1 = p1;
		this.points2 = p2;
		this.points3 = p3;
	}
	public int getSumBeforeKarma() {return base+points1+points2;}
	public int getKarmaInvest() {
		int start = base+points1+points2;
		int invest = 0;
		for (int i=1; i<=points3; i++)
			invest+=(start+i)*5;
		return invest;
	}
	public String toString() {return String.format("B=%2d  S=%2d  A=%2d  K=%2d  %d Karma", base, points1,points2,points3,getKarmaInvest());}

}
