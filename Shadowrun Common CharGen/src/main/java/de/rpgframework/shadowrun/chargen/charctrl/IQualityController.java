package de.rpgframework.shadowrun.chargen.charctrl;

import java.util.UUID;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.QualityValue;

/**
 * @author prelle
 *
 */
public interface IQualityController extends NumericalValueController<Quality, QualityValue>, ComplexDataItemController<Quality, QualityValue> {

	public final static UUID MENTOR_SPIRIT_ADVANTAGES = QualityValue.MENTOR_SPIRIT_ADVANTAGES;


}
