package de.rpgframework.shadowrun.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.Possible.State;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.modification.ModificationChoice;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.QualityValue;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IQualityController;
import de.rpgframework.shadowrun.chargen.charctrl.IRejectReasons;

/**
 * @author prelle
 *
 */
public abstract class QualityGenerator<C extends ShadowrunCharacter<?,?,?,?>> implements IQualityController {

	public final static MultiLanguageResourceBundle RES = IShadowrunCharacterGenerator.RES;

	private final static Logger logger = System.getLogger(QualityGenerator.class.getPackageName());

	protected IShadowrunCharacterGenerator<?, ?, ?, C> parent;
	protected C model;
	protected List<ToDoElement> todos;
	protected List<UUID> choices;
	/** How many Karma is gained by negative qualities */
	protected int karmaGain;
	/** How many Karma is spent (net) on SURGE */
	protected int karmaSURGE;

	//-------------------------------------------------------------------
	public QualityGenerator(IShadowrunCharacterGenerator<?, ?, ?,C> parent) {
		this.parent = parent;
		this.model  = parent.getModel();
		this.todos  = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/** How many Karma is gained by negative qualities */
	public int getKarmaGain() { return karmaGain; }
	//-------------------------------------------------------------------
	/** How many Karma is spent (net) on SURGE */
	public int getKarmaForSURGE() { return karmaSURGE; }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.ComplexDataItem)
	 */
	@Override
	public RecommendationState getRecommendationState(Quality value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getRecommendationState(de.rpgframework.genericrpg.data.ComplexDataItemValue)
	 */
	@Override
	public RecommendationState getRecommendationState(QualityValue value) {
		return RecommendationState.NEUTRAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.NumericalDataItemController#canBeIncreased(de.rpgframework.genericrpg.data.ComplexDataItemValue)
	 */
	@Override
	public Possible canBeIncreased(QualityValue value) {
		if (!model.getQualities().contains(value)) {
			return new Possible(Severity.STOPPER, RES, IRejectReasons.IMPOSS_NOT_PRESENT);
		}

		Quality qual = value.getModifyable();
		if (!qual.hasLevel())
			// Quality has no levels
			return new Possible(Severity.STOPPER, RES, IRejectReasons.IMPOSS_ITEM_HAS_NO_LEVELS);

		if (value.getModifiedValue()>=qual.getMax())
			// Maximum already reached
			return new Possible(Severity.STOPPER, RES, IRejectReasons.IMPOSS_MAX_LEVEL_REACHED);

		// Is there enough Karma
		if (model.getKarmaFree() < qual.getKarmaCost()) {
			// Not enough Karma
			return new Possible(Severity.STOPPER, IRejectReasons.RES, IRejectReasons.IMPOSS_NOT_ENOUGH_KARMA, qual.getKarmaCost());
		}

		// More checks need to be done by overriding generators for specific rule editions
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<QualityValue> increase(QualityValue value) {
		logger.log(Level.TRACE, "ENTER increase({0})", value);
		try {
			Possible poss = canBeIncreased(value);
			if (!poss.get()) {
				logger.log(Level.ERROR, "Tried to increase {0} which is not allowed: {1}", value, poss.toString());
				return new OperationResult<QualityValue>(poss);
			}

			value.setDistributed(value.getDistributed()+1);
			logger.log(Level.INFO, "increased quality ''{0}'' to {1}", value.getModifyable().getId(), value.getDistributed());

			parent.runProcessors();

			return new OperationResult<QualityValue>(value);
		} finally {
			logger.log(Level.TRACE, "LEAVE increase({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public Possible canBeDecreased(QualityValue value) {
		if (!model.getQualities().contains(value)) {
			return new Possible(Severity.STOPPER, RES, IRejectReasons.IMPOSS_NOT_PRESENT);
		}

		Quality qual = value.getModifyable();
		if (!qual.hasLevel())
			// Quality has no levels
			return new Possible(Severity.STOPPER, RES, IRejectReasons.IMPOSS_ITEM_HAS_NO_LEVELS);

		if (value.getDistributed()<=0)
			// Maximum already reached
			return new Possible(Severity.STOPPER, RES, IRejectReasons.IMPOSS_MIN_LEVEL_REACHED);

		// More checks need to be done by overriding generators for specific rule editions
		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public OperationResult<QualityValue> decrease(QualityValue value) {
		logger.log(Level.TRACE, "ENTER decrease({0})", value);
		try {
			Possible poss = canBeDecreased(value);
			if (!poss.get()) {
				logger.log(Level.ERROR, "Tried to decrease {0} which is not allowed: {1}", value, poss.toString());
				return new OperationResult<QualityValue>(poss);
			}

			value.setDistributed(value.getDistributed()-1);
			logger.log(Level.INFO, "decreased quality '{0}' to {1}", value.getModifyable().getId(), value.getDistributed());

			parent.runProcessors();

			return new OperationResult<QualityValue>(value);
		} finally {
			logger.log(Level.TRACE, "LEAVE decrease({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.ComplexDataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public Possible canBeSelected(Quality value, Decision... decisions) {
		if (model.hasQuality(value.getId()) && !value.isMulti()) {
			return new Possible(Severity.STOPPER, IRejectReasons.RES, IRejectReasons.IMPOSS_ALREADY_PRESENT);
		}
		// After ensuring presence of all required decisions, compare uniqueness of decisions

		// Is there enough Karma
		if (value.isPositive() && model.getKarmaFree() < value.getKarmaCost()) {
			// Not enough Karma
			return new Possible(Severity.STOPPER, IRejectReasons.RES, IRejectReasons.IMPOSS_NOT_ENOUGH_KARMA, value.getKarmaCost());
		}

		// Ensure all required choices are made
		List<UUID> requiredChoices = new ArrayList<>();
		value.getChoices().forEach(c -> requiredChoices.add(c.getUUID()));

		QualityValue selected = new QualityValue(value,0);
		for (Decision dec : decisions) {
			if (dec!=null) {
				selected.addDecision(dec);
				requiredChoices.remove(dec.getChoiceUUID());
			}
		}
		if (!requiredChoices.isEmpty()) {
			return new Possible(Severity.INFO, IRejectReasons.RES, IRejectReasons.IMPOSS_MISSING_DECISIONS);
		}

		if (model.hasQuality(value.getId()) && value.isMulti() && !value.getChoices().isEmpty()) {
			QualityValue exist = model.getQuality(value.getId());
			for (Decision dec : decisions) {
				Decision existDec = exist.getDecision(dec.getChoiceUUID());
				if (dec.getValue().equals(existDec.getValue())) {
					return new Possible(Severity.STOPPER, IRejectReasons.RES, IRejectReasons.IMPOSS_ALREADY_PRESENT);
				}
			}
		}

		// More checks need to be done by overriding generators for specific rule
		// editions
		return Possible.TRUE;
	}

	// -------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#select(de.rpgframework.genericrpg.data.ComplexDataItem,
	 *      de.rpgframework.genericrpg.data.Decision[])
	 */
	@Override
	public OperationResult<QualityValue> select(Quality value, Decision... decisions) {
		logger.log(Level.TRACE, "ENTER select({0})", value);
		try {
			Possible possible = canBeSelected(value, decisions);
			if (possible.getState()!=State.POSSIBLE) {
				logger.log(Level.ERROR, "Trying to select a quality that cannot be selected: {0}",possible.getI18NKey());
				return new OperationResult<QualityValue>(possible, false);
			}

			QualityValue selected = new QualityValue(value, 0);
			selected.setUuid(UUID.randomUUID());
			logger.log(Level.INFO, "{0} has Level = {1}", value, value.hasLevel());
			if (value.hasLevel()) {
				selected.setDistributed(1);
			}
			for (Decision dec : decisions) {
				selected.addDecision(dec);
			}

			model.addQuality(selected);
			logger.log(Level.INFO, "Add quality '" + value.getId() + "' for " + value.getKarmaCost() + " karma");

			parent.runProcessors();
			return new OperationResult<QualityValue>(selected);
		} finally {
			logger.log(Level.TRACE, "LEAVE select({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeDeselected(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public Possible canBeDeselected(QualityValue value) {
		// Can only remove what is there
		if (!model.getQualities().contains(value)) {
			return new Possible(Severity.STOPPER, RES, IRejectReasons.IMPOSS_NOT_PRESENT);
		}

		// Auto-Added qualities cannot be removed
		if (value.isAutoAdded()) {
			return new Possible(Severity.STOPPER, RES, IRejectReasons.IMPOSS_AUTO_ADDED);
		}

		return Possible.TRUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#deselect(de.rpgframework.genericrpg.data.DataItemValue)
	 */
	@Override
	public boolean deselect(QualityValue value) {
		logger.log(Level.TRACE, "ENTER deselect({0})", value);
		try {
			Possible possible = canBeDeselected(value);
			if (!possible.get()) {
				logger.log(Level.ERROR, "Trying to deselect a quality that cannot be deselected, because "+possible.getMostSevere());
				return false;
			}

			model.removeQuality(value);
			logger.log(Level.INFO, "Remove quality '{0}'", value.getKey());

			parent.runProcessors();
			return true;
		} finally {
			logger.log(Level.TRACE, "LEAVE deselect({0})", value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getCharacterController()
	 */
	@Override
	public CharacterController<ShadowrunAttribute ,?> getCharacterController() {
		return parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getModel()
	 */
	@Override
	public RuleSpecificCharacterObject<? extends IAttribute,?,?,?> getModel() {
		return parent.getModel();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#roll()
	 */
	@Override
	public void roll() {
		logger.log(Level.WARNING, "Not implemented: roll");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getChoiceUUIDs()
	 */
	@Override
	public List<UUID> getChoiceUUIDs() {
		return choices;
	}

	//-------------------------------------------------------------------
	public Choice getAsChoice(ComplexDataItem value, UUID uuid) {
		return value.getChoice(uuid);
	}

	//-------------------------------------------------------------------
	public ModificationChoice getAsModificationChoice(ComplexDataItem value, UUID uuid) {
		return value.getModificationChoice(uuid);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#decide(java.lang.Object, de.rpgframework.genericrpg.data.Choice, de.rpgframework.genericrpg.data.Decision)
	 */
	@Override
	public void decide(Quality decideFor, UUID choice, Decision decision) {
		logger.log(Level.WARNING, "Not implemented: decide");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#getSelected()
	 */
	@Override
	public List<QualityValue> getSelected() {
		return new ArrayList<QualityValue>(model.getQualities());
	}

	//-------------------------------------------------------------------
	public int getKarmaByNegative() {
		return karmaGain;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#getValue(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public int getValue(QualityValue value) {
		return value.getModifiedValue();
	}

}
