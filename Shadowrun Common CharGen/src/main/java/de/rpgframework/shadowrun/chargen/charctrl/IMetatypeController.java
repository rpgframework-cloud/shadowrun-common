package de.rpgframework.shadowrun.chargen.charctrl;

import java.util.List;

import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.shadowrun.BodyType;
import de.rpgframework.shadowrun.MetaType;
import de.rpgframework.shadowrun.MetaTypeOption;

/**
 * @author prelle
 *
 */
public interface IMetatypeController<M extends MetaType> extends PartialController<M> {

	//--------------------------------------------------------------------
	/**
	 * Returns all metatypes that may be selected.
	 * @return
	 */
	public List<MetaTypeOption> getAvailable();
	
	//--------------------------------------------------------------------
	public int getKarmaCost(M type);
	
	//--------------------------------------------------------------------
	public boolean canBeSelected(M type);
	
	//--------------------------------------------------------------------
	public boolean select(M value);
	
	//--------------------------------------------------------------------
	public void randomizeSizeWeight();
	
	//--------------------------------------------------------------------
	public boolean selectBodyType(BodyType value);
	

}
