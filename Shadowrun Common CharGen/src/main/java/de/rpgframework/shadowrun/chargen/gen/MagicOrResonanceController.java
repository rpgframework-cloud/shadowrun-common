package de.rpgframework.shadowrun.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.ModificationChoice;
import de.rpgframework.shadowrun.MagicOrResonanceType;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IMagicOrResonanceController;

/**
 * @author prelle
 *
 */
public abstract class MagicOrResonanceController implements IMagicOrResonanceController {

	private final static Logger logger = System.getLogger(MagicOrResonanceController.class.getPackageName());
	private final static MultiLanguageResourceBundle RES = IShadowrunCharacterGenerator.RES;

	private static Random RANDOM = new Random();

	protected IShadowrunCharacterGenerator parent;
	protected ShadowrunCharacter model;

	//-------------------------------------------------------------------
	public MagicOrResonanceController(IShadowrunCharacterGenerator parent) {
		this.parent = parent;
		this.model  = (ShadowrunCharacter) parent.getModel();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun.chargen.charctrl.IMagicOrResonanceController#select(de.rpgframework.shadowrun.MagicOrResonanceType)
	 */
	public void select(MagicOrResonanceType value) {
		logger.log(Level.DEBUG, "selectMagicOrResonance("+value+")");
		if (model.getMagicOrResonanceType()==value) return;
		if (value!=null)
			model.setMagicOrResonanceType(value);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getCharacterController()
	 */
	@Override
	public CharacterController<ShadowrunAttribute,?> getCharacterController() {
		return parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getModel()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public RuleSpecificCharacterObject getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		List<ToDoElement> ret = new ArrayList<>();
		if (model.getMagicOrResonanceType()==null) {
			ret.add(new ToDoElement(Severity.STOPPER, RES, "chargen.magicresonance.nothing_selected"));
		} else if (model.getMagicOrResonanceType().isAspected() && model.getAspectSkill()==null) {
			ret.add(new ToDoElement(Severity.STOPPER, RES, "chargen.magicresonance.no_aspect_skill"));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#roll()
	 */
	@Override
	public void roll() {
		List<MagicOrResonanceType> possible = getAvailable();
		int choice = RANDOM.nextInt(possible.size());
		logger.log(Level.INFO, "Rolled "+possible.get(choice).getId()+" for magic or resonance option");
		select(possible.get(choice));
	}

	@Override
	public List<UUID> getChoiceUUIDs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void decide(MagicOrResonanceType decideFor, UUID choice, Decision decision) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	public Choice getAsChoice(ComplexDataItem value, UUID uuid) {
		return value.getChoice(uuid);
	}

	//-------------------------------------------------------------------
	public ModificationChoice getAsModificationChoice(ComplexDataItem value, UUID uuid) {
		return value.getModificationChoice(uuid);
	}

}
