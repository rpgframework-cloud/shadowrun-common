package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.AdeptPower;
import de.rpgframework.shadowrun.AdeptPowerValue;

/**
 * @author prelle
 *
 */
public interface IAdeptPowerController extends ComplexDataItemController<AdeptPower, AdeptPowerValue>, NumericalValueController<AdeptPower, AdeptPowerValue> {

	public int getMaxPowerPoints();

	public float getUnsedPowerPoints();
	
	//-------------------------------------------------------------------
	/**
	 * @return TRUE, when the controller in theory allows buying PP for Karma
	 */
	public boolean canBuyPowerPoints();

	//-------------------------------------------------------------------
	public boolean canIncreasePowerPoints();

	//-------------------------------------------------------------------
	public boolean canDecreasePowerPoints();

	//-------------------------------------------------------------------
	public boolean increasePowerPoints();

	//-------------------------------------------------------------------
	public boolean decreasePowerPoints();

}
