package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.Ritual;
import de.rpgframework.shadowrun.RitualValue;

/**
 * @author prelle
 *
 */
public interface IRitualController extends ComplexDataItemController<Ritual, RitualValue> {

}
