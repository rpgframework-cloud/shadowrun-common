package de.rpgframework.shadowrun.chargen.charctrl;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;

/**
 * @author prelle
 *
 */
public interface IRejectReasons {

	public final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(IRejectReasons.class, Locale.ENGLISH, Locale.GERMAN);

	public static String toLocale(String key) {
		return RES.getString(key);
	}

	public final static String IMPOSS_NOT_ENOUGH_KARMA   = "impossible.notEnoughKarma";
	public final static String IMPOSS_NOT_ENOUGH_NUYEN   = "impossible.notEnoughNuyen";
	public final static String IMPOSS_MISSING_DECISIONS  = "impossible.missingDecisions";
	public final static String IMPOSS_NOT_AVAILABLE      = "impossible.notAvailable";
	public final static String IMPOSS_NOT_PRESENT        = "impossible.notPresent";
	public final static String IMPOSS_NO_SPELLCASTER     = "impossible.noSpellcaster";
	/** Not enough free capacity */
	public final static String IMPOSS_CAPACITY           = "impossible.notEnoughCapacity";
	/** Item exceeds the maximum size per accessory */
	public final static String IMPOSS_SIZE               = "impossible.tooLarge";
	public final static String IMPOSS_SLOT_OCCUPIED      = "impossible.slotOccupied";
	public final static String IMPOSS_NO_SUCH_SLOT       = "impossible.noSuchSlot";
	public final static String IMPOSS_ALREADY_PRESENT    = "impossible.alreadyPresent";
	public final static String IMPOSS_AUTO_ADDED         = "impossible.autoAdded";
	public final static String IMPOSS_ITEM_HAS_NO_LEVELS = "impossible.noLevels";
	public final static String IMPOSS_MAX_LEVEL_REACHED  = "impossible.maxLevelReached";
	public final static String IMPOSS_MIN_LEVEL_REACHED  = "impossible.minLevelReached";
	public final static String IMPOSS_NOT_ENOUGH_PPOINTS = "impossible.notEnoughPowerPoints";
	public final static String IMPOSS_NOT_ENOUGH_POINTS  = "impossible.notEnoughPoints";
	public final static String IMPOSS_NOT_EMBEDDABLE     = "impossible.notEmbeddable";
	public final static String IMPOSS_AVAILABLE_TOO_HIGH = "impossible.availTooHigh";
	public final static String IMPOSS_QUALITY_KARMAGAIN  = "impossible.qualityKarmaGain";
	public final static String IMPOSS_QUALITY_KARMASURGE = "impossible.qualityKarmaSURGE";
	public final static String IMPOSS_MUST_CHOOSE_VARIANT  = "impossible.mustChooseVariant";
	public final static String IMPOSS_INVALID_VARIANT    = "impossible.invalidVariant";
	public final static String IMPOSS_INVALID_CARRYMODE  = "impossible.invalidCarryMode";
	public static final String IMPOSS_PREVIOUS_SESSION   = "impossible.cannotUndoPreviousSession";
	public static final String IMPOSS_UNDO_CHARGEN       = "impossible.cannotUndoChargen";
	public static final String IMPOSS_MAX_COMPLEX_FORMS  = "impossible.maxComplexFormsReached";
	public static final String IMPOSS_MAX_SPELLS         = "impossible.maxSpellsReached";
	public static final String IMPOSS_NO_LIFESTYLE       = "todo.noLifestyle";
	public static final String IMPOSS_DRAKE_ONLY         = "impossible.drake_only";
	public static final String IMPOSS_USED_CULTURED_BIOWARE = "impossible.used_cultured_bioware";

	public final static String TODO_NEGATIVE_NUYEN       = "todo.negativeNuyen";
//	public final static String TODO_TOO_MANY_NUYEN       = "todo.tooManyNuyen";
	public final static String TODO_CONTACT_POINTS_LEFT  = "todo.contactPointsLeft";
	public static final String TODO_UNUSED_POWER_POINTS  = "todo.powerPointsLeft";
	public static final String TODO_TOO_MANY_POWERS      = "todo.tooManyPowers";
	public static final String TODO_LOOSE_NUYEN          = "todo.looseNuyen";
	public static final String TODO_LOOSE_KARMA          = "todo.looseKarma";
	public final static String TODO_NEGATIVE_KARMA       = "todo.negativeKarma";
	public static final String TODO_TOO_MANY_CONTACT_POINTS = "todo.tooManyContactPoints";
	public static final String TODO_METATYPE_NOT_ALLOWED = "todo.metatypeNotAllowed";
	public static final String TODO_NO_AMMUNITION        = "todo.buyAmmunition";
	public static final String TODO_METATYPE_NOT_SELECTED = "todo.metatypeNotSelected";

}
