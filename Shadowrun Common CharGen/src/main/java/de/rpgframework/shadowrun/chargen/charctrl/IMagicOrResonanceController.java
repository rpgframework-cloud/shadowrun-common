package de.rpgframework.shadowrun.chargen.charctrl;

import java.util.List;

import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.shadowrun.MagicOrResonanceType;
import de.rpgframework.shadowrun.Tradition;

/**
 * @author prelle
 *
 */
public interface IMagicOrResonanceController extends PartialController<MagicOrResonanceType>, RecommendingController<Tradition> {

	public List<MagicOrResonanceType> getAvailable();

	public void select(MagicOrResonanceType morType);

	public void selectTradition(Tradition n);

	public int getCost(MagicOrResonanceType morType);

}
