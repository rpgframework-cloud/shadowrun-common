package de.rpgframework.shadowrun.chargen.gen;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.chargen.CharacterGenerator;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.MetaType;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IMagicOrResonanceController;
import de.rpgframework.shadowrun.chargen.charctrl.IMetatypeController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;

/**
 * @author prelle
 *
 */
public interface IShadowrunCharacterGenerator<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>,X extends ASpell, C extends ShadowrunCharacter<S,V,?,X>> 
	extends IShadowrunCharacterController<S,V,X,C>,
	CharacterGenerator<ShadowrunAttribute,C> {

	public final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(IShadowrunCharacterGenerator.class,
			Locale.ENGLISH, Locale.GERMAN);;

	public String getId();
	

	//--------------------------------------------------------------------
	public WizardPageType[] getWizardPages();
	
	//--------------------------------------------------------------------
	public <T extends IMetatypeController<? extends MetaType>> T getMetatypeController();
	
	//--------------------------------------------------------------------
	public IMagicOrResonanceController getMagicOrResonanceController();
	
}
