package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.MetamagicOrEcho;
import de.rpgframework.shadowrun.MetamagicOrEchoValue;

/**
 * @author Stefan Prelle
 *
 */
public interface IMetamagicOrEchoController extends NumericalValueController<MetamagicOrEcho, MetamagicOrEchoValue>, ComplexDataItemController<MetamagicOrEcho, MetamagicOrEchoValue> {

	public int getGrade();
	
}
