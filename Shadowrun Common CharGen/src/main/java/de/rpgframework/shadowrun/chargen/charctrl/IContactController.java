package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.shadowrun.Contact;

/**
 * @author prelle
 *
 */
public interface IContactController extends PartialController<Contact> {

	//-------------------------------------------------------------------
	// Only for Generator
	public int getPointsLeft();

	//-------------------------------------------------------------------
	public Possible canCreateContact();
	
	//-------------------------------------------------------------------
	public OperationResult<Contact> createContact();

	//-------------------------------------------------------------------
	public void removeContact(Contact con);

	//-------------------------------------------------------------------
	public Possible canIncreaseRating(Contact con);

	//-------------------------------------------------------------------
	public boolean increaseRating(Contact con);
	
	//-------------------------------------------------------------------
	public Possible canDecreaseRating(Contact con);
	
	//-------------------------------------------------------------------
	public boolean decreaseRating(Contact con);
	
	//-------------------------------------------------------------------
	public Possible canIncreaseLoyalty(Contact con);
	
	//-------------------------------------------------------------------
	public boolean increaseLoyalty(Contact con);
	
	//-------------------------------------------------------------------
	public Possible canDecreaseLoyalty(Contact con);
	
	//-------------------------------------------------------------------
	public boolean decreaseLoyalty(Contact con);

}
