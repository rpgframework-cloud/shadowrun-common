package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.NumericalValueWith2PoolsController;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;

public interface ISkillGenerator<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>> 
	extends ISkillController<S, V>, NumericalValueWith2PoolsController<S, V> {

//	//--------------------------------------------------------------------
//	/**
//	 * @return -1 if this does not apply, otherwise a 0+
//	 */
//	public int getPointsLeftSkills();
//
//	//--------------------------------------------------------------------
//	public int getPointsLeftInKnowledgeAndLanguage();
	

}