package de.rpgframework.shadowrun.chargen.gen;

import de.rpgframework.shadowrun.chargen.charctrl.IRitualController;

/**
 * @author prelle
 *
 */
public interface IRitualGenerator extends IRitualController {

	public boolean usesFreeRituals();

	/** How many spells and rituals are left to select for free */
	public int getFreeRituals();

	public int getMaxFree();

}
