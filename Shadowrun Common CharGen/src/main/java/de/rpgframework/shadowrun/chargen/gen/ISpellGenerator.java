package de.rpgframework.shadowrun.chargen.gen;

import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.chargen.charctrl.ISpellController;

/**
 * @author prelle
 *
 */
public interface ISpellGenerator<D extends ASpell> extends ISpellController<D> {

	public boolean usesFreeSpells();
	
	/** How many spells and rituals are left to select for free */
	public int getFreeSpells();

	public int getMaxFree();
	
}
