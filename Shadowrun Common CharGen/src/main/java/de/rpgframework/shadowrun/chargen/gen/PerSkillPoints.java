package de.rpgframework.shadowrun.chargen.gen;

public class PerSkillPoints extends APerValuePoints3 {

	public int karmaSpec;
	public int pointSpec;

	public PerSkillPoints() {
		base=0;
	}
	public PerSkillPoints(int p1, int p2, int p3) {
		base=0;
		this.points1 = p1;
		this.points2 = p2;
		this.points3 = p3;
	}
	public void clear() {
		super.clear();
		karmaSpec = 0;
		pointSpec = 0;
	}
	public int getSumBeforeKarma() {return base+points2+points1;}
	public int getKarmaInvestSR6() {
		int start = base+points1+points2;
		int invest = 0;
		for (int i=1; i<=points3; i++)
			invest+=(start+i)*5;
		//invest += (karmaSpec*5);
		return invest;
	}
	public int getPointsInvestSR6() {
		return points1+points2+pointSpec;
	}
	public String toString() {return String.format("BA=%d p1=%d  p2=%d  K=%d/%d = %d Karma", base, points1,points2,points3,karmaSpec,getKarmaInvestSR6());}

}
