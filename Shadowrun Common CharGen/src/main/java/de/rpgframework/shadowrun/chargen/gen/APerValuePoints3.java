package de.rpgframework.shadowrun.chargen.gen;

/**
 * @author prelle
 *
 */
public class APerValuePoints3 {

	public transient int base =1;
	public int points1;
	public int points2;
	public int points3;

	//-------------------------------------------------------------------
	public APerValuePoints3() {
	}

	public int getSum() {return base+points1+points2+points3;}

	public int getSumWithoutBase() {return points1+points2+points3;}

	public void clear() {
		points1=0;
		points2=0;
		points3=0;
	}
}
