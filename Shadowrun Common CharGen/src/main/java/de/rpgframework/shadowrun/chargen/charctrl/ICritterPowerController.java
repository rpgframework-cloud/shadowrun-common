package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.CritterPower;
import de.rpgframework.shadowrun.CritterPowerValue;

/**
 * @author prelle
 *
 */
public interface ICritterPowerController extends ComplexDataItemController<CritterPower, CritterPowerValue>, NumericalValueController<CritterPower, CritterPowerValue> {

}
