package de.rpgframework.shadowrun.chargen.gen;

import de.rpgframework.shadowrun.chargen.charctrl.IAttributeController;

/**
 * @author Stefan Prelle
 *
 */
public interface KarmaAttributeGenerator extends IAttributeController {

}
