package de.rpgframework.shadowrun.chargen.gen;

import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public interface IPriorityGenerator<C extends ShadowrunCharacter<?,?,?,?>, P extends IPrioritySettings> {

	//-------------------------------------------------------------------
	public <U extends IPrioritySettings> U getSettings();

	//-------------------------------------------------------------------
	public PriorityTableController<C,P> getPriorityController();

	//-------------------------------------------------------------------
	public PriorityAttributeGenerator getPriorityAttributeController();
	
}
