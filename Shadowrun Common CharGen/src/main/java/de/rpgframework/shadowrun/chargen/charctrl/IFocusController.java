package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.Focus;
import de.rpgframework.shadowrun.FocusValue;

/**
 * @author prelle
 *
 */
public interface IFocusController extends ComplexDataItemController<Focus, FocusValue> {

	//--------------------------------------------------------------------
	/**
	 * You cannot bind more than MAGICx5 Force points
	 */
	public int getFocusPointsLeft();

}
