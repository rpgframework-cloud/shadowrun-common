package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.shadowrun.ShadowrunAttribute;

/**
 * @author prelle
 *
 */
public interface IAttributeController extends NumericalValueController<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>>, PartialController<ShadowrunAttribute>{

	public boolean isRacialAttribute(ShadowrunAttribute key);

}
