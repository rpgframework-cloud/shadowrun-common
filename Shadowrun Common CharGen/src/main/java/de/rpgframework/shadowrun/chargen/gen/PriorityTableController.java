package de.rpgframework.shadowrun.chargen.gen;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.BiFunction;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.chargen.CharacterControllerImpl;
import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.shadowrun.Priority;
import de.rpgframework.shadowrun.PriorityTableEntry;
import de.rpgframework.shadowrun.PriorityType;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author Stefan
 *
 */
public class PriorityTableController<M extends ShadowrunCharacter<?,?,?,?>,P extends IPrioritySettings> implements PartialController<String> {

	protected final static Logger logger = System.getLogger(PriorityTableController.class.getPackageName()+".prio");

	private Class<P> clazz;
	protected CharacterControllerImpl<ShadowrunAttribute,M> parent;
	protected BiFunction<PriorityType,Priority,PriorityTableEntry> resolver;

	//--------------------------------------------------------------------
	public PriorityTableController(CharacterControllerImpl<ShadowrunAttribute,M> parent, Class<P> clazz, BiFunction<PriorityType,Priority,PriorityTableEntry> resolver) {
		this.parent = parent;
		this.clazz  = clazz;
		this.resolver = resolver;

		if (parent.getModel()==null)
			throw new NullPointerException("No character associated with CharacterController");
		P settings = (P)parent.getModel().getCharGenSettings(clazz);
		logger.log(Level.DEBUG, "Priority table initialized with " + settings.priorities());
	}

	//-------------------------------------------------------------------
	public P getPrioritySettings() {
		return (P)parent.getModel().getCharGenSettings(clazz);
	}

	//-------------------------------------------------------------------
	public Priority getPriority(PriorityType option) {
		return getPrioritySettings().priorities().get(option);
	}

	//-------------------------------------------------------------------
	public PriorityType getTypeForPrio(Priority prio) {
		for (PriorityType type : PriorityType.values()) {
			if (getPriority(type)==prio) {
				return type;
			}
		}

		return null;
	}

//	//-------------------------------------------------------------------
//	public Priority getPriorityWithLevelOfPlay(PriorityType option) {
//		Priority ret = getPriority(option);
//
//
//		if (settings.variant==PriorityVariant.STREET_LEVEL && ret!=null && ret!=Priority.E)
//			ret = Priority.values()[ret.ordinal()+1];
//
//		return ret;
//	}

	//--------------------------------------------------------------------
	public void setPriority(PriorityType option, Priority prio) {
		P settings = ((P)parent.getModel().getCharGenSettings(clazz));
		logger.log(Level.DEBUG, "........"+settings.priorities());

		Priority oldPriority = getPriority(option);
		PriorityType oldOption = getTypeForPrio(prio);

		if (oldOption==option && oldPriority==prio) {
//			parent.runProcessors();
			return;
		}

		settings.priorities().put(option, prio);
		settings.priorities().put(oldOption, oldPriority);
		logger.log(Level.INFO, "Set "+option+" to "+prio+" and change "+oldOption+" to "+oldPriority);

		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		List<Modification> ret = new ArrayList<>(unprocessed);

		verifySettings();

		// Get table entries
		for (PriorityType type : PriorityType.values()) {
			if (type==PriorityType.KARMA) continue;
			Priority prio = getPriority(type);
			logger.log(Level.INFO, type+" has priority "+prio);
			PriorityTableEntry entry = resolver.apply(type, prio);
			P settings = ((P)parent.getModel().getCharGenSettings(clazz));
			if (entry==null || entry.getId()==null) {
				logger.log(Level.ERROR, "No priority table entry found for "+type+"/"+prio);
				settings.setPriorityOptions(type, null);
			} else {
				logger.log(Level.DEBUG, "  Found "+entry.getId()+" with options: "+entry.getOptions());
				logger.log(Level.DEBUG, "  Found "+entry.getId()+" with modifications: "+entry.getOutgoingModifications());
				ret.addAll(entry.getOutgoingModifications());
				settings.setPriorityOptions(type, entry.getOptions());
			}
		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getCharacterController()
	 */
	@Override
	public CharacterController<ShadowrunAttribute,?> getCharacterController() {
		return parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getModel()
	 */
	@Override
	public M getModel() {
		return parent.getModel();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#getChoiceUUIDs()
	 */
	@Override
	public List<UUID> getChoiceUUIDs() {
		return new ArrayList<UUID>();
	}

	@Override
	public void decide(String decideFor, UUID choice, Decision decision) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.PartialController#roll()
	 */
	@Override
	public void roll() {
		logger.log(Level.WARNING, "ToDo: roll()");
	}

	//-------------------------------------------------------------------
	public void verifySettings() {

	}

}
