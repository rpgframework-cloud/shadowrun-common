package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.ComplexForm;
import de.rpgframework.shadowrun.ComplexFormValue;

/**
 * @author prelle
 *
 */
public interface IComplexFormController extends ComplexDataItemController<ComplexForm, ComplexFormValue> {

}
