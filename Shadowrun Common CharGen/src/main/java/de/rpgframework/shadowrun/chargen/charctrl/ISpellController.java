package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.SpellValue;

/**
 * @author prelle
 *
 */
public interface ISpellController<D extends ASpell> extends ComplexDataItemController<D, SpellValue<D>> {

	public int getNuyenCost(D value);

}
