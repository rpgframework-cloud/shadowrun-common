package de.rpgframework.shadowrun.chargen.charctrl;

import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.shadowrun.LicenseValue;
import de.rpgframework.shadowrun.SIN;

/**
 * @author prelle
 *
 */
public interface SINController extends PartialController<SIN> {

	//-------------------------------------------------------------------
	public boolean canCreateNewSIN(SIN.FakeRating quality);

	//-------------------------------------------------------------------
	public boolean canCreateNewSIN(SIN.FakeRating quality, int count);

	//-------------------------------------------------------------------
	public boolean canDeleteSIN(SIN data);

	//-------------------------------------------------------------------
	public SIN createNewSIN(String name, SIN.FakeRating quality);

	//-------------------------------------------------------------------
	public SIN[] createNewSIN(SIN.FakeRating quality, int count);

	//-------------------------------------------------------------------
	public boolean deleteSIN(SIN data);

	
	//-------------------------------------------------------------------
	public boolean canCreateNewLicense(SIN sin, SIN.FakeRating quality);
	
	//-------------------------------------------------------------------
	public LicenseValue createNewLicense(SIN sin, SIN.FakeRating quality, String name);

	//-------------------------------------------------------------------
	public boolean deleteLicense(LicenseValue data);

}
