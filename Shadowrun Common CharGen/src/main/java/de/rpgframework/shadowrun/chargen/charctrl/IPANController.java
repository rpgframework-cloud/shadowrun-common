package de.rpgframework.shadowrun.chargen.charctrl;

import java.util.List;

import de.rpgframework.genericrpg.items.CarriedItem;

/**
 * @author prelle
 *
 */
public interface IPANController {

	//-------------------------------------------------------------------
	public List<CarriedItem<?>> getPossibleRootDevices();

	//-------------------------------------------------------------------
	public void setPANRoot(CarriedItem<?> device);

	//-------------------------------------------------------------------
	public List<CarriedItem<?>> getDevices();

}
