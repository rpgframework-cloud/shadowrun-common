package de.rpgframework.shadowrun.chargen.gen;

import de.rpgframework.genericrpg.NumericalValueWith3PoolsController;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.chargen.charctrl.IAttributeController;

/**
 * @author Stefan Prelle
 *
 */
public interface PointBuyAttributeGenerator extends IAttributeController, NumericalValueWith3PoolsController<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>> {

}
