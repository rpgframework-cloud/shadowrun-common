package de.rpgframework.shadowrun.chargen.gen;

import de.rpgframework.shadowrun.chargen.charctrl.IComplexFormController;

/**
 * @author prelle
 *
 */
public interface IComplexFormGenerator extends IComplexFormController {

	public int getFree();

	public int getMaxFree();
	
}
