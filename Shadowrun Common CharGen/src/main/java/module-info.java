open module shadowrun.common.chargen {
	exports de.rpgframework.shadowrun.chargen.charctrl;
	exports de.rpgframework.shadowrun.chargen.gen;
	exports de.rpgframework.shadowrun.chargen.lvl;

	requires de.rpgframework.rules;
	requires transitive shadowrun.common;
	requires com.google.gson;
}