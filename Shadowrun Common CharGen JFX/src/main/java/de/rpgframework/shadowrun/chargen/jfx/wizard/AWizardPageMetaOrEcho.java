package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.MetamagicOrEcho;
import de.rpgframework.shadowrun.MetamagicOrEchoValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class AWizardPageMetaOrEcho extends WizardPage implements ControllerListener{
	
	private final static Logger logger = System.getLogger(AWizardPageMetaOrEcho.class.getPackageName());
	
	private final static ResourceBundle RES = ResourceBundle.getBundle(AWizardPageMetaOrEcho.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterController charGen;
	
	protected ComplexDataItemControllerNode<MetamagicOrEcho, MetamagicOrEchoValue> selection;
	protected GenericDescriptionVBox bxDescription;
	protected OptionalNodePane layout;
	private NumberUnitBackHeader backHeader;
	
	protected Label lbGrade;

	//-------------------------------------------------------------------
	public AWizardPageMetaOrEcho(Wizard wizard, IShadowrunCharacterController charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.metaecho.title"));
		initComponents();
		initLayout();
		initInteractivity();
		
		charGen.addListener(this);
	}
	
	//-------------------------------------------------------------------
	protected void initComponents() {
		selection = new ComplexDataItemControllerNode<>(charGen.getMetamagicOrEchoController());
		
		selection.setAvailablePlaceholder(ResourceI18N.get(RES, "page.metaecho.placeholder.available"));
		selection.setSelectedPlaceholder(ResourceI18N.get(RES, "page.metaecho.placeholder.selected"));
		
		selection.setAvailableCellFactory(lv -> new ComplexDataItemListCell<MetamagicOrEcho>( () -> selection.getController()));
//		selection.setSelectedCellFactory(lv -> new MetamagicEchoValueListCell(
//				new IShadowrunCharacterControllerProvider<IShadowrunCharacterController>() {
//					public IShadowrunCharacterController getCharacterController() {
//						return charGen;
//					}}, 
//				null));
		selection.setSelectedCellFactory(lv -> new ComplexDataItemValueListCell( () -> charGen.getMetamagicOrEchoController()));
		selection.setShowHeadings(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
		
		lbGrade = new Label("?");
	}
	
	//-------------------------------------------------------------------
	protected void initLayout() {
		backHeader = new NumberUnitBackHeader(ResourceI18N.get(RES, "label.karma"));
		backHeader.setValue( ((ShadowrunCharacter)charGen.getModel()).getKarmaFree());
		HBox.setMargin(backHeader, new Insets(0,10,0,10));
		if (ResponsiveControlManager.getCurrentMode()==WindowMode.EXPANDED) {
			super.setBackHeader(null);
		} else {
			super.setBackHeader(backHeader);
		}

		layout = new OptionalNodePane(selection, bxDescription);
		setContent(layout);
		
		// Information about reached grade
		Label hdGrade = new Label(ResourceI18N.get(RES, "page.metaecho.grade"));
		hdGrade.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		HBox selectedHeading = new HBox(10, hdGrade, lbGrade);
		selection.setSelectedListHead(selectedHeading);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.showHelpForProperty().addListener( (ov,o,n) -> {
			logger.log(Level.DEBUG, "show help for "+n);
			bxDescription.setData(n);
			if (n!=null) {
				layout.setTitle(n.getName());
			} else {
				layout.setTitle(null);
			}
		});
	}
	
	//-------------------------------------------------------------------
	protected void refresh() {
		backHeader.setValue(  ((ShadowrunCharacter)charGen.getModel()).getKarmaFree());
		
		selection.refresh();
		lbGrade.setText( String.valueOf(charGen.getMetamagicOrEchoController().getGrade()));
		
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			selection.setController(charGen.getMetamagicOrEchoController());
			refresh();
		}
		if (type==BasicControllerEvents.CHARACTER_CHANGED) {
			refresh();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		selection.setShowHeadings(value!=WindowMode.MINIMAL);
	}

}
