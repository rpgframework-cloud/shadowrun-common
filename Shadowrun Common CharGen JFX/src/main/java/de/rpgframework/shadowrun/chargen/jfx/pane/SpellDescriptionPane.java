package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.CustomResourceManagerLoader;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.jfx.ADescriptionPane;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import de.rpgframework.jfx.attach.PDFViewerServiceFactory;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.SpellFeatureReference;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;

/**
 * @author stefa
 *
 */
public class SpellDescriptionPane extends ADescriptionPane<ASpell> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(ComplexFormDescriptionPane.class.getPackageName()+".DescriptionPanes");

//	private Label descTitle;
//	private Label descSources;
	private Label lbFeatures;
	private Label lbRange;
	private Label lbType;
	private Label lbDurat;
	private Label lbDrain;
	private GridPane grid;
//	private TextFlow description;
//	private TextArea taDescr;
//	private TextField tfKey;

	//-------------------------------------------------------------------
	public SpellDescriptionPane() {
	}

	//-------------------------------------------------------------------
	protected void initExtraComponents() {
		// Features
		lbFeatures  = new Label();
		lbFeatures.setWrapText(true);
		lbFeatures.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);


		lbRange = new Label();
		lbRange.getStyleClass().add(JavaFXConstants.STYLE_TABLE_DATA);
		lbType  = new Label();
		lbType.getStyleClass().add(JavaFXConstants.STYLE_TABLE_DATA);
		lbDurat = new Label();
		lbDurat.getStyleClass().add(JavaFXConstants.STYLE_TABLE_DATA);
		lbDrain = new Label();
		lbDrain.getStyleClass().add(JavaFXConstants.STYLE_TABLE_DATA);
	}

	//-------------------------------------------------------------------
	protected void initExtraLayout() {
		// Spell Table
		Label hdRange = new Label(ResourceI18N.get(UI,"spell.range"));
		Label hdType  = new Label(ResourceI18N.get(UI,"spell.type"));
		Label hdDurat = new Label(ResourceI18N.get(UI,"spell.duration"));
		Label hdDrain = new Label(ResourceI18N.get(UI,"spell.drain"));
		hdRange.setMaxWidth(Double.MAX_VALUE);
		hdType.setMaxWidth(Double.MAX_VALUE);
		hdDurat.setMaxWidth(Double.MAX_VALUE);
		hdDrain.setMaxWidth(Double.MAX_VALUE);

		hdRange.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD);
		hdType.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD);
		hdDurat.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD);
		hdDrain.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD);

		grid = new GridPane();
		grid.add(hdRange, 0, 0);
		grid.add(lbRange, 0, 1);
		grid.add(hdType , 1, 0);
		grid.add(lbType , 1, 1);
		grid.add(hdDurat, 2, 0);
		grid.add(lbDurat, 2, 1);
		grid.add(hdDrain, 3, 0);
		grid.add(lbDrain, 3, 1);
		for (int i = 0; i < 4; i++) {
			ColumnConstraints col1 = new ColumnConstraints();
			col1.setPercentWidth(25);
			col1.setHalignment(HPos.CENTER);
			col1.setFillWidth(true);
			grid.getColumnConstraints().addAll(col1);
		}
		grid.setStyle("-fx-background-color: #e9e9e2;");
		grid.setVisible(false);
//		grid.setMaxWidth(Double.MAX_VALUE);

		// Effect
//		description = new TextFlow();
//		taDescr = new TextArea();
//		taDescr.setVisible(false);
//		taDescr.setManaged(false);
//
//		setStyle("-fx-pref-width: 20em");
//		setStyle("-fx-max-width: 30em");
		inner.getChildren().add(0, lbFeatures);
		inner.getChildren().add(1, grid);
//		getChildren().addAll(
//				descTitle, descSources,
//				lbFeatures,
//				grid,
//				description, taDescr);
//		VBox.setMargin(description, new Insets(20,0,0,0));
		VBox.setMargin(   grid, new Insets(20,0,0,0));

	}
//
//	//-------------------------------------------------------------------
//	private void initInteractivity() {
//		description.setOnMouseEntered(ev -> enterDescription());
//		taDescr.setOnMouseExited(ev -> exitDescription());
//		taDescr.textProperty().addListener( (ov,o,n) -> customDescriptionChanged(item, n));
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.ADescriptionPane#setData(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public void setData(ASpell spell) {
		super.setData(spell);
		if (spell==null) {
			grid.setVisible(false);
			return;
		}
		grid.setVisible(true);

		// Eventually open a PDF
		if (spell != null) {
			PageReference pageRef = spell.getPageReferences()
					.stream()
					.filter(pr -> pr.getLanguage().equals(Locale.getDefault().getLanguage()))
					.findFirst()
					.get();
			if (pageRef != null) {
				PDFViewerServiceFactory.create().ifPresent(service -> {
					service.show(pageRef.getProduct().getRules(), pageRef.getProduct().getID(),
							pageRef.getLanguage(), pageRef.getPage());
				});
			}
		}

		StringBuffer buf = new StringBuffer();
		List<String> feats = new ArrayList<>();
		for (SpellFeatureReference ref : spell.getFeatures()) {
			if (ref.getFeature()==null)
				feats.add("NULL");
			else
				feats.add(ref.getFeature().getName());
		}
		if (!feats.isEmpty()) {
			buf.append("( "+String.join(", ", feats)+" )");
		}
		lbFeatures.setText(buf.toString());

		lbRange.setText(spell.getRange().getShortName(Locale.getDefault()));
		lbType.setText(spell.getType().getShortName(Locale.getDefault()));
		lbDurat.setText(spell.getDuration().getShortName(Locale.getDefault()));
		String drainText = String.valueOf(spell.getDrain());
		if (spell.getDrain()<0)
			drainText=Math.abs(spell.getDrain())+"+";
		lbDrain.setText(drainText);

//		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, spell.getDescription(Locale.getDefault()));
	}
}
