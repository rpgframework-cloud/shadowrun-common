package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.javafx.layout.AutoBox;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.Contact;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.jfx.listcell.ContactListCell;
import de.rpgframework.shadowrun.chargen.jfx.pane.ContactDetailPane;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public abstract class WizardPageContacts extends WizardPage implements ControllerListener {

	private final static Logger logger = System.getLogger(AWizardPageQualities.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageContacts.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterController<?,?,?,?> charGen;

	protected Label lbPoints;
	protected Button btnAdd,btnDel;
	protected ListView<Contact> selection;
	protected GenericDescriptionVBox bxDescription;
	protected OptionalNodePane layout;
	protected ContactDetailPane specific;
	private NumberUnitBackHeader backHeader;

	//-------------------------------------------------------------------
	public WizardPageContacts(Wizard wizard, IShadowrunCharacterController<?,?,?,?> charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.contacts.title"));
		initComponents();
		initLayout();
		initInteractivity();

		charGen.addListener(this);
		refresh();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		btnAdd = new Button(null, new SymbolIcon("Add"));
		btnDel = new Button(null, new SymbolIcon("Delete"));
		btnDel.setDisable(true);
		selection = new ListView<>();
		lbPoints = new Label();

		Label ph = new Label(ResourceI18N.get(RES, "page.contacts.placeholder"));
		ph.setWrapText(true);
		selection.setPlaceholder(ph);

		selection.setCellFactory(lv -> new ContactListCell(charGen.getContactController()));
//		selection.setShowHeadings(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
		bxDescription = new GenericDescriptionVBox(null,null);
	}

	//-------------------------------------------------------------------
	protected ContactDetailPane getRuleSpecificNode() {
		return null;
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		backHeader = new NumberUnitBackHeader("Karma");
		backHeader.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeader, new Insets(0,10,0,10));
//		if (ResponsiveControlManager.getCurrentMode()==WindowMode.EXPANDED) {
//			super.setBackHeader(null);
//		} else {
			super.setBackHeader(backHeader);
//		}

		// Explain Line
		lbPoints.getStyleClass().add(JavaFXConstants.STYLE_HEADING3);
		lbPoints.setStyle("-fx-text-fill: highlight");
		Label lbPointsText = new Label(ResourceI18N.get(RES, "page.contacts.points"));
		HBox explain = new HBox(10, lbPointsText, lbPoints);

		// Head Line
		Region buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		HBox headLine = new HBox(btnAdd,buf,btnDel);
		HBox.setHgrow(buf, Priority.ALWAYS);

		// Column
		VBox column = new VBox(10, explain, headLine, selection);
		specific = getRuleSpecificNode();
		specific.setVisible(false);

		AutoBox auto = new AutoBox();
		auto.getContent().addAll(column, specific);
		auto.setSpacing(20);
		layout = new OptionalNodePane(auto, bxDescription);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			btnDel.setDisable(n==null);
			if (specific!=null) {
				if (n!=null) {
					specific.setVisible(true);
					specific.setController(charGen.getContactController());
					specific.setData(n);
				} else {
					specific.setVisible(false);
				}
			}

		});
		btnDel.setOnAction(ev -> deleteClicked());
		btnAdd.setOnAction(ev -> addClicked());

		setOnExtraActionHandler( button -> onExtraAction(button));

		specific.cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				bxDescription.setTitle(n.getName(Locale.getDefault()));
				bxDescription.setData(n.getName(Locale.getDefault()), null, n.getDescription(Locale.getDefault()));
			}
		});
	}

	//-------------------------------------------------------------------
	private void refresh() {
		specific.setController(charGen.getContactController());
		Contact lastSelect = selection.getSelectionModel().getSelectedItem();

		selection.getItems().setAll(charGen.getModel().getContacts());
		if (lastSelect!=null && selection.getItems().contains(lastSelect))
			selection.getSelectionModel().select(lastSelect);

		lbPoints.setText(charGen.getContactController().getPointsLeft()+"");

		btnAdd.setDisable( !charGen.getContactController().canCreateContact().get() );

		backHeader.setValue( charGen.getModel().getKarmaFree());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.DEBUG, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.CHARACTER_CHANGED || type==BasicControllerEvents.GENERATOR_CHANGED) {
			refresh();
		}
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			charGen = (IShadowrunCharacterController<?, ?, ?, ?>) param[0];
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		//selection.setShowHeadings(value!=WindowMode.MINIMAL);
	}

	//-------------------------------------------------------------------
	public abstract void addClicked();

	//-------------------------------------------------------------------
	public void deleteClicked() {
		Contact toDelete = selection.getSelectionModel().getSelectedItem();
		charGen.getContactController().removeContact(toDelete);
	}

	//-------------------------------------------------------------------
	private void onExtraAction(CloseType button) {
		charGen.getContactController().roll();
		refresh();
	}

}
