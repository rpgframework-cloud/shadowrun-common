package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.util.Locale;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.shadowrun.MentorSpirit;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.QualityValue;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class MentorSpiritCell extends ListCell<MentorSpirit> {

	private Label lbName;
	private Label lbSimilar;
	private VBox layout;

	//-------------------------------------------------------------------
	public MentorSpiritCell() {
		lbName   = new Label();
		lbSimilar= new Label();
		lbName.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, JavaFXConstants.STYLE_HEADING4);
		lbSimilar.getStyleClass().add(JavaFXConstants.STYLE_TEXT_TERTIARY);

		layout = new VBox(lbName, lbSimilar);
		layout.setMaxWidth(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	public void updateItem(MentorSpirit item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lbName.setText(item.getName(Locale.getDefault()));
			lbSimilar.setText(item.getSimilarNames(Locale.getDefault()));

			setGraphic(layout);
		}
	}

}
