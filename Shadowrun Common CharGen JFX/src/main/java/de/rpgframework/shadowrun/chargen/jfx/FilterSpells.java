package de.rpgframework.shadowrun.chargen.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.IRefreshableList;
import de.rpgframework.shadowrun.ASpell;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class FilterSpells extends AFilterInjector<ASpell> {
	
	private final static Logger logger = System.getLogger(FilterSpells.class.getPackageName());

	private ChoiceBox<ASpell.Category> cbType;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#updateChoices(java.util.List)
	 */
	@Override
	public void updateChoices(List<ASpell> available) {
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#addFilter(de.rpgframework.jfx.FilteredListPage, javafx.scene.layout.FlowPane)
	 */
	@Override
	public void addFilter(IRefreshableList page, Pane filterPane) {
		/*
		 * Spell Category
		 */
		cbType = new ChoiceBox<ASpell.Category>();
		cbType.getItems().add(null);
		cbType.getItems().addAll(ASpell.Category.values());
		Collections.sort(cbType.getItems(), new Comparator<ASpell.Category>() {
			public int compare(ASpell.Category o1, ASpell.Category o2) {
				if (o1==null) return -1;
				if (o2==null) return  1;
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		cbType.setConverter(new StringConverter<ASpell.Category>() {
			public String toString(ASpell.Category val) {
				if (val==null) return "alle Typen";
				return val.getName();
			}
			public ASpell.Category fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbType);

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#applyFilter(de.rpgframework.jfx.FilteredListPage, java.util.List)
	 */
	@Override
	public List<ASpell> applyFilter(List<ASpell> input) {
		// Match spell category
		if (cbType.getValue()!=null) {
			input = input.stream()
					.filter(spell -> spell.getCategory()==cbType.getValue())
					.collect(Collectors.toList());
			logger.log(Level.INFO, "After filter cbType={0} remain {1} items", cbType, input.size());
		}
		return input;
	}

}
