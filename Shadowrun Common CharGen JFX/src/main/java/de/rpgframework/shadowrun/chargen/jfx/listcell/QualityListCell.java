package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.QualityValue;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class QualityListCell extends ListCell<Quality> {

	private ComplexDataItemController<Quality, QualityValue> controller;

	private Label lbName;
	private Label lbRequire;
	private Label lbCost;
	private HBox layout;
	private Function<Requirement, String> reqResolver;

	//-------------------------------------------------------------------
	public QualityListCell(ComplexDataItemController<Quality, QualityValue> controller, Function<Requirement, String> reqResolver) {
		this.controller = controller;
		this.reqResolver= reqResolver;
		lbName   = new Label();
		lbRequire= new Label();
		lbCost   = new Label();
		lbName.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5, JavaFXConstants.STYLE_HEADING4);
		lbCost.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);
		lbRequire.getStyleClass().add(JavaFXConstants.STYLE_TEXT_TERTIARY);
		lbRequire.setStyle("-fx-text-fill: highlight");

		VBox bxNameReq = new VBox(lbName, lbRequire);
		bxNameReq.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxNameReq, Priority.ALWAYS);
		lbName.setMaxWidth(Double.MAX_VALUE);

		layout = new HBox(bxNameReq, lbCost);
//		layout.setStyle("-fx-max-width: 22em; -fx-pref-width: 22em");
	}


	//-------------------------------------------------------------------
	public void updateItem(Quality item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lbName.setText(item.getName());
			if (controller!=null) {
				lbCost.setText(controller.getSelectionCostString(item));
				Possible poss = controller.canBeSelected(item);
				lbName.setDisable(!poss.get());
				if (poss.get())
					lbRequire.setText(null);
				else if (poss.getMostSevere()==null) {
					lbRequire.setText(String.join(", ",poss.getUnfulfilledRequirements().stream().map(r -> requirementToText(r)).collect(Collectors.toList())));
				} else
					lbRequire.setText(poss.getMostSevere().getMessage(Locale.getDefault()));
			} else {
				lbRequire.setText(null);
				lbName.setDisable(false);
				lbCost.setText(String.valueOf(item.getKarmaCost()));
			}
			if (item.isPositive()) {
				lbCost.setStyle(null);
			} else {
				lbCost.setStyle("-fx-text-fill: negative");
			}

			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	public String requirementToText(Requirement req) {
		if (reqResolver!=null)
			return reqResolver.apply(req);
		return String.valueOf(req);
	}
}
