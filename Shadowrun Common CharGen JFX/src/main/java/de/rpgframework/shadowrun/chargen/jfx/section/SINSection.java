package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.section.ListSection;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.LicenseValue;
import de.rpgframework.shadowrun.SIN;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.SpellValue;
import de.rpgframework.shadowrun.Tradition;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.jfx.dialog.EditSINDialog;
import de.rpgframework.shadowrun.chargen.jfx.listcell.SINListCell;
import de.rpgframework.shadowrun.chargen.jfx.listcell.SpellValueListCell;
import de.rpgframework.shadowrun.chargen.jfx.pane.SpellSelector;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class SINSection extends ListSection<SIN> implements IShadowrunCharacterControllerProvider<IShadowrunCharacterController> {

	private final static Logger logger = System.getLogger(SINSection.class.getPackageName());

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SINSection.class.getPackageName()+".Section");

	private IShadowrunCharacterController control;
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	public SINSection(String title) {
		super(title);

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		//list.setStyle("-fx-min-height: 10em; -fx-pref-height: 30em; -fx-pref-width: 25em"); 
		list.setCellFactory(lv -> {
			SINListCell cell = new SINListCell(control);
			if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
				cell.setStyle("-fx-pref-width: 18em");
			} else {
				cell.setStyle("-fx-pref-width: 22em");
			}
			return cell;
			});
	}

	// -------------------------------------------------------------------
	private void initLayout() {
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				btnDel.setDisable(true);
			} else {
				btnDel.setDisable( !control.getSINController().canDeleteSIN(n));
			}
		});
		btnAdd.setOnAction(ev -> onAdd());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	@Override
	public IShadowrunCharacterController getCharacterController() {
		return control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void refresh() {
		logger.log(Level.TRACE, "refresh");

		if (model!=null)
			setData(model.getSINs());
		else
			setData(new ArrayList<>());
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected void onAdd() {
		logger.log(Level.DEBUG, "opening SIN selection dialog");
		
		SIN sin = new SIN();
		EditSINDialog dialog = new EditSINDialog(control, sin, false);
		CloseType closed = FlexibleApplication.getInstance().showAlertAndCall(dialog, dialog.getButtonControl());
		if (closed==CloseType.OK) {
			logger.log(Level.INFO, "User chose a SIN to add");
			sin = control.getSINController().createNewSIN(dialog.getName(), dialog.getRating());
			if (sin==null) {
				logger.log(Level.ERROR, "Adding a SIN failed");
				return;
			}
			sin.setDescription(dialog.getDescription());
			logger.log(Level.DEBUG, "Adding a SIN done - now add licenses");
			for (LicenseValue val : dialog.getLicenses()) {
				control.getSINController().createNewLicense(sin, val.getRating(), val.getName());
			}
//			refresh();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(SIN item) {
		logger.log(Level.DEBUG, "onDelete "+item);
		if (control.getSINController().deleteSIN(item)) {
			list.getItems().remove(item);
		}
	}

//	//-------------------------------------------------------------------
//	@SuppressWarnings("unchecked")
//	private void onSettings() {
//		CharacterLeveller ctrl = (CharacterLeveller) this.control;
//		
//		VBox content = new VBox(20);
//		for (ConfigOption<?> opt : ctrl.getSpellController().getConfigOptions()) {
//			CheckBox cb = new CheckBox(opt.getName());
//			cb.setSelected((Boolean)opt.getValue());
//			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
//			content.getChildren().add(cb);
//		}
//		
//		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
//	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (ShadowrunCharacter) ctrl.getModel();
		refresh();
	}

}
