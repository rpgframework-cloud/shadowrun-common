package de.rpgframework.shadowrun.chargen.jfx.dialog;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.layout.AutoBox;

import de.rpgframework.ResourceI18N;
import de.rpgframework.shadowrun.LicenseValue;
import de.rpgframework.shadowrun.SIN;
import de.rpgframework.shadowrun.SIN.FakeRating;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.SINController;
import de.rpgframework.shadowrun.chargen.jfx.listcell.LicenseValueListCell;
import de.rpgframework.shadowrun.chargen.jfx.wizard.WizardPageSINs;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
@SuppressWarnings("rawtypes")
public class EditSINDialog extends ManagedDialog {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(EditSINDialog.class.getPackageName()+".AllDialogs");

	class MyTitledComponent extends VBox {
		public MyTitledComponent(String title, Node node) {
			Label head = new Label(title);
			head.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
			getChildren().addAll(head, node);
		}
	}

	private IShadowrunCharacterController  control;
	private SINController sinControl;
	private NavigButtonControl btnControl;
	private boolean isEdit;
	private SIN data;

	private Label    lblNuyenFree;
	private TextField tfName;
	private ChoiceBox<FakeRating> cbQuality;
	private Label    lblCost;
	private TextArea taDesc;

	private TextField tfLicenseName;
	private ChoiceBox<FakeRating> cbLicenseRating;
	private Button btnAdd,btnDel;
	private ListView<LicenseValue> lvLicenses;

	//-------------------------------------------------------------------
	public EditSINDialog(IShadowrunCharacterController control, SIN value, boolean isEdit) {
		super(RES.getString("dialog.sin.title"), null, CloseType.APPLY, CloseType.CANCEL);
		this.isEdit = isEdit;
		if (!isEdit) {
			buttons.setAll(CloseType.OK, CloseType.CANCEL);
		} else {
			buttons.setAll(CloseType.APPLY);
		}
		this.control = control;
		this.sinControl = control.getSINController();
		this.data    = value;
		btnControl = new NavigButtonControl();
		btnControl.setCallback( close -> (close==CloseType.CANCEL)?true:hasEnoughData());
		btnControl.initialize(FlexibleApplication.getInstance(), this);

		initComponents();
		initLayout();
		lblNuyenFree.setText(control.getModel().getNuyen()+" ");

		initInteractivity();
		cbQuality.setValue(FakeRating.ANYONE);
		cbQuality.setDisable(isEdit);

		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		cbQuality = new ChoiceBox<>();
		cbQuality.getItems().addAll(FakeRating.getSelectableValues());
		cbQuality.setConverter(new StringConverter<SIN.FakeRating>() {
			public String toString(FakeRating value) {return value.getValue()+" - \u00A5 "+(value.getValue()*2500);}
			public FakeRating fromString(String string) {return null;}
		});
		cbQuality.setDisable(isEdit);
		taDesc = new TextArea();
		taDesc.setStyle("-fx-max-height: 10em");
		//taDesc.setStyle("-fx-max-height: 6em; -fx-min-height: 4em; -fx-min-width: 20em;");
		lblCost= new Label();

		lblNuyenFree = new Label();
		lblNuyenFree.setStyle("-fx-text-fill: accent");

		/* License column */
		lvLicenses = new ListView<>();
		lvLicenses.setCellFactory(lv -> new LicenseValueListCell(false, control));
		lvLicenses.setStyle("-fx-max-height: 10em");
		Label ph = new Label(ResourceI18N.get(RES, "dialog.sin.license_placeholder"));
		ph.setWrapText(true);
		ph.setWrapText(true);
		lvLicenses.setPlaceholder(ph);
		tfLicenseName = new TextField();
		tfLicenseName.setPromptText(ResourceI18N.get(RES, "dialog.sin.license_prompt"));
		cbLicenseRating = new ChoiceBox<>();
		cbLicenseRating.getItems().addAll(FakeRating.getSelectableValues());
		cbLicenseRating.setValue(cbQuality.getValue());
		cbLicenseRating.setConverter(new StringConverter<SIN.FakeRating>() {
			public String toString(FakeRating value) {return value.getValue()+" - \u00A5 "+(value.getValue()*200);}
			public FakeRating fromString(String string) {return null;}
		});
		btnAdd = new Button(null, new SymbolIcon("Add"));
		btnDel = new Button(null, new SymbolIcon("Delete"));
		btnDel.setDisable(true);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		MyTitledComponent name = new MyTitledComponent(ResourceI18N.get(RES, "label.name"), tfName);
		MyTitledComponent qual = new MyTitledComponent(ResourceI18N.get(RES, "label.quality"), cbQuality);
		MyTitledComponent desc = new MyTitledComponent(ResourceI18N.get(RES, "label.description"), taDesc);


		HBox bxNameAndQual = new HBox(10,name, qual);

		VBox layout = new VBox();
		layout.getChildren().addAll(bxNameAndQual, desc);
		layout.setStyle("-fx-max-width: 25em; -fx-min-width: 20em");

		// License column
		Region buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		HBox line1Select = new HBox(5,tfLicenseName,cbLicenseRating,buf,btnAdd,btnDel);
		HBox.setHgrow(buf, Priority.ALWAYS);
		Label hdLicenses = new Label(ResourceI18N.get(RES,"dialog.sin.licenses"));
		hdLicenses.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);
		VBox licenseColumn = new VBox(5, hdLicenses, line1Select, lvLicenses);

		AutoBox outerLayout = new AutoBox();
		outerLayout.getContent().addAll(layout, licenseColumn);
		outerLayout.setSpacing(10);

		Label hdCost = new Label(ResourceI18N.get(RES, "label.nuyen")+": ");
		hdCost.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		HBox lastLine = new HBox(5, hdCost, lblCost, new Label("/"), lblNuyenFree);
		outerLayout.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(outerLayout, Priority.ALWAYS);
		setContent(new VBox(5, outerLayout, lastLine));
	}

	//-------------------------------------------------------------------
	private boolean hasEnoughData() {
		boolean enoughText = tfName.getText()!=null && tfName.getText().length()>0;
		boolean canSelect  = sinControl.canCreateNewSIN(cbQuality.getValue());
		return enoughText && (canSelect || isEdit);
	}

	//-------------------------------------------------------------------
	private void updateOKButton() {
		btnControl.setDisabled(CloseType.APPLY, !hasEnoughData());
	}

	//-------------------------------------------------------------------
	private void addLicense() {
		String name = tfLicenseName.getText();
		FakeRating rating = cbLicenseRating.getValue();
		if (isEdit) {
			if (control.getSINController().canCreateNewLicense(null, rating)) {
				LicenseValue val = control.getSINController().createNewLicense(null, rating, name);
				lvLicenses.getItems().add(val);
				tfLicenseName.clear();
			}
		} else {
			LicenseValue val = new LicenseValue(name, rating);
			lvLicenses.getItems().add(val);
			tfLicenseName.clear();
			updateAllCost();
		}
	}

	//-------------------------------------------------------------------
	private void updateAllCost() {
		int cost = cbQuality.getValue().getValue()*2500;
		for (LicenseValue val : lvLicenses.getItems()) {
			cost += val.getRating().getValue()*200;
		}
		lblCost.setText("\u00A5 "+ String.valueOf(cost));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbQuality.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)-> {
			cbLicenseRating.setValue(n);
			if (n!=null) {
				updateAllCost();
				updateOKButton();
			}
		});
		tfName.textProperty().addListener( (ov,o,n) -> {data.setName(n); updateOKButton();});
		taDesc.textProperty().addListener( (ov,o,n) -> data.setDescription(n));

		lvLicenses.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> btnDel.setDisable(n==null));
		btnAdd.setOnAction(ev -> { if (tfLicenseName.getText()!=null && !tfLicenseName.getText().isBlank()) addLicense();});
		btnDel.setOnAction(ev -> {
			LicenseValue val = lvLicenses.getSelectionModel().getSelectedItem();
			control.getSINController().deleteLicense(val);
			lvLicenses.getItems().remove(val);
		});
		tfLicenseName.setOnAction(ev -> { if (tfLicenseName.getText()!=null && !tfLicenseName.getText().isBlank()) addLicense();});
	}

	//-------------------------------------------------------------------
	public Object[] getData() {
		return new Object[]{tfName.getText(), cbQuality.getValue(), taDesc.getText()};
	}

	//-------------------------------------------------------------------
	private void refresh() {
		tfName.setText(data.getName());
		taDesc.setText(data.getDescription());
		cbQuality.setValue(data.getQuality());
		cbQuality.setDisable(data.getQuality()==FakeRating.REAL_SIN);
		lvLicenses.getItems().clear();
		//control.getModel().getLicenses(data)

		lblNuyenFree.setText(control.getModel().getNuyen()+" ");
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	public String getName() { return tfName.getText(); }
	public FakeRating getRating() { return cbQuality.getValue(); }
	public String getDescription() { return taDesc.getText(); }
	public List<LicenseValue> getLicenses() { return lvLicenses.getItems(); }

}
