package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.lang.System.Logger.Level;
import java.util.function.Supplier;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell.CellAction;
import de.rpgframework.shadowrun.AdeptPower;
import de.rpgframework.shadowrun.AdeptPowerValue;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

/**
 * Adds a small cost label in the upper right corner
 * @author prelle
 *
 */
public class AdeptPowerValueListCell extends ComplexDataItemValueListCell<AdeptPower, AdeptPowerValue> {

	private CellAction actEdit;
	
	//-------------------------------------------------------------------
	public AdeptPowerValueListCell(Supplier<ComplexDataItemController<AdeptPower, AdeptPowerValue>> ctrlProv) {
		super(ctrlProv);
//		actEdit = new CellAction("Edit",ICON_PEN, "tooltip", (act,item) -> editClicked(item));
//		addAction(actEdit);
		name.setMaxWidth(250);
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
//	 */
//	@Override
//	public void updateItem(AdeptPowerValue item, boolean empty) {
//		super.updateItem(item, empty);
//
//		if (!empty) {
//			Label lbCost = new Label( String.valueOf(item.getCost()) );
//			StackPane stack = new StackPane(lbCost, layout);
//			StackPane.setAlignment(lbCost, Pos.TOP_RIGHT);
//			setGraphic(stack);
//
//			btnEdit.setVisible(item.getResolved().getChoices().stream().anyMatch(c -> "TEXT".equals(String.valueOf(c.getChooseFrom()))));
//		}
//	}

}
