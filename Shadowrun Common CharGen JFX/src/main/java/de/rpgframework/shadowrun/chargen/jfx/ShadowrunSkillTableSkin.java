package de.rpgframework.shadowrun.chargen.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.controlsfx.control.ToggleSwitch;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.public_skins.GridPaneTableViewSkin;
import org.prelle.javafx.public_skins.SecondLineTableRow;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.NumericalValueWith3PoolsController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.SkillSpecializationValue;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.SkillType;
import de.rpgframework.shadowrun.chargen.charctrl.ISkillController;
import de.rpgframework.shadowrun.chargen.charctrl.ISkillGenerator;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.collections.SetChangeListener;
import javafx.css.PseudoClass;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public abstract class ShadowrunSkillTableSkin<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, C extends ShadowrunCharacter<S, V,?,?>> extends SkinBase<ShadowrunSkillTable<S,V,C>> {

	protected final static Logger logger = System.getLogger(ShadowrunSkillTableSkin.class.getPackageName());

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(ShadowrunSkillTable.class.getName());

	private ToggleSwitch tsExpertMode;
	private Label lbAdjust, lbPoints;

	private TableView<V> table;
	private TableColumn<V, String> colName;
	protected TableColumn<V, Number> colCombi;
	private TableColumn<V, Number> colFinal;
	protected TableColumn<V, Number> colPoints1;
	protected TableColumn<V, Number> colPoints2;
	protected TableColumn<V, Number> colPoints1Only;
	protected TableColumn<V, Number> colPoints2Only;
	/** Value means ENABLED(true) oR DISABLED(false) */
	protected TableColumn<V, Boolean> colDec;
	/** Value means ENABLED(true) oR DISABLED(false) */
	protected TableColumn<V, Boolean> colInc;
	private List<SkillType> allowedTypes;
	private Label hdPoints1, hdPoints2;
	
	private boolean isUpdating;
	
	private ToggleButton headBtnPoints, headBtnKarma;
	private ToggleGroup toggles = new ToggleGroup();

	private MapChangeListener<Object, Object> propertiesMapListener = c -> {
        if (! c.wasAdded()) return;
        if (SkinProperties.WINDOW_MODE.equals(c.getKey())) {
            updateLayout();
            getSkinnable().requestLayout();
            getSkinnable().getProperties().remove(SkinProperties.WINDOW_MODE);
        }
        if (SkinProperties.REFRESH.equals(c.getKey())) {
            refresh();
	        getSkinnable().getProperties().remove(SkinProperties.REFRESH);
        }
    };
    
    private Callback<V, Node> dataLineFactory;

	//-------------------------------------------------------------------
	public ShadowrunSkillTableSkin(ShadowrunSkillTable<S,V,C> control, SkillType...allowedTypes) {
		super(control);
		this.allowedTypes = Arrays.asList(allowedTypes);
		initColumns();
		initComponents();
		initLayout();
		refresh();
		initInteractivity();		
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initColumns() {
		headBtnPoints = createToggle("column.points");
		headBtnKarma  = createToggle("column.karma");

		ISkillController<S, V> ctrl = (ISkillController<S, V>) getSkinnable().getController().getSkillController();
		final NumericalValueWith3PoolsController<S,V> numCtrl = (ctrl instanceof NumericalValueWith3PoolsController)
				?
					(NumericalValueWith3PoolsController<S, V>) ctrl : null;

		colName = new TableColumn<>(ResourceI18N.get(RES,"column.name"));
		colName.setCellValueFactory(cdv -> new SimpleStringProperty(
				cdv.getValue().getModifyable().getChoices().isEmpty()
				?
				cdv.getValue().getModifyable().getName(Locale.getDefault())
				:
					(cdv.getValue().getDecision(cdv.getValue().getModifyable().getChoices().get(0).getUUID())!=null)
					?
							cdv.getValue().getDecision(cdv.getValue().getModifyable().getChoices().get(0).getUUID()).getValue()
							:"?"
				));
		colName.setCellFactory(col -> new HoverNameCell<S,V>(this.getSkinnable()));

		colFinal = new TableColumn<>(ResourceI18N.get(RES,"column.final"));
		colFinal.setCellValueFactory(cdv -> new SimpleIntegerProperty(cdv.getValue().getModifiedValue()));
		colFinal.setStyle("-fx-pref-width: 2em; -fx-max-width: 3em");
		colFinal.setMaxWidth(36);

		colCombi = new TableColumn<>(ResourceI18N.get(RES,"column.final"));
		colCombi.setCellValueFactory(cdv -> new SimpleIntegerProperty(cdv.getValue().getModifiedValue()));
		colCombi.setCellFactory(col -> {
			NumericalValueController<S,V> c = (NumericalValueController<S, V>) getSkinnable().getController().getSkillController();
			return new ShadowrunSkillValueCell<S,V>(c);
		});
		colCombi.setPrefWidth(80);

		colPoints1 = new TableColumn<>(ResourceI18N.get(RES, "column.points"));
		colPoints1.setCellValueFactory(cdv -> new SimpleIntegerProperty(getPoints1(cdv.getValue())));
		colPoints1.setCellFactory(col -> {
			NumericalValueController<S,V> c = new NumericalValueController<S, V>() {

				@Override
				public RecommendationState getRecommendationState(S item) {
					return RecommendationState.NEUTRAL;
				}
				public int getValue(V value) { return value.getDistributed(); }
				@Override
				public Possible canBeIncreased(V value) {
					return ((ISkillGenerator<S, V>)getSkinnable().getController().getSkillController()).canBeIncreasedPoints(value);
				}

				@Override
				public Possible canBeDecreased(V value) {
					return ((ISkillGenerator<S, V>)getSkinnable().getController().getSkillController()).canBeDecreasedPoints(value);
				}

				@Override
				public OperationResult<V> increase(V value) {
					return ((ISkillGenerator<S, V>)getSkinnable().getController().getSkillController()).increasePoints(value);
				}

				@Override
				public OperationResult<V> decrease(V value) {
					return ((ISkillGenerator<S, V>)getSkinnable().getController().getSkillController()).decreasePoints(value);
				}
			};
			return new ShadowrunSkillValueCell<S,V>(c, true);
		});
		colPoints1Only = new TableColumn<>();
		colPoints1Only.setGraphic(headBtnPoints);
		colPoints1Only.setCellValueFactory(cdv -> new SimpleIntegerProperty(getPoints1(cdv.getValue())));
		colPoints1Only.setPrefWidth(50);

		colPoints2 = new TableColumn<>(ResourceI18N.get(RES, "column.karma"));
		colPoints2.setCellValueFactory(cdv -> new SimpleIntegerProperty(getPoints2(cdv.getValue())));
		colPoints2.setCellFactory(col -> {
			NumericalValueController<S,V> c = new NumericalValueController<S, V>() {

				@Override
				public RecommendationState getRecommendationState(S item) {
					return RecommendationState.NEUTRAL;
				}
				public int getValue(V value) { return value.getDistributed(); }

				@Override
				public Possible canBeIncreased(V value) {
					if (ctrl instanceof NumericalValueWith3PoolsController) {
						return ((NumericalValueWith3PoolsController<S,V>)ctrl).canBeIncreasedPoints3(value);
					} else
						return ((ISkillGenerator<S, V>)getSkinnable().getController().getSkillController()).canBeIncreasedPoints2(value);
				}

				@Override
				public Possible canBeDecreased(V value) {
					if (ctrl instanceof NumericalValueWith3PoolsController) {
						return ((NumericalValueWith3PoolsController<S,V>)ctrl).canBeDecreasedPoints3(value);
					} else
						return ((ISkillGenerator<S, V>)getSkinnable().getController().getSkillController()).canBeDecreasedPoints2(value);
				}

				@Override
				public OperationResult<V> increase(V value) {
					if (ctrl instanceof NumericalValueWith3PoolsController) {
						return ((NumericalValueWith3PoolsController<S,V>)ctrl).increasePoints3(value);
					} else
						return ((ISkillGenerator<S, V>)getSkinnable().getController().getSkillController()).increasePoints2(value);
				}

				@Override
				public OperationResult<V> decrease(V value) {
					if (ctrl instanceof NumericalValueWith3PoolsController) {
						return ((NumericalValueWith3PoolsController<S,V>)ctrl).decreasePoints3(value);
					} else
						return ((ISkillGenerator<S, V>)getSkinnable().getController().getSkillController()).decreasePoints2(value);
				}
			};
			return new ShadowrunSkillValueCell<S,V>(c, true);
		});
		colPoints2Only = new TableColumn<>();
		colPoints2Only.setGraphic(headBtnKarma);
		colPoints2Only.setCellValueFactory(cdv -> new SimpleIntegerProperty(getPoints2(cdv.getValue())));
		colPoints2Only.setPrefWidth(46);

		colDec = new TableColumn<>();
		colDec.setCellValueFactory(cdv -> new SimpleBooleanProperty(
				(toggles.getSelectedToggle()==headBtnPoints)?(numCtrl.canBeDecreasedPoints(cdv.getValue()).get() || numCtrl.canBeDecreasedPoints2(cdv.getValue()).get() ) 
				: 
				numCtrl.canBeDecreasedPoints3(cdv.getValue()).get()));
		colDec.setCellFactory(cdv -> new TableCell<>() {
			public void updateItem(Boolean item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					Button btn = new Button(null, new SymbolIcon("remove"));
					btn.setDisable(!item);
					btn.getStyleClass().add("mini-button");
					btn.setOnAction(ev -> {
					if (toggles.getSelectedToggle()==headBtnPoints) {
						logger.log(Level.DEBUG, "DEC");
						V val = getTableRow().getItem();
						if (((NumericalValueWith3PoolsController)ctrl).canBeDecreasedPoints2(val).get()) {
							((NumericalValueWith3PoolsController)ctrl).decreasePoints2(val);
						} else if (((NumericalValueWith3PoolsController)ctrl).canBeDecreasedPoints(val).get()) {
							((NumericalValueWith3PoolsController)ctrl).decreasePoints(val);
						}
					}
					});
					setGraphic(btn);
				}
			}
		});
		colDec.setPrefWidth(36);
		colInc = new TableColumn<>();
		colInc.setCellValueFactory(cdv -> new SimpleBooleanProperty(
				(toggles.getSelectedToggle()==headBtnPoints)?(numCtrl.canBeIncreasedPoints(cdv.getValue()).get() || numCtrl.canBeIncreasedPoints2(cdv.getValue()).get())
				: 				
				numCtrl.canBeIncreasedPoints3(cdv.getValue()).get()));
		colInc.setCellFactory(cdv -> new TableCell<>() {
			@SuppressWarnings("rawtypes")
			public void updateItem(Boolean item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					Button btn = new Button(null, new SymbolIcon("add"));
					btn.setDisable(!item);
					btn.getStyleClass().add("mini-button");
					V val = getTableRow().getItem();
					btn.setOnAction(ev -> {
						if (toggles.getSelectedToggle()==headBtnPoints) {
							logger.log(Level.DEBUG, "INC Points");
							if (((NumericalValueWith3PoolsController)ctrl).canBeIncreasedPoints2(val).get()) {
								((NumericalValueWith3PoolsController)ctrl).increasePoints2(val);
							} else if (((NumericalValueWith3PoolsController)ctrl).canBeIncreasedPoints(val).get()) {
								((NumericalValueWith3PoolsController)ctrl).increasePoints(val);
							}
						} else if (toggles.getSelectedToggle()==headBtnKarma) {
							logger.log(Level.DEBUG, "INC Karma");
							((NumericalValueWith3PoolsController)ctrl).increasePoints3(val);
							refresh();
						}
					});
					setGraphic(btn);
				}
			}
		});
		colInc.setPrefWidth(40);

	}
	
	//-------------------------------------------------------------------
	private ToggleButton createToggle(String key) {
		ToggleButton lab = new ToggleButton( (key!=null)?ResourceI18N.get(RES, key):"");
		lab.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD+"-toggle");
		lab.setMaxWidth(Double.MAX_VALUE);
		lab.setToggleGroup(toggles);
		return lab;
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		tsExpertMode = new ToggleSwitch("Expert");
		lbAdjust = new Label("?");
		lbPoints = new Label("?");
		lbAdjust.setStyle("-fx-text-fill: -fx-text-base-color");
		lbPoints.setStyle("-fx-text-fill: -fx-text-base-color");
		
		// Factory to create a node for a second line when the SkillValue
		// has been selected
		dataLineFactory   = sVal -> {
			if (sVal.getModifyable().getType()==SkillType.LANGUAGE) return null;
			if (sVal.getModifyable().getType()==SkillType.KNOWLEDGE) return null;
			return createDataLine(sVal);
		};
		
		table = new TableView<>();
		table.setRowFactory( tv -> {
			return new SecondLineTableRow<V>();
		});
		//table.setTableMenuButtonVisible(false);
		table.setSkin(new GridPaneTableViewSkin<V>(table, true, dataLineFactory));
		table.setId("ShadowrunSkillTableSkin.table");
		
		table.setPlaceholder(new Label("Empty"));
		
		table.getColumns().addAll(colName, colCombi);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		hdPoints1 = new Label(ResourceI18N.get(RES, "head.points")+":");
		hdPoints2 = new Label(ResourceI18N.get(RES, "head.points2")+":");
		
		HBox line = new HBox(5, tsExpertMode, hdPoints1, lbAdjust, hdPoints2, lbPoints);
		HBox.setMargin(hdPoints2, new Insets(0,0,0,10));

		VBox layout = new VBox(5, line, table);
		getChildren().add(layout);		
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().getProperties().addListener(propertiesMapListener);
		getSkinnable().useExpertModeProperty().addListener( (ov,o,n) -> updateLayout());
		tsExpertMode.selectedProperty().bindBidirectional(getSkinnable().useExpertModeProperty());
		
        final ObservableMap<Object, Object> properties = getSkinnable().getProperties();
        properties.remove(SkinProperties.REFRESH);
        properties.remove(SkinProperties.WINDOW_MODE);
        properties.addListener(propertiesMapListener);
        
        toggles.selectedToggleProperty().addListener( (ov,o,n) -> refresh());
        getSkinnable().selectedItemProperty().bind( table.getSelectionModel().selectedItemProperty() );
//		logger.log(Level.DEBUG, "After initInteractivity() grid has "+grid.getChildrenUnmodifiable().size()+" children");
	}
	
	//-------------------------------------------------------------------
	public abstract int getPoints1(V sVal);
	public abstract int getPoints2(V sVal);

	//-------------------------------------------------------------------
	private void updateLayout() {
		logger.log(Level.INFO, "updateLayout");
		if (isUpdating) return;
		isUpdating = true;
		
		synchronized (table.getColumns()) {
			try {
				boolean expertMode = getSkinnable().isUseExpertMode();
				boolean enoughSpace = ResponsiveControlManager.getCurrentMode() != WindowMode.MINIMAL;

				if (!expertMode) {
					toggles.selectToggle(null);
					table.getColumns().setAll(colName, colCombi);
				} else {
					if (enoughSpace) {
						toggles.selectToggle(null);
						table.getColumns().setAll(colName, colPoints1, colPoints2, colFinal);
					} else {
						toggles.selectToggle(headBtnPoints);
						table.getColumns().setAll(colName, colDec, colPoints1Only, colPoints2Only, colInc, colFinal);
					}
				}

//		if (getController()!=null) {
//			refresh();
//		}
			} finally {
				isUpdating = false;
			}
		}
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public void refresh() {
		logger.log(Level.WARNING, "refresh");
		List<V> selected = (List<V>) getSkinnable().getController().getSkillController().getSelected();
		if (!allowedTypes.isEmpty()) {
			selected = selected.stream().filter(sv -> allowedTypes.contains(sv.getSkill().getType()))
					.collect(Collectors.toList());
		}
		// Sort by type and name
		Collections.sort(selected, new Comparator<V>() {
			public int compare(V o1, V o2) {
				SkillType t1 = o1.getModifyable().getType();
				SkillType t2 = o2.getModifyable().getType();
				boolean b1 = t1==SkillType.KNOWLEDGE || t1==SkillType.LANGUAGE;
				boolean b2 = t2==SkillType.KNOWLEDGE || t2==SkillType.LANGUAGE;
				if (b1 && !b2) return 1;
				if (!b1 && b2) return -1;
				if (b1 && b2 && (t1!=t2)) return t1.compareTo(t2);
				return o1.getName(Locale.getDefault()).compareTo(o2.getName(Locale.getDefault()));
			}});
		// Eventually modify list to enter header
		if (getSkinnable().getSelectedListModifier()!=null) {
			getSkinnable().getSelectedListModifier().accept(selected);
		}
		
		table.getItems().setAll(selected);

		ISkillController<S,V> ctrl = (ISkillController<S, V>) getSkinnable().getController().getSkillController();
		if (ctrl instanceof ISkillGenerator) {
			ISkillGenerator<S,V> gen = (ISkillGenerator<S, V>) ctrl;
			lbAdjust.setText(String.valueOf(gen.getPointsLeft()));
			lbPoints.setText(String.valueOf(gen.getPointsLeft2()));
		}
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected Node createDataLine(V sVal) {
		ISkillController<S, V> gen = ((ISkillController<S,V>)getSkinnable().getController().getSkillController());
		return new DataLine<S,V>(sVal, gen);
	}

}

class DataLine<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>> extends HBox {

	protected final static Logger logger = System.getLogger(ShadowrunSkillTableSkin.class.getPackageName());
	
	private V sVal;
	private Label node;
	
	//-------------------------------------------------------------------
	public DataLine(V sVal, ISkillController<S, V> control) {
		super(10);
		this.getStyleClass().add("data-line");
		this.sVal    = sVal;
		
		initComponents();
		initLayout();
	}
	
	//-------------------------------------------------------------------
	private String getSpecName(SkillSpecializationValue<S> spec) {
		if (spec.getDecisions().isEmpty())
			return spec.getModifyable().getName();
		return spec.getDecisions().get(0).getValue();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		node = new Label();
		node.getStyleClass().add(JavaFXConstants.STYLE_TEXT_SECONDARY);
		node.setWrapText(true);
		
		List<String> names = sVal.getSpecializations()
			.stream()
			.map(spec -> getSpecName(spec))
			.collect(Collectors.toList());
		node.setText(String.join(", ", names));
		
		HBox.setMargin(node, new Insets(2,0,5,20));
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		if (node.getText()!=null && !node.getText().isBlank())
			getChildren().add(node);
		else
			getChildren().clear();
	}
	
}

class HoverNameCell<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>> extends TableCell<V,String> {

	protected final static Logger logger = System.getLogger(ShadowrunSkillTableSkin.class.getPackageName());
	
	private ShadowrunSkillTable<S,V,?> parent;
	private Label lbName;
	private Button button;
	private StackPane stack;
	private VBox box ;
	
	public HoverNameCell(ShadowrunSkillTable<S,V,?> parent) {
		this.parent = parent;
		lbName = new Label();
		lbName.setMaxWidth(Double.MAX_VALUE);
		box = new VBox(2, lbName);
		box.setAlignment(Pos.TOP_LEFT);
		
		button = new Button(null, new SymbolIcon("Edit"));
		button.setVisible(false);
		
		stack = new StackPane();
		stack.getChildren().addAll(box, button);
		StackPane.setAlignment(button, Pos.CENTER_RIGHT);
		StackPane.setAlignment(lbName, Pos.CENTER_LEFT);
		
		button.setOnAction(ev -> {
			logger.log(Level.WARNING, "Clicked");
			Callback<V, CloseType> callback = parent.getActionCallback();
			if (callback!=null) {
				CloseType close = callback.call(getTableRow().getItem());
				logger.log(Level.WARNING, "Returned "+close);
				if (close==CloseType.OK || close==CloseType.APPLY) {
					parent.refresh();
				}
			}
			
		});
		
		// Only show the edit button, when cursor hovers above the row
		getPseudoClassStates().addListener(new SetChangeListener<PseudoClass>() {
			public void onChanged(Change<? extends PseudoClass> change) {
				if (change.getElementRemoved()!=null && change.getElementRemoved().getPseudoClassName().equals("rowhover")) {
					button.setVisible(false);
				} 
				if (change.getElementAdded()!=null && change.getElementAdded().getPseudoClassName().equals("rowhover")) {
					button.setVisible(true);
				} 
				
			}});
	}
	
	public void updateItem(String item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setGraphic(null);
		} else {
			box.getChildren().retainAll(lbName);
			lbName.setText(item);
			V sVal = getTableRow().getItem();
			if (!sVal.getSpecializations().isEmpty()) {
				for (SkillSpecializationValue<S> tmp : sVal.getSpecializations()) {
					Label lb = new Label("- "+tmp.getResolved().getName(Locale.getDefault()));
					box.getChildren().add(lb);
				}
			}

			setMaxWidth(Double.MAX_VALUE);
			setAlignment(Pos.CENTER_LEFT);
			setGraphic(stack);
		}
	}
}
