package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell.CellAction;
import de.rpgframework.jfx.section.ListSection;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.QualityValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.jfx.listcell.QualityValueListCell;
import de.rpgframework.shadowrun.chargen.jfx.pane.QualitySelector;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class QualitySection extends ListSection<QualityValue> implements IShadowrunCharacterControllerProvider<IShadowrunCharacterController> {

	private final static Logger logger = System.getLogger(QualitySection.class.getPackageName());

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(QualitySection.class.getPackageName()+".Section");

	protected IShadowrunCharacterController control;
	protected ShadowrunCharacter model;
	private Function<Requirement, String> requirementResolver;
	private Function<Modification,String> modResolver;

	//-------------------------------------------------------------------
	public QualitySection(Function<Requirement, String> requirementResolver, Function<Modification,String> modResolver) {
		super(ResourceI18N.get(RES, "section.quality.title"));
		this.requirementResolver = requirementResolver;
		this.modResolver  = modResolver;

		initComponents();
		setContent(list);
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list.setStyle("-fx-min-height: 10em; -fx-pref-height: 30em; -fx-pref-width: 25em");
		list.setCellFactory(cell -> new QualityValueListCell(this, true) {
			protected boolean canPerformAction(QualityValue item, CellAction action) {
				return false; //!item.getDecisions().isEmpty();
			}
			protected OperationResult<QualityValue> editClicked(QualityValue ref) {
				logger.log(Level.WARNING, "TODO: editClicked");
				Decision[] decisions = handleEditChoices(ref);

				return new OperationResult<>();
			}
		});

//		if (control instanceof CharacterLeveller)
//			setSettingsButton( new Button(null, new SymbolIcon("setting")) );
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		btnAdd.setOnAction(ev -> onAdd());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	@Override
	public IShadowrunCharacterController getCharacterController() {
		return control;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public void refresh() {
		if (model!=null) {
			setData(model.getQualities());
		} else
			setData(new ArrayList<>());
	}

	//-------------------------------------------------------------------
	protected void onAdd() {
		logger.log(Level.INFO, "opening quality selection dialog");

		QualitySelector selector = new QualitySelector(control.getQualityController(), requirementResolver, modResolver);
//		selector.setPlaceholder(new Label(Resource.get(RES,"metaechosection.selector.placeholder")));
//		SelectorWithHelp<MetamagicOrEcho> pane = new SelectorWithHelp<>(selector);
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES,"section.quality.selector.title"), selector, CloseType.OK, CloseType.CANCEL);

		CloseType close = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		logger.log(Level.DEBUG,"Closed with "+close);
		if (close==CloseType.OK) {
			Quality data = selector.getSelected();
			logger.log(Level.DEBUG, "Selected quality: "+data);

			OperationResult<QualityValue> result = null;
			if (data.getChoices().isEmpty()) {
				result = control.getQualityController().select(data);
			} else {
				Decision[] dec = handleChoices(data);
				if (dec!=null) {
					result = control.getQualityController().select(data, dec);
				}
			}
			if (result!=null) {
				if (result.wasSuccessful()) {
					list.refresh();
				} else {
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, result.getError());
				}
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(QualityValue item) {
		logger.log(Level.DEBUG, "onDelete "+item);
		if (control.getQualityController().deselect(item)) {
			list.getItems().remove(item);
		}
	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (ShadowrunCharacter) ctrl.getModel();
		refresh();
	}

	//-------------------------------------------------------------------
	protected Decision[] handleChoices(Quality data) {
		return new Decision[0];
	}

	//-------------------------------------------------------------------
	protected Decision[] handleEditChoices(QualityValue toEdit) {
		logger.log(Level.WARNING, "TODO: implement handleEditChoices in {0}", this.getClass().getName());
		return new Decision[0];
	}

}
