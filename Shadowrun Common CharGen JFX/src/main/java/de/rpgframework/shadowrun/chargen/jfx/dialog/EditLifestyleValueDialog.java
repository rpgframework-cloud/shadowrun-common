package de.rpgframework.shadowrun.chargen.jfx.dialog;

import java.util.Collections;
import java.util.Comparator;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.data.GenericCore;
import de.rpgframework.jfx.NumericalValueField;
import de.rpgframework.jfx.ThreeColumnPane;
import de.rpgframework.shadowrun.Lifestyle;
import de.rpgframework.shadowrun.LifestyleQuality;
import de.rpgframework.shadowrun.SIN;
import de.rpgframework.shadowrun.chargen.charctrl.ILifestyleController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
public class EditLifestyleValueDialog<L extends Lifestyle> extends ManagedDialog {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(EditContactDialog.class.getPackageName()+".AllDialogs");

	private IShadowrunCharacterController control;
	private ILifestyleController<L> lifeControl;
	private SimpleObjectProperty<NumericalValueController<LifestyleQuality,L>> lifeControlProp;
	private NavigButtonControl btnControl;
	private L data;

	private Label    lblNuyenFree;
	private NumericalValueField<LifestyleQuality, L> nvMonths;
	private TextField tfName;
	private ChoiceBox<LifestyleQuality> cbLifestyle;
	private ChoiceBox<SIN> cbSINs;
	private Label    lblBaseCost;
	private Label    lblTotalCost;
	private Label lblName;
	private Label lblProduct;
	private Label lblDesc;
	private TextArea taDesc;

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public EditLifestyleValueDialog(IShadowrunCharacterController control, L value, boolean isEdit) {
		super(ResourceI18N.get(RES,"dialog.lifestyles.title"), null, CloseType.APPLY);
		if (!isEdit) {
			buttons.setAll(CloseType.OK, CloseType.CANCEL);
		}
		this.control = control;
		this.lifeControl = control.getLifestyleController();
		lifeControlProp  = new SimpleObjectProperty<NumericalValueController<LifestyleQuality,L>>(lifeControl);
		this.data    = value;
		btnControl = new NavigButtonControl();
		btnControl.setCallback(close -> canBeClosed(close));

		initComponents();
		initLayout();
		lblNuyenFree.setText(control.getModel().getNuyen()+" ");

		initInteractivity();
		if (isEdit)
			refresh();
	}

	//-------------------------------------------------------------------
	private boolean canBeClosed(CloseType close) {
		if (close==CloseType.CANCEL) return true;
		if (data==null) return false;
		System.err.println("EditLifestyleValueDialog.canBeClosed: "+lifeControl.canBeSelected(data));
		return lifeControl.canBeSelected(data).get();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		cbLifestyle = new ChoiceBox<>();
		cbLifestyle.getItems().addAll(GenericCore.getItemList(LifestyleQuality.class));
		cbLifestyle.setConverter(new StringConverter<LifestyleQuality>() {
			public String toString(LifestyleQuality data) { return data!=null?data.getName():null; }
			public LifestyleQuality fromString(String string) { return null; }
		});
		Collections.sort(cbLifestyle.getItems(), new Comparator<LifestyleQuality>() {

			@Override
			public int compare(LifestyleQuality l1, LifestyleQuality l2) {
				if (l1.getId().startsWith("artist") && !l2.getId().startsWith("artist")) return +1;
				if (l2.getId().startsWith("artist") && !l1.getId().startsWith("artist")) return -1;
				return Integer.compare(l1.getCost(), l2.getCost());
			}});
		cbSINs = new ChoiceBox<>();
		cbSINs.getItems().addAll(control.getModel().getSINs());
		cbSINs.setConverter(new StringConverter<SIN>() {
			public String toString(SIN data) { return data!=null?data.getName():null; }
			public SIN fromString(String string) { return null; }
		});
		taDesc = new TextArea();
		lblBaseCost= new Label();
		lblTotalCost= new Label();

		lblNuyenFree = new Label();
		lblNuyenFree.getStyleClass().add("text-header");
		nvMonths = new NumericalValueField<LifestyleQuality, L>(()->data, lifeControlProp.get());

		lblName     = new Label("");
		lblName.getStyleClass().add("text-small-subheader");
		lblProduct  = new Label();
		lblDesc     = new Label("");
		lblDesc.setWrapText(true);
		lblDesc.setStyle("-fx-pref-width: 25em");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblName2 = new Label(ResourceI18N.get(RES, "dialog.lifestyles.name"));
		Label lblQual = new Label(ResourceI18N.get(RES, "dialog.lifestyles.quality"));
		Label lblDesc2 = new Label(ResourceI18N.get(RES, "dialog.lifestyles.description"));
		Label heaBaseCost  = new Label(ResourceI18N.get(RES, "dialog.lifestyles.cost.base"));
		Label heaTotalCost = new Label(ResourceI18N.get(RES, "dialog.lifestyles.cost.total"));
		Label heaSIN       = new Label(ResourceI18N.get(RES, "dialog.lifestyles.sin"));

		lblName2.getStyleClass().add("base");
		lblQual.getStyleClass().add("base");
		heaSIN.getStyleClass().add("base");
		lblDesc2.getStyleClass().add("base");

		HBox qualLine = new HBox(cbLifestyle, heaBaseCost, lblBaseCost, heaTotalCost, lblTotalCost);
		qualLine.setStyle("-fx-spacing: 1em");

		VBox bxDesc = new VBox(20);
		bxDesc.getChildren().addAll(lblName, lblProduct, lblDesc);

		ThreeColumnPane threeCol = new ThreeColumnPane();
		threeCol.setColumn3Node(bxDesc);

		VBox layout = new VBox();
		layout.getChildren().addAll(lblName2, tfName, lblQual, qualLine, heaSIN, cbSINs, threeCol, lblDesc2, taDesc);
		layout.getChildren().remove(threeCol);
		VBox.setMargin(lblQual, new Insets(20,0,0,0));
		VBox.setMargin(lblDesc2, new Insets(20,0,0,0));
		VBox.setMargin(threeCol, new Insets(20,0,0,0));
		VBox.setMargin(heaSIN  , new Insets(20,0,0,0));

		Label heaNuyen      = new Label(ResourceI18N.get(RES, "dialog.lifestyles.nuyen"));
		Label heaMonths     = new Label(ResourceI18N.get(RES, "dialog.lifestyles.months"));
		VBox points = new VBox();
		points.setAlignment(Pos.TOP_CENTER);
//		points.getStyleClass().add("section-bar");
		points.setStyle("-fx-pref-width: 12em");
		points.getChildren().addAll(heaNuyen, lblNuyenFree, heaMonths, nvMonths);
		VBox.setMargin(heaMonths, new Insets(20, 0, 0, 0));

		HBox outerLayout = new HBox(points, layout);
		outerLayout.setStyle("-fx-spacing: 2em");

		setContent(outerLayout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbLifestyle.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)-> {
			if (n!=null) {
				data.setResolved(n);
				updateCost();
			}
		});
		cbSINs.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)-> {
			if (n!=null) {
				data.setSIN(n.getUniqueId());
				updateCost();
			}
		});
		tfName.textProperty().addListener( (ov,o,n) -> {data.setName(n); updateCost();});
		taDesc.textProperty().addListener( (ov,o,n) -> {data.setDescription(n); updateCost();});
		nvMonths.setOnAction(ev -> refresh());
	}

	//-------------------------------------------------------------------
	private void refresh() {
		cbLifestyle.setValue(data.getModifyable());
		cbLifestyle.setDisable(true);

		if (data.getSIN()!=null) {
			cbSINs.setValue(control.getModel().getSIN(data.getSIN()));
			cbSINs.setDisable(true);
		}

		tfName.setText(data.getNameWithoutRating());
		taDesc.setText(data.getDescription());
		btnControl.setDisabled(CloseType.OK, false);


		lblNuyenFree.setText(control.getModel().getNuyen()+" \u00A5");
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	private void updateCost() {
		if (cbLifestyle.getValue()==null)
			lblBaseCost.setText("? \u00A5");
		else
			lblBaseCost.setText(cbLifestyle.getValue().getCost()+" \u00A5");
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragDroppedAvailable(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	// Get reference for ID
//        	if (enhanceID.startsWith("lifestyleoption:")) {
//        		String id = enhanceID.substring("lifestyleoption:".length());
//        		LifestyleOption master = ShadowrunCore.getLifestyleOption(id);
//         		if (master!=null) {
//         			LifestyleOptionValue added = lifeControl.selectOption(master);
//         			if (added!=null) {
//         				lvAvailable.getItems().remove(master);
//         				if (!lvSelected.getItems().contains(master))
//         					lvSelected.getItems().add(added);
//         				updateCost();
//         			}
//        		}
//        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("lifestyleoption:")) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/*
	 * Deselect
	 */
	private void dragDroppedSelected(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	// Get reference for ID
//        	if (enhanceID.startsWith("lifestyleoption:")) {
//        		String id = enhanceID.substring("lifestyleoption:".length());
//        		LifestyleOption master = ShadowrunCore.getLifestyleOption(id);
//         		if (master!=null) {
//         			lvSelected.getItems().remove(master);
//         			if (!lvAvailable.getItems().contains(master))
//         				lvAvailable.getItems().add(master);
//    				updateCost();
//         		}
//        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	/*
	 * Deselect
	 */
	private void dragOverSelected(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("lifestyleoption:")) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	public Object[] getData() {
		return new Object[]{tfName.getText(), cbLifestyle.getValue(), taDesc.getText()};
	}

	//-------------------------------------------------------------------
	public L getResult() {
		return data;
	}

	//-------------------------------------------------------------------
	public LifestyleQuality getSelectedLifeStyle() {
		return cbLifestyle.getValue();
	}

	//-------------------------------------------------------------------
	public String getSelectedName() {
		if (tfName.getText()!=null && !tfName.getText().isEmpty())
			return tfName.getText();
		return null;
	}

	//-------------------------------------------------------------------
	public String getSelectedDescription() {
		if (taDesc.getText()!=null && !taDesc.getText().isEmpty())
			return taDesc.getText();
		return null;
	}

	//-------------------------------------------------------------------
	public SIN getSelectedSIN() {
		return cbSINs.getValue();
	}

}
