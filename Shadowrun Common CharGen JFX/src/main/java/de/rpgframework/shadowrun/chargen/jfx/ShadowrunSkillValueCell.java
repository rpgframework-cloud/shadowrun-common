package de.rpgframework.shadowrun.chargen.jfx;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.jfx.NumericalValueField;
import de.rpgframework.jfx.NumericalValueTableCell;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.SkillType;
import javafx.beans.property.ObjectProperty;

/**
 * @author prelle
 *
 */
public class ShadowrunSkillValueCell<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>> extends NumericalValueTableCell<S, V> {

	//-------------------------------------------------------------------
	/**
	 * @param ctrl
	 */
	public ShadowrunSkillValueCell(ObjectProperty<NumericalValueController<S, V>> ctrl) {
		super(ctrl);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param data
	 */
	public ShadowrunSkillValueCell(NumericalValueController<S, V> data) {
		super(data);
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	public ShadowrunSkillValueCell(NumericalValueController<S,V> data, boolean useItem) {
		super(data,useItem);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Number item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			if (getTableRow()!=null && getTableRow().getItem()!=null) {
				V skillVal = getTableRow().getItem();
				S skill    = skillVal.getModifyable();
				if (skill.getType()==SkillType.KNOWLEDGE) {
					setGraphic(null);
				}
			}
		}
	}

}
