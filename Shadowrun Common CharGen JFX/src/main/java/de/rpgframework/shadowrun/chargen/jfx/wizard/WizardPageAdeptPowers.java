package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.AdeptPower;
import de.rpgframework.shadowrun.AdeptPowerValue;
import de.rpgframework.shadowrun.MagicOrResonanceType;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import de.rpgframework.shadowrun.chargen.jfx.listcell.AdeptPowerValueListCell;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class WizardPageAdeptPowers extends WizardPage implements ControllerListener{

	protected final static Logger logger = System.getLogger(WizardPageAdeptPowers.class.getPackageName()+".adept");

	protected final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageAdeptPowers.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterController<?,?,?,?> charGen;

	private Label lbPPCurrent, lbPPMax;

	private Button dec;
	private Button inc;
	protected Label lbValue;
	protected Label hdConvert;
	private TilePane bxButtons;
	private HBox bxLine;
	protected ComplexDataItemControllerNode<AdeptPower, AdeptPowerValue> selection;
	protected GenericDescriptionVBox bxDescription;
	protected OptionalNodePane layout;
	protected NumberUnitBackHeader backHeaderKarma;

	//-------------------------------------------------------------------
	public WizardPageAdeptPowers(Wizard wizard, IShadowrunCharacterController<?,?,?,?> charGen, Function<Requirement, String> req) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.adeptpowers.title"));
		initComponents(req);
		initLayout();
		initBackHeader();
		initInteractivity();

		charGen.addListener(this);
	}

	//-------------------------------------------------------------------
	protected void initComponents(Function<Requirement, String> req) {
		lbPPCurrent = new Label("?");
		lbPPMax     = new Label("?");

		selection = new ComplexDataItemControllerNode<>(charGen.getAdeptPowerController());

		selection.setAvailablePlaceholder(ResourceI18N.get(RES, "page.adeptpowers.placeholder.available"));
		selection.setSelectedPlaceholder(ResourceI18N.get(RES, "page.adeptpowers.placeholder.selected"));

		selection.setAvailableCellFactory(lv -> new ComplexDataItemListCell<AdeptPower>( () -> charGen.getAdeptPowerController(),req));
		selection.setSelectedCellFactory(lv -> new AdeptPowerValueListCell( () -> charGen.getAdeptPowerController()));
		selection.setShowHeadings(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		bxDescription = new GenericDescriptionVBox(null,null);

		dec  = new Button(null, new SymbolIcon("remove"));
		inc  = new Button(null, new SymbolIcon("add"));
		lbValue = new Label("0");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Information about PP bought by Karma
		hdConvert = new Label(ResourceI18N.get(RES, "page.adeptpowers.convert"));
		hdConvert.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		bxButtons = new TilePane(5, 0, dec, lbValue, inc);
		bxLine = new HBox(10, hdConvert, bxButtons);
		bxLine.setAlignment(Pos.CENTER);

		// Information about spent PP
		Label hdUnspent = new Label(ResourceI18N.get(RES, "page.adeptpowers.unspent"));
		hdUnspent.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		HBox selectedHeading = new HBox(10, hdUnspent, lbPPCurrent, new Label("/"), lbPPMax);
		selection.setSelectedListHead(selectedHeading);


		layout = new OptionalNodePane(new VBox(10, bxLine, selection), bxDescription);
		layout.setId("optional-adeptpowers");
		setContent(layout);

		bxLine.setManaged(charGen.getAdeptPowerController().canBuyPowerPoints());
		bxLine.setVisible(charGen.getAdeptPowerController().canBuyPowerPoints());
	}

	//-------------------------------------------------------------------
	protected void initBackHeader() {
		// Current Karma
		backHeaderKarma = new NumberUnitBackHeader(ResourceI18N.get(RES, "label.karma"));
		backHeaderKarma.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeaderKarma, new Insets(0,10,0,10));
		super.setBackHeader(backHeaderKarma);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.showHelpForProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "show help for "+n);
			bxDescription.setData(n);
			if (n!=null) {
				layout.setTitle(n.getName());
			} else {
				layout.setTitle(null);
			}
		});

		inc.setOnAction(ev -> charGen.getAdeptPowerController().increasePowerPoints());
		dec.setOnAction(ev -> charGen.getAdeptPowerController().decreasePowerPoints());
	}

	//-------------------------------------------------------------------
	protected void refresh() {
		MagicOrResonanceType morType = charGen.getModel().getMagicOrResonanceType();
		activeProperty().set( morType!=null && morType.usesPowers());
		selection.refresh();

		backHeaderKarma.setValue(charGen.getModel().getKarmaFree());

		lbPPCurrent.setText( String.valueOf(charGen.getAdeptPowerController().getUnsedPowerPoints()) );
		lbPPMax    .setText( String.valueOf(charGen.getAdeptPowerController().getMaxPowerPoints()) );

		dec.setDisable( !charGen.getAdeptPowerController().canDecreasePowerPoints());
		inc.setDisable( !charGen.getAdeptPowerController().canIncreasePowerPoints());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.DEBUG, "RCV " + type + " with " + Arrays.toString(param));
			charGen = (IShadowrunCharacterGenerator<?, ?, ?, ?>) param[0];
			selection.setController(charGen.getAdeptPowerController());
		}
		if (type==BasicControllerEvents.CHARACTER_CHANGED)
			refresh();

		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			refresh();
			bxLine.setManaged(charGen.getAdeptPowerController().canBuyPowerPoints());
			bxLine.setVisible(charGen.getAdeptPowerController().canBuyPowerPoints());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		selection.setShowHeadings(value!=WindowMode.MINIMAL);
	}

}
