package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.jfx.section.ComplexDataItemListSection;
import de.rpgframework.shadowrun.MetamagicOrEcho;
import de.rpgframework.shadowrun.MetamagicOrEcho.Type;
import de.rpgframework.shadowrun.MetamagicOrEchoValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.jfx.pane.MetamagicOrEchoSelector;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class MetamagicOrEchoSection extends ComplexDataItemListSection<MetamagicOrEcho,MetamagicOrEchoValue> implements IShadowrunCharacterControllerProvider<IShadowrunCharacterController<?,?,?,?>> {

	private final static Logger logger = System.getLogger(MetamagicOrEchoSection.class.getPackageName()+".meta");

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(MetamagicOrEchoSection.class.getPackageName()+".Section");

	private IShadowrunCharacterController control;
	private ShadowrunCharacter<?,?,?,?> model;
	private Function<Requirement, String> requirementResolver;
	private Function<Modification,String> modResolver;
	private Type type;
	private ObjectProperty<BiFunction<MetamagicOrEcho, List<Choice>, Decision[]>> optionCallbackProperty;

	private Label lbGrade;

	//-------------------------------------------------------------------
	public MetamagicOrEchoSection(String title, Function<Requirement, String> requirementResolver, Function<Modification,String> modResolver, Type type) {
		super(title);
		this.requirementResolver = requirementResolver;
		this.modResolver = modResolver;
		this.type = type;
		optionCallbackProperty = new SimpleObjectProperty<>();

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list.setStyle("-fx-min-height: 10em; -fx-pref-height: 30em; -fx-pref-width: 25em");
		list.setCellFactory( lv -> new ComplexDataItemValueListCell<MetamagicOrEcho,MetamagicOrEchoValue>( () -> control.getMetamagicOrEchoController()));

		lbGrade = new Label("?");
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		String key = null;
		switch (type) {
		case METAMAGIC:
		case METAMAGIC_ADEPT:
			key = "metaechosection.typename.initiation";
			break;
		case ECHO:
			key = "metaechosection.typename.submersion";
			break;
		case TRANSHUMANISM:
			key = "metaechosection.typename.transhumanism";
			break;
		}

		Label lbGradeName = new Label(ResourceI18N.get(RES, key));
		HBox headerNode = new HBox(10, lbGradeName, lbGrade);
		headerNode.setAlignment(Pos.CENTER_LEFT);
		setHeaderNode(headerNode);
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				btnDel.setDisable(true);
			} else {
				btnDel.setDisable( !control.getMetamagicOrEchoController().canBeDeselected(n).get());
			}
		});
		btnAdd.setOnAction(ev -> onAdd());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	@Override
	public IShadowrunCharacterController<?,?,?,?> getCharacterController() {
		return control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	public void refresh() {
		logger.log(Level.TRACE, "refresh");

		if (model!=null && control.getMetamagicOrEchoController()!=null) {
			lbGrade.setText( String.valueOf( control.getMetamagicOrEchoController().getGrade()));
			setData(model.getMetamagicOrEchoes());
		} else
			setData(new ArrayList<>());
	}

	//-------------------------------------------------------------------
	protected void onAdd() {
		logger.log(Level.DEBUG, "opening metamagic selection dialog");

		MetamagicOrEchoSelector selector = new MetamagicOrEchoSelector(
				control.getMetamagicOrEchoController(),
				requirementResolver, modResolver,
				Type.METAMAGIC);
//		selector.setPlaceholder(new Label(Resource.get(RES,"metaechosection.selector.placeholder")));
//		SelectorWithHelp<MetamagicOrEcho> pane = new SelectorWithHelp<>(selector);
		String key = null;
		switch (type) {
		case METAMAGIC:
		case METAMAGIC_ADEPT:
			key = "metaechosection.selector.initiation.title";
			break;
		case ECHO:
			key = "metaechosection.selector.submersion.title";
			break;
		case TRANSHUMANISM:
			key = "metaechosection.selector.transhumanism.title";
			break;
		}

		ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES,key), selector, CloseType.OK, CloseType.CANCEL);

		CloseType close = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		logger.log(Level.DEBUG,"Closed with "+close);
		if (close==CloseType.OK) {
			MetamagicOrEcho data =  selector.getSelected();
			logger.log(Level.DEBUG, "Selected metaecho: "+data);
			logger.log(Level.INFO, "  quality needs choice: "+data.getChoices().size()+" choices for "+data);
			if (data.getChoices().size()>0) {
				Decision[] dec = getOptionCallback().apply(data, data.getChoices());
				control.getMetamagicOrEchoController().select(data, dec);
			} else {
				control.getMetamagicOrEchoController().select(data);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(MetamagicOrEchoValue item) {
		logger.log(Level.DEBUG, "onDelete "+item);
		if (control.getMetamagicOrEchoController().deselect(item)) {
			list.getItems().remove(item);
		}
	}

//	//-------------------------------------------------------------------
//	@SuppressWarnings("unchecked")
//	private void onSettings() {
//		CharacterLeveller ctrl = (CharacterLeveller) this.control;
//
//		VBox content = new VBox(20);
//		for (ConfigOption<?> opt : ctrl.getSpellController().getConfigOptions()) {
//			CheckBox cb = new CheckBox(opt.getName());
//			cb.setSelected((Boolean)opt.getValue());
//			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
//			content.getChildren().add(cb);
//		}
//
//		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
//	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController<?,?,?,?> ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (ShadowrunCharacter) ctrl.getModel();
		refresh();
	}

}
