package de.rpgframework.shadowrun.chargen.jfx.fxml;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import de.rpgframework.jfx.fxml.ExtendedFilteredListPageController;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.chargen.jfx.listcell.SpellListCell;
import de.rpgframework.shadowrun.chargen.jfx.pane.SpellDescriptionPane;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.util.StringConverter;

public class SpellPageController extends ExtendedFilteredListPageController<ASpell, SpellListCell, SpellDescriptionPane> {
	
	private static Logger logger = System.getLogger(SpellPageController.class.getPackageName());

	@FXML
	private ChoiceBox<ASpell.Category> cbType;
	
	private transient Supplier<List<ASpell>> dataSupplier;

	//-------------------------------------------------------------------
	public SpellPageController(Supplier<List<ASpell>> dataSupplier) {
		this.dataSupplier = dataSupplier;
	}

	//-------------------------------------------------------------------
	@FXML
	public void initialize() {
		super.initialize();
		logger.log(Level.INFO, "initialize()");
		
		/*
		 * Spell Types
		 */
		cbType = new ChoiceBox<ASpell.Category>();
		cbType.getItems().add(null);
		cbType.getItems().addAll(ASpell.Category.values());
		Collections.sort(cbType.getItems(), new Comparator<ASpell.Category>() {
			public int compare(ASpell.Category o1, ASpell.Category o2) {
				if (o1==null && o2!=null) return -1;
				if (o2==null && o1!=null) return +1;
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refreshList());
		cbType.setConverter(new StringConverter<ASpell.Category>() {
			public String toString(ASpell.Category val) {
				if (val==null) return "alle Typen";
				return val.getName();
			}
			public ASpell.Category fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbType);

		
		// React to double clicks on creatures
		lvResult.setCellFactory(lv -> {
			SpellListCell cell = new SpellListCell();
			cell.setOnMouseClicked(ev -> {
				if (ev.getClickCount()==2) {
					showAction(cell.getItem());
				}
			});
			return cell;
			});
		
		refreshList();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.fxml.FilteredListPageController#filterItems(java.util.List)
	 */
	@Override
	protected List<ASpell> filterItems(List<ASpell> list) {
		logger.log(Level.INFO, "filterItems()");
		// Match spell type
		if (cbType.getValue()!=null) {
			list = list.stream().filter(crea -> crea.getCategory()==cbType.getValue()).collect(Collectors.toList());
		}
		return list;
	}
	
//	//-------------------------------------------------------------------
//	public void setComponent(Page page) {
//		super.setComponent(page, 
//				() -> ShadowrunCore.getItemList(Spell.class),
//				SpellListCell.class, SpellDescriptionPane.class);
//	}

}
