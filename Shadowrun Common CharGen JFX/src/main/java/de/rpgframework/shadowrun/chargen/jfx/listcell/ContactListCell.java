package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.shadowrun.Contact;
import de.rpgframework.shadowrun.chargen.charctrl.IContactController;
import de.rpgframework.shadowrun.chargen.jfx.SkinProperties;
import de.rpgframework.shadowrun.chargen.jfx.dialog.EditSINDialog;
import de.rpgframework.shadowrun.chargen.jfx.pane.ContactDetailPane;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;

/**
 * @author prelle
 *
 */
public class ContactListCell extends ListCell<Contact> {

	private final static ResourceBundle RES = ResourceBundle.getBundle(SkinProperties.class.getPackageName()+".ListCells");

	private final static Logger logger = System.getLogger(ContactListCell.class.getPackageName());

	private final static String NORMAL_STYLE = "connection-cell";

	private IContactController charGen;

	private HBox layout;
	private Label lblName;
	private Label lblType;
	private Button btnEdit;
	private Label  lblValInfl;
	private Label  lblValLoyl;

	//-------------------------------------------------------------------
	/**
	 */
	public ContactListCell(IContactController ctrl) {
		this.charGen = ctrl;

		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_CENTER);

		// Content
		lblName  = new Label();
		lblType  = new Label();
		lblType.setWrapText(true);
		btnEdit = new Button("\uE1C2");
		btnEdit.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
//		btnDecInfl  = new Button("\uE0C9");
		lblValInfl  = new Label("?");
//		btnIncInfl  = new Button("\uE0C8");

		lblValLoyl  = new Label("?");

		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		getStyleClass().add(NORMAL_STYLE);

//		btnDecInfl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
//		btnIncInfl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
//		btnDecLoyl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
//		btnIncLoyl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		lblName.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lblValInfl.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lblValLoyl.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
//		lblValInfl.getStyleClass().add("text-subheader");
//		lblValLoyl.getStyleClass().add("text-subheader");

		btnEdit.setStyle("-fx-background-color: transparent");

		//		setStyle("-fx-pref-width: 24em");
//		layout.getStyleClass().add("content");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label heaInfl = new Label(ResourceI18N.get(RES,"listcell.contact.rating"));
		Label heaLoyl = new Label(ResourceI18N.get(RES,"listcell.contact.loyalty"));
		heaInfl.setStyle("-fx-min-width: 2em");
		heaLoyl.setStyle("-fx-min-width: 2em");

		GridPane col1 = new GridPane();
		col1.setStyle("-fx-hgap: 0.4em; -fx-vgap: 0.4em;");
		col1.add(lblName, 0, 0);
		col1.add(lblType, 0, 1);
		col1.setMaxWidth(Double.MAX_VALUE);

		GridPane col2 = new GridPane();
		col2.setStyle("-fx-hgap: 0.2em; -fx-vgap: 0.2em;");
		col2.add(heaInfl   , 0, 0);
		col2.add(lblValInfl, 1, 0);
		col2.add(heaLoyl   , 0, 1);
		col2.add(lblValLoyl, 1, 1);
		btnEdit.setStyle("-fx-min-width: 3em");

		layout = new HBox(10);
		layout.setAlignment(Pos.CENTER_LEFT);
		HBox.setHgrow(col1, Priority.ALWAYS);
		layout.getChildren().addAll(col1, col2, btnEdit);

		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.setOnAction(event -> {
			logger.log(Level.DEBUG, "edit contact {0} ",getItem());
			ContactDetailPane pane = new ContactDetailPane(charGen, () -> this.changed());
			pane.setData(getItem());
			ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES, "listcell.contact.editdialog.title"),pane, CloseType.OK);
			FlexibleApplication.getInstance().showAlertAndCall(dialog, null);
			getListView().refresh();
		});
	}

	//-------------------------------------------------------------------
	private void changed() {
		Contact item = this.getItem();
		lblName.setText(item.getName());
		String typeText = item.getTypeName();
		if (item.getType()!=null) {
			typeText+=" ("+item.getType().getName(Locale.getDefault())+")";
		}
		lblType.setText(typeText);
		btnEdit.setText("\uE1C2");
		btnEdit.setTooltip(new Tooltip(ResourceI18N.get(RES,"listcell.contact.tooltip.edit")));

		lblValInfl.setText(String.valueOf(item.getRating()));

		lblValLoyl.setText(String.valueOf(item.getLoyalty()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Contact item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(item.getName());
			String typeText = item.getTypeName();
			if (item.getType()!=null) {
				typeText+=" ("+item.getType().getName(Locale.getDefault())+")";
			}
			lblType.setText(typeText);
			btnEdit.setText("\uE1C2");
			btnEdit.setTooltip(new Tooltip(ResourceI18N.get(RES,"listcell.contact.tooltip.edit")));

			lblValInfl.setText(String.valueOf(item.getRating()));

			lblValLoyl.setText(String.valueOf(item.getLoyalty()));

			setGraphic(layout);
		}
	}

}
