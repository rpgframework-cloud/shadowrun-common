package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ExtendedListView;
import org.prelle.javafx.layout.ResponsiveBox;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.items.AAvailableSlot;
import de.rpgframework.genericrpg.items.Hook;
import de.rpgframework.shadowrun.items.AShadowrunItemEnhancement;
import javafx.scene.control.Control;

/**
 * @author prelle
 *
 */
public class SimpleCarriedItemPane<H extends Hook, A extends AAvailableSlot<H,?>> extends ResponsiveBox {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SimpleCarriedItemPane.class.getPackageName()+".DescriptionPanes");
	
	private ExtendedListView<A> lvAccessorySlots;
	private ExtendedListView<AShadowrunItemEnhancement> lvModifications;

	//-------------------------------------------------------------------
	/**
	 */
	public SimpleCarriedItemPane() {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lvAccessorySlots = new ExtendedListView<>(ResourceI18N.get(RES, "carried.list.accessories"));
		lvModifications  = new ExtendedListView<>(ResourceI18N.get(RES, "carried.list.modifications"));
		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		getChildren().addAll(lvAccessorySlots, lvModifications);
	}

}
