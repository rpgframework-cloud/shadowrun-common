package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.Lifestyle;
import de.rpgframework.shadowrun.LifestyleQuality;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.jfx.listcell.ALifestyleListCell;
import javafx.geometry.Insets;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class AWizardPageLifestyles extends WizardPage implements ControllerListener {

	private final static Logger logger = System.getLogger(WizardPageAdeptPowers.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageAdeptPowers.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterController<?,?,?,?> charGen;
	protected ComplexDataItemControllerNode<LifestyleQuality, Lifestyle> selection;
	protected GenericDescriptionVBox bxDescription;
	protected OptionalNodePane layout;
	private NumberUnitBackHeader backHeader;

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public AWizardPageLifestyles(Wizard wizard, IShadowrunCharacterController<?,?,?,?> charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.lifestyles.title"));
		initComponents();
		initLayout();
		initInteractivity();

		charGen.addListener(this);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected void initComponents() {
		selection = new ComplexDataItemControllerNode<>(charGen.getLifestyleController());

		selection.setAvailablePlaceholder(ResourceI18N.get(RES, "page.lifestyles.placeholder.available"));
		selection.setSelectedPlaceholder(ResourceI18N.get(RES, "page.lifestyles.placeholder.selected"));

		selection.setAvailableCellFactory(lv -> new ComplexDataItemListCell<LifestyleQuality>( () -> charGen.getLifestyleController()));
		selection.setSelectedCellFactory(lv -> new ALifestyleListCell( () -> charGen));
		selection.setShowHeadings(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		bxDescription = new GenericDescriptionVBox(null,null);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Current Karma
		backHeader = new NumberUnitBackHeader("Karma");
		backHeader.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeader, new Insets(0,10,0,10));
		if (ResponsiveControlManager.getCurrentMode()==WindowMode.EXPANDED) {
			super.setBackHeader(null);
		} else {
			super.setBackHeader(backHeader);
		}

		layout = new OptionalNodePane(selection, bxDescription);
		layout.setId("optional-adeptpowers");
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.showHelpForProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "show help for "+n);
			bxDescription.setData(n);
			if (n!=null) {
				layout.setTitle(n.getName());
			} else {
				layout.setTitle(null);
			}
		});
	}

	//-------------------------------------------------------------------
	protected void refresh() {
		selection.refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			selection.setController(charGen.getLifestyleController());
			refresh();
		}
		if (type==BasicControllerEvents.CHARACTER_CHANGED)
			refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		selection.setShowHeadings(value!=WindowMode.MINIMAL);
	}

}
