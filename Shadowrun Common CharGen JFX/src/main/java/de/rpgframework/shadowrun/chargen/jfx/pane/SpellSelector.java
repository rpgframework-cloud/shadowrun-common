package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.function.Function;

import org.prelle.javafx.ResponsiveControl;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.Selector;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.SpellValue;
import de.rpgframework.shadowrun.chargen.charctrl.ISpellController;
import de.rpgframework.shadowrun.chargen.jfx.FilterSpells;
import de.rpgframework.shadowrun.chargen.jfx.listcell.SpellListCell;
import javafx.scene.control.ListCell;
import javafx.scene.layout.Pane;

/**
 * @author prelle
 *
 */
public class SpellSelector<T extends ASpell> extends Selector<T, SpellValue<T>> implements ResponsiveControl {

	//-------------------------------------------------------------------
	public SpellSelector(ISpellController<T> ctrl, Function<Requirement,String> resolver, Function<Modification,String> modResolver) {
		super(ctrl, resolver, modResolver, (AFilterInjector<T>) new FilterSpells());
		listPossible.setCellFactory( lv -> (ListCell<T>)new SpellListCell());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.Selector#getDescriptionNode(de.rpgframework.genericrpg.data.ComplexDataItem)
	 */
	@Override
	protected Pane getDescriptionNode(T selected) {
		SpellDescriptionPane descr = new SpellDescriptionPane();
		if (selected!=null)
			descr.setData(selected);
		return descr;
	}

}
