package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.WindowMode;

import de.rpgframework.ResourceI18N;
import de.rpgframework.shadowrun.Priority;
import de.rpgframework.shadowrun.PriorityType;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.gen.IPrioritySettings;
import de.rpgframework.shadowrun.chargen.gen.PriorityTableController;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class PriorityTable<C extends ShadowrunCharacter<?,?,?,?>, P extends IPrioritySettings>
	extends GridPane
	implements ResponsiveControl {

	protected final static Logger logger = System.getLogger(PriorityTable.class.getPackageName());

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle
			.getBundle(PriorityTable.class.getName());

	private ToggleButton[] btnMeta;
	private ToggleButton[] btnAttrib;
	private ToggleButton[] btnSkill;
	private ToggleButton[] btnMagic;
	private ToggleButton[] btnResrc;

	protected Label lblMeta;
	protected ToggleGroup tgMeta;
	private ToggleGroup tgAttrib;
	private ToggleGroup tgSkill;
	private ToggleGroup tgMagic;
	private ToggleGroup tgResrc;

	private boolean refreshing;
	private PriorityTableController<C, P> charGen;

	// -------------------------------------------------------------------
	public PriorityTable() {
		initCompontents();
		initLayout();
		initInteractivity();
	}

	// -------------------------------------------------------------------
	private void initCompontents() {
		getStyleClass().add("priority-table");

		/*
		 * Metatypes
		 */
		btnMeta = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			btnMeta[prio.ordinal()] = new ToggleButton(" ");
			btnMeta[prio.ordinal()].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnMeta[prio.ordinal()].setUserData(prio);
			btnMeta[prio.ordinal()].getStyleClass().addAll("priobutton", "priobutton-metatype");
//			btnMeta[prio.ordinal()].setWrapText(true);
		}

		tgMeta = new ToggleGroup();
		tgMeta.getToggles().addAll(btnMeta);
		tgMeta.setUserData(PriorityType.METATYPE);

		/*
		 * Attributes
		 */
		btnAttrib = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			btnAttrib[prio.ordinal()] = new ToggleButton(" ");
			btnAttrib[prio.ordinal()].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnAttrib[prio.ordinal()].setUserData(prio);
			btnAttrib[prio.ordinal()].getStyleClass().addAll("priobutton", "attribute");
		}
		tgAttrib = new ToggleGroup();
		tgAttrib.getToggles().addAll(btnAttrib);
		tgAttrib.setUserData(PriorityType.ATTRIBUTE);

		/*
		 * Magic or resonance
		 */
		btnMagic = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			int i = prio.ordinal();
			btnMagic[i] = new ToggleButton(" ");
			btnMagic[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnMagic[i].setUserData(prio);
			btnMagic[i].getStyleClass().addAll("priobutton", "magic");
		}
		tgMagic = new ToggleGroup();
		tgMagic.getToggles().addAll(btnMagic);
		tgMagic.setUserData(PriorityType.MAGIC);

		/*
		 * Skills
		 */
		btnSkill = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			int i = prio.ordinal();
			btnSkill[i] = new ToggleButton(" ");
			btnSkill[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnSkill[i].setUserData(prio);
			btnSkill[i].getStyleClass().addAll("priobutton", "skill");
		}
		tgSkill = new ToggleGroup();
		tgSkill.getToggles().addAll(btnSkill);
		tgSkill.setUserData(PriorityType.SKILLS);

		/*
		 * Resources
		 */
		btnResrc = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			int i = prio.ordinal();
			btnResrc[i] = new ToggleButton(" ");
			btnResrc[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnResrc[i].setUserData(prio);
			btnResrc[i].getStyleClass().addAll("priobutton", "resources");
		}
		tgResrc = new ToggleGroup();
		tgResrc.getToggles().addAll(btnResrc);
		tgResrc.setUserData(PriorityType.RESOURCES);
	}

	// -------------------------------------------------------------------
	/* Override by SR5/SR6 */
	public Node getMetatypeNode(Priority prio) {
		return new Label(prio.name());
	}

	// -------------------------------------------------------------------
	/* Override by SR5/SR6 */
	public Node getAttributeNode(Priority prio) {
		return new Label(prio.name());
	}

	// -------------------------------------------------------------------
	/* Override by SR5/SR6 */
	public Node getMagicOrResonanceNode(Priority prio) {
		return new Label(prio.name());
	}

	// -------------------------------------------------------------------
	/* Override by SR5/SR6 */
	public Node getSkillsNode(Priority prio) {
		return new Label(prio.name());
	}

	// -------------------------------------------------------------------
	/* Override by SR5/SR6 */
	public Node getResourcesNode(Priority prio) {
		return new Label(prio.name());
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		setGridLinesVisible(true);

		/*
		 * Headings
		 */
		// Heading row
		Label lblPrio = new Label(ResourceI18N.get(RES, "priotable.label.prio"));
		lblMeta = new Label(ResourceI18N.get(RES, "priotable.label.meta"));
		Label lblAttrib = new Label(ResourceI18N.get(RES, "priotable.label.attrib"));
		Label lblMagic = new Label(ResourceI18N.get(RES, "priotable.label.magic"));
		Label lblSkill = new Label(ResourceI18N.get(RES, "priotable.label.skill"));
		Label lblResrc = new Label(ResourceI18N.get(RES, "priotable.label.resrc"));
		lblPrio.getStyleClass().add("table-head");
		lblMeta.getStyleClass().add("table-head");
		lblAttrib.getStyleClass().add("table-head");
		lblMagic.getStyleClass().add("table-head");
		lblSkill.getStyleClass().add("table-head");
		lblResrc.getStyleClass().add("table-head");
		lblPrio.setMaxWidth(Double.MAX_VALUE);
		lblMeta.setMaxWidth(Double.MAX_VALUE);
		lblAttrib.setMaxWidth(Double.MAX_VALUE);
		lblMagic.setMaxWidth(Double.MAX_VALUE);
		lblSkill.setMaxWidth(Double.MAX_VALUE);
		lblResrc.setMaxWidth(Double.MAX_VALUE);
		GridPane.setHgrow(lblPrio, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblMeta, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblAttrib, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblMagic, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblSkill, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblResrc, javafx.scene.layout.Priority.ALWAYS);
		add(lblPrio, 0, 0);
		add(lblMeta, 1, 0);
		add(lblAttrib, 2, 0);
		add(lblSkill, 3, 0);
		add(lblMagic, 4, 0);
		add(lblResrc, 5, 0);

		// Heading column
		for (Priority prio : Priority.values()) {
			Label label = new Label(prio.name());
			label.getStyleClass().addAll("text-subheader", "row-heading");
			add(label, 0, prio.ordinal() + 1);
		}
//		for (int i=0; i<5; i++) {
//			Label label = new Label(String.valueOf((char)('A'+i)));
//			label.getStyleClass().addAll("text-subheader","row-heading");
//			add(label, 0,i+1);
//		}

		/*
		 * Content
		 */
		for (int i = 0; i < 5; i++)
			add(btnMeta[i], 1, i + 1);
		for (int i = 0; i < 5; i++)
			add(btnAttrib[i], 2, i + 1);
		for (int i = 0; i < 5; i++)
			add(btnSkill[i], 3, i + 1);
		for (int i = 0; i < 5; i++) {
			add(btnMagic[i], 4, i + 1);
			GridPane.setHalignment(btnMagic[i], HPos.LEFT);
			GridPane.setFillWidth(btnMagic[i], true);
		}
		for (int i = 0; i < 5; i++)
			add(btnResrc[i], 5, i + 1);

		// Column layout
		ColumnConstraints center = new ColumnConstraints();
		center.setPercentWidth(7);
		center.setHalignment(HPos.CENTER);
		getColumnConstraints().add(center);
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		tgMeta.selectedToggleProperty().addListener((ov, o, n) -> {
			if (charGen==null) {logger.log(Level.ERROR, "No controller assigned"); return;	}
			if (refreshing) return;
			if (n != null) {
				PriorityType what = (PriorityType) tgMeta.getUserData();
				charGen.setPriority(what, (Priority) ((ToggleButton) n).getUserData());
			}
			update();
		});
		tgAttrib.selectedToggleProperty().addListener((ov, o, n) -> {
			if (charGen==null) {logger.log(Level.ERROR, "No controller assigned"); return;	}
			if (refreshing) return;
			if (n != null)
				charGen.setPriority(PriorityType.ATTRIBUTE, (Priority) ((ToggleButton) n).getUserData());
			update();
		});
		tgMagic.selectedToggleProperty().addListener((ov, o, n) -> {
			if (charGen==null) {logger.log(Level.ERROR, "No controller assigned"); return;	}
			if (refreshing) return;
			if (n != null)
				charGen.setPriority(PriorityType.MAGIC, (Priority) ((ToggleButton) n).getUserData());
			update();
		});
		tgSkill.selectedToggleProperty().addListener((ov, o, n) -> {
			if (charGen==null) {logger.log(Level.ERROR, "No controller assigned"); return;	}
			if (refreshing) return;
			if (n != null)
				charGen.setPriority(PriorityType.SKILLS, (Priority) ((ToggleButton) n).getUserData());
			update();
		});
		tgResrc.selectedToggleProperty().addListener((ov, o, n) -> {
			if (charGen==null) {logger.log(Level.ERROR, "No controller assigned"); return;	}
			if (refreshing) return;
			if (n != null)
				charGen.setPriority(PriorityType.RESOURCES, (Priority) ((ToggleButton) n).getUserData());
			update();
		});
	}

	// -------------------------------------------------------------------
	public void update() {
		if (charGen == null)
			return;
		refreshing = true;
		if (charGen.getPriority(PriorityType.METATYPE) != null)
			tgMeta.selectToggle(btnMeta[charGen.getPriority(PriorityType.METATYPE).ordinal()]);
		if (charGen.getPriority(PriorityType.KARMA) != null)
			tgMeta.selectToggle(btnMeta[charGen.getPriority(PriorityType.KARMA).ordinal()]);
		if (charGen.getPriority(PriorityType.ATTRIBUTE) != null)
			tgAttrib.selectToggle(btnAttrib[charGen.getPriority(PriorityType.ATTRIBUTE).ordinal()]);
		if (charGen.getPriority(PriorityType.MAGIC) != null)
			tgMagic.selectToggle(btnMagic[charGen.getPriority(PriorityType.MAGIC).ordinal()]);
		if (charGen.getPriority(PriorityType.SKILLS) != null)
			tgSkill.selectToggle(btnSkill[charGen.getPriority(PriorityType.SKILLS).ordinal()]);
		if (charGen.getPriority(PriorityType.RESOURCES) != null)
			tgResrc.selectToggle(btnResrc[charGen.getPriority(PriorityType.RESOURCES).ordinal()]);

		refreshing=false;
	}

	// -------------------------------------------------------------------
	/**
	 * @param charGen
	 */
	public void setController(PriorityTableController<C, P> charGen) {
		this.charGen = charGen;
		logger.log(Level.WARNING, "setController--------------------"+this);
		update();
	}

	//-------------------------------------------------------------------
	protected void refreshMinimal() {
		for (ToggleButton btn : btnMeta) {
			Tooltip tip = new Tooltip();
			tip.setGraphic(getMetatypeNode( (Priority)btn.getUserData()));
			btn.setTooltip(tip);
			btn.setGraphic(null);
		}
		for (ToggleButton btn : btnAttrib) {
			Tooltip tip = new Tooltip();
			tip.setGraphic(getAttributeNode( (Priority)btn.getUserData()));
			btn.setTooltip(tip);
			btn.setGraphic(null);
		}
		for (ToggleButton btn : btnMagic) {
			Tooltip tip = new Tooltip();
			tip.setGraphic(getMagicOrResonanceNode( (Priority)btn.getUserData()));
			btn.setTooltip(tip);
			btn.setGraphic(null);
		}
		for (ToggleButton btn : btnSkill) {
			Tooltip tip = new Tooltip();
			tip.setGraphic(getSkillsNode( (Priority)btn.getUserData()));
			btn.setTooltip(tip);
			btn.setGraphic(null);
		}
		for (ToggleButton btn : btnResrc) {
			Tooltip tip = new Tooltip();
			tip.setGraphic(getResourcesNode( (Priority)btn.getUserData()));
			btn.setTooltip(tip);
			btn.setGraphic(null);
		}
	}

	//-------------------------------------------------------------------
	private void refreshCompact() {
		for (ToggleButton btn : btnMeta) {
			btn.setTooltip(null);
			btn.setGraphic(getMetatypeNode( (Priority)btn.getUserData()));
		}
		for (ToggleButton btn : btnAttrib) {
			btn.setTooltip(null);
			btn.setGraphic(getAttributeNode( (Priority)btn.getUserData()));
		}
		for (ToggleButton btn : btnMagic) {
			btn.setTooltip(null);
			btn.setGraphic(getMagicOrResonanceNode( (Priority)btn.getUserData()));
		}
		for (ToggleButton btn : btnSkill) {
			btn.setTooltip(null);
			btn.setGraphic(getSkillsNode( (Priority)btn.getUserData()));
		}
		for (ToggleButton btn : btnResrc) {
			btn.setTooltip(null);
			btn.setGraphic(getResourcesNode( (Priority)btn.getUserData()));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		logger.log(Level.ERROR, "setResponsive: "+value);
		switch (value) {
		case MINIMAL:
			refreshMinimal();
			break;
		default:
			refreshCompact();
		}

	}
}
