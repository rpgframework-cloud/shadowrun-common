package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.MagicOrResonanceType;
import de.rpgframework.shadowrun.SpellValue;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.gen.ISpellGenerator;
import de.rpgframework.shadowrun.chargen.jfx.pane.SpellDescriptionPane;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class WizardPageSpells<D extends ASpell> extends WizardPage implements ControllerListener{

	private final static Logger logger = System.getLogger(WizardPageSpells.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageSpells.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterController charGen;

	private Label lbSpellsCurrent, lbSpellsMax;

	protected ComplexDataItemControllerNode<D, SpellValue<D>> selection;
	protected SpellDescriptionPane bxDescription;
	protected OptionalNodePane layout;
	protected NumberUnitBackHeader backHeaderKarma;

	//-------------------------------------------------------------------
	public WizardPageSpells(Wizard wizard, IShadowrunCharacterController charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.spells.title"));
		initComponents();
		initBackHeader();
		initLayout();
		initInteractivity();

		charGen.addListener(this);
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		lbSpellsCurrent = new Label("?");
		lbSpellsMax     = new Label("?");

		selection = new ComplexDataItemControllerNode<>(charGen.getSpellController());

		selection.setAvailablePlaceholder(ResourceI18N.get(RES, "page.spells.placeholder.available"));
		selection.setSelectedPlaceholder(ResourceI18N.get(RES, "page.spells.placeholder.selected"));

		selection.setAvailableCellFactory(lv -> new ComplexDataItemListCell<D>( () -> charGen.getSpellController()));
		selection.setSelectedCellFactory(lv -> new ComplexDataItemValueListCell( () -> charGen.getSpellController()));
		selection.setShowHeadings(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		bxDescription = new SpellDescriptionPane();
	}

	//-------------------------------------------------------------------
	protected void initBackHeader() {
		// Current Karma
		backHeaderKarma = new NumberUnitBackHeader(ResourceI18N.get(RES, "label.karma"));
		backHeaderKarma.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeaderKarma, new Insets(0,10,0,10));
		super.setBackHeader(backHeaderKarma);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Information about spent PP
		Label hdUnspent = new Label(ResourceI18N.get(RES, "page.spells.unspent"));
		hdUnspent.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		HBox selectedHeading = new HBox(10, hdUnspent, lbSpellsCurrent, new Label("/"), lbSpellsMax);
		selection.setSelectedListHead(selectedHeading);


		layout = new OptionalNodePane(selection, bxDescription);
		layout.setId("optional-spells");
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.showHelpForProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "show help for "+n);
			bxDescription.setData(n);
			if (n!=null) {
				layout.setTitle(n.getName());
			} else {
				layout.setTitle(null);
			}
		});
	}

	//-------------------------------------------------------------------
	protected void refresh() {
		MagicOrResonanceType morType = charGen.getModel().getMagicOrResonanceType();
		activeProperty().set( morType!=null && morType.usesSpells());

		backHeaderKarma.setValue(charGen.getModel().getKarmaFree());
		selection.refresh();

		ISpellGenerator<?> spells = ((ISpellGenerator<?>)charGen.getSpellController());
		int sum = spells.getMaxFree() - spells.getFreeSpells();
		lbSpellsCurrent.setText( String.valueOf(sum) );
		lbSpellsMax.setText( String.valueOf(spells.getMaxFree()) );

		backHeaderKarma.setValue( charGen.getModel().getKarmaFree());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.CHARACTER_CHANGED)
			refresh();

		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			selection.setController(charGen.getSpellController());
			refresh();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		selection.setShowHeadings(value!=WindowMode.MINIMAL);
	}

}
