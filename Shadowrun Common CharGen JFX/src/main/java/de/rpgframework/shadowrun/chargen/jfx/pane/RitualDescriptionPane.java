package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.jfx.ADescriptionPane;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import de.rpgframework.jfx.attach.PDFViewerServiceFactory;
import de.rpgframework.shadowrun.Ritual;
import de.rpgframework.shadowrun.RitualFeatureReference;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;

/**
 * @author stefa
 *
 */
public class RitualDescriptionPane extends ADescriptionPane<Ritual> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(RitualDescriptionPane.class.getPackageName()+".DescriptionPanes");

	private Label descTitle;
	private Label descSources;
	private Label lbFeatures;
	private Label lbDrain;
	private GridPane grid;
	private TextFlow lbDescr;

	//-------------------------------------------------------------------
	public RitualDescriptionPane() {
		getStyleClass().add("description-pane");

		descTitle = new Label();
		descTitle.setWrapText(true);
		descTitle.getStyleClass().add(JavaFXConstants.STYLE_HEADING3);
		descSources = new Label();

		// Features
		lbFeatures  = new Label();
		lbFeatures.setWrapText(true);
		lbFeatures.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);

		// Spell Table
		Label hdDrain = new Label(ResourceI18N.get(UI,"ritual.threshold"));
		lbDrain = new Label();
		hdDrain.setMaxWidth(Double.MAX_VALUE);

		hdDrain.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD);

		lbDrain.getStyleClass().add(JavaFXConstants.STYLE_TABLE_DATA);

		grid = new GridPane();
		grid.add(hdDrain, 3, 0);
		grid.add(lbDrain, 3, 1);
		for (int i = 0; i < 4; i++) {
			ColumnConstraints col1 = new ColumnConstraints();
			col1.setPercentWidth(25);
			col1.setHalignment(HPos.CENTER);
			col1.setFillWidth(true);
			grid.getColumnConstraints().addAll(col1);
		}
		grid.setStyle("-fx-background-color: #e9e9e2;");
		grid.setVisible(false);
//		grid.setMaxWidth(Double.MAX_VALUE);

		// Effect
		lbDescr = new TextFlow();


		setStyle("-fx-pref-width: 20em");
		setStyle("-fx-max-width: 30em");
		getChildren().addAll(
				descTitle, descSources,
				lbFeatures,
				grid,
				lbDescr);
		VBox.setMargin(lbDescr, new Insets(20,0,0,0));
		VBox.setMargin(   grid, new Insets(20,0,0,0));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.ADescriptionPane#setData(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public void setData(Ritual spell) {
		if (spell==null) {
			grid.setVisible(false);
			descTitle.setText(null);
			descSources.setText(null);
			lbDescr.getChildren().clear();
			return;
		}
		grid.setVisible(true);
		descTitle.setText(spell.getName());
		descSources.setText(RPGFrameworkJavaFX.createSourceText(spell));

		// Eventually open a PDF
		if (spell != null) {
			PageReference pageRef = spell.getPageReferences()
					.stream()
					.filter(pr -> pr.getLanguage().equals(Locale.getDefault().getLanguage()))
					.findFirst()
					.get();
			if (pageRef != null) {
				PDFViewerServiceFactory.create().ifPresent(service -> {
					service.show(pageRef.getProduct().getRules(), pageRef.getProduct().getID(),
							pageRef.getLanguage(), pageRef.getPage());
				});
			}
		}

		StringBuffer buf = new StringBuffer();
		List<String> feats = new ArrayList<>();
		for (RitualFeatureReference ref : spell.getFeatures()) {
			if (ref.getModifyable()==null)
				feats.add("NULL");
			else
				feats.add(ref.getModifyable().getName());
		}
		if (!feats.isEmpty()) {
			buf.append("( "+String.join(", ", feats)+" )");
		}
		lbFeatures.setText(buf.toString());

//		String drainText = String.valueOf(spell.getThreshold());
//		lbDrain.setText(drainText);

		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(lbDescr, spell.getDescription(Locale.getDefault()));
	}
}
