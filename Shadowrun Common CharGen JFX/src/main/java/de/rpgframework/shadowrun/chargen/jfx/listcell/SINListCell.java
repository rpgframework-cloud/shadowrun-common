package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.ResourceI18N;
import de.rpgframework.shadowrun.LicenseValue;
import de.rpgframework.shadowrun.SIN;
import de.rpgframework.shadowrun.SIN.FakeRating;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.SINController;
import de.rpgframework.shadowrun.chargen.jfx.SkinProperties;
import de.rpgframework.shadowrun.chargen.jfx.dialog.EditSINDialog;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class SINListCell extends ListCell<SIN> {
	
	private final static ResourceBundle RES = ResourceBundle.getBundle(SkinProperties.class.getPackageName()+".ListCells");
	
	private IShadowrunCharacterController<?,?,?,?> control;
	private SINController sinCtrl;
	
	private Button btnEdit;
	private Label  lbName;
	private Label  lbDesc;
	private Label  lbQual;
	
	private HBox layout;

	//-------------------------------------------------------------------
	public SINListCell(IShadowrunCharacterController<?,?,?,?> control) {
		this.control = control;
		this.sinCtrl = control.getSINController();

		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbName = new Label();
		lbName.getStyleClass().add("base");
		lbDesc = new Label();
		lbDesc.setWrapText(true);
		lbQual = new Label();
		lbQual.setStyle("-fx-font-size: 200%");
		btnEdit = new Button(null, new SymbolIcon("edit"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox colNameDesc = new VBox(lbName, lbDesc);
		colNameDesc.setStyle("-fx-spacing: 0.2em");
		colNameDesc.setMaxWidth(Double.MAX_VALUE);

		this.layout = new HBox(colNameDesc, lbQual, btnEdit);
		this.layout.setAlignment(Pos.CENTER_LEFT);
		this.layout.setStyle("-fx-spacing: 2em");
		HBox.setHgrow(colNameDesc, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.setOnAction(ev -> {
			EditSINDialog dialog = new EditSINDialog(control, this.getItem(), true);
			FlexibleApplication.getInstance().showAlertAndCall(dialog, dialog.getButtonControl());
			getListView().refresh();
		});
	}

	//-------------------------------------------------------------------
	public void updateItem(SIN item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			btnEdit.setVisible(item.getQuality()!=FakeRating.REAL_SIN);
			
			List<String> licenseNames = new ArrayList<>();
			for (LicenseValue lic : control.getModel().getLicenses(item)) {
				licenseNames.add(lic.getNameWithRating());
			}
			
			lbName.setText(item.getName());
			lbDesc.setText(String.join(", ", licenseNames));
//			if (item.getDescription()!=null && item.getDescription().length()>50)
//				lbDesc.setText(item.getDescription().substring(0,50));
//			else
//				lbDesc.setText(item.getDescription());
			if (item.getQuality()==FakeRating.REAL_SIN) {
				lbQual.setText(ResourceI18N.get(RES,"listcell.contact.quality.real"));
			} else
				lbQual.setText(String.valueOf(item.getQuality().getValue()));
		}
	}

}
