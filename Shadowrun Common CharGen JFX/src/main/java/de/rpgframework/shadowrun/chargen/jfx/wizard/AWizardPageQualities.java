package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.Quality.QualityType;
import de.rpgframework.shadowrun.QualityValue;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.gen.QualityGenerator;
import de.rpgframework.shadowrun.chargen.jfx.listcell.QualityListCell;
import de.rpgframework.shadowrun.chargen.jfx.listcell.QualityValueListCell;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public abstract class AWizardPageQualities extends WizardPage implements ControllerListener{

	private final static Logger logger = System.getLogger(AWizardPageQualities.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageAdeptPowers.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterController<?,?,?,?> charGen;

	protected ComplexDataItemControllerNode<Quality, QualityValue> selection;
	protected GenericDescriptionVBox<Quality> bxDescription;
	protected OptionalNodePane layout;
	private NumberUnitBackHeader backHeader;
	private Label lbKarmaGain;
	protected HBox line;

	protected List<QualityType> typesToShow = new ArrayList<>();
	private Function<Requirement, String> requirementResolver;

	//-------------------------------------------------------------------
	public AWizardPageQualities(Wizard wizard, IShadowrunCharacterController<?,?,?,?> charGen, Function<Requirement, String> requirementResolver) {
		super(wizard);
		typesToShow.add(QualityType.NORMAL);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.qualities.title"));
		initComponents();
		initLayout();
		initInteractivity();

		charGen.addListener(this);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes"})
	protected void initComponents() {
		lbKarmaGain = new Label("?");
		lbKarmaGain.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		selection = new ComplexDataItemControllerNode<>(charGen.getQualityController());

		selection.setAvailablePlaceholder(ResourceI18N.get(RES, "page.qualities.placeholder.available"));
		selection.setSelectedPlaceholder(ResourceI18N.get(RES, "page.qualities.placeholder.selected"));

		selection.setAvailableCellFactory(lv -> new QualityListCell(selection.getController(), requirementResolver));
		selection.setSelectedCellFactory(lv -> new QualityValueListCell(
				new IShadowrunCharacterControllerProvider<IShadowrunCharacterController>() {
					public IShadowrunCharacterController getCharacterController() {
						return charGen;
					}},
				true));
		selection.setShowHeadings(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		Label hdGain = new Label(ResourceI18N.get(RES, "page.qualities.karmaGained"));
		Label lbGainMax = new Label("/20");
		line = new HBox(5, hdGain, lbKarmaGain, lbGainMax);
		selection.setSelectedListHead(line);

		backHeader = new NumberUnitBackHeader("Karma");
		backHeader.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeader, new Insets(0,10,0,10));
//		if (ResponsiveControlManager.getCurrentMode()==WindowMode.EXPANDED) {
//			super.setBackHeader(null);
//		} else {
			super.setBackHeader(backHeader);
//		}

		layout = new OptionalNodePane(selection, bxDescription);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.showHelpForProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "show help for "+n);
			bxDescription.setData(n);
			if (n!=null) {
				layout.setTitle(n.getName());
			} else {
				layout.setTitle(null);
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		backHeader.setValue(charGen.getModel().getKarmaFree());
		selection.refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			charGen = (IShadowrunCharacterController<?, ?, ?, ?>) param[0];
			selection.setController(charGen.getQualityController());
		}
		if (type==BasicControllerEvents.CHARACTER_CHANGED || type==BasicControllerEvents.GENERATOR_CHANGED) {
			refresh();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		selection.setShowHeadings(value!=WindowMode.MINIMAL);
	}

	//-------------------------------------------------------------------
	protected void refresh() {
		selection.refresh();
		backHeader.setValue(charGen.getModel().getKarmaFree());
		lbKarmaGain.setText(String.valueOf( ((QualityGenerator)charGen.getQualityController()).getKarmaByNegative()));
	}

}
