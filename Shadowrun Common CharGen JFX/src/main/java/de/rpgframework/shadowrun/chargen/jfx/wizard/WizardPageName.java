package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.Section;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.javafx.layout.FlexGridPane;
import org.prelle.javafx.skin.SectionSkinPlain;

import com.onexip.flexboxfx.FlexBox;

import de.rpgframework.ResourceI18N;
import de.rpgframework.classification.Gender;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.jfx.section.AppearanceSection;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import de.rpgframework.shadowrun.chargen.jfx.CommonShadowrunJFXResourceHook;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class WizardPageName<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, C extends ShadowrunCharacter<S,V,?,?>> extends WizardPage implements ChangeListener<String> {

	private final static Logger logger = System.getLogger(WizardPageName.class.getPackageName());

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageName.class.getName());

	private IShadowrunCharacterGenerator<S,V,?,C> charGen;

	private GridPane content;
	private TextField runnerName;
	private TextField realName;
	private ChoiceBox<Gender> gender;

	private AppearanceSection secAppear;
	private Label lblToDosHead;
	private Label lblToDos;

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public WizardPageName(Wizard wizard, IShadowrunCharacterGenerator<S,V,?,C> charGen) {
		super(wizard);
		this.charGen = charGen;
		initComponents();
		initLayout();
		initStyle();

		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(ResourceI18N.get(UI,"wizard.selectName.title"));

		runnerName= new TextField();
		realName  = new TextField();

		secAppear = new AppearanceSection(charGen.getModel());
		secAppear.setSkin(new SectionSkinPlain(secAppear));
		gender    = new ChoiceBox<Gender>(FXCollections.observableArrayList(Gender.values()));
		gender.setConverter(new StringConverter<Gender>() {
			public String toString(Gender val) { return ResourceI18N.get(UI, "gender."+val.name().toLowerCase()); }
			public Gender fromString(String string) { return null; }
		});
		lblToDos = new Label("-");
		lblToDos.setStyle("-fx-text-fill: red;");
		lblToDos.setWrapText(true);

		String fName = "images/DummyPortrait.jpg";
		InputStream in = CommonShadowrunJFXResourceHook.class.getResourceAsStream(fName);
		if (in==null) {
			logger.log(Level.WARNING,"Missing "+fName);
		} else {
			Image img = new Image(in);
			secAppear.setDummyImage(img);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		lblToDosHead = new Label(ResourceI18N.get(UI,"label.todos"));

		content = new GridPane();
		content.setVgap(5);
		content.setHgap(5);
		content.add(new Label(ResourceI18N.get(UI,"label.streetname")), 0, 0);
		content.add(new Label(ResourceI18N.get(UI,"label.realname")), 0, 1);
		content.add(new Label(ResourceI18N.get(UI,"label.gender")), 0, 2);
		content.add(runnerName, 1, 0);
		content.add(realName  , 1, 1);
		content.add(gender    , 1, 2);
		content.add(lblToDosHead, 0, 4);
		content.add(lblToDos  , 1, 4, 3,1);
//		content.setMinWidth(300);

//		Region padding = new Region();
//		content.add(padding, 1, 3);
//		GridPane.setVgrow(padding, Priority.ALWAYS);

		Section nameSection = new Section(ResourceI18N.get(UI,"section.name"), content);
		nameSection.setSkin(new SectionSkinPlain(nameSection));
//		nameSection.setMinWidth(350);


//		FlowPane layout = new FlowPane(10,10,content, secAppear);
		FlexGridPane layout = new FlexGridPane();
		layout.getChildren().addAll(nameSection, secAppear);
		layout.setSpacing(20);
		nameSection.setMaxHeight(Double.MAX_VALUE);
		FlexGridPane.setMinWidth(nameSection, 4);
		FlexGridPane.setMediumWidth(nameSection, 6);
		FlexGridPane.setMinHeight(nameSection, 4);
		FlexGridPane.setMinWidth(secAppear, 4);
		FlexGridPane.setMediumWidth(secAppear, 7);
		FlexGridPane.setMinHeight(secAppear, 7);
		FlexGridPane.setMediumHeight(secAppear, 4);

//		layout.setPrefWrapLength(370);
		ScrollPane scroll = new ScrollPane(layout);
		scroll.setFitToWidth(true);
		layout.maxWidthProperty().bind(scroll.widthProperty());
		super.setContent(scroll);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
//		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		runnerName.textProperty().addListener( (ov,o,n) -> charGen.getModel().setName(n));
		realName.textProperty().addListener( (ov,o,n) -> charGen.getModel().setRealName(n));
		gender.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> charGen.getModel().setGender(n));

//		charGen.addListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.beans.value.ChangeListener#changed(javafx.beans.value.ObservableValue, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void changed(ObservableValue<? extends String> textfield, String oldVal,	String newVal) {
		if (logger.isLoggable(Level.TRACE))
			logger.log(Level.TRACE, "changed "+textfield+" from "+oldVal+" to "+newVal);

		String n = runnerName.getText();
		if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); runnerName.setText(n); }
		if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); runnerName.setText(n); }
		if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); runnerName.setText(n); }
		if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); runnerName.setText(n); }

		n = realName.getText();
		if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); realName.setText(n); }
		if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); realName.setText(n); }
		if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); realName.setText(n); }
		if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); realName.setText(n); }

//		n = ethnicity.getText();
//		if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); ethnicity.setText(n); }
//		if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); ethnicity.setText(n); }
//		if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); ethnicity.setText(n); }
//		if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); ethnicity.setText(n); }

		List<String> todo = new ArrayList<>();
		charGen.getToDos().stream().filter(tmp -> tmp.getSeverity()==Severity.STOPPER).forEach(tmp -> todo.add(tmp.getMessage()));
		lblToDos.setText(String.join(", ", todo));
		lblToDosHead.setVisible(!charGen.getToDos().isEmpty());
	}

	//-------------------------------------------------------------------
	private void refresh() {
		List<String> todo = new ArrayList<>();
		charGen.getToDos().forEach(tmp -> {
			if (tmp.getSeverity()==Severity.STOPPER)
				todo.add(tmp.getMessage());
			});
		lblToDos.setText(String.join(", ",todo));
		lblToDosHead.setVisible(!charGen.getToDos().isEmpty());

		realName.setText(charGen.getModel().getRealName());
		runnerName.setText(charGen.getModel().getName());
		gender.setValue(charGen.getModel().getGender());
		secAppear.refresh();
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
//	 */
//	@Override
//	public void handleGenerationEvent(GenerationEvent event) {
//		switch (event.getType()) {
//		case CONSTRUCTIONKIT_CHANGED:
//			logger.log(Level.DEBUG, "RCV "+event);
//			refresh();
//			break;
//		default:
//			refresh();
//		}
//	}

	// -------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		// TODO Auto-generated method stub
		logger.log(Level.WARNING,"responsive mode " + value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.DEBUG, "pageVisited");
		refresh();
	}

}
