package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.section.ListSection;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.Contact;
import de.rpgframework.shadowrun.LicenseValue;
import de.rpgframework.shadowrun.SIN;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.SpellValue;
import de.rpgframework.shadowrun.Tradition;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import de.rpgframework.shadowrun.chargen.jfx.dialog.EditContactDialog;
import de.rpgframework.shadowrun.chargen.jfx.dialog.EditSINDialog;
import de.rpgframework.shadowrun.chargen.jfx.listcell.ContactListCell;
import de.rpgframework.shadowrun.chargen.jfx.listcell.SINListCell;
import de.rpgframework.shadowrun.chargen.jfx.listcell.SpellValueListCell;
import de.rpgframework.shadowrun.chargen.jfx.pane.SpellSelector;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class ContactSection extends ListSection<Contact> implements IShadowrunCharacterControllerProvider<IShadowrunCharacterController> {

	protected final static Logger logger = System.getLogger(ContactSection.class.getPackageName());

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(ContactSection.class.getPackageName()+".Section");

	private IShadowrunCharacterController control;
	private ShadowrunCharacter model;
	
	private Label lbPoints, lbKarma;
	private HBox headerNode;

	//-------------------------------------------------------------------
	public ContactSection(String title) {
		super(title);

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list.setStyle("-fx-min-height: 10em; -fx-pref-height: 30em; -fx-pref-width: 24em"); 
		list.setCellFactory(lv -> {
			ContactListCell cell =  new ContactListCell(control.getContactController());
			cell.setStyle("-fx-pref-width: 23em"); 
			cell.setOnMouseClicked(ev -> {
				if (ev.getClickCount()==2) {
					onEdit(cell.getItem());
				}
			});
			return cell;
		});
		
		lbPoints = new Label("?");
		lbPoints.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);
		lbKarma = new Label("?");
		lbKarma.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		Label hdPoints = new Label(ResourceI18N.get(RES, "contactsection.points"));
		Label hdUnspent = new Label(ResourceI18N.get(RES, "contactsection.karma"));
		headerNode = new HBox(5, lbPoints, hdPoints, lbKarma, hdUnspent);
		HBox.setMargin(lbKarma, new Insets(0,0,0,5));
		headerNode.setAlignment(Pos.CENTER_LEFT);
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				btnDel.setDisable(true);
			} else {
//				btnDel.setDisable( !control.getContactController().canDeleteSIN(n));
			}
		});
		btnAdd.setOnAction(ev -> onAdd());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	@Override
	public IShadowrunCharacterController getCharacterController() {
		return control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void refresh() {
		logger.log(Level.TRACE, "refresh");
		
		if (control!=null) {
			lbPoints.setText( String.valueOf( control.getContactController().getPointsLeft() ));
		}

		if (model!=null)
			setData(model.getContacts());
		else
			setData(new ArrayList<>());
	}

	//-------------------------------------------------------------------
	protected void onAdd() {
		logger.log(Level.DEBUG, "opening contact selection dialog");
		
		Contact data = control.getContactController().createContact().get();
		EditContactDialog dialog = new EditContactDialog(control, data, false);
		CloseType closed = FlexibleApplication.getInstance().showAlertAndCall(dialog, dialog.getButtonControl());
		if (closed==CloseType.OK) {
			logger.log(Level.INFO, "User chose a SIN to add");
			data.setDescription(dialog.getDescription());
			logger.log(Level.DEBUG, "Adding a SIN done - now add licenses");
//			refresh();
		} else {
			control.getContactController().removeContact(data);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(Contact item) {
		logger.log(Level.DEBUG, "onDelete "+item);
		control.getContactController().removeContact(item);
		list.getItems().remove(item);
	}

	//-------------------------------------------------------------------
	private void onEdit(Contact data) {
		logger.log(Level.DEBUG, "onEdit "+data);
		EditContactDialog dialog = new EditContactDialog(control, data, true);
		FlexibleApplication.getInstance().showAlertAndCall(dialog, dialog.getButtonControl());
		refresh();
	}

//	//-------------------------------------------------------------------
//	@SuppressWarnings("unchecked")
//	private void onSettings() {
//		CharacterLeveller ctrl = (CharacterLeveller) this.control;
//		
//		VBox content = new VBox(20);
//		for (ConfigOption<?> opt : ctrl.getSpellController().getConfigOptions()) {
//			CheckBox cb = new CheckBox(opt.getName());
//			cb.setSelected((Boolean)opt.getValue());
//			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
//			content.getChildren().add(cb);
//		}
//		
//		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
//	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (ShadowrunCharacter) ctrl.getModel();
		if (control instanceof IShadowrunCharacterGenerator) {
			setHeaderNode(headerNode);
//			Label lbGradeName = new Label(ResourceI18N.get(RES, key));
		}
		refresh();
	}

}
