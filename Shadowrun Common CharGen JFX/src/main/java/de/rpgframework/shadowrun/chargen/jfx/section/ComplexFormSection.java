package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.AlertManager;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.jfx.section.ListSection;
import de.rpgframework.shadowrun.ComplexForm;
import de.rpgframework.shadowrun.ComplexFormValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.jfx.pane.ComplexFormSelector;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class ComplexFormSection extends ListSection<ComplexFormValue> implements IShadowrunCharacterControllerProvider<IShadowrunCharacterController> {

	private final static Logger logger = System.getLogger(ComplexFormSection.class.getPackageName());

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(ComplexFormSection.class.getPackageName()+".Section");

	private IShadowrunCharacterController control;
	private ShadowrunCharacter model;
	private Function<Requirement, String> requirementResolver;
	private Function<Modification, String> modResolver;
	private Function<ComplexForm, Decision[]> decisionCallback;

	//-------------------------------------------------------------------
	public ComplexFormSection(String title,
			Function<Requirement, String> requirementResolver,
			Function<Modification, String> modResolver,
			Function<ComplexForm, Decision[]> decisionCallback) {
		super(title);
		this.requirementResolver = requirementResolver;
		this.modResolver         = modResolver;
		this.decisionCallback    = decisionCallback;

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list.setStyle("-fx-min-height: 10em; -fx-pref-height: 30em; -fx-pref-width: 25em");
		list.setCellFactory(cell -> new ComplexDataItemValueListCell( () -> control.getComplexFormController()));
	}

	// -------------------------------------------------------------------
	private void initLayout() {
//		Label lbTradition = new Label(ResourceI18N.get(RES, "spellsection.tradition"));
//		HBox headerNode = new HBox(10, lbTradition, cbTradition);
//		headerNode.setAlignment(Pos.CENTER_LEFT);
//		setHeaderNode(headerNode);
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				btnDel.setDisable(true);
			} else {
				btnDel.setDisable( !control.getComplexFormController().canBeDeselected(n).get());
			}
		});
		btnAdd.setOnAction(ev -> onAdd());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	@Override
	public IShadowrunCharacterController getCharacterController() {
		return control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void refresh() {
		logger.log(Level.TRACE, "refresh");

		if (model!=null)
			setData(model.getComplexForms());
		else
			setData(new ArrayList<>());
	}

	//-------------------------------------------------------------------
	protected void onAdd() {
		logger.log(Level.DEBUG, "opening complex form selection dialog");

		ComplexFormSelector selector = new ComplexFormSelector(control.getComplexFormController(), requirementResolver, modResolver);
//		selector.setPlaceholder(new Label(Resource.get(RES,"metaechosection.selector.placeholder")));
//		SelectorWithHelp<MetamagicOrEcho> pane = new SelectorWithHelp<>(selector);
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES,"complexformsection.selector.title"), selector, CloseType.OK, CloseType.CANCEL);

		CloseType close = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		logger.log(Level.DEBUG,"Closed with "+close);
		if (close==CloseType.OK) {
			ComplexForm data = selector.getSelected();
			logger.log(Level.DEBUG, "Selected complex form: "+data);
			OperationResult<ComplexFormValue> res = null;
			if (!data.getChoices().isEmpty()) {
				Decision[] dec = askUserForDecisions(data);
				res = control.getComplexFormController().select(data, dec);
			} else {
				res = control.getComplexFormController().select(data);
			}
			if (res.wasSuccessful()) {
				logger.log(Level.INFO, "Selecting {0} was successful", data);
			} else {
				logger.log(Level.WARNING, "Selecting {0} failed: {1}", data, res.getError());
				AlertManager.showAlertAndCall(javafx.scene.control.Alert.AlertType.ERROR, "Failed adding", res.getError());
			}
		}
	}

	//-------------------------------------------------------------------
	protected Decision[] askUserForDecisions(ComplexForm data) {
		if (decisionCallback!=null)
			return decisionCallback.apply(data);
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(ComplexFormValue item) {
		logger.log(Level.DEBUG, "onDelete "+item);
		if (control.getComplexFormController().deselect(item)) {
			list.getItems().remove(item);
		}
	}

//	//-------------------------------------------------------------------
//	@SuppressWarnings("unchecked")
//	private void onSettings() {
//		CharacterLeveller ctrl = (CharacterLeveller) this.control;
//
//		VBox content = new VBox(20);
//		for (ConfigOption<?> opt : ctrl.getSpellController().getConfigOptions()) {
//			CheckBox cb = new CheckBox(opt.getName());
//			cb.setSelected((Boolean)opt.getValue());
//			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
//			content.getChildren().add(cb);
//		}
//
//		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
//	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (ShadowrunCharacter) ctrl.getModel();
		refresh();
	}

}
