package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.MagicOrResonanceType;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import de.rpgframework.shadowrun.chargen.jfx.ShadowrunAttributeTable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public abstract class WizardPageAttributes<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, C extends ShadowrunCharacter<S, V,?,?>>
		extends WizardPage implements ControllerListener {

	private final static Logger logger = System.getLogger(WizardPageAttributes.class.getPackageName()+".attr");

	protected static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle
			.getBundle(WizardPageAttributes.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterGenerator<S, V, ?,C> charGen;

	protected GenericDescriptionVBox bxDescription;
	protected OptionalNodePane layout;

	protected ShadowrunAttributeTable<S,V,C> table;
	protected NumberUnitBackHeader backHeaderKarma;

	// -------------------------------------------------------------------
	public WizardPageAttributes(Wizard wizard, IShadowrunCharacterGenerator<S, V,?, C> charGen) {
		super(wizard);
		if (charGen==null)
			throw new NullPointerException("charGen");
		this.charGen = charGen;
		setContent(new Label("WizardPageAttributes"));
		initComponents();
		initLayout();
		initBackHeader();
		updateTableToController(charGen);
		initInteractivity();

		if (charGen != null) {
			C model = charGen.getModel();
			MagicOrResonanceType mor = model.getMagicOrResonanceType();
			if (mor != null) {
				table.setShowMagic(mor.usesMagic());
				table.setShowResonance(mor.usesResonance());
			}
		}
	}

	// -------------------------------------------------------------------
	private void initComponents() {
		setTitle(ResourceI18N.get(UI, "wizard.page.attributes.title"));

		bxDescription = new GenericDescriptionVBox(null,null);
	}

	// -------------------------------------------------------------------
	protected void initBackHeader() {
		backHeaderKarma = new NumberUnitBackHeader(ResourceI18N.get(UI, "label.karma"));
		backHeaderKarma.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeaderKarma, new Insets(0,10,0,10));
		super.setBackHeader(backHeaderKarma);
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		layout = new OptionalNodePane(table, null);
		setContent(layout);
	}

	protected abstract ShadowrunAttributeTable<S,V,C> getTableForController(IShadowrunCharacterGenerator<S, V, ?,C> controller);

	// -------------------------------------------------------------------
	private void updateTableToController(IShadowrunCharacterGenerator<S, V, ?,C> controller) {
		logger.log(Level.DEBUG, "updateTableToController({0})", controller);
		this.charGen = controller;
		charGen.addListener(this);
		table = getTableForController(controller);
		if (table==null) {
			logger.log(Level.ERROR, "No table returned for "+controller);
			System.err.println( "WizardPageAttributes: No table returned for "+controller);
		} else {
			table.setMaxWidth(500);
			table.setNameMapper(table.getNameMapper());
		}

		logger.log(Level.DEBUG, "  setContent to {0}", table);
		layout.setContent(table);
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		if (charGen!=null)
			charGen.addListener(this);

		setOnExtraActionHandler( button -> onExtraAction(button));

//		table.useExpertModeProperty().bind(tsExpertMode.selectedProperty());
//		tsExpertMode.visibleProperty().bind(table.expertModeAvailableProperty());
	}

	// -------------------------------------------------------------------
	protected void refresh() {
		if (table==null) return;
		C model = charGen.getModel();
		if (model != null) {
			backHeaderKarma.setValue(model.getKarmaFree());
			MagicOrResonanceType mor = model.getMagicOrResonanceType();
			if (mor != null) {
				logger.log(Level.DEBUG, "Magic={0}  Resonance={1}", mor.usesMagic(), mor.usesResonance());
				table.setShowMagic(mor.usesMagic());
				table.setShowResonance(mor.usesResonance());
			}
		}
		table.refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.DEBUG, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type == BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
			charGen = (IShadowrunCharacterGenerator<S, V,?, C>) param[0];
			updateTableToController(charGen);
			refresh();
			return;
		}
		if (type == BasicControllerEvents.CHARACTER_CHANGED) {
			logger.log(Level.DEBUG, "RCV " + type + " with " + Arrays.toString(param));
			refresh();
		}
	}

	//-------------------------------------------------------------------
	private void onExtraAction(CloseType button) {
		switch (button) {
		case RANDOMIZE:
			charGen.getAttributeController().roll();
			break;
		default:
			logger.log(Level.WARNING, "ToDo: handle "+button);
		}
	}

}
