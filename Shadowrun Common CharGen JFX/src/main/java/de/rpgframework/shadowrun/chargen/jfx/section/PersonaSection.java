package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Section;

import de.rpgframework.ResourceI18N;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 * @author Stefan Prelle
 *
 */
public abstract class PersonaSection<C extends ShadowrunCharacter<?,?,?,?>> extends Section {

	protected final static Logger logger = System.getLogger(PersonaSection.class.getPackageName());

	protected static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(PersonaSection.class.getPackageName()+".Section");

	protected IShadowrunCharacterController<?,?,?,C> control;
	protected C model;
	
	protected GridPane grid;
	protected Label     lbAttack, lbSleaze, lbDatap, lbFirew;
	protected TextField tfAttack, tfSleaze, tfDatap, tfFirew;
	
	protected ObjectProperty<Node> deviceSelectNode = new SimpleObjectProperty<>();
	protected ObjectProperty<Node> ruleSpecificNode = new SimpleObjectProperty<>();
	private VBox layout;
	
	//-------------------------------------------------------------------
	public PersonaSection(String title) {
		setTitle(title);
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbAttack = new Label("?"); lbAttack.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbSleaze = new Label("?"); lbSleaze.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbDatap  = new Label("?"); lbDatap.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbFirew  = new Label("?"); lbFirew.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		tfAttack = new TextField(); tfAttack.setPrefColumnCount(2);
		tfSleaze = new TextField(); tfSleaze.setPrefColumnCount(2);
		tfDatap  = new TextField(); tfDatap.setPrefColumnCount(2);
		tfFirew  = new TextField(); tfFirew.setPrefColumnCount(2);
		
		GridPane.setHalignment(lbAttack, HPos.CENTER);
		GridPane.setHalignment(lbSleaze, HPos.CENTER);
		GridPane.setHalignment(lbDatap , HPos.CENTER);
		GridPane.setHalignment(lbFirew , HPos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdAttack = new Label(ResourceI18N.get(RES,"section.persona.attack"));
		Label hdSleaze = new Label(ResourceI18N.get(RES,"section.persona.sleaze"));
		Label hdDatap  = new Label(ResourceI18N.get(RES,"section.persona.dataprocessing"));
		Label hdFirew  = new Label(ResourceI18N.get(RES,"section.persona.firewall"));
		Label hdNormal = new Label(ResourceI18N.get(RES,"section.persona.normal"));
		Label hdCurrent= new Label(ResourceI18N.get(RES,"section.persona.current"));
		hdAttack.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdSleaze.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdDatap.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdFirew.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdNormal.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdCurrent.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		grid = new GridPane();
		grid.setVgap(5);
		grid.setHgap(5);
		grid.add(hdAttack, 1, 0);
		grid.add(hdSleaze, 2, 0);
		grid.add(hdDatap , 3, 0);
		grid.add(hdFirew , 4, 0);
		
		grid.add(hdNormal, 0, 1);
		grid.add(lbAttack, 1, 1);
		grid.add(lbSleaze, 2, 1);
		grid.add(lbDatap, 3, 1);
		grid.add(lbFirew, 4, 1);
		
		grid.add(hdCurrent, 0, 2);
		grid.add(tfAttack, 1, 2);
		grid.add(tfSleaze, 2, 2);
		grid.add(tfDatap , 3, 2);
		grid.add(tfFirew , 4, 2);
		
		layout = new VBox(20, grid);
		
		setContent(layout);
	}

	//-------------------------------------------------------------------
	public ObjectProperty<Node> deviceSelectNodeProperty() { return deviceSelectNode; }
	public PersonaSection setDeviceSelectNode(Node value) { deviceSelectNode.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<Node> ruleSpecificNodeProperty() { return ruleSpecificNode; }
	public PersonaSection setRuleSpecificNode(Node value) { ruleSpecificNode.set(value); return this; }

	//-------------------------------------------------------------------
	private void initInteractivity() {
		deviceSelectNode.addListener( (ov,o,n) -> {
			if (o!=null) layout.getChildren().remove(o);
			if (n!=null) layout.getChildren().add(0,n);
		});
		ruleSpecificNode.addListener( (ov,o,n) -> {
			if (o!=null) layout.getChildren().remove(o);
			if (n!=null) layout.getChildren().add(n);
		});
	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController<?,?,?,C> ctrl) {
		logger.log(Level.DEBUG, "updateController");
		this.control = ctrl;
		model = ctrl.getModel();
		refresh();
	}
	
}
