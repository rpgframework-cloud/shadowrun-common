package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.function.Function;

import org.prelle.javafx.ResponsiveControl;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.shadowrun.ComplexForm;
import de.rpgframework.shadowrun.ComplexFormValue;
import de.rpgframework.shadowrun.chargen.charctrl.IComplexFormController;
import javafx.scene.layout.Pane;

/**
 * @author prelle
 *
 */
public class ComplexFormSelector extends Selector<ComplexForm, ComplexFormValue> implements ResponsiveControl {

	//-------------------------------------------------------------------
	public ComplexFormSelector(
			IComplexFormController ctrl,
			Function<Requirement,String> resolver,
			Function<Modification,String> mResolver) {
		super(ctrl, resolver, mResolver, (AFilterInjector<ComplexForm>) null);
		listPossible.setCellFactory( lv -> new ComplexDataItemListCell<ComplexForm>());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.Selector#getDescriptionNode(de.rpgframework.genericrpg.data.ComplexDataItem)
	 */
	@Override
	protected Pane getDescriptionNode(ComplexForm selected) {
		ComplexFormDescriptionPane descr = new ComplexFormDescriptionPane();
		if (selected!=null)
			descr.setData(selected);
		return descr;
	}

}
