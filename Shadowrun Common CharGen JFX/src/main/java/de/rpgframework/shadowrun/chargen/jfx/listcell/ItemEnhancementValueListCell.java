package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.PropertyResourceBundle;
import java.util.function.Consumer;

import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ScreenManagerProvider;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.items.AItemEnhancement;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.Hook;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.jfx.pages.ACarriedItemPage;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class ItemEnhancementValueListCell<E extends AItemEnhancement> extends ListCell<ItemEnhancementValue<E>> {

	private static PropertyResourceBundle UI = ACarriedItemPage.UI;

	private final static Logger logger = System.getLogger(ItemEnhancementValueListCell.class.getPackageName());

	private final static String NORMAL_STYLE = "carrieditem-cell";

	private IShadowrunCharacterController<?,?,?,?> control;
	private CarriedItem container;
	private Consumer<CarriedItem> refreshCallback;

	private Label lbName;
	private Button btnDel;
	private MenuItem menUndo;
	private MenuItem menSell;
	private MenuItem menDelete;

	private HBox layout;

	//-------------------------------------------------------------------
	public ItemEnhancementValueListCell(IShadowrunCharacterController charGen, CarriedItem container, Consumer<CarriedItem> refreshCallback) {
		this.control = charGen;
		this.container = container;
		this.refreshCallback = refreshCallback;
		getStyleClass().add(NORMAL_STYLE);

		FontIcon icoPopup  = new FontIcon("\uE17E\uE107");
		btnDel = new Button(null, icoPopup);
		btnDel.getStyleClass().add("mini-button");
//		btnDel.setOnAction(ev -> {
//			logger.log(Level.DEBUG, "remove accessory "+item+" from "+container.getName());
//			if (control.remove(container, item)) {
//				slot.removeEmbeddedItem(item);
//				refresh();
//			}
//		});
		// Context menu
		FontIcon icoDelete = new FontIcon("\uE17E\uE107");
		FontIcon icoSell   = new FontIcon("\uE17E \u00A5");
		FontIcon icoUndo   = new FontIcon("\uE17E\uE10E");
		icoDelete.setStyle("-fx-padding: 3px");
		icoSell.setStyle("-fx-padding: 3px");
		icoUndo.setStyle("-fx-padding: 3px");
		menUndo = new MenuItem(ResourceI18N.get(UI, "label.removeitem.undo"), icoUndo);
		menSell = new MenuItem(ResourceI18N.get(UI, "label.removeitem.sell"), icoSell);
		menDelete = new MenuItem(ResourceI18N.get(UI, "label.removeitem.trash"), icoDelete);
		ContextMenu menu = new ContextMenu();
		menu.getItems().add(menUndo);
		menu.getItems().add(menSell);
		menu.getItems().add(menDelete);
		btnDel.setContextMenu(menu);

		btnDel.setOnAction(event -> menu.show(btnDel, Side.BOTTOM, 0, 0));

		lbName = new Label();
		lbName.setMaxWidth(Double.MAX_VALUE);
		lbName.setStyle("-fx-font-size: 120%");

		layout = new HBox(15, lbName, btnDel);
		layout.setAlignment(Pos.CENTER_LEFT);
		HBox.setHgrow(lbName, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	public void updateItem(ItemEnhancementValue<E> item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lbName.setText(item.getNameWithRating());
			ComplexDataItemController<E, ItemEnhancementValue<E>> enhCtrl = control.getEquipmentController().getItemEnhancementController(container);
			logger.log(Level.WARNING, "isAutoAdded( {0} ) = {1}", item.getKey(), item.isAutoAdded());
			btnDel.setVisible(!item.isAutoAdded());
			btnDel.setVisible(true);
//			menDelete.setOnAction(event -> { logger.log(Level.INFO, "Trash "+item); control.getEquipmentController().sell(item, 0f); });
//			menSell  .setOnAction(event -> { logger.log(Level.INFO, "Sell  "+item); control.getEquipmentController().sell(item, 0.5f);});
			menUndo  .setOnAction(event -> {
				logger.log(Level.INFO, "Undo  "+item);
				boolean removed = false;
				removed = enhCtrl.deselect(item);
				if (removed) {
					getListView().getItems().remove(item);
					refreshCallback.accept(container);
				}
			});

			setGraphic(layout);
		}
	}

}