package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.controlsfx.control.ToggleSwitch;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.rules.SkillTable;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.SkillType;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public abstract class WizardPageSkills<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, C extends ShadowrunCharacter<S, V,?,?>>
		extends WizardPage implements ControllerListener, ResponsiveControl {

	private final static Logger logger = System.getLogger(WizardPageSkills.class.getPackageName()+".skill");

	protected static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageSkills.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterGenerator<S, V, ?,C> charGen;
	protected GenericDescriptionVBox bxDescription;
	protected OptionalNodePane layout;

	private ToggleSwitch tsShowAllSkills;
	private ChoiceBox<S> cbAvailable;
	private Button btnAdd;
	protected Button btnAddKnow, btnAddLang;
	private Button btnDelAct, btnDelKnow;
	private HBox actionLine1, actionLine2;
//	protected ShadowrunSkillTable<S,V,C> table;
	protected SkillTable<ShadowrunAttribute, S, V> table;
	protected SkillTable<ShadowrunAttribute, S, V> knowTable;
	protected NumberUnitBackHeader backHeaderKarma;
	private VBox content;
	private Pane generator;
	private VBox col1, col2;

	private S setSelectionTo;

	// -------------------------------------------------------------------
	public WizardPageSkills(Wizard wizard, IShadowrunCharacterGenerator<S, V, ?,C> charGen) {
		super(wizard);
		if (charGen==null)
			throw new NullPointerException("charGen");
		this.charGen = charGen;
		initComponents();
		initBackHeader();
		initSecondaryContent();
		initLayout();
		updateTableToController(charGen);
		initInteractivity();
	}

	// -------------------------------------------------------------------
	protected void initComponents() {
		tsShowAllSkills = new ToggleSwitch(ResourceI18N.get(UI, "page.skills.showAll"));
		tsShowAllSkills.setSelected(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
		cbAvailable = new ChoiceBox<>();
		cbAvailable.setConverter( new StringConverter<S>() {
			public String toString(S value) { return (value==null)?ResourceI18N.get(UI, "page.skills.selectSkillToAdd"):value.getName();}
			public S fromString(String string) {return null;}});

		table = new SkillTable<>();
		// Placeholder
		Label placeholder = new Label(ResourceI18N.get(UI, "page.skills.placeholder"));
		placeholder.setWrapText(true);
		placeholder.setAlignment(Pos.CENTER);
		placeholder.setStyle("-fx-font-style: italic; -fx-pref-height: 5em");
		table.setPlaceholder(placeholder);
		knowTable = new SkillTable<>();
		knowTable.setHideValueColumns(true);
		setTitle(ResourceI18N.get(UI, "page.skills.title"));

		btnAdd = new Button(null, new SymbolIcon("Add"));
		btnAddKnow = new Button("+K");
		btnAddLang = new Button("+L");
		btnDelAct = new Button(null, new SymbolIcon("Delete"));
		btnDelKnow = new Button(null, new SymbolIcon("Delete"));
		btnAdd.setDisable(true);
		btnDelAct.setDisable(true);
		btnDelKnow.setDisable(true);

		bxDescription = new GenericDescriptionVBox(null,null);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		table.setMaxWidth(500);
		knowTable.setMaxWidth(400);

		Region buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		actionLine1 = new HBox(10, cbAvailable, btnAdd, buf, btnDelAct);
		actionLine1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(buf, Priority.ALWAYS);
		HBox.setMargin(btnDelAct, new Insets(0, 50, 0, 0));

		buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(buf, Priority.ALWAYS);
		actionLine2 = new HBox(10, btnAddKnow, btnAddLang, buf, btnDelKnow);
		actionLine2.setMaxWidth(Double.MAX_VALUE);

		col1 = new VBox(10, actionLine1, table);
		col2 = new VBox(10, actionLine2, knowTable);

		content = new VBox(10);
		layout = new OptionalNodePane(content, bxDescription);
		refreshTableLayout();
		setContent(layout);

		// Action Line only visible when not in "All Skills" mode
		actionLine1.visibleProperty().bind(tsShowAllSkills.selectedProperty().not());
		actionLine1.managedProperty().bind(tsShowAllSkills.selectedProperty().not());
	}

	//-------------------------------------------------------------------
	protected void initBackHeader() {
		backHeaderKarma = new NumberUnitBackHeader(ResourceI18N.get(UI, "label.karma"), true);
		backHeaderKarma.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeaderKarma, new Insets(0,10,0,10));
		super.setBackHeader(backHeaderKarma);
	}

	//-------------------------------------------------------------------
	protected void initSecondaryContent() {

		super.setBackContent(tsShowAllSkills);
	}

	//-------------------------------------------------------------------
	protected Pane getGeneratorLine() {
		return null;
	}

//	protected abstract ShadowrunSkillTable<S,V,C> getTableForController(IShadowrunCharacterGenerator<S, V, ?,C> controller);

	//-------------------------------------------------------------------
	private void refreshTableLayout() {
		content.getChildren().clear();
		generator = getGeneratorLine();
		if (generator!=null)
			content.getChildren().add(generator);

		if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
			content.getChildren().addAll(actionLine1, col1);
		} else {
			content.getChildren().add(new HBox(20, col1, col2));
		}
	}

	// -------------------------------------------------------------------
	private void updateTableToController(IShadowrunCharacterGenerator<S, V, ?, C> controller) {
		logger.log(Level.DEBUG, "updateTableToController("+controller+")");
		this.charGen = controller;
		charGen.addListener(this);
		table.getItems().clear();
		knowTable.getItems().clear();
		logger.log(Level.INFO, "Update skill table controller to "+controller.getSkillController());
		table.setController( controller.getSkillController());
		knowTable.setController( controller.getSkillController());

		refreshTableLayout();

		// Optional generator line
		layout.setContent(content);
	}

	// -------------------------------------------------------------------
	protected void initInteractivity() {
		if (charGen!=null)
			charGen.addListener(this);

		btnAdd.setOnAction(ev -> addClicked());
		btnAddKnow.setOnAction(ev -> addKnowledgeClicked());
		btnAddLang.setOnAction(ev -> addLanguageClicked());
		btnDelAct.setOnAction(ev -> delClicked());
		btnDelKnow.setOnAction(ev -> delKnowledgeClicked());

		table.selectedItemProperty().addListener( (ov,o,n) -> {
			btnDelAct.setDisable( (n==null) || !charGen.getSkillController().canBeDeselected(n).get());
		});
		knowTable.selectedItemProperty().addListener( (ov,o,n) -> {
			btnDelKnow.setDisable( (n==null) || !charGen.getSkillController().canBeDeselected(n).get());
		});
		cbAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> btnAdd.setDisable(n==null));
//		table.useExpertModeProperty().bind(tsExpertMode.selectedProperty());
//		tsExpertMode.visibleProperty().bind(table.expertModeAvailableProperty());

		table.selectedItemProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "Selected {0}",n);
			knowTable.getSelectionModel().clearSelection();
			updateHelpWith(n);
		});
		knowTable.selectedItemProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "Selected {0}",n);
//			table.getSelectionModel().clearSelection();
			updateHelpWith(n);
		});
	}

	//-------------------------------------------------------------------
	private void updateHelpWith(V n) {
		if (n!=null) {
			bxDescription.setData(n.getModifyable());
			layout.setTitle(n.getModifyable().getName(Locale.getDefault()));
			// If in MINIMAL mode, hide title after some time
			// since it covers the final values
			if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
				Thread hideThread = new Thread( () -> {
					try {
						Thread.sleep(4000);
					} catch (Exception e) {
					}
					Platform.runLater( () -> layout.setTitle(null));
				});
				hideThread.start();
			}
		} else {
			bxDescription.setData((DataItem)null);
			layout.setTitle(null);
		}
	}

	//-------------------------------------------------------------------
	protected void refresh() {
		logger.log(Level.INFO, "refresh() - current table controller is "+table.getController()+" - it should be "+charGen.getSkillController());
//		logger.log(Level.ERROR," refresh");
		// Update available
		List<S> data = new ArrayList<S>(charGen.getSkillController().getAvailable());
		Collections.sort(data, new Comparator<S>() {
			public int compare(S o1, S o2) {
				boolean isKnow1 = o1.getType()==SkillType.KNOWLEDGE || o1.getType()==SkillType.LANGUAGE;
				boolean isKnow2 = o2.getType()==SkillType.KNOWLEDGE || o2.getType()==SkillType.LANGUAGE;
				if (isKnow1 && !isKnow2) return +1;
				if (!isKnow1 && isKnow2) return -1;
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}});
		cbAvailable.getItems().setAll(data);
		// Preselect the first option, if present
		if (!cbAvailable.getItems().isEmpty()) {
			if (setSelectionTo!=null && cbAvailable.getItems().contains(setSelectionTo)) {
				cbAvailable.getSelectionModel().select(setSelectionTo);
			} else {
				cbAvailable.getSelectionModel().select(0);
			}
		}

		// Update table
		if (ResponsiveControlManager.getCurrentMode() == WindowMode.MINIMAL) {
			// One shared table
			if (showAllSkillsProperty().get()) {
				table.setData(charGen.getSkillController().getAll());
			} else {
				if (logger.isLoggable(Level.TRACE)) {
					for (V tmp : charGen.getSkillController().getSelected()) {
						logger.log(Level.TRACE, "  {0}", tmp);
					}
				}
				table.setData(charGen.getSkillController().getSelected());
			}
		} else {
			// Action and knowledge skills in their own table
			if (showAllSkillsProperty().get()) {
				table.setData(
						charGen.getSkillController().getAll()
						.stream()
						.filter(sv -> sv.getSkill().getType()!=SkillType.KNOWLEDGE && sv.getSkill().getType()!=SkillType.LANGUAGE)
						.toList()
					);
				knowTable.setData(
						charGen.getSkillController().getSelected()
						.stream()
						.filter(sv -> sv.getSkill().getType()==SkillType.KNOWLEDGE || sv.getSkill().getType()==SkillType.LANGUAGE)
						.toList()
					);
			} else {
				table.setData(charGen.getSkillController().getSelected()
						.stream()
						.filter(sv -> sv.getSkill().getType()!=SkillType.KNOWLEDGE && sv.getSkill().getType()!=SkillType.LANGUAGE)
						.toList());
				knowTable.setData(charGen.getSkillController().getSelected()
						.stream()
						.filter(sv -> sv.getSkill().getType()==SkillType.KNOWLEDGE || sv.getSkill().getType()==SkillType.LANGUAGE)
						.toList());
			}
		}
		table.refresh();
		backHeaderKarma.setValue(charGen.getModel().getKarmaFree());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.DEBUG, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type == BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
			charGen = (IShadowrunCharacterGenerator<S, V, ?, C>) param[0];
			updateTableToController(charGen);
			refresh();
			return;
		}
		if (type == BasicControllerEvents.CHARACTER_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
			refresh();
		}
	}

	//-------------------------------------------------------------------
	protected Decision[] makeDecision(S skill) {
		logger.log(Level.WARNING, "TODO: override makeDecision() in "+getClass());
		return new Decision[] {new Decision(skill.getChoices().get(0).getUUID(), "Unnamed")};
	}

	//-------------------------------------------------------------------
	private void addClicked() {
		S skill = cbAvailable.getSelectionModel().getSelectedItem();
		logger.log(Level.DEBUG, "addClicked for {0}",skill);
		setSelectionTo = skill;

		if (skill.getChoices().isEmpty()) {
			cbAvailable.getSelectionModel().clearSelection();
			OperationResult<V> ret = charGen.getSkillController().select(skill);
			logger.log(Level.WARNING, "Add result was {0}", ret);
		} else {
			Decision[] dec = makeDecision(skill);
			if (dec==null)
				return;
			OperationResult<V> ret = charGen.getSkillController().select(skill, dec);
			logger.log(Level.WARNING, "Add result was {0}", ret);
		}
	}

	//-------------------------------------------------------------------
	protected void addKnowledgeClicked() {}
	protected void addLanguageClicked() {}

	//-------------------------------------------------------------------
	private void delClicked() {
		V sVal = table.getSelectedItem();
		logger.log(Level.DEBUG, "delClicked for {0}",sVal);
		boolean ret = charGen.getSkillController().deselect(sVal);
		logger.log(Level.DEBUG, "Add result was {0}", ret);
	}
	//-------------------------------------------------------------------
	private void delKnowledgeClicked() {
		V sVal = knowTable.getSelectedItem();
		logger.log(Level.DEBUG, "delClicked for {0}",sVal);
		boolean ret = charGen.getSkillController().deselect(sVal);
		logger.log(Level.DEBUG, "Add result was {0}", ret);
	}

	public BooleanProperty showAllSkillsProperty() { return tsShowAllSkills.selectedProperty(); }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		refreshTableLayout();
		refresh();
		wizard.requestLayout();
	}
}
