package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.public_skins.ImageSpinnerSkin;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;
import de.rpgframework.jfx.ADescriptionPane;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import de.rpgframework.jfx.attach.PDFViewerServiceFactory;
import de.rpgframework.shadowrun.MetaType;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.chargen.jfx.CommonShadowrunJFXResourceHook;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class MetatypePane<M extends MetaType> extends ADescriptionPane<M> {

	private final static ResourceBundle RES = ResourceBundle.getBundle(MetatypePane.class.getName());

	private final static Logger logger = System.getLogger(MetatypePane.class.getPackageName());

	private Map<MetaType, Image> cachedImage;

	private Spinner<M> spinner;

	protected GridPane gridAttrib;
	protected Map<ShadowrunAttribute,Label> ranges;
	private TextFlow tfQualities;
	private TextFlow description;

	private BiFunction<M,Modification,String> modResolver;
	private transient boolean lockSetting;

	private Function<M, Image> TO_IMG = new Function<M, Image>() {
		public Image apply(M key) {
			logger.log(Level.INFO, "-----------------------apply("+key+")");
			if (cachedImage.containsKey(key)) {
				logger.log(Level.INFO, "Image "+cachedImage.get(key).getWidth()+"x+"+cachedImage.get(key).getHeight());
				return cachedImage.get(key);
			}
			String imgFile = "images/metatypes/metatype_"+key.getId()+".jpg";
			if (key.getVariantOf()!=null) {
				imgFile = "images/metatypes/metatype_"+key.getVariantOf().getId()+"_"+key.getId()+".jpg";
			}
			InputStream ins = CommonShadowrunJFXResourceHook.class.getResourceAsStream(imgFile);
			if (ins==null) {
				logger.log(Level.WARNING, "Missing resource "+imgFile);
				return null;
			} else {
				Image img = new Image(ins);
				if (img.isError()) {
					logger.log(Level.ERROR, "Java failed to load "+imgFile);
					return null;
				}
//				logger.log(Level.WARNING, "Image "+img.getWidth()+"x+"+img.getHeight());
//				logger.log(Level.WARNING, "Image bg loading = "+img.isBackgroundLoading());
//				logger.log(Level.WARNING, "Image error      = "+img.isError());
				cachedImage.put(key, img);
				return img;
			}
		}
	};

	//-------------------------------------------------------------------
	public MetatypePane(BiFunction<M,Modification,String> modResolver) {
		getStyleClass().add("description-pane");
		getChildren().clear();
		this.modResolver = modResolver;
		cachedImage = new HashMap<>();
		initComponents();
		spinner.setStyle("-fx-background-color: transparent");
		//		spinner.setSkin(new ImageSpinnerSkin<SR6MetaType>(spinner));
		//		setItems(Shadowrun6Core.getItemList(SR6MetaType.class));

		// Ensure image is always half of available height
		spinner.prefHeightProperty().bind(heightProperty().divide(2.0));
		spinner.maxHeightProperty().bind(heightProperty().divide(2.0));
		spinner.prefWidthProperty().bind(widthProperty().subtract(10));
		spinner.maxWidthProperty().bind(widthProperty().subtract(10));

		initGrid();


		VBox perMetaDetails = new VBox(5, descTitle, descSources, gridAttrib, description);
		perMetaDetails.setMaxHeight(Double.MAX_VALUE);
		ScrollPane scroll = new ScrollPane(perMetaDetails);
		scroll.setFitToWidth(true);
		scroll.setMaxHeight(Double.MAX_VALUE);

		VBox.setVgrow(scroll, Priority.ALWAYS);
		getChildren().addAll(spinner, scroll);


	}

	//-------------------------------------------------------------------
	private void initComponents() {
		spinner = new Spinner<M>();
		ObjectProperty<StringConverter<M>> nameConv = new SimpleObjectProperty<>(new StringConverter<M>() {
			public M fromString(String arg0) {return null;}
			public String toString(M item) {
				return item.getName();
			}});
		spinner.setSkin(new ImageSpinnerSkin<M>(
				spinner,
				new SimpleObjectProperty<>(TO_IMG),
				nameConv,
				FXCollections.observableArrayList(),
				null
				));

		tfQualities = new TextFlow();
		gridAttrib = new GridPane();
		description = new TextFlow();
	}

	//-------------------------------------------------------------------
	public void setItems(List<M> list) {
		spinner.setValueFactory(new SpinnerValueFactory.ListSpinnerValueFactory<M>(FXCollections.observableArrayList(list)));
		spinner.getValueFactory().setWrapAround(true);
		spinner.getValueFactory().valueProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				setData(n);
			} else {
				descTitle.setText(null);
				descSources.setText(null);
			}
		});
	}

	//	//-------------------------------------------------------------------
	//	private void recalculateSize() {
	//		double maxHeight = getHeight()/2.0;
	//		double maxWidth = getWidth();
	//		double size = Math.min(maxHeight, maxWidth);
	//		logger.log(Level.INFO, "w="+maxWidth+"  h="+maxHeight+"  => "+size);
	//		spinner.setPrefSize(size, size);
	//		spinner.setMaxSize(size, size);
	//	}

	//-------------------------------------------------------------------
	private void initGrid() {
		ranges = new HashMap<>();
		//		gridAttrib.setStyle("-fx-background-color: #e9e9e2;");
		gridAttrib.getStyleClass().add("metatype-table");
		gridAttrib.setMaxWidth(Double.MAX_VALUE);
		int x=0;
		for (ShadowrunAttribute key : ShadowrunAttribute.primaryValuesPlusEdge()) {
			Label head = new Label(key.getShortName(Locale.getDefault()));
			head.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD);
			head.setMaxWidth(Double.MAX_VALUE);
			Label range = new Label();
			ranges.put(key, range);
			range.setMaxWidth(Double.MAX_VALUE);
			range.setTextAlignment(TextAlignment.CENTER);
			range.setAlignment(Pos.CENTER);
			range.getStyleClass().addAll(JavaFXConstants.STYLE_TABLE_DATA,"odd");
			gridAttrib.add(head, x,0);
			gridAttrib.add(range, x,1);
			GridPane.setHalignment(head, HPos.CENTER);
			GridPane.setHalignment(range, HPos.CENTER);
			GridPane.setFillWidth(head, true);
			GridPane.setFillWidth(range, true);
			GridPane.setHgrow(head, Priority.ALWAYS);
			GridPane.setHgrow(range, Priority.ALWAYS);
			x++;
		}

		gridAttrib.add(tfQualities, 0, 2, 9,1);

		GridPane.setMargin(tfQualities, new Insets(0,3,0,3));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.ADescriptionPane#setData(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public void setData(M value) {
		logger.log(Level.DEBUG, "setData: "+value);
		if (lockSetting)
			return;
		lockSetting = true;

		// Eventually open a PDF
		if (value != null) {
			Optional<PageReference> pageRefO = value.getPageReferences()
					.stream()
					.filter(pr -> pr.getLanguage().equals(Locale.getDefault().getLanguage()))
					.findFirst();
			if (pageRefO.isPresent()) {
				PageReference pageRef = pageRefO.get();
				PDFViewerServiceFactory.create().ifPresent(service -> {
					service.show(pageRef.getProduct().getRules(), pageRef.getProduct().getID(),
							pageRef.getLanguage(), pageRef.getPage());
				});
			}
		}
		try {
			if (spinner.getValue()!=value) {
				spinner.getValueFactory().setValue(value);
			}
			descTitle.setText(value.getName(Locale.getDefault()));
			descSources.setText(RPGFrameworkJavaFX.createSourceText(value));
			Label text2 = new Label(ResourceI18N.get(RES, "heading.qualities")+":  ");
			text2.getStyleClass().addAll(JavaFXConstants.STYLE_HEADING5);
			tfQualities.getChildren().setAll(text2);

			// Prepare min and max values
			Map<ShadowrunAttribute, Integer> min = new HashMap<ShadowrunAttribute, Integer>();
			Map<ShadowrunAttribute, Integer> max = new HashMap<ShadowrunAttribute, Integer>();
			for (ShadowrunAttribute key : ShadowrunAttribute.primaryValuesPlusEdge()) {
				min.put(key, 1);
				max.put(key, 6);
			}
			// Now learn min and max
			for (Modification tmp : value.getAttributeModifications()) {
				if (tmp instanceof ValueModification) {
					ValueModification mod = (ValueModification)tmp;
					ShadowrunAttribute attr = ShadowrunAttribute.valueOf(mod.getKey());
					if (mod.getSet()==ValueType.MAX) {
						max.put(attr, mod.getValue());
					} else if (mod.getSet()==ValueType.MIN) {
						min.put(attr, mod.getValue());
					} else
						logger.log(Level.WARNING, "TODO: set type "+mod.getSet()+" for "+tmp);
				}
			}
			// Now make labels
			for (ShadowrunAttribute key : ShadowrunAttribute.primaryValuesPlusEdge()) {
				Label range = ranges.get(key);
				range.setText(min.get(key)+"-"+max.get(key));
			}

			List<String> modTexts = new ArrayList<>();
			for (Modification tmp : value.getNonAttributeModifications()) {
				String resolved = modResolver.apply(value,tmp);
				if (resolved==null) {
					if (tmp instanceof DataItemModification) {
						resolved = tmp.getReferenceType()+":"+((DataItemModification)tmp).getKey();
					} else {
						resolved = tmp.getReferenceType()+":"+String.valueOf(tmp);
					}
				}
				modTexts.add(resolved);
			}
			Text text3 = new Text(String.join(", ", modTexts));
			text3.setStyle("-fx-fill: -fx-second-background");
			tfQualities.getChildren().add(text3);

			// Set descriptive text
			RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, value.getDescription(Locale.getDefault()));
		} finally {
			lockSetting = false;
		}
	}

}
