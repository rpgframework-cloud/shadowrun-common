package de.rpgframework.shadowrun.chargen.jfx;

import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import javafx.scene.control.Skin;

/**
 * @author prelle
 *
 */
public class PriorityAttributeTable<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, C extends ShadowrunCharacter<S,V,?,?>> extends ShadowrunAttributeTable<S,V,C>  {

	//-------------------------------------------------------------------
	public PriorityAttributeTable(IShadowrunCharacterController<S, V, ?,C> ctrl) {
		super(ctrl);
		expertModeAvailable.set(true);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Control#createDefaultSkin()
	 */
	public Skin<?> createDefaultSkin() {
		return new PriorityAttributeTableSkin(this);
	}

}
