package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.function.Function;

import org.prelle.javafx.ResponsiveControl;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.shadowrun.Ritual;
import de.rpgframework.shadowrun.RitualValue;
import de.rpgframework.shadowrun.chargen.charctrl.IRitualController;
import de.rpgframework.shadowrun.chargen.jfx.FilterRituals;
import javafx.scene.layout.Pane;

/**
 * @author prelle
 *
 */
public class RitualSelector extends Selector<Ritual, RitualValue> implements ResponsiveControl {

	//-------------------------------------------------------------------
	public RitualSelector(IRitualController ctrl, Function<Requirement,String> resolver, Function<Modification,String> modResolver) {
		super(ctrl, resolver, modResolver, new FilterRituals());
		listPossible.setCellFactory( lv -> new ComplexDataItemListCell<Ritual>());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.Selector#getDescriptionNode(de.rpgframework.genericrpg.data.ComplexDataItem)
	 */
	@Override
	protected Pane getDescriptionNode(Ritual selected) {
		genericDescr.setData(selected);
		return genericDescr;
	}

}
