package de.rpgframework.shadowrun.chargen.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.gen.KarmaAttributeGenerator;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class KarmaAttributeTableSkin extends SkinBase<KarmaAttributeTable<?,?,?>> {

	private final static Logger logger = System.getLogger(KarmaAttributeTableSkin.class.getPackageName()+".attrib");

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle
			.getBundle(ShadowrunAttributeTable.class.getName());

	private GridPane grid;

	private Map<ShadowrunAttribute, Label>  lblRec = new HashMap<>();
	private Map<ShadowrunAttribute, Label>  lblNam = new HashMap<>();

	private Map<ShadowrunAttribute, Button> btnDec = new HashMap<>();
	private Map<ShadowrunAttribute, Label>  lblAll = new HashMap<>();
	private Map<ShadowrunAttribute, Button> btnInc = new HashMap<>();
	private Map<ShadowrunAttribute, Label>  lblFin = new HashMap<>();
	private Map<ShadowrunAttribute, Label>  lblMax = new HashMap<>();

	private Map<ShadowrunAttribute, List<Control>> allPerAttr = new HashMap<>();

	private MapChangeListener<Object, Object> propertiesMapListener = c -> {
        if (! c.wasAdded()) return;
//        if (SkinProperties.WINDOW_MODE.equals(c.getKey())) {
//            updateLayout();
//            getSkinnable().requestLayout();
//            getSkinnable().getProperties().remove(SkinProperties.WINDOW_MODE);
//        }
        if (SkinProperties.REFRESH.equals(c.getKey())) {
            refresh();
            getSkinnable().getProperties().remove(SkinProperties.REFRESH);
        }
    };

    private transient boolean refreshing;

	//-------------------------------------------------------------------
	public KarmaAttributeTableSkin(KarmaAttributeTable<?,?,?> control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private KarmaAttributeGenerator getController() {
		if (getSkinnable().getController()==null) return null;
		return (KarmaAttributeGenerator) getSkinnable().getController().getAttributeController();
	}

	//-------------------------------------------------------------------
	private Button createButton(Map<ShadowrunAttribute,Button> map,
			ShadowrunAttribute key, String icon, int x, int y) {
		Button btn = new Button(null, new SymbolIcon(icon));
		map.put(key, btn);
		GridPane.setMargin(btn, new Insets(5,10,5,10));

		grid.add(btn, x, y);

		// In allPer
		List<Control> list = allPerAttr.get(key);
		if (list==null) {
			list = new ArrayList<>();
			allPerAttr.put(key, list);
		}
		list.add(btn);
		return btn;
	}

	//-------------------------------------------------------------------
	private Label createLabel(Map<ShadowrunAttribute,Label> map,
			ShadowrunAttribute key, int x, int y) {
		Label lab = new Label("?");
		lab.setMaxWidth(Double.MAX_VALUE);
		lab.setMaxHeight(Double.MAX_VALUE);
		lab.setAlignment(Pos.CENTER);
		map.put(key, lab);
//		GridPane.setMargin(lab, new Insets(0,5,0,5));
		grid.add(lab, x, y);

		// In allPer
		List<Control> list = allPerAttr.get(key);
		if (list==null) {
			list = new ArrayList<>();
			allPerAttr.put(key, list);
		}
		list.add(lab);

		return lab;
	}

	//-------------------------------------------------------------------
	private void createName(Map<ShadowrunAttribute,Label> map,
			ShadowrunAttribute key, int x, int y) {
		Label lab = new Label(key.getName());
		lab.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		map.put(key, lab);

		grid.add(lab, x, y);

		// In allPer
		List<Control> list = allPerAttr.get(key);
		if (list==null) {
			list = new ArrayList<>();
			allPerAttr.put(key, list);
		}
		list.add(lab);
	}

	//-------------------------------------------------------------------
	private void createRecomLabel(Map<ShadowrunAttribute,Label> map,
			ShadowrunAttribute key, int x, int y) {
		Label lab = new Label(null, new SymbolIcon("favorite"));
		map.put(key, lab);


		// In allPer
		List<Control> list = allPerAttr.get(key);
		if (list==null) {
			list = new ArrayList<>();
			allPerAttr.put(key, list);
		}
		list.add(lab);
		grid.add(lab, x, y);
	}

	//-------------------------------------------------------------------
	private Label createHeading(String key, int x, int y, int span) {
		Label lab = new Label( (key!=null)?ResourceI18N.get(RES, key):"");
		lab.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD);
		lab.setStyle("-fx-padding: 2px");
		lab.setMaxWidth(Double.MAX_VALUE);
		grid.add(lab, x, y, span,1);
		return lab;
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		grid = new GridPane();
//		grid.setVgap(5);
		grid.setGridLinesVisible(false);

		int y=0;
		createHeading("head.attribute", 1, y, 1);
		createHeading(null         , 2, y, 1);
		createHeading("head.karma" , 3, y, 1);
		createHeading(null         , 4, y, 1);
		createHeading("head.result", 5, y, 1);
		createHeading("head.max"   , 6, y, 1);
		y++;


		for (ShadowrunAttribute key : ShadowrunAttribute.primaryAndSpecialValues()) {
			int x=0;
			createRecomLabel(lblRec, key, x++, y);
			createName(lblNam, key, x++, y);

			createButton(btnDec, key, "remove", x++, y);
			createLabel (lblAll, key, x++, y);
			createButton(btnInc, key, "add", x++, y);

			createLabel (lblFin, key, x++, y);
			createLabel (lblMax, key, x++, y);

			y++;
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		logger.log(Level.DEBUG, "updateLayoutSimple");
		boolean showMagic  = getSkinnable().isShowMagic();
		boolean showReson  = getSkinnable().isShowResonance();

		lblNam.get(ShadowrunAttribute.MAGIC).setVisible(showMagic);
		lblNam.get(ShadowrunAttribute.RESONANCE).setVisible(showReson);
		grid.getColumnConstraints().add(new ColumnConstraints(15,30,30));
		grid.getColumnConstraints().add(new ColumnConstraints(80,150,200));
		grid.getColumnConstraints().add(new ColumnConstraints()); // Dec
		grid.getColumnConstraints().add(new ColumnConstraints(50)); // Value

//		grid.setGridLinesVisible(true);
		getChildren().add(grid);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private AttributeValue<ShadowrunAttribute> value(ShadowrunAttribute key) {
		return ((ShadowrunCharacter)getController().getModel()).getAttribute(key);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
//		getSkinnable().useExpertModeProperty().addListener( (ov,o,n) -> updateLayout());

		btnDec.entrySet().forEach(e -> e.getValue().setOnAction(ev -> {getController().decrease(value(e.getKey())); refresh();}));
		btnInc.entrySet().forEach(e -> e.getValue().setOnAction(ev -> {getController().increase(value(e.getKey())); refresh();}));

		getSkinnable().showMagicProperty().addListener( (ov,o,n) -> {
			if (refreshing) return;
			for (Control ctrl : allPerAttr.get(ShadowrunAttribute.MAGIC)) {
				ctrl.setVisible(true);
				ctrl.setManaged(true);
			}
//			updateLayout();
		});
		getSkinnable().showResonanceProperty().addListener( (ov,o,n) -> {
			if (refreshing) return;
			for (Control ctrl : allPerAttr.get(ShadowrunAttribute.RESONANCE)) {
				ctrl.setVisible(true);
				ctrl.setManaged(true);
			}
//			updateLayout();
		});


        final ObservableMap<Object, Object> properties = getSkinnable().getProperties();
        properties.remove(SkinProperties.REFRESH);
        properties.remove(SkinProperties.WINDOW_MODE);
        properties.addListener(propertiesMapListener);
	}

	//-------------------------------------------------------------------
	private void updateAttributeNames() {
		// Eventually update names
		for (ShadowrunAttribute key : ShadowrunAttribute.primaryAndSpecialValues()) {
			Label label = lblNam.get(key);
			if (label==null) continue;
			if (getSkinnable().getNameMapper()!=null) {
				String mapped = getSkinnable().getNameMapper().apply(key);
				if (mapped!=null)
					label.setText(mapped);
			} else {
				label.setText(key.getName(Locale.getDefault()));
			}
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void refresh() {
		logger.log(Level.WARNING, "refresh with "+grid.getChildrenUnmodifiable().size()+" children");
		if (refreshing) return;
		if (getSkinnable().getController()==null) return;
		ShadowrunCharacter model = (ShadowrunCharacter) getSkinnable().getController().getModel();
		//logger.log(Level.WARNING, "+++++++++++++++++++++++++++++"+model.getChargenSettingsJSON());

		refreshing = true;
		try {
			boolean showMagic  = getSkinnable().isShowMagic();
			boolean showReson  = getSkinnable().isShowResonance();
			// Hide if not needed
			allPerAttr.get(ShadowrunAttribute.MAGIC).forEach(ctrl -> {ctrl.setVisible(showMagic); ctrl.setManaged(showMagic);});
			allPerAttr.get(ShadowrunAttribute.RESONANCE).forEach(ctrl -> {ctrl.setVisible(showReson); ctrl.setManaged(showReson);});

			lblRec.entrySet().forEach(e -> {
				if (getController()==null) return;
				RecommendationState state = getController().getRecommendationState(e.getKey());
				e.getValue().setVisible(state!=null && state!=RecommendationState.NEUTRAL);
			});

			btnDec.entrySet().forEach(e -> e.getValue().setDisable(!getController().canBeDecreased(value(e.getKey())).get()));
			btnInc.entrySet().forEach(e -> e.getValue().setDisable(!getController().canBeIncreased(value(e.getKey())).get()));

			updateAttributeNames();

			for (ShadowrunAttribute key : ShadowrunAttribute.primaryAndSpecialValues()) {
				AttributeValue<ShadowrunAttribute> val = model.getAttribute(key);

				lblAll.get(key).setText(String.valueOf(val.getDistributed()));
				if (val.getPool()==null) {
					lblFin.get(key).setText("?");
					logger.log(Level.WARNING, "No pool for attribute {0}", key);
				} else {
					lblFin.get(key).setText(val.getPool().toString());
					lblFin.get(key).setTooltip(new Tooltip(val.getPool().toExplainString()));
				}
				lblMax.get(key).setText(String.valueOf(val.getMaximum()));
			}
//			if (!(((ShadowrunCharacter)getController().getModel()).getCharGenSettings(Object.class) instanceof IKarmaSettings)) {
//				logger.log(Level.ERROR, "Character doesn't have AKarmaSettings");
//				return;
//			}
//			IKarmaSettings settings = (IKarmaSettings) model.getCharGenSettings(IKarmaSettings.class);
		} finally {
			refreshing = false;
		}
	}

}
