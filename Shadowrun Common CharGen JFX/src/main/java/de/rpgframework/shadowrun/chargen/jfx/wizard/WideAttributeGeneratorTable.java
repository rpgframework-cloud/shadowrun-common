package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger.Level;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.MappedNumericalValue;
import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.jfx.NumericalValueField;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.gen.IPrioritySettings;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import de.rpgframework.shadowrun.chargen.gen.PerAttributePoints;
import de.rpgframework.shadowrun.chargen.gen.PriorityAttributeGenerator;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.TextAlignment;

/**
 * @author prelle
 *
 */
public class WideAttributeGeneratorTable<C extends ShadowrunCharacter<?,?,?,?>> extends GridPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WideAttributeGeneratorTable.class.getName());

	private IShadowrunCharacterGenerator<?,?,?,C>  charGen;
	private C    model;
	private IPrioritySettings     settings;
	private ObjectProperty<NumericalValueController<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>>> ctrlProperty;

	private Label headAttr, headAdjust, headPoints, headKarma, headValue, headMax;
	private Map<ShadowrunAttribute, NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>>> adjust;
	private Map<ShadowrunAttribute, NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>>> normal;
	private Map<ShadowrunAttribute, NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>>> karma;
	private Map<ShadowrunAttribute, Label> distributed;
	private Map<ShadowrunAttribute, Label> maxValue;

	//--------------------------------------------------------------------
	public WideAttributeGeneratorTable(IShadowrunCharacterGenerator<?,?,?,C> charGen, int x) {
		if (charGen==null) throw new NullPointerException("Control not set");
		this.charGen = charGen;
		this.model   = charGen.getModel();
		this.settings= (IPrioritySettings) model.getCharGenSettings(IPrioritySettings.class);
		PriorityAttributeGenerator control = (PriorityAttributeGenerator) charGen.getAttributeController();
		this.ctrlProperty = new SimpleObjectProperty<>(control);

		adjust      = new HashMap<ShadowrunAttribute, NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>>>();
		normal      = new HashMap<ShadowrunAttribute, NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>>>();
		karma       = new HashMap<ShadowrunAttribute, NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>>>();
		distributed = new HashMap<ShadowrunAttribute, Label>();
		maxValue    = new HashMap<ShadowrunAttribute, Label>();

		doInit();
		doLayout();
		doInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void doInit() {
		super.setVgap(2);
		super.setMaxWidth(Double.MAX_VALUE);

		headAttr   = new Label(ResourceI18N.get(UI,"label.attribute"));
		headAdjust = new Label(ResourceI18N.get(UI,"label.adjust"));
		headPoints = new Label(ResourceI18N.get(UI,"label.points"));
		headKarma  = new Label(ResourceI18N.get(UI,"label.karma"));
		headValue  = new Label(ResourceI18N.get(UI,"label.value"));
		headMax    = new Label(ResourceI18N.get(UI,"label.maximum"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headAdjust.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headPoints.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headMax.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headKarma.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		headAttr  .getStyleClass().add("table-head");
		headAdjust.getStyleClass().add("table-head");
		headPoints.getStyleClass().add("table-head");
		headKarma .getStyleClass().add("table-head");
		headValue .getStyleClass().add("table-head");
		headMax   .getStyleClass().add("table-head");

		headAdjust.setAlignment(Pos.CENTER);
		headPoints.setAlignment(Pos.CENTER);
		headKarma .setAlignment(Pos.CENTER);
		headValue.setAlignment(Pos.CENTER);
		headAdjust.setTextAlignment(TextAlignment.CENTER);
		headPoints.setTextAlignment(TextAlignment.CENTER);
		headKarma .setTextAlignment(TextAlignment.CENTER);

		for (final ShadowrunAttribute attr : ShadowrunAttribute.primaryAndSpecialValues()) {
			if (attr==ShadowrunAttribute.ESSENCE)
				continue;
			model.getAttribute(attr);
			AttributeValue<ShadowrunAttribute> bla = charGen.getModel().getAttribute(attr);

			// Adjustment points
			NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>> adjVal =
					new NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>>(()->new MappedNumericalValue<ShadowrunAttribute>(attr, ()->settings.perAttrib().get(attr).points1));

//					new NumericalValueField<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>>(charGen.getModel().getAttribute(attr), ctrlProperty, 1, true);
//			adjVal.setValueFactory(() -> settings.perAttrib().get(attr).points1);
			// ShadowrunAttribute points
			NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>> norVal =
					new NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>>(()->new MappedNumericalValue<ShadowrunAttribute>(attr, ()->settings.perAttrib().get(attr).points2));
//			NumericalValueField<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>> norVal =
//					new NumericalValueField<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>>(charGen.getModel().getAttribute(attr), ctrlProperty, 2, true);
//			norVal.setValueCallback(aVal -> settings.perAttrib().get(aVal.getModifyable()).points2);
			// Karma
			NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>> karVal =
					new NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>>(()->new MappedNumericalValue<ShadowrunAttribute>(attr, ()->settings.perAttrib().get(attr).points3));
//			NumericalValueField<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>> karVal =
//					new NumericalValueField<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>>(charGen.getModel().getAttribute(attr) , ctrlProperty, 7, true);
//			karVal.setValueCallback(aVal -> settings.perAttrib().get(aVal.getModifyable()).points3);
			karVal.setConverter( aVal -> {
				PerAttributePoints per = settings.perAttrib().get(aVal.getModifyable());
				if (per.getKarmaInvest()==0)
					return "-";
				String ret = per.getSumBeforeKarma() + "\u2192"+per.getSum()+"("+per.getKarmaInvest()+")";
				return ret;
			});


			Label value= new Label();
			value.setAlignment(Pos.CENTER);
			value.getStyleClass().add("result");
			Label maxVal= new Label();
			maxVal.setAlignment(Pos.CENTER);
			maxVal.getStyleClass().add("result");

			normal     .put(attr, norVal);
			adjust     .put(attr, adjVal);
			karma      .put(attr, karVal);
			maxValue   .put(attr, maxVal);
			distributed.put(attr, value);
		}
	}

	//-------------------------------------------------------------------
	private void doLayout() {
		setVgap(10);
		this.add(headAttr  , 0,0);
		this.add(headAdjust, 1,0);
		this.add(headPoints, 2,0);
		this.add(headKarma , 3,0);
		this.add(headValue , 4,0);
		this.add(headMax   , 5,0);

		int y=0;
		for (final ShadowrunAttribute attr : ShadowrunAttribute.primaryAndSpecialValues()) {
			if (attr==ShadowrunAttribute.ESSENCE)
				continue;
			y++;
			Label longName  = new Label(attr.getName());
			NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>> adjVal = adjust.get(attr);
			NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>> norVal    = normal.get(attr);
			NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>> karVal    = karma.get(attr);
			Label points    = distributed.get(attr);
			Label maxVal    = maxValue.get(attr);

			String lineStyle = ((y%2)==1)?"even":"odd";
			GridPane.setVgrow(longName, Priority.ALWAYS);
			longName .getStyleClass().add(lineStyle);
			points.getStyleClass().add(lineStyle);
			maxVal.getStyleClass().add(lineStyle);
			adjVal.getStyleClass().add(lineStyle);
			norVal.getStyleClass().add(lineStyle);
			karVal.getStyleClass().add(lineStyle);

			longName.setUserData(attr);

			longName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			norVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			adjVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			points.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			karVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			maxVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

			this.add(   longName, 0, y);
			this.add(     adjVal, 1, y);
			this.add(     norVal, 2, y);
			this.add(     karVal, 3, y);
			this.add(     points, 4, y);
			this.add(     maxVal, 5, y);

			GridPane.setMargin(adjVal, new Insets(0,10,0,10));
			GridPane.setMargin(norVal, new Insets(0,10,0,10));
			GridPane.setMargin(karVal, new Insets(0,10,0,10));
			GridPane.setMargin(points, new Insets(0,10,0,10));
			// Add extra line after CHARISMA
			if (attr==ShadowrunAttribute.CHARISMA)
				y++;
		}
	}

	//-------------------------------------------------------------------
	private void doInteractivity() {
//		for (final ShadowrunAttribute attr : ShadowrunAttribute.primaryAndSpecialValues()) {
//			if (attr==Attribute.ESSENCE)
//				continue;
//			NumericalValueField<Attribute, AttributeValue> field = distributed.get(attr);
//			field.inc.setOnAction(new EventHandler<ActionEvent>() {
//				public void handle(ActionEvent event) {
//					control.increase(attr);
//				}
//			});
//			field.dec.setOnAction(new EventHandler<ActionEvent>() {
//				public void handle(ActionEvent event) {
//					control.decrease(attr);
//				}
//			});
//		}
	}

	//-------------------------------------------------------------------
	public void updateCharacterGenerator(IShadowrunCharacterGenerator<?, ?, ?, C> charGen) {
		this.charGen = charGen;
		refresh();
	}

	//-------------------------------------------------------------------
	private void refresh() {
		for (final ShadowrunAttribute attr : ShadowrunAttribute.primaryAndSpecialValues()) {
			if (attr==ShadowrunAttribute.ESSENCE)
				continue;
			NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>> adjVal  = adjust.get(attr);
			NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>> norVal  = normal.get(attr);
			NumericalValueField<ShadowrunAttribute, MappedNumericalValue<ShadowrunAttribute>> karVal  = karma .get(attr);
			Label field = distributed.get(attr);
			Label max_l = maxValue.get(attr);

			adjVal.setVisible(charGen.getAttributeController().isRacialAttribute(attr)  || attr.isSpecial());
			norVal.setVisible(attr.isPrimary());

			PerAttributePoints perAttrib = settings.perAttrib().get(attr);
			adjVal.refresh();
			norVal.refresh();
			karVal.refresh();

			AttributeValue<ShadowrunAttribute> data = model.getAttribute(attr);
			if (System.getLogger("shadowrun6.jfx").isLoggable(Level.TRACE))
				System.getLogger("shadowrun6.jfx").log(Level.TRACE,"data = "+data+" / "+data.getDistributed()+" /"+data.getIncomingModifications()+" \t=== "+perAttrib);

//			field.setText(Integer.toString(data.getModifiedValue()));
			field.setText(String.valueOf(perAttrib.getSum()));
			max_l.setText(String.valueOf(data.getMaximum()));
			//			start_l.setText(Integer.toString(data.getStart()));
//			cost_l .setText(Integer.toString(control.getIncreaseCost(attr)));
//			field.inc.setDisable(!control.canBeIncreased(attr));
//			field.dec.setDisable(!control.canBeDecreased(attr));
		}
	}

}
