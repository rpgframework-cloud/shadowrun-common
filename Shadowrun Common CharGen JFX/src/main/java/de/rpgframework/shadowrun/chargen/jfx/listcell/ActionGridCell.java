package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.util.function.Function;

import org.controlsfx.control.GridCell;

import de.rpgframework.shadowrun.ShadowrunAction;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author stefa
 *
 */
public class ActionGridCell<A extends ShadowrunAction> extends GridCell<A> {
	
	private Function<ShadowrunAction, Node> resolver;

	private Label lbName;
	private HBox layout;
	
	public ActionGridCell(Function<ShadowrunAction, Node> valueNodeResolver) {
		this.resolver = valueNodeResolver;
		lbName = new Label();
		layout = new HBox(5, lbName);
		HBox.setHgrow(lbName, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(A item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setGraphic(null);
			setText(null);
		} else {
			lbName.setText(item.getName());
			if (resolver!=null) {
				Node node = resolver.apply(item);
				if (node!=null)
					layout.getChildren().setAll(lbName, node);
				else
					layout.getChildren().setAll(lbName);
			} else
				layout.getChildren().setAll(lbName);
			setGraphic(layout);
		}
	}
}
