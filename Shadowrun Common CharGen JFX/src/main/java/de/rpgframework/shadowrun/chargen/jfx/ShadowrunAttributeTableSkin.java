package de.rpgframework.shadowrun.chargen.jfx;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.shadowrun.ShadowrunAttribute;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class ShadowrunAttributeTableSkin extends SkinBase<ShadowrunAttributeTable> {

	private GridPane grid;
	
	//-------------------------------------------------------------------
	public ShadowrunAttributeTableSkin(ShadowrunAttributeTable control) {
		super(control);
		initComponents();
		initLayout();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		grid = new GridPane();
		grid.setVgap(5);
		grid.setHgap(10);
		
		int y=0;
		for (ShadowrunAttribute key : ShadowrunAttribute.primaryAndSpecialValues()) {
			Label name = new Label(key.getName());
			name.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
			
			grid.add(name, 0,y);
			grid.add(new Label("NormalSkin"), 1,y);
			
			y++;
		}
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		getChildren().add(grid);		
	}

}
