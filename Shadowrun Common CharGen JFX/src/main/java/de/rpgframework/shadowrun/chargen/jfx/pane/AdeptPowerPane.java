package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.shadowrun.AdeptPower;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class AdeptPowerPane extends GenericDescriptionVBox {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(ComplexFormDescriptionPane.class.getPackageName()+".DescriptionPanes");

	private Label lbCost;
	private Label lbActivation;
	private Label hdActivation  ;

	//-------------------------------------------------------------------
	public AdeptPowerPane(Function<Requirement,String> requirementResolver, Function<Modification,String> mResolver) {
		super(requirementResolver, mResolver);
		initLocalComponents();
		initLocalLayout();
	}

	public AdeptPowerPane(Function<Requirement, String> requirementResolver, Function<Modification,String> mResolver, DataItem item) {
		super(requirementResolver, mResolver, item);
		initLocalComponents();
		initLocalLayout();
	}

	//-------------------------------------------------------------------
	private void initLocalComponents() {
		lbCost = new Label();
		lbActivation = new Label();
	}

	//-------------------------------------------------------------------
	private void initLocalLayout() {
		Label hdCost = new Label(ResourceI18N.get(RES, "adeptpower.cost")+":");
		hdActivation  = new Label(ResourceI18N.get(RES, "adeptpower.activation")+":");
		hdCost.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdActivation.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		HBox lineCost = new HBox(5, hdCost, lbCost);
		HBox lineAct  = new HBox(5, hdActivation, lbActivation);

		super.inner.getChildren().add(0, lineCost);
		super.inner.getChildren().add(1, lineAct);
	}

	//-------------------------------------------------------------------
	public void setData(AdeptPower data) {
		super.setData(data);

		lbCost.setText(data.getCostString(Locale.getDefault()));
		if (data.getActivation()!=null) {
			lbActivation.setText(data.getActivation().getName());
			hdActivation.setVisible(true);
			hdActivation.setManaged(true);
			lbActivation.setVisible(true);
			lbActivation.setManaged(true);
		} else {
			hdActivation.setVisible(true);
			hdActivation.setManaged(true);
			lbActivation.setVisible(true);
			lbActivation.setManaged(true);
		}
	}

}
