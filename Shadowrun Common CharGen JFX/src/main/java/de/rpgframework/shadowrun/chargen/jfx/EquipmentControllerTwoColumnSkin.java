package de.rpgframework.shadowrun.chargen.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.javafx.AlertManager;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.FlexibleApplication;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.genericrpg.items.Usage;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.ComplexDataItemControllerTwoColumnSkin;
import de.rpgframework.shadowrun.chargen.charctrl.IEquipmentController;
import javafx.application.Platform;

@SuppressWarnings("rawtypes")
public class EquipmentControllerTwoColumnSkin<T extends PieceOfGear> extends ComplexDataItemControllerTwoColumnSkin<T, CarriedItem<T>> {

	//-------------------------------------------------------------------
	protected final static Logger logger = System.getLogger(EquipmentControllerTwoColumnSkin.class.getPackageName());

	public EquipmentControllerTwoColumnSkin(ComplexDataItemControllerNode<T, CarriedItem<T>> control) {
		super(control);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked" })
	@Override
	protected void userSelects(T toSelect) {
		logger.log(Level.INFO, "userSelects({0})", toSelect);
		Usage prim = toSelect.getUsage(CarryMode.CARRIED);
		Usage secnd= toSelect.getUsage(CarryMode.IMPLANTED);
		if (prim==null && toSelect.getVariant(CarryMode.CARRIED)!=null) prim = toSelect.getVariant(CarryMode.CARRIED).getUsage(CarryMode.CARRIED);
		if (secnd==null && toSelect.getVariant(CarryMode.IMPLANTED)!=null) secnd = toSelect.getVariant(CarryMode.IMPLANTED).getUsage(CarryMode.IMPLANTED);

		CarryMode carry = (prim!=null)?prim.getMode():(secnd!=null)?secnd.getMode():null;
		logger.log(Level.DEBUG, "Assume mode {0}", carry);
		if (carry==null) {
			logger.log(Level.ERROR, "Unsupported carry mode {0} for {1}", toSelect.getUsages(), toSelect.getId());
			AlertManager.showAlertAndCall(javafx.scene.control.Alert.AlertType.ERROR, "Failed adding", "Cannot add non-carried and non-implanted gear here");
			return;
		}

		IEquipmentController ctrl = (IEquipmentController) getSkinnable().getController();
		Possible possible = ctrl.canBeSelected(toSelect,null,carry);
		logger.log(Level.DEBUG, "possible = "+possible);
		if (possible.get()) {
			// Is there a need for a selection
			logger.log(Level.DEBUG, "ctrl = " + ctrl);
			if (!ctrl.getChoicesToDecide(toSelect).isEmpty()) {
				// Yes, user must choose
				List<Choice> options = ctrl.getChoicesToDecide(toSelect);
				logger.log(Level.DEBUG, "called getChoicesToDecide returns {0} choices", options.size());
				if (getSkinnable().getOptionCallback() != null) {
					Platform.runLater(() -> {
						logger.log(Level.DEBUG, "call getOptionCallback");

						Decision[] decisions = getSkinnable().getOptionCallback().apply(toSelect, options);
						if (decisions != null) {
							logger.log(Level.WARNING, "call select(option, decision[{0}])", decisions.length);
							String variant = null;
							for (Decision dec : decisions) {
								if (dec.getChoiceUUID()==PieceOfGear.VARIANT) {
									variant = dec.getValue();
									System.err.println("Variant is "+variant);
								}
							}
							OperationResult<CarriedItem<T>> res = ctrl.select(toSelect, variant, carry, decisions);
							if (res.wasSuccessful()) {
								logger.log(Level.INFO, "Selecting {0} with options was successful", toSelect);
							} else {
								logger.log(Level.WARNING, "Selecting {0} with options failed: {1}", toSelect, res.getError());
								AlertManager.showAlertAndCall(javafx.scene.control.Alert.AlertType.ERROR, "Failed adding", res.getError());
							}
						}
					});
				} else {
					logger.log(Level.ERROR, "Item {0} has choices to make, but no GUI callback defined", toSelect.getId());
				}
			} else {
				// No
				logger.log(Level.DEBUG, "call select(option)");
				OperationResult<CarriedItem<T>> res = ctrl.select(toSelect);
				if (res.wasSuccessful()) {
					logger.log(Level.INFO, "Selecting {0} was successful", toSelect);
				} else {
					logger.log(Level.WARNING, "Selecting {0} failed: {1}", toSelect, res.getError());
					AlertManager.showAlertAndCall(javafx.scene.control.Alert.AlertType.ERROR, "Failed adding", res.getError());
				}
			}
		} else {
			logger.log(Level.DEBUG, "can not be Selected(" + toSelect + "): " + possible.getI18NKey());

    		FlexibleApplication.getInstance().showAlertAndCall(AlertType.NOTIFICATION, "Selection failed", possible.toString());
    	}
    }

}
