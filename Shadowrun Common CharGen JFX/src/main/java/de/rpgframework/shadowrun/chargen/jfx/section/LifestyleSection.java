package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Supplier;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.jfx.section.ListSection;
import de.rpgframework.shadowrun.Lifestyle;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.ILifestyleController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.jfx.dialog.EditLifestyleValueDialog;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class LifestyleSection<L extends Lifestyle> extends ListSection<L> implements IShadowrunCharacterControllerProvider<IShadowrunCharacterController> {

	private final static Logger logger = System.getLogger(LifestyleSection.class.getPackageName());

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(LifestyleSection.class.getPackageName()+".Section");

	private IShadowrunCharacterController control;
	private ShadowrunCharacter model;
	private Supplier<L> creator;

	private Label lbTotal;

	//-------------------------------------------------------------------
	public LifestyleSection(String title, Supplier<L> creator) {
		super(title);
		this.creator = creator;

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list.setStyle("-fx-min-height: 10em; -fx-pref-height: 30em; -fx-pref-width: 25em");
//		list.setCellFactory(cell -> new LifestyleListCell(control.getLifestyleController()));

		lbTotal = new Label("?");
		lbTotal.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		Label hdTotal = new Label(ResourceI18N.get(RES,"section.lifestyle.total"));
		HBox header = new HBox(10, hdTotal, lbTotal);
		setHeaderNode(header);
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				btnDel.setDisable(true);
			} else {
//				btnDel.setDisable( !control.getLifestyleController().canDeleteSIN(n));
			}
		});
		btnAdd.setOnAction(ev -> onAdd());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	@Override
	public IShadowrunCharacterController getCharacterController() {
		return control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void refresh() {
		logger.log(Level.TRACE, "refresh");

		if (model!=null) {
			setData(model.getLifestyles());
			AttributeValue<ShadowrunAttribute> aVal = model.getAttribute(ShadowrunAttribute.MONTHLY_COST);
			lbTotal.setText(aVal!=null ? String.valueOf(aVal.getModifiedValue()) : "?");
			lbTotal.setTooltip(new Tooltip(aVal.getPool().toExplainString()));
		} else
			setData(new ArrayList<>());
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected void onAdd() {
		logger.log(Level.DEBUG, "opening contact selection dialog");

		ILifestyleController<L> ctrl = control.getLifestyleController();

		EditLifestyleValueDialog<L> dialog = new EditLifestyleValueDialog<L>(control, creator.get(), false);
		CloseType closed = FlexibleApplication.getInstance().showAlertAndCall(dialog, dialog.getButtonControl());
		if (closed==CloseType.OK) {
			logger.log(Level.ERROR, "User chose a Lifestyle to add");
			OperationResult<L> result = ctrl.select(dialog.getSelectedLifeStyle());
			if (result==null || result.hasError()) {
				logger.log(Level.ERROR, "Adding a lifestyle failed");
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, result.toString());
				return;
			}
			//refresh();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(L item) {
		logger.log(Level.DEBUG, "onDelete "+item);
		if (control.getLifestyleController().deselect(item)) {
			list.getItems().remove(item);
		}
	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (ShadowrunCharacter) ctrl.getModel();
		refresh();
	}

}
