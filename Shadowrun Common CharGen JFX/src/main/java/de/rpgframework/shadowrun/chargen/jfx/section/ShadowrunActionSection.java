package de.rpgframework.shadowrun.chargen.jfx.section;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.controlsfx.control.GridCell;
import org.controlsfx.control.GridView;
import org.prelle.javafx.Section;

import de.rpgframework.ResourceI18N;
import de.rpgframework.shadowrun.ShadowrunAction;
import de.rpgframework.shadowrun.chargen.jfx.listcell.ActionGridCell;
import javafx.scene.Node;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SingleSelectionModel;
import javafx.util.Callback;

/**
 * @author stefa
 *
 */
public class ShadowrunActionSection extends Section {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(ShadowrunActionSection.class.getPackageName()+".Section");
	
	protected GridView<ShadowrunAction> grid;
	private SelectionModel<ShadowrunAction> selectionModel = new SingleSelectionModel<ShadowrunAction>() {
		protected ShadowrunAction getModelItem(int index) {
			if (index==-1) return null;
			return grid.getItems().get(index);
		}
		protected int getItemCount() {
			return grid.getItems().size();
		}
	};
	
	/** Returns a node with value for test and eventually additional information */
	private Function<ShadowrunAction, Node> valueNodeResolver;

	//-------------------------------------------------------------------
	public ShadowrunActionSection(Function<ShadowrunAction, Node> resolver) {
		this.valueNodeResolver = resolver;
		setTitle(ResourceI18N.get(RES, "section.matrixactions.title"));
		initComponents();
		setContent(grid);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		grid = new GridView<ShadowrunAction>();
		grid.setCellWidth(150);
		grid.setCellHeight(20);
		grid.setVerticalCellSpacing(3);
		grid.setCellFactory( gv -> new ActionGridCell<ShadowrunAction>(valueNodeResolver));
	}

	//-------------------------------------------------------------------
	public void setCellFactory(Callback<GridView<ShadowrunAction>, GridCell<ShadowrunAction>> value) {
		grid.setCellFactory(value);
	}

	//-------------------------------------------------------------------
	public SelectionModel<ShadowrunAction> getSelectionModel() {
		return selectionModel;
	}

	//-------------------------------------------------------------------
	public void setAll(List<ShadowrunAction> data) {
		grid.getItems().setAll(data);		
	}

}
