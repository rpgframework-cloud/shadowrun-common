package de.rpgframework.shadowrun.chargen.jfx.listcell;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.shadowrun.LicenseValue;
import de.rpgframework.shadowrun.SIN;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
@SuppressWarnings("rawtypes")
public class LicenseValueListCell extends ListCell<LicenseValue> {
	
	private Label  lbName;
	private Label  lbRtg;
	private Label  lbSIN;
	
	private boolean withSIN;
	private HBox layout;
	private IShadowrunCharacterController<?, ?, ?, ?>  control;

	//-------------------------------------------------------------------
	public LicenseValueListCell(boolean withSIN, IShadowrunCharacterController<?,?,?,?> control) {
		this.withSIN = withSIN;
		this.control   = control;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbName = new Label();
		lbName.getStyleClass().add("base");
		lbRtg = new Label();
		lbName.setWrapText(true);
		lbSIN = new Label();
		lbSIN.getStyleClass().add(JavaFXConstants.STYLE_TEXT_SECONDARY);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.layout = new HBox(lbName, lbRtg);
		this.layout.setAlignment(Pos.CENTER_LEFT);
		this.layout.setStyle("-fx-spacing: 0.2em");
		HBox.setHgrow(lbName, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	public void updateItem(LicenseValue item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			if (withSIN) {
				VBox tmp = new VBox(2,layout, lbSIN);
				tmp.setFillWidth(true);
				setGraphic(tmp);
			} else {
				setGraphic(layout);
			}
			
			lbName.setText(item.getName());
			lbRtg.setText(String.valueOf(item.getRating().getValue()));
			lbSIN.setText("-");				
			if (item.getSIN()!=null) {
				SIN sin = null;
				for (SIN tmp : control.getModel().getSINs()) {
					if (tmp.getUniqueId().equals(item.getSIN())) {
						sin = tmp;
						break;
					}
				}
				if (sin!=null)
					lbSIN.setText(sin.getName());				
			}
		}
	}

}
