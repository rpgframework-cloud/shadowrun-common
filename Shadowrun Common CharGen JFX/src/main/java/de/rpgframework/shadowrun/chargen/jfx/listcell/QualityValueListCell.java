package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.jfx.cells.ComplexDataItemValueListCell;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.QualityValue;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

public class QualityValueListCell extends ComplexDataItemValueListCell<Quality,QualityValue> {

	protected final static Logger logger = System.getLogger(QualityValueListCell.class.getPackageName());

	private boolean withKarma;

	private Button btnEdit;
	private Label karma;
	private CellAction editAction = new CellAction("Edit",ICON_PEN, "tooltip", (act,item) -> editClicked(item));

	//-------------------------------------------------------------------
	@SuppressWarnings("rawtypes")
	public QualityValueListCell(IShadowrunCharacterControllerProvider<IShadowrunCharacterController> ctrlProv, boolean withKarma) {
		super( () -> ctrlProv.getCharacterController().getQualityController() );
		this.withKarma = withKarma;

		// Content
		karma   = new Label();
		karma.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		btnEdit = new Button("\uE1C2");
		btnEdit.getStyleClass().add("mini-button");

//		initStyle();
		initQualityLayout();
		initQualityInteractivity();
		addAction(editAction);

		getStyleClass().add("qualityvalue-list-cell");
	}

	//-------------------------------------------------------------------
	private void initQualityLayout() {
		if (withKarma)
			nameLine.getChildren().addAll(karma);

		actionLine.getChildren().clear();
		lines.getChildren().remove(actionLine);
		decisionLine.getChildren().add(0, btnEdit);
		decisionLine.setSpacing(10);
	}

	//-------------------------------------------------------------------
	private void initQualityInteractivity() {
		btnEdit.setOnAction(event -> {
			editClicked(this.getItem());
		});

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.log(Level.DEBUG, "drag started for "+this.getItem());
		if (this.getItem()==null)
			return;

		Node source = (Node) event.getSource();
		logger.log(Level.DEBUG, "drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "skill:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = "qualityval:"+this.getItem().getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(QualityValue item, boolean empty) {
		super.updateItem(item, empty);

		getChildren().remove(flagLine);

		if (empty || item==null) {
			setText(null);
			setGraphic(null);
		} else {
			karma.setText( String.valueOf(item.getKarmaCost()) );
			if (item.getResolved().isPositive()) {
				karma.setStyle(null);
			} else {
				karma.setStyle("-fx-text-fill: negative");
			}
			Possible removeable = controlProvider.get().canBeDeselected(item);
			if (!removeable.get())
				karma.setText(null);

			btnEdit.setVisible( canPerformAction(item, editAction));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.cells.ComplexDataItemValueListCell#canPerformAction(de.rpgframework.genericrpg.data.ComplexDataItemValue, de.rpgframework.jfx.cells.ComplexDataItemValueListCell.CellAction)
	 */
	@Override
	protected boolean canPerformAction(QualityValue item, CellAction action) {
		return false;
	}

	//-------------------------------------------------------------------
	protected OperationResult<QualityValue> editClicked(QualityValue ref) {
		logger.log(Level.WARNING, "editClicked");

//		TextField tf = new TextField(ref.getDescription());
//		// Prevent entering XML relevant chars
//		tf.textProperty().addListener( (ov,o,n) -> {
//			if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); tf.setText(n); }
//			if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); tf.setText(n); }
//			if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); tf.setText(n); }
//			if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); tf.setText(n); }
//		});
//		IQualityController control = (IQualityController) controlProvider.get();
		return new OperationResult<>();
	}

}