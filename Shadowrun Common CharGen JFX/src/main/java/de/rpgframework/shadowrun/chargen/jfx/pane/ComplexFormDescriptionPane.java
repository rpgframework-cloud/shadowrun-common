package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.jfx.ADescriptionPane;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import de.rpgframework.jfx.attach.PDFViewerServiceFactory;
import de.rpgframework.shadowrun.ComplexForm;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;

/**
 * @author stefa
 *
 */
public class ComplexFormDescriptionPane extends ADescriptionPane<ComplexForm> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(ComplexFormDescriptionPane.class.getPackageName()+".DescriptionPanes");

	private Label lbDurat;
	private Label lbDrain;
	private GridPane grid;

	//-------------------------------------------------------------------
	public ComplexFormDescriptionPane() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.ADescriptionPane#initExtraComponents()
	 */
	@Override
	protected void initExtraComponents() {
		// Spell Table
		Label hdDurat = new Label(ResourceI18N.get(UI,"complexform.duration"));
		lbDurat = new Label();
		Label hdDrain = new Label(ResourceI18N.get(UI,"complexform.fade"));
		lbDrain = new Label();
		hdDurat.setMaxWidth(Double.MAX_VALUE);
		hdDrain.setMaxWidth(Double.MAX_VALUE);

		hdDurat.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD);
		hdDrain.getStyleClass().add(JavaFXConstants.STYLE_TABLE_HEAD);

		lbDurat.getStyleClass().add(JavaFXConstants.STYLE_TABLE_DATA);
		lbDrain.getStyleClass().add(JavaFXConstants.STYLE_TABLE_DATA);

		grid = new GridPane();
		grid.add(hdDrain, 0, 0);
		grid.add(lbDrain, 0, 1);
		grid.add(hdDurat, 1, 0);
		grid.add(lbDurat, 1, 1);
		for (int i = 0; i < 2; i++) {
			ColumnConstraints col1 = new ColumnConstraints();
			col1.setPercentWidth(50);
			col1.setHalignment(HPos.CENTER);
			col1.setFillWidth(true);
			grid.getColumnConstraints().addAll(col1);
		}
		grid.setStyle("-fx-background-color: #e9e9e2;");
		grid.setVisible(false);
//		grid.setMaxWidth(Double.MAX_VALUE);

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.ADescriptionPane#initExtraComponents()
	 */
	@Override
	protected void initExtraLayout() {
		inner.getChildren().add(4, grid);
		VBox.setMargin(   grid, new Insets(20,0,0,0));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.ADescriptionPane#setData(de.rpgframework.genericrpg.data.DataItem)
	 */
	@Override
	public void setData(ComplexForm data) {
		super.setData(data);
		if (data==null) {
			grid.setVisible(false);
			return;
		}
		grid.setVisible(true);

		// Eventually open a PDF
		if (data != null) {
			PageReference pageRef = data.getPageReferences()
					.stream()
					.filter(pr -> pr.getLanguage().equals(Locale.getDefault().getLanguage()))
					.findFirst()
					.get();
			if (pageRef != null) {
				PDFViewerServiceFactory.create().ifPresent(service -> {
					service.show(pageRef.getProduct().getRules(), pageRef.getProduct().getID(),
							pageRef.getLanguage(), pageRef.getPage());
				});
			}
		}

		lbDurat.setText(data.getDuration().getName());
		String drainText = String.valueOf(data.getFading());
		if (data.getFading()<0)
			drainText=Math.abs(data.getFading())+"+";
		lbDrain.setText(drainText);

		System.out.println("ComplexFormDescriptionPane: "+data.getDescription(Locale.getDefault()));
//		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(lbDescr, data.getDescription(Locale.getDefault()));
	}
}
