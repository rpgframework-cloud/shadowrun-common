package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.section.ListSection;
import de.rpgframework.shadowrun.Ritual;
import de.rpgframework.shadowrun.RitualValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.jfx.listcell.RitualValueListCell;
import de.rpgframework.shadowrun.chargen.jfx.pane.RitualSelector;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class RitualSection extends ListSection<RitualValue> implements IShadowrunCharacterControllerProvider<IShadowrunCharacterController> {

	private final static Logger logger = System.getLogger(RitualSection.class.getPackageName());

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(RitualSection.class.getPackageName()+".Section");

	private IShadowrunCharacterController control;
	private ShadowrunCharacter model;
	private Function<Requirement, String> requirementResolver;
	private Function<Modification, String> modResolver;

	//-------------------------------------------------------------------
	public RitualSection(String title, Function<Requirement, String> requirementResolver, Function<Modification, String> modResolver) {
		super(title);
		this.requirementResolver = requirementResolver;
		this.modResolver = modResolver;

		initComponents();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list.setStyle("-fx-min-height: 10em; -fx-pref-height: 30em; -fx-pref-width: 25em");
		list.setCellFactory(cell -> new RitualValueListCell(this));
	}


	// -------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				btnDel.setDisable(true);
			} else {
				btnDel.setDisable( !control.getRitualController().canBeDeselected(n).get());
			}
		});
		btnAdd.setOnAction(ev -> onAdd());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	@Override
	public IShadowrunCharacterController getCharacterController() {
		return control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void refresh() {
		logger.log(Level.TRACE, "refresh");

		if (model!=null)
			setData(model.getRituals());
		else
			setData(new ArrayList<>());
	}

	//-------------------------------------------------------------------
	protected void onAdd() {
		logger.log(Level.DEBUG, "opening ritual selection dialog");

		RitualSelector selector = new RitualSelector(control.getRitualController(), requirementResolver, modResolver);
		NavigButtonControl btnCtrl = new NavigButtonControl();
    	btnCtrl.setDisabled(CloseType.OK, true);
    	selector.setButtonControl(btnCtrl);

		ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES,"ritualsection.selector.title"), selector, CloseType.OK, CloseType.CANCEL);
    	btnCtrl.initialize(FlexibleApplication.getInstance(), dialog);

		CloseType close = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		logger.log(Level.DEBUG,"Closed with "+close);
		if (close==CloseType.OK) {
			Ritual data = selector.getSelected();
			logger.log(Level.DEBUG, "Selected ritual: "+data);
//			logger.info("  quality needs choice: "+data.needsChoice()+" for "+data.getSelect());
//			if (data.needsChoice()) {
//				Object choosen = ShadowrunJFXUtils.choose(control, managerProvider, model, data.getSelect(), data.getName());
//				control.getRitualController().select(data, choosen);
//			} else {
				control.getRitualController().select(data);
//			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(RitualValue item) {
		logger.log(Level.DEBUG, "onDelete "+item);
		if (control.getRitualController().deselect(item)) {
			list.getItems().remove(item);
		}
	}

//	//-------------------------------------------------------------------
//	@SuppressWarnings("unchecked")
//	private void onSettings() {
//		CharacterLeveller ctrl = (CharacterLeveller) this.control;
//
//		VBox content = new VBox(20);
//		for (ConfigOption<?> opt : ctrl.getRitualController().getConfigOptions()) {
//			CheckBox cb = new CheckBox(opt.getName());
//			cb.setSelected((Boolean)opt.getValue());
//			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
//			content.getChildren().add(cb);
//		}
//
//		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
//	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (ShadowrunCharacter) ctrl.getModel();
		refresh();
	}

}
