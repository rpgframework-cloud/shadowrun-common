package de.rpgframework.shadowrun.chargen.jfx.pages;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.IRefreshableList;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.Quality.QualityCategory;
import de.rpgframework.shadowrun.Quality.QualityType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class FilterQualities extends AFilterInjector<Quality> {

	static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(Quality.class, Locale.GERMAN, Locale.ENGLISH);

	public static enum PosNeg {
		POSITIVE,
		NEGATIVE,
		;
		public String getName(Locale loc) { return RES.getString("quality.posneg."+name().toLowerCase(), loc); }
		public String getName() { return getName(Locale.getDefault()); }
		public static String getAllName(Locale loc) { return RES.getString("quality.posneg.all", loc); }
	}

	private ChoiceBox<PosNeg> cbPosNeg;
	private ChoiceBox<Quality.QualityType> cbType;
	private ChoiceBox<Quality.QualityCategory> cbCat;
	private List<QualityType> allowed;
	private PosNeg restrictPosNeg;

	//-------------------------------------------------------------------
	public FilterQualities(QualityType...allowedTypes) {
		allowed = List.of(allowedTypes);
		if (allowedTypes.length==0) {
			allowed = List.of(QualityType.values());
		}
	}

	//-------------------------------------------------------------------
	public FilterQualities(PosNeg positveOrNegative, QualityType...allowedTypes) {
		allowed = List.of(allowedTypes);
		if (allowedTypes.length==0) {
			allowed = List.of(QualityType.values());
		}
		restrictPosNeg = positveOrNegative;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#addFilter(de.rpgframework.jfx.FilteredListPage, javafx.scene.layout.FlowPane)
	 */
	@Override
	public void addFilter(IRefreshableList page, Pane filterPane) {
		/*
		 * Quality QualityType
		 */
		cbPosNeg = new ChoiceBox<PosNeg>();
		cbPosNeg.getItems().add(null);
		cbPosNeg.getItems().addAll(PosNeg.values());
		Collections.sort(cbPosNeg.getItems(), new Comparator<PosNeg>() {
			public int compare(PosNeg o1, PosNeg o2) {
				if (o1==null) return -1;
				if (o2==null) return  1;
				return Integer.compare(o1.ordinal(), o2.ordinal());
			}
		});
		cbPosNeg.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		cbPosNeg.setConverter(new StringConverter<PosNeg>() {
			public String toString(PosNeg val) {
				if (val==null) return PosNeg.getAllName(Locale.getDefault());
				return val.getName();
			}
			public PosNeg fromString(String string) { return null; }
		});

		/*
		 * Quality QualityType
		 */
		cbType = new ChoiceBox<Quality.QualityType>();
		cbType.getItems().add(null);
		cbType.getItems().addAll(allowed);
		Collections.sort(cbType.getItems(), new Comparator<Quality.QualityType>() {
			public int compare(Quality.QualityType o1, Quality.QualityType o2) {
				if (o1==null) return -1;
				if (o2==null) return  1;
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		cbType.setConverter(new StringConverter<Quality.QualityType>() {
			public String toString(Quality.QualityType val) {
				if (val==null) return QualityType.getAllName(Locale.getDefault());
				return val.getName();
			}
			public Quality.QualityType fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbType);

		/*
		 * Quality QualityCategory
		 */
		cbCat = new ChoiceBox<QualityCategory>();
		cbCat.getItems().add(null);
		cbCat.getItems().addAll(QualityCategory.values());
		Collections.sort(cbCat.getItems(), new Comparator<QualityCategory>() {
			public int compare(QualityCategory o1, QualityCategory o2) {
				if (o1==null) return -1;
				if (o2==null) return  1;
				return Integer.compare(o1.ordinal(), o2.ordinal());
			}
		});
		cbCat.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> page.refreshList());
		cbCat.setConverter(new StringConverter<QualityCategory>() {
			public String toString(QualityCategory val) {
				if (val==null) return QualityCategory.getAllName(Locale.getDefault());
				return val.getName();
			}
			public QualityCategory fromString(String string) { return null; }
		});
		filterPane.getChildren().add(cbCat);

		// Set some data
		if (restrictPosNeg==null) {
			filterPane.getChildren().add(cbPosNeg);
		} else {
			cbPosNeg.setValue(restrictPosNeg);
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#applyFilter(de.rpgframework.jfx.FilteredListPage, java.util.List)
	 */
	@Override
	public List<Quality> applyFilter(List<Quality> input) {
		input = input.stream()
				.filter(q -> allowed.contains(q.getType()))
				.collect(Collectors.toList());

		// Match positive or negative
		if (cbPosNeg.getValue()!=null) {
			input = input.stream()
					.filter(spell -> (spell.isPositive() && cbPosNeg.getValue()==PosNeg.POSITIVE) || (!spell.isPositive() && cbPosNeg.getValue()==PosNeg.NEGATIVE))
					.collect(Collectors.toList());
		}
		// Match Type
		if (cbType.getValue()!=null) {
			input = input.stream()
					.filter(spell -> spell.getType()==cbType.getValue())
					.collect(Collectors.toList());
		}
		// Match Category
		if (cbCat.getValue()!=null) {
			input = input.stream()
					.filter(spell -> spell.getCategory()==cbCat.getValue())
					.collect(Collectors.toList());
		}
		return input;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.AFilterInjector#updateChoices(java.util.List)
	 */
	@Override
	public void updateChoices(List<Quality> available) {
		// TODO Auto-generated method stub

	}

}
