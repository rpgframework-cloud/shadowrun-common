package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.shadowrun.Lifestyle;
import de.rpgframework.shadowrun.QualityValue;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import de.rpgframework.shadowrun.chargen.jfx.dialog.EditLifestyleValueDialog;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * @author prelle
 *
 */
public class ALifestyleListCell<L extends Lifestyle> extends ListCell<L> {

	private final static Logger logger = System.getLogger(ALifestyleListCell.class.getPackageName());
	
	protected IShadowrunCharacterControllerProvider<IShadowrunCharacterController> ctrlProv;

	protected Label lbName;
	protected Label lbMonths;
	protected Label lbQuality;
	protected Label lbNuyen;
	protected HBox bxLayout;
	private Button btnEdit;

	//-------------------------------------------------------------------
	public ALifestyleListCell(IShadowrunCharacterControllerProvider<IShadowrunCharacterController> ctrlProv) {
		this.ctrlProv = ctrlProv;
		initComponents();
		initLayout();
		initInteractivity();
		
		getStyleClass().add("lifestyle-list-cell");
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lbName  = new Label();
		lbName.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbMonths= new Label();
		lbMonths.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbQuality = new Label();
		lbNuyen = new Label();
		
		btnEdit = new Button("\uE1C2");
		//btnEdit.getStyleClass().add("mini-button");
		//btnEdit.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnEdit.setStyle("-fx-min-width: 3em");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Region buf1 = new Region(); buf1.setMaxWidth(Double.MAX_VALUE);
		Region buf2 = new Region(); buf2.setMaxWidth(Double.MAX_VALUE);
		Label hdMonths = new Label("Months"+":");
		HBox line1 = new HBox(lbName, buf1, hdMonths, lbMonths);
		HBox line2 = new HBox(btnEdit, lbQuality, buf2, lbNuyen);
		HBox.setHgrow(buf1, Priority.ALWAYS);
		HBox.setHgrow(buf2, Priority.ALWAYS);
		
		VBox bxData = new VBox(5);
		bxData.getChildren().addAll(line1, line2);
		bxData.setMaxWidth(Double.MAX_VALUE);
		
		bxLayout = new HBox(10, bxData, btnEdit);
		bxLayout.setAlignment(Pos.TOP_LEFT);
		HBox.setHgrow(bxData, Priority.ALWAYS);
	}
	
	private void initInteractivity() {
		btnEdit.setOnAction(event -> {
			editClicked(this.getItem());
		});
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(L item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lbName.setText(item.getNameWithoutRating(Locale.getDefault()));
			lbQuality.setText(item.getResolved().getName());
			lbMonths.setText( String.valueOf(item.getDistributed()));
			lbNuyen.setText(String.valueOf(item.getCost())+" \u00A5");
//			addDetails(item, flow);
			setGraphic(bxLayout);
		}
	}
	
	//-------------------------------------------------------------------
	protected void addDetails(L data, FlowPane flow) {
		Text text = new Text(data.getResolved().getName(Locale.getDefault()));
		text.setStyle("-fx-font-weight: bold");
		flow.getChildren().add(text);
	}

	//-------------------------------------------------------------------
	private void editClicked(L ref) {
		logger.log(Level.WARNING, "editClicked");
		
		EditLifestyleValueDialog<L> dialog = new EditLifestyleValueDialog<L>(ctrlProv.getCharacterController(), ref, ctrlProv.getCharacterController() instanceof IShadowrunCharacterGenerator);
		CloseType result = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		if (result==CloseType.OK || result==CloseType.APPLY) {
			Lifestyle toAdd = dialog.getResult();
			logger.log(Level.INFO,"Edited Lifestyle: "+toAdd);
		}
	}
}
