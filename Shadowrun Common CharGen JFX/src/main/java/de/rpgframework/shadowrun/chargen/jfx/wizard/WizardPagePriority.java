package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.gen.IPrioritySettings;
import de.rpgframework.shadowrun.chargen.gen.IPriorityGenerator;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;

/**
 * @author prelle
 *
 */
public class WizardPagePriority<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, C extends ShadowrunCharacter<S,V,?,?>, P extends IPrioritySettings>
	extends WizardPage
	implements ControllerListener {

	private final static Logger logger = System.getLogger(WizardPagePriority.class.getPackageName());

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPagePriority.class.getPackageName()+".WizardPages");

	private IShadowrunCharacterGenerator<S,V,?,C> charGen;

	private PriorityTable<C,P> table;

	//-------------------------------------------------------------------
	public WizardPagePriority(Wizard wizard, IShadowrunCharacterGenerator<S,V,?,C> charGen, PriorityTable<C,P> table) {
		super(wizard);
		this.charGen = charGen;
		this.table   = table;
		initComponents();
		initLayout();
		if (charGen instanceof IPriorityGenerator) {
			IPriorityGenerator<C, P> ctrl = (IPriorityGenerator<C, P>) charGen;
			table.setController(ctrl.getPriorityController());
		}

		refresh();
		initInteractivity();
	}
	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(ResourceI18N.get(UI,"wizard.page.priorities.title"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		super.setContent(table);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		charGen.addListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		super.setResponsiveMode(value);
	}

	//-------------------------------------------------------------------
	private void refresh() {
		table.update();
	}

	//-------------------------------------------------------------------
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		// TODO Auto-generated method stub
		if (type == BasicControllerEvents.GENERATOR_CHANGED) {
			logger.log(Level.INFO, "RCV " + type + " with " + Arrays.toString(param));
			if (param.length > 0 && param[0] instanceof IPriorityGenerator) {
				IPriorityGenerator<C, P> ctrl = (IPriorityGenerator<C, P>) param[0];
				table.setController(ctrl.getPriorityController());
			} else {
			//	System.err.println("WizardPagePriority NO");
			}
		}
		if (type == BasicControllerEvents.CHARACTER_CHANGED) {
			refresh();
		}
	}

}
