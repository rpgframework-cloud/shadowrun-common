package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.List;
import java.util.function.Function;

import org.prelle.javafx.ResponsiveControl;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.AFilterInjector;
import de.rpgframework.jfx.Selector;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import de.rpgframework.shadowrun.MetamagicOrEcho;
import de.rpgframework.shadowrun.MetamagicOrEcho.Type;
import de.rpgframework.shadowrun.MetamagicOrEchoValue;
import de.rpgframework.shadowrun.chargen.charctrl.IMetamagicOrEchoController;
import de.rpgframework.shadowrun.chargen.charctrl.IRejectReasons;
import de.rpgframework.shadowrun.chargen.jfx.FilterMetamagicOrEcho;

/**
 * @author prelle
 *
 */
public class MetamagicOrEchoSelector extends Selector<MetamagicOrEcho, MetamagicOrEchoValue> implements ResponsiveControl {

	private Type type;

	//-------------------------------------------------------------------
	public MetamagicOrEchoSelector(IMetamagicOrEchoController ctrl, Function<Requirement,String> resolver, Function<Modification,String> modResolver, Type type) {
		super(ctrl, resolver, modResolver, (AFilterInjector<MetamagicOrEcho>) new FilterMetamagicOrEcho());
		listPossible.setCellFactory( lv -> new ComplexDataItemListCell<MetamagicOrEcho>( () -> ctrl, resolver) {

			//-------------------------------------------------------------------
			@Override
			protected List<String> getIgnoreKeys() {
				return List.of(IRejectReasons.IMPOSS_NOT_ENOUGH_KARMA);
			}
		});
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.jfx.Selector#getDescriptionNode(de.rpgframework.genericrpg.data.ComplexDataItem)
//	 */
//	@Override
//	protected Pane getDescriptionNode(MetamagicOrEcho selected) {
//		SpellDescriptionPane descr = new SpellDescriptionPane();
//		if (selected!=null)
//			descr.setData(selected);
//		return descr;
//	}

}
