package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.section.ListSection;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.SpellValue;
import de.rpgframework.shadowrun.Tradition;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import de.rpgframework.shadowrun.chargen.jfx.listcell.SpellValueListCell;
import de.rpgframework.shadowrun.chargen.jfx.pane.SpellSelector;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class SpellSection<T extends ASpell> extends ListSection<SpellValue<T>> implements IShadowrunCharacterControllerProvider<IShadowrunCharacterController> {

	private final static Logger logger = System.getLogger(SpellSection.class.getPackageName());

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SpellSection.class.getPackageName()+".Section");

	protected IShadowrunCharacterController control;
	protected ShadowrunCharacter model;
	private Function<Requirement, String> requirementResolver;
	private Function<Modification,String> modResolver;

	protected ChoiceBox<Tradition> cbTradition;

	//-------------------------------------------------------------------
	public SpellSection(String title, Function<Requirement, String> requirementResolver, Function<Modification,String> modResolver) {
		super(title);
		this.requirementResolver = requirementResolver;
		this.modResolver  = modResolver;

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list.setStyle("-fx-min-height: 10em; -fx-pref-height: 30em; -fx-pref-width: 25em");
		list.setCellFactory(cell -> new SpellValueListCell<T>(control));

		cbTradition = new ChoiceBox<>();
		cbTradition.setConverter(new StringConverter<Tradition>() {
			public String toString(Tradition trad) {
				return (trad!=null)?trad.getName(Locale.getDefault()):"-";
			}
			public Tradition fromString(String string) { return null;}
		});
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		Label lbTradition = new Label(ResourceI18N.get(RES, "spellsection.tradition"));
		HBox headerNode = new HBox(10, lbTradition, cbTradition);
		headerNode.setAlignment(Pos.CENTER_LEFT);
		setHeaderNode(headerNode);
	}

	// -------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				btnDel.setDisable(true);
			} else {
				btnDel.setDisable( !control.getSpellController().canBeDeselected(n).get());
			}
		});
		btnAdd.setOnAction(ev -> onAdd());

		cbTradition.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "Select tradition "+n);
			if (n!=null) {
				model.setTradition(n);
				control.runProcessors();
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.CharacterControllerProvider#getCharacterController()
	 */
	@Override
	public IShadowrunCharacterController getCharacterController() {
		return control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void refresh() {
		logger.log(Level.TRACE, "refresh");

		if (model!=null) {
			setData(model.getSpells());

			if (model.getMagicOrResonanceType()!=null) {
				boolean usesSpells = model.getMagicOrResonanceType().usesSpells() && (model.getSkillValue("sorcery")!=null && model.getSkillValue("sorcery").getModifiedValue()>0);
				btnAdd.setVisible(usesSpells);
			}

		} else
			setData(new ArrayList<>());
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected void onAdd() {
		logger.log(Level.DEBUG, "opening quality selection dialog");

		SpellSelector selector = new SpellSelector(control.getSpellController(), requirementResolver, modResolver);
		NavigButtonControl btnCtrl = new NavigButtonControl();
    	btnCtrl.setDisabled(CloseType.OK, true);
    	selector.setButtonControl(btnCtrl);

    	ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES,"spellsection.selector.title"), selector, CloseType.OK, CloseType.CANCEL);
    	btnCtrl.initialize(FlexibleApplication.getInstance(), dialog);

		CloseType close = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		logger.log(Level.DEBUG,"Closed with "+close);
		if (close==CloseType.OK) {
			T data = (T) selector.getSelected();
			logger.log(Level.DEBUG, "Selected spell: "+data);
//			logger.info("  quality needs choice: "+data.needsChoice()+" for "+data.getSelect());
//			if (data.needsChoice()) {
//				Object choosen = ShadowrunJFXUtils.choose(control, managerProvider, model, data.getSelect(), data.getName());
//				control.getSpellController().select(data, choosen);
//			} else {
				control.getSpellController().select(data);
//			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(SpellValue item) {
		logger.log(Level.DEBUG, "onDelete "+item);
		if (control.getSpellController().deselect(item)) {
			list.getItems().remove(item);
		}
	}

//	//-------------------------------------------------------------------
//	@SuppressWarnings("unchecked")
//	private void onSettings() {
//		CharacterLeveller ctrl = (CharacterLeveller) this.control;
//
//		VBox content = new VBox(20);
//		for (ConfigOption<?> opt : ctrl.getSpellController().getConfigOptions()) {
//			CheckBox cb = new CheckBox(opt.getName());
//			cb.setSelected((Boolean)opt.getValue());
//			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
//			content.getChildren().add(cb);
//		}
//
//		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
//	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (ShadowrunCharacter) ctrl.getModel();
		refresh();
	}

}
