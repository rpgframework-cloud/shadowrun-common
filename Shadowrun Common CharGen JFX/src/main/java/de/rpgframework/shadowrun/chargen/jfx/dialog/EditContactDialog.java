package de.rpgframework.shadowrun.chargen.jfx.dialog;

import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.layout.AutoBox;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.GenericCore;
import de.rpgframework.shadowrun.Contact;
import de.rpgframework.shadowrun.ContactType;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.SINController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
public class EditContactDialog extends ManagedDialog {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(EditContactDialog.class.getPackageName()+".AllDialogs");

	class MyTitledComponent extends VBox {
		public MyTitledComponent(String title, Node node) {
			Label head = new Label(title);
			head.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
			getChildren().addAll(head, node);
		}
	}

	private IShadowrunCharacterController<?,?,?,?>  control;
	private SINController sinControl;
	private NavigButtonControl btnControl;
	private boolean isEdit;
	private Contact data;

	private TextField tfName;
	private TextField tfType;
	private TextArea  taDesc;
	private TextField tfFavo;
	private Label lbRat, lbLoy;
	private Button btnDecRat, btnIncRat, btnDecLoy, btnIncLoy;
	private ChoiceBox<ContactType> cbType;

	//-------------------------------------------------------------------
	public EditContactDialog(IShadowrunCharacterController<?,?,?,?> control, Contact value, boolean isEdit) {
		super(ResourceI18N.get(RES,"dialog.contact.title"), null, CloseType.APPLY, CloseType.CANCEL);
		this.data = value;
		this.isEdit = isEdit;
		if (!isEdit) {
			buttons.setAll(CloseType.OK, CloseType.CANCEL);
		} else {
			buttons.setAll(CloseType.APPLY);
		}
		this.control = control;
		this.sinControl = control.getSINController();
		this.data    = value;
		btnControl = new NavigButtonControl();
		btnControl.initialize(FlexibleApplication.getInstance(), this);

		initComponents();
		initLayout();
		initInteractivity();
		cbType.setDisable(isEdit);

		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		cbType = new ChoiceBox<>();
		cbType.getItems().addAll(GenericCore.getItemList(ContactType.class));
		cbType.setConverter(new StringConverter<ContactType>() {
			public String toString(ContactType value) {return (value!=null)?value.getName(Locale.getDefault()):"";}
			public ContactType fromString(String string) {return null;}
		});
		cbType.setDisable(isEdit);
		tfType = new TextField();
//		tfType.setPrefColumnCount(20);
		tfFavo = new TextField();
		tfFavo.setPrefColumnCount(2);
		tfFavo.setStyle("-fx-max-width: 2em");
		taDesc = new TextArea();
		tfName.setPromptText(ResourceI18N.get(RES, "dialog.contact.prompt.name"));
		tfType.setPromptText(ResourceI18N.get(RES, "dialog.contact.prompt.type"));

		lbRat = new Label("?");
		lbLoy = new Label("?");
		btnDecRat  = new Button("\uE0C9");
		btnIncRat  = new Button("\uE0C8");
		btnDecLoy  = new Button("\uE0C9");
		btnIncLoy  = new Button("\uE0C8");
		btnDecRat.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnIncRat.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnDecLoy.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnIncLoy.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Description
		Label heaName = new Label(ResourceI18N.get(RES, "dialog.contact.name"));
		Label heaType = new Label(ResourceI18N.get(RES, "dialog.contact.type"));
		Label heaFav  = new Label(ResourceI18N.get(RES, "dialog.contact.favors"));
		Label heaDesc = new Label(ResourceI18N.get(RES, "dialog.contact.desc"));

		AutoBox bxType = new AutoBox();
		bxType.getContent().addAll(tfType, cbType);
		bxType.setSpacing(5);

		HBox bxRating  = new HBox(5, btnDecRat, lbRat, btnIncRat);
		HBox bxLoyalty = new HBox(5, btnDecLoy, lbLoy, btnIncLoy);
		bxRating.setAlignment(Pos.CENTER);
		bxLoyalty.setAlignment(Pos.CENTER);

		GridPane descForm = new GridPane();
		descForm.add(heaName , 0, 0);
		descForm.add(tfName  , 1, 0);
		descForm.add(new Label("R"), 2, 0);
		descForm.add(bxRating, 3, 0);
		descForm.add(heaType , 0, 1);
		descForm.add(bxType  , 1, 1);
		descForm.add(new Label("L"), 2, 1);
		descForm.add(bxLoyalty, 3, 1);
		descForm.add(heaFav, 0, 2);
		descForm.add(tfFavo, 1, 2);
//		descForm.add(cbCoven, 0, 3, 2,1);
		descForm.setStyle("-fx-vgap: 1em; -fx-hgap: 1em");

		VBox colDescription = new VBox();
		colDescription.getChildren().addAll(descForm, heaDesc, taDesc);
		colDescription.setMaxHeight(Double.MAX_VALUE);
		VBox.setMargin(heaDesc, new Insets(20, 0, 0, 0));

		this.setMaxHeight(Double.MAX_VALUE);
		setContent(colDescription);
	}

	//-------------------------------------------------------------------
	private void updateOKButton() {
		btnControl.setDisabled(CloseType.OK, tfName.getText()==null || tfType.getText()==null);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)-> {
			if (n==null && data!=null)
				data.setType(n);
			if (n!=null) {
				updateOKButton();
			}
		});
		tfName.textProperty().addListener( (ov,o,n) -> {
			if (data!=null)
				data.setName(n);
			updateOKButton();
		});
		tfType.textProperty().addListener( (ov,o,n) -> {
			if (data!=null)
				data.setTypeName(n);
			updateOKButton();
		});
		taDesc.textProperty().addListener( (ov,o,n) -> {
			if (data!=null)
				data.setDescription(n);
		});
		tfName.setOnAction(ev -> refresh());
		tfName.focusedProperty().addListener( (ov,o,n) -> refresh());
		tfType.setOnAction(ev -> refresh());
		tfType.focusedProperty().addListener( (ov,o,n) -> refresh());
		tfFavo.textProperty().addListener( (ov,o,n) -> {
			if (data!=null) {
				try {
					data.setFavors(Integer.parseInt(n));
				} catch (NumberFormatException e) {
					data.setFavors(0);
				}
			}
		});

		btnDecLoy.setOnAction(ev -> {control.getContactController().decreaseLoyalty(data); refresh();});
		btnIncLoy.setOnAction(ev -> {control.getContactController().increaseLoyalty(data); refresh();});
		btnDecRat.setOnAction(ev -> {control.getContactController().decreaseRating(data); refresh();});
		btnIncRat.setOnAction(ev -> {control.getContactController().increaseRating(data); refresh();});
	}

	//-------------------------------------------------------------------
	public Object[] getData() {
		return new Object[]{tfName.getText(), cbType.getValue(), taDesc.getText()};
	}

	//-------------------------------------------------------------------
	private void refresh() {
		if (data == null) {
			cbType.setValue(null);
			tfName.setText(null);
			tfType.setText(null);
			taDesc.setText(null);
			tfFavo.setText(null);
			cbType.setValue(null);
		} else {
			cbType.setValue(data.getType());
			tfName.setText(data.getName());
			tfType.setText(data.getTypeName());
			taDesc.setText(data.getDescription());
			tfFavo.setText("" + data.getFavors());
			cbType.setValue(data.getType());
			lbRat.setText(String.valueOf(data.getRating()));
			lbLoy.setText(String.valueOf(data.getLoyalty()));
//			System.err.println(control.getContactController().getClass().getSimpleName()+": canDecRat = "+control.getContactController().canDecreaseRating(data));
//			System.err.println(control.getContactController().getClass().getSimpleName()+": canIncRat = "+control.getContactController().canIncreaseRating(data));
//			System.err.println(control.getContactController().getClass().getSimpleName()+": canDecLoy = "+control.getContactController().canDecreaseLoyalty(data));
//			System.err.println(control.getContactController().getClass().getSimpleName()+": canIncLoy = "+control.getContactController().canIncreaseLoyalty(data));
			btnDecLoy.setDisable(!control.getContactController().canDecreaseLoyalty(data).get());
			btnIncLoy.setDisable(!control.getContactController().canIncreaseLoyalty(data).get());
			btnDecRat.setDisable(!control.getContactController().canDecreaseRating(data).get());
			btnIncRat.setDisable(!control.getContactController().canIncreaseRating(data).get());
		}
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	public String getName() { return tfName.getText(); }
	public ContactType getRating() { return cbType.getValue(); }
	public String getDescription() { return taDesc.getText(); }

}
