package de.rpgframework.shadowrun.chargen.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.section.ComplexDataItemListSection;
import de.rpgframework.shadowrun.Focus;
import de.rpgframework.shadowrun.FocusValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IFocusController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.jfx.listcell.FocusValueListCell;
import de.rpgframework.shadowrun.chargen.jfx.pane.FocusSelector;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author stefa
 *
 */
public abstract class FocusSection extends ComplexDataItemListSection<Focus, FocusValue> {

	private final static Logger logger = System.getLogger(FocusSection.class.getPackageName());

	public static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(FocusSection.class.getPackageName()+".Section");

	private IShadowrunCharacterController control;
	protected ShadowrunCharacter model;
	private Function<Requirement, String> requirementResolver;
	private Function<Modification, String> modResolver;

	protected Label lbNum;
	protected Label lbSum;

	//-------------------------------------------------------------------
	/**
	 * @param title
	 */
	public FocusSection(Function<Requirement, String> reqResolver, Function<Modification, String> modResolver) {
		super(ResourceI18N.get(RES, "section.foci"));
		this.requirementResolver = reqResolver;
		this.modResolver = modResolver;
		list.setCellFactory( lv -> new FocusValueListCell( () -> control.getFocusController()));
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbNum = new Label("?");
		lbNum.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);
		lbSum = new Label("?");
		lbSum.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);
	}

	// -------------------------------------------------------------------
	private void initLayout() {
		Label hdBound = new Label(ResourceI18N.get(RES, "section.foci.maxBound"));
		Label hdSum  = new Label(ResourceI18N.get(RES, "section.foci.totalSum"));
		HBox headerNode = new HBox(5, hdBound, lbNum, hdSum, lbSum);
		HBox.setMargin(hdSum, new Insets(0,0,0,5));
		headerNode.setAlignment(Pos.CENTER_LEFT);
		setHeaderNode(headerNode);
	}

	//-------------------------------------------------------------------
	protected abstract Decision[] requestUserDecisions(Focus value);

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.log(Level.INFO, "opening focus selection dialog");

		FocusSelector selector = new FocusSelector(control.getFocusController(), requirementResolver, modResolver);
		ManagedDialog dialog = new ManagedDialog(ResourceI18N.get(RES,"section.foci.selector.title"), selector, CloseType.OK, CloseType.CANCEL);

		CloseType close = (CloseType) FlexibleApplication.getInstance().showAndWait(dialog);
		logger.log(Level.ERROR,"Closed with "+close);
		if (close==CloseType.OK) {
			Focus data = selector.getSelected();
			logger.log(Level.INFO, "Selected focus: "+data);
			OperationResult<FocusValue> result = null;
			if (data.getChoices().isEmpty() && !data.hasLevel()) {
				result = control.getFocusController().select(data);
			} else {
				Decision[] dec = requestUserDecisions(data);
				logger.log(Level.INFO, "User decisions for focus: "+dec);
				if (dec!=null) {
					// Not cancelled
					result = control.getFocusController().select(data, dec);
				}
			}
			if (result!=null && result.hasError()) {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, result.getError());
			}

		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.section.ListSection#onDelete(java.lang.Object)
	 */
	@Override
	protected void onDelete(FocusValue item) {
		logger.log(Level.DEBUG, "onDelete "+item);
		if (control.getFocusController().deselect(item)) {
			list.getItems().remove(item);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Section#refresh()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void refresh() {
		logger.log(Level.TRACE, "refresh");

		if (model!=null)
			setData(model.getFoci());
		else
			setData(new ArrayList<>());

		IFocusController ctrl = control.getFocusController();
		if (model!=null && ctrl!=null) {
			lbSum.setText( String.valueOf( ctrl.getFocusPointsLeft() ));
		}
	}

	//-------------------------------------------------------------------
	public void updateController(IShadowrunCharacterController ctrl) {
		assert ctrl!=null;
		control = ctrl;
		model = (ShadowrunCharacter) ctrl.getModel();
		refresh();
	}

}
