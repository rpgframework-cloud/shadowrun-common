package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.prelle.javafx.SymbolIcon;

import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.ASpell.Category;
import de.rpgframework.shadowrun.SpellFeatureReference;
import de.rpgframework.shadowrun.SpellValue;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.ISpellController;
import de.rpgframework.shadowrun.chargen.jfx.CommonShadowrunJFXResourceHook;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class SpellValueListCell<T extends ASpell> extends ListCell<SpellValue<T>> {
	
	private final static Map<ASpell.Category, Image> IMAGES = new HashMap<>();
	
	private IShadowrunCharacterController charGen;
	private ImageView iview;
	private Label lblName;
	private Label lblLine1;
	private StackPane stack;
	private SymbolIcon lblLock;
	
	private HBox bxLayout;
	private VBox bxData;
	private SpellValue<T> data;
	
	//-------------------------------------------------------------------
	static {
		IMAGES.put(Category.COMBAT   , new Image(CommonShadowrunJFXResourceHook.class.getResourceAsStream("images/spelltypes/spelltype.combat.png")));
		IMAGES.put(Category.HEALTH   , new Image(CommonShadowrunJFXResourceHook.class.getResourceAsStream("images/spelltypes/spelltype.health.png")));
		IMAGES.put(Category.DETECTION, new Image(CommonShadowrunJFXResourceHook.class.getResourceAsStream("images/spelltypes/spelltype.detection.png")));
		IMAGES.put(Category.ILLUSION , new Image(CommonShadowrunJFXResourceHook.class.getResourceAsStream("images/spelltypes/spelltype.illusion.png")));
		IMAGES.put(Category.MANIPULATION, new Image(CommonShadowrunJFXResourceHook.class.getResourceAsStream("images/spelltypes/spelltype.manipulation.png")));
	}
	
	//-------------------------------------------------------------------
	public SpellValueListCell(IShadowrunCharacterController charGen) {
		this.charGen = charGen;
		initComponents();
		initLayout();
		initInteractivity();
		
		getStyleClass().add("spellvalue-list-cell");
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lblName  = new Label();
		lblName.setStyle("-fx-font-family: Roboto-Medium; ");
		lblLine1 = new Label();
		
		iview = new ImageView();
		iview.setFitHeight(48);
		iview.setFitWidth(48);
		lblLock = new SymbolIcon("lock");
		lblLock.setMaxWidth(50);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		bxData = new VBox();
		bxData.getChildren().addAll(lblName, lblLine1);
		
		bxLayout = new HBox(5);
		bxLayout.getChildren().addAll(iview, bxData);

		stack = new StackPane();
		stack.setMaxWidth(Double.MAX_VALUE);
		stack.setAlignment(Pos.CENTER_RIGHT);
		stack.getChildren().addAll(lblLock, bxLayout);
		StackPane.setAlignment(lblLock, Pos.TOP_RIGHT);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		this.setOnDragDetected(event -> dragStarted(event));
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(SpellValue<T> item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(stack);
			T spell = item.getModifyable();
			lblName.setText(item.getNameWithoutRating().toUpperCase());
			lblName.setStyle("-fx-font-family: Roboto-Medium; ");
			lblLine1.setText(makeFeatureString(spell));
			iview.setImage(IMAGES.get(spell.getCategory()));
			lblLock.setVisible( !charGen.getSpellController().canBeDeselected(item).get());
		}
	}
	
	//-------------------------------------------------------------------
	private static String makeFeatureString(ASpell spell) {
		StringBuffer buf = new StringBuffer();
		Iterator<SpellFeatureReference> it = spell.getFeatures().iterator();
		while (it.hasNext()) {
			SpellFeatureReference ref = it.next();
			buf.append(ref.getFeature().getName());
			if (it.hasNext())
				buf.append(", ");
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		if (data==null)
			return;

		Node source = (Node) event.getSource();

		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		ClipboardContent content = new ClipboardContent();
		String id = "spellval:"+((SpellValue)data).getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}
}