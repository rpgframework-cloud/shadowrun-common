package de.rpgframework.shadowrun.chargen.jfx;

import java.util.List;
import java.util.function.Consumer;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.WindowMode;

import de.rpgframework.jfx.rules.AttributeTable;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Control;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class ShadowrunSkillTable<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, C extends ShadowrunCharacter<S,V,?,?>> extends Control implements ResponsiveControl {
	
	private IShadowrunCharacterController<S, V, ?,C> controller;
	private ObjectProperty<AttributeTable.Mode> mode = new SimpleObjectProperty<>(AttributeTable.Mode.CAREER);
	private ObjectProperty<V> selectedItem = new SimpleObjectProperty<>();
	/**
	 * Use expert mode for priority generators
	 */
	private BooleanProperty useExpertMode;
	protected BooleanProperty expertModeAvailable;
	
    /**
     * Callback to open an edit action dialog
     */
	private ObjectProperty<Callback<V, CloseType>> actionCallback = new SimpleObjectProperty<>();
	
	private ObjectProperty<Consumer<List<V>>> selectedListModifier = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	/**
	 */
	public ShadowrunSkillTable(IShadowrunCharacterController<S, V, ?, C> ctrl, AttributeTable.Mode mode) {
		this.mode.set(mode);
		controller = ctrl;
		expertModeAvailable = new SimpleBooleanProperty(false);
		useExpertMode = new SimpleBooleanProperty(false);
		
//		setSkin(new ShadowrunSkillTableSkin(this));
	}

	//-------------------------------------------------------------------
	public IShadowrunCharacterController<?, ?, ?, C> getController() { return controller; }

	//-------------------------------------------------------------------
	public ObjectProperty<Consumer<List<V>>> selectedListModifierroperty() { return selectedListModifier; }
	public Consumer<List<V>> getSelectedListModifier() { return selectedListModifier.get(); }
	public ShadowrunSkillTable<S,V,C> setSelectedListModifier(Consumer<List<V>> value) { selectedListModifier.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<AttributeTable.Mode> modeProperty() { return mode; }
	public AttributeTable.Mode getMode() { return mode.get(); }
	public ShadowrunSkillTable<S,V,C> setMode(AttributeTable.Mode value) { mode.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<V> selectedItemProperty() { return selectedItem; }
	public V getSelectedItem() { return selectedItem.get(); }
	public ShadowrunSkillTable<S,V,C> setSelectedItem(V value) { selectedItem.set(value); return this; }

	//-------------------------------------------------------------------
	public BooleanProperty useExpertModeProperty() { return useExpertMode; }
	public boolean isUseExpertMode() { return useExpertMode.get(); }
	public ShadowrunSkillTable<S,V,C> setUseExpertMode(boolean value) { useExpertMode.set(value); return this; }

	//-------------------------------------------------------------------
	public ReadOnlyBooleanProperty expertModeAvailableProperty() { return expertModeAvailable; }
	public boolean isExpertModeAvailable() { return expertModeAvailable.get(); }

	//-------------------------------------------------------------------
	public ObjectProperty<Callback<V, CloseType>> actionCallbackProperty() { return actionCallback; }
	public Callback<V, CloseType> getActionCallback() { return actionCallback.get(); }
	public ShadowrunSkillTable<S,V,C> setActionCallback(Callback<V, CloseType> value) { actionCallback.set(value); return this; }

	//-------------------------------------------------------------------
   public void refresh() {
      getProperties().put(SkinProperties.REFRESH, Boolean.TRUE);
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
        getProperties().put(SkinProperties.WINDOW_MODE, Boolean.TRUE);
	}

}
