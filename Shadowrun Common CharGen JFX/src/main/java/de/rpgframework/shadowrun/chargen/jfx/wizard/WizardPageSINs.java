package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.javafx.layout.ResponsiveBox;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.LicenseValue;
import de.rpgframework.shadowrun.SIN;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import de.rpgframework.shadowrun.chargen.jfx.dialog.AddLicenseDialog;
import de.rpgframework.shadowrun.chargen.jfx.dialog.EditSINDialog;
import de.rpgframework.shadowrun.chargen.jfx.listcell.LicenseValueListCell;
import de.rpgframework.shadowrun.chargen.jfx.listcell.SINListCell;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class WizardPageSINs extends WizardPage implements ControllerListener {

	private final static Logger logger = System.getLogger(WizardPageSINs.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(WizardPageSINs.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterController<?,?,?,?> charGen;

	protected Button btnAdd,btnDel;
	protected ListView<SIN> selection;
	private Button btnAddLic,btnDelLic;
	protected ListView<LicenseValue> licenses;
	protected GenericDescriptionVBox bxDescription;
	protected OptionalNodePane layout;
	private NumberUnitBackHeader backHeader;

	private ResponsiveBox columns;
	private VBox col1, col2;

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public WizardPageSINs(Wizard wizard, IShadowrunCharacterGenerator<?,?,?,?> charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(RES, "page.sins_license.title"));
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes"})
	protected void initComponents() {
		btnAdd = new Button(null, new SymbolIcon("Add"));
		btnDel = new Button(null, new SymbolIcon("Delete"));
		btnDel.setDisable(true);
		btnAddLic = new Button(null, new SymbolIcon("Add"));
		btnDelLic = new Button(null, new SymbolIcon("Delete"));
		btnDelLic.setDisable(true);

		selection = new ListView<SIN>();
		Label ph = new Label(ResourceI18N.get(RES, "page.sins_license.placeholder.sins"));
		ph.setWrapText(true);
		selection.setPlaceholder(ph);
		selection.setCellFactory(lv -> new SINListCell(charGen));

		licenses = new ListView<LicenseValue>();
		licenses.setCellFactory(lv -> new LicenseValueListCell(true, charGen));
		ph = new Label(ResourceI18N.get(RES, "page.sins_license.placeholder.license"));
		ph.setWrapText(true);
		licenses.setPlaceholder(ph);
		//licenses.setCellFactory(lv -> new SINListCell(charGen));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		backHeader = new NumberUnitBackHeader("Nuyen");
		backHeader.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeader, new Insets(0,10,0,10));
		super.setBackHeader(backHeader);

		Region buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		HBox line1Select = new HBox(buf,btnAdd,btnDel);
		HBox.setHgrow(buf, Priority.ALWAYS);
		col1 = new VBox(5, line1Select, selection);

		buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		HBox line2Select = new HBox(buf,btnAddLic,btnDelLic);
		HBox.setHgrow(buf, Priority.ALWAYS);
		col2 = new VBox(5, line2Select, licenses);

		columns = new ResponsiveBox(col1, col2);
		layout = new OptionalNodePane(columns, bxDescription);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selection.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			btnDel.setDisable(n==null);
		});
		btnDel.setOnAction(ev -> deleteClicked());
		btnAdd.setOnAction(ev -> addSINClicked());

		licenses.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			btnDelLic.setDisable(n==null);
		});
		btnDelLic.setOnAction(ev -> deleteLicenseClicked());
		btnAddLic.setOnAction(ev -> addLicenseClicked());
		setOnExtraActionHandler( button -> onExtraAction(button));
	}

	//-------------------------------------------------------------------
	private void refresh() {
		backHeader.setValue(charGen.getModel().getNuyen());

		selection.getItems().setAll(charGen.getModel().getSINs());
		licenses.getItems().setAll(charGen.getModel().getLicenses());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.INFO, "pageVisited");
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		logger.log(Level.DEBUG,"RCV {0}", type);
		if (type==BasicControllerEvents.CHARACTER_CHANGED)
			refresh();

		if (type==BasicControllerEvents.GENERATOR_CHANGED) {
			refresh();
		}
	}

	//-------------------------------------------------------------------
	public void addSINClicked() {
		SIN sin = new SIN();
		EditSINDialog dialog = new EditSINDialog(charGen, sin, false);
		CloseType closed = FlexibleApplication.getInstance().showAlertAndCall(dialog, dialog.getButtonControl());
		if (closed==CloseType.OK) {
			logger.log(Level.INFO, "User chose a SIN to add");
			sin = charGen.getSINController().createNewSIN(dialog.getName(), dialog.getRating());
			if (sin==null) {
				logger.log(Level.ERROR, "Adding a SIN failed");
				return;
			}
			sin.setDescription(dialog.getDescription());
			logger.log(Level.DEBUG, "Adding a SIN done - now add licenses");
			for (LicenseValue val : dialog.getLicenses()) {
				charGen.getSINController().createNewLicense(sin, val.getRating(), val.getName());
			}
//			refresh();
		}
	}

	//-------------------------------------------------------------------
	public void deleteClicked() {
		SIN toDelete = selection.getSelectionModel().getSelectedItem();
		charGen.getSINController().deleteSIN(toDelete);
	}

	//-------------------------------------------------------------------
	public void addLicenseClicked() {
		AddLicenseDialog dialog = new AddLicenseDialog(charGen);
		CloseType closed = FlexibleApplication.getInstance().showAlertAndCall(dialog, dialog.getButtonControl());
		if (closed==CloseType.OK) {
			logger.log(Level.INFO, "User chose a license to add");
			LicenseValue  sin = charGen.getSINController().createNewLicense(dialog.getSIN(), dialog.getRating(), dialog.getName());
			if (sin==null) {
				logger.log(Level.ERROR, "Adding a license failed");
				return;
			}
			logger.log(Level.DEBUG, "Adding a license done");
		}
	}

	//-------------------------------------------------------------------
	public void deleteLicenseClicked() {
		LicenseValue toDelete = licenses.getSelectionModel().getSelectedItem();
		charGen.getSINController().deleteLicense(toDelete);
	}

	//-------------------------------------------------------------------
	private void onExtraAction(CloseType button) {
		charGen.getSINController().roll();
	}

}
