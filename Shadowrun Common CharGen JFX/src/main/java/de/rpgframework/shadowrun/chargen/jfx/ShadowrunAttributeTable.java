package de.rpgframework.shadowrun.chargen.jfx;

import java.util.function.Function;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.WindowMode;

import de.rpgframework.jfx.rules.AttributeTable;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.AShadowrunSkillValue;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Control;

/**
 * @author prelle
 *
 */
public abstract class ShadowrunAttributeTable<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, C extends ShadowrunCharacter<S,V,?,?>> extends Control implements ResponsiveControl {

	private IShadowrunCharacterController<S, V, ?,C> controller;
	private ObjectProperty<AttributeTable.Mode> mode = new SimpleObjectProperty<>(AttributeTable.Mode.CAREER);
	/**
	 * Use expert mode for priority generators
	 */
	private BooleanProperty useExpertMode;
	private BooleanProperty showMagic;
	private BooleanProperty showResonance;
	protected BooleanProperty expertModeAvailable;
	protected ObjectProperty<Function<ShadowrunAttribute,String>> nameMapperProperty;

	//-------------------------------------------------------------------
	public ShadowrunAttributeTable(IShadowrunCharacterController<S, V, ?,C> ctrl) {
		controller = ctrl;
		expertModeAvailable = new SimpleBooleanProperty(false);
		useExpertMode = new SimpleBooleanProperty(false);
		showMagic     = new SimpleBooleanProperty(false);
		showResonance = new SimpleBooleanProperty(false);
		nameMapperProperty = new SimpleObjectProperty<Function<ShadowrunAttribute,String>>(null);

		// Skin is set by child classes
	}

	//-------------------------------------------------------------------
	public IShadowrunCharacterController<?, ?, ?, C> getController() { return controller; }

	//-------------------------------------------------------------------
	public ObjectProperty<Function<ShadowrunAttribute,String>> nameMapperProperty() { return nameMapperProperty; }
	public Function<ShadowrunAttribute,String> getNameMapper() { return nameMapperProperty.get(); }
	public ShadowrunAttributeTable<S,V,C> setNameMapper(Function<ShadowrunAttribute,String> value) { nameMapperProperty.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<AttributeTable.Mode> modeProperty() { return mode; }
	public AttributeTable.Mode getMode() { return mode.get(); }
	public ShadowrunAttributeTable<S,V,C> setMode(AttributeTable.Mode value) { mode.set(value); return this; }

	//-------------------------------------------------------------------
	public BooleanProperty useExpertModeProperty() { return useExpertMode; }
	public boolean isUseExpertMode() { return useExpertMode.get(); }
	public ShadowrunAttributeTable<S,V,C> setUseExpertMode(boolean value) { useExpertMode.set(value); return this; }

	//-------------------------------------------------------------------
	public BooleanProperty showMagicProperty() { return showMagic; }
	public boolean isShowMagic() { return showMagic.get(); }
	public ShadowrunAttributeTable<S,V,C> setShowMagic(boolean value) { showMagic.set(value); return this; }

	//-------------------------------------------------------------------
	public BooleanProperty showResonanceProperty() { return showResonance; }
	public boolean isShowResonance() { return showResonance.get(); }
	public ShadowrunAttributeTable<S,V,C> setShowResonance(boolean value) { showResonance.set(value); return this; }

	//-------------------------------------------------------------------
	public ReadOnlyBooleanProperty expertModeAvailableProperty() { return expertModeAvailable; }
	public boolean isExpertModeAvailable() { return expertModeAvailable.get(); }

	//-------------------------------------------------------------------
   public void refresh() {
        getProperties().put(SkinProperties.REFRESH, Boolean.TRUE);
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
        getProperties().put(SkinProperties.WINDOW_MODE, Boolean.TRUE);
	}

}
