package de.rpgframework.shadowrun.chargen.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;
import java.util.ResourceBundle;

import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.BasicControllerEvents;
import de.rpgframework.genericrpg.chargen.ControllerEvent;
import de.rpgframework.genericrpg.chargen.ControllerListener;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import de.rpgframework.jfx.wizard.NumberUnitBackHeader;
import de.rpgframework.shadowrun.MagicOrResonanceType;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IMagicOrResonanceController;
import de.rpgframework.shadowrun.chargen.gen.IShadowrunCharacterGenerator;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public abstract class WizardPageMagicOrResonance
	extends WizardPage implements ControllerListener {

	protected final static Logger logger = System.getLogger(WizardPageMagicOrResonance.class.getPackageName());

	protected final static ResourceBundle UI = ResourceBundle.getBundle(WizardPageAdeptPowers.class.getPackageName()+".WizardPages");

	protected IShadowrunCharacterGenerator<?, ?, ?,?> charGen;

	private ChoiceBox<MagicOrResonanceType> cbMoRType;
	protected ListView<MagicOrResonanceType> lvMoRType;

	private OptionalNodePane layout;
	private GenericDescriptionVBox bxDescription;
	private transient MagicOrResonanceType current;
	private NumberUnitBackHeader backHeaderKarma;
	protected NumberUnitBackHeader backHeaderCP;

	protected GenericDescriptionVBox descTradition;

	private boolean surpressEvents;

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public WizardPageMagicOrResonance(Wizard wizard, IShadowrunCharacterGenerator<?, ?, ?,?> charGen) {
		super(wizard);
		this.charGen = charGen;
		setTitle(ResourceI18N.get(UI, "wizard.page.mortype.title"));
		initComponents();
		initLayout();
		initInteractivity();

		charGen.addListener(this);

		if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
			cbMoRType.setValue(charGen.getModel().getMagicOrResonanceType());
		} else {
			lvMoRType.getSelectionModel().select(charGen.getModel().getMagicOrResonanceType());
		}
	}

	// -------------------------------------------------------------------
	private void initComponents() {
		cbMoRType = new ChoiceBox<MagicOrResonanceType>();
		lvMoRType = new ListView<MagicOrResonanceType>();
		bxDescription = new GenericDescriptionVBox(null,null);

		cbMoRType.setConverter(new StringConverter<MagicOrResonanceType>() {
			public String toString(MagicOrResonanceType type) { return (type!=null)?type.getName():"-";}
			public MagicOrResonanceType fromString(String arg0) {return null;}
		});
		lvMoRType.setCellFactory( lv -> new MagicOrResonanceCell());

		descTradition = new GenericDescriptionVBox(null,null);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		layout = new OptionalNodePane(null, bxDescription);
		reLayout(charGen.getModel().getMagicOrResonanceType());
		setContent(layout);

		backHeaderKarma = new NumberUnitBackHeader(ResourceI18N.get(UI, "label.karma"));
		backHeaderKarma.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeaderKarma, new Insets(0,10,0,10));
		backHeaderCP = new NumberUnitBackHeader(ResourceI18N.get(UI, "label.cp"));
//		backHeaderCP.setValue(charGen.getModel().getKarmaFree());
		HBox.setMargin(backHeaderCP, new Insets(0,10,0,10));
		backHeaderCP.setVisible(false);
		backHeaderCP.setManaged(false);

		super.setBackHeader(new HBox(20,backHeaderKarma,backHeaderCP));
		descTradition.setMinHeight(500);
	}

	//-------------------------------------------------------------------
	private void reLayout(MagicOrResonanceType type) {
		Pane selection;
		if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
			selection = new VBox(10, cbMoRType);
			descTradition.setMinHeight(300);
		} else {
			selection = new HBox(20, lvMoRType);
			descTradition.setMinHeight(500);
		}

		Node bxTune = getChoiceConfigNode(type);
		if (bxTune!=null) {
			selection.getChildren().add(bxTune);
		}

		layout.setContent(selection);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbMoRType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (!surpressEvents) selectNow(n);
		});
		lvMoRType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (!surpressEvents) selectNow(n);
		});
	}

	//-------------------------------------------------------------------
	private void selectNow(MagicOrResonanceType value) {
		logger.log(Level.DEBUG, "select now "+value);
		current = value;

		bxDescription.setData(value);
		if (value!=null)
			layout.setTitle(value.getName());
		else
			layout.setTitle(null);

		IMagicOrResonanceController ctrl = charGen.getMagicOrResonanceController();
		ctrl.select(value);
		reLayout(value);
	}

	//-------------------------------------------------------------------
	protected void refresh() {
		surpressEvents = true;
		try {
			IMagicOrResonanceController ctrl = charGen.getMagicOrResonanceController();
			List<MagicOrResonanceType> avail = ctrl.getAvailable();
			cbMoRType.getItems().setAll(avail);
			lvMoRType.getItems().setAll(avail);

			if (avail.size() == 1) {
				cbMoRType.getSelectionModel().select(avail.get(0));
				lvMoRType.getSelectionModel().select(avail.get(0));
				ctrl.select(avail.get(0));
				activeProperty().set(false);
				return;
			} else {
				activeProperty().set(true);
			}

			current = charGen.getModel().getMagicOrResonanceType();
			cbMoRType.getSelectionModel().select(current);
			lvMoRType.getSelectionModel().select(current);
			backHeaderKarma.setValue(charGen.getModel().getKarmaFree());

			updateChoiceConfigNode(current, charGen.getModel());
		} finally {
			surpressEvents = false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.ControllerListener#handleControllerEvent(de.rpgframework.genericrpg.chargen.ControllerEvent, java.lang.Object[])
	 */
	@Override
	public void handleControllerEvent(ControllerEvent type, Object... param) {
		if (type==BasicControllerEvents.CHARACTER_CHANGED)
			refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		reLayout(current);
	}

	//-------------------------------------------------------------------
	protected abstract Node getChoiceConfigNode(MagicOrResonanceType type);

	//-------------------------------------------------------------------
	protected abstract void updateChoiceConfigNode(MagicOrResonanceType type, ShadowrunCharacter<?, ?,?,?> model);

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is shown to user
	 */
	@Override
	public void pageVisited() {
		logger.log(Level.DEBUG, "pageVisited");
		surpressEvents = true;
		MagicOrResonanceType old = charGen.getModel().getMagicOrResonanceType();
		cbMoRType.getItems().setAll(charGen.getMagicOrResonanceController().getAvailable());
		lvMoRType.getItems().setAll(charGen.getMagicOrResonanceController().getAvailable());
		cbMoRType.setValue(old);
		lvMoRType.getSelectionModel().select(old);

		surpressEvents = false;
		updateChoiceConfigNode(charGen.getModel().getMagicOrResonanceType(), charGen.getModel());
	}

}


class MagicOrResonanceCell extends ListCell<MagicOrResonanceType> {

	private VBox layout;
	private Label lblHeading;
	private Label lblSecondLine;
//	private Label lblKarma;

	//--------------------------------------------------------------------
	public MagicOrResonanceCell() {
		lblHeading = new Label();
		lblSecondLine = new Label();
//		lblKarma = new Label();
		lblHeading.getStyleClass().add("base");
		lblSecondLine.setStyle("-fx-text-fill: derive(#efeee9, -30%)");
		layout = new VBox(5);
		layout.getChildren().addAll(lblHeading, lblSecondLine);

//		layout.setMaxWidth(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MagicOrResonanceType item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			lblHeading.setText(item.getName());
			lblSecondLine.setText(RPGFrameworkJavaFX.createSourceText(item));
			lblSecondLine.setUserData(item);
		}
	}
}
