package de.rpgframework.shadowrun.chargen.jfx.listcell;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;

import org.prelle.javafx.ScreenManagerProvider;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.shadowrun.MetamagicOrEcho;
import de.rpgframework.shadowrun.MetamagicOrEchoValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;
import de.rpgframework.shadowrun.chargen.charctrl.IMetamagicOrEchoController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterControllerProvider;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

public class MetamagicEchoValueListCell extends ListCell<MetamagicOrEchoValue> {

//	private static PropertyResourceBundle UI = SR6Constants.RES;

	private final static String NORMAL_STYLE = "quality-cell";

	private final static Logger logger = System.getLogger(MetamagicEchoValueListCell.class.getPackageName());

	private IShadowrunCharacterControllerProvider<IShadowrunCharacterController> controlProvider;
	private ScreenManagerProvider provider;

	private HBox layout;
	private Label name;
	private Label total;
	private Button btnEdit;
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	private ImageView imgRecommended;

	private TilePane tiles;

	private Label  tfDescr;
//	private ChoiceBox<MetamagicOrEchoValue.MentorSpiritMods> cbMystic;

	//-------------------------------------------------------------------
	public MetamagicEchoValueListCell(IShadowrunCharacterControllerProvider<IShadowrunCharacterController> ctrlProv, ScreenManagerProvider provider) {
		this.controlProvider = ctrlProv;
		this.provider = provider;
		

		// Content		
		layout  = new HBox(5);
		name    = new Label();
		total   = new Label();
		tfDescr = new Label();
		btnEdit = new Button("\uE1C2");
		btnEdit.getStyleClass().add("mini-button");
		btnDec  = new Button("\uE0C6");
		btnDec.getStyleClass().add("mini-button");
		lblVal  = new Label("?");
		btnInc  = new Button("\uE0C5");
		btnDec.getStyleClass().add("mini-button");

		// Recommended icon
		imgRecommended = new ImageView();
//		imgRecommended = new ImageView(new Image(SR6Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		initStyle();
		initLayout();
		initInteractivity();
		
		getStyleClass().add("qualityvalue-list-cell");
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		total.getStyleClass().add("text-secondary-info");
		btnInc.getStyleClass().add("mini-button");
		btnDec.getStyleClass().add("mini-button");
//		btnDec.setStyle("-fx-background-color: transparent; ");
//		btnInc.setStyle("-fx-background-color: transparent; -fx-border-color: -fx-outer-border;");
		name.setStyle("-fx-font-weight: bold");
		lblVal.getStyleClass().add("text-subheader");
		//		lblVal.setStyle("-fx-text-fill: -fx-focus-color");

		btnEdit.setStyle("-fx-background-color: transparent; -fx-border-color: -fx-outer-border;");

		//		setStyle("-fx-pref-width: 24em");
//		layout.getStyleClass().add("content");		
		
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox line1 = new HBox(10);
		line1.getChildren().addAll(name);		

		HBox line2 = new HBox(5);
		line2.getChildren().addAll(btnEdit, tfDescr);
		line2.setAlignment(Pos.CENTER_LEFT);

		VBox bxCenter = new VBox(2);
		bxCenter.getChildren().addAll(line1, line2);

		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		tiles = new TilePane(Orientation.HORIZONTAL);
		tiles.setPrefColumns(3);
		tiles.setHgap(4);
		tiles.getChildren().addAll(btnDec, lblVal, btnInc);
		tiles.setAlignment(Pos.CENTER_LEFT);
		layout.getChildren().addAll(bxCenter, tiles);

		bxCenter.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxCenter, Priority.ALWAYS);


		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("rawtypes")
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			logger.log(Level.DEBUG, "Increase "+this.getItem());
			IShadowrunCharacterController ctrl = controlProvider.getCharacterController();
			IMetamagicOrEchoController charGen = ctrl.getMetamagicOrEchoController();
			charGen.increase(this.getItem());
			updateItem(this.getItem(), false); 
			MetamagicEchoValueListCell.this.getListView().refresh();
		});
		btnDec .setOnAction(event -> {
			logger.log(Level.DEBUG, "Decrease "+this.getItem());
			IShadowrunCharacterController ctrl = controlProvider.getCharacterController();
			IMetamagicOrEchoController charGen = ctrl.getMetamagicOrEchoController();
			logger.log(Level.DEBUG, "Call "+charGen.getClass());
			charGen.decrease(this.getItem());
			updateItem(this.getItem(), false); 
			MetamagicEchoValueListCell.this.getListView().refresh();
		});
		btnEdit.setOnAction(event -> {
			editClicked(this.getItem());
		});
//		cbMystic.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//			if (n==getItem().getMysticAdeptUses())
//				return;
//			getItem().setMysticAdeptUses(n);
//			logger.log(Level.DEBUG, "Treat mentor spirit as "+n);
//			Platform.runLater( () -> parentCharGen.runProcessors());			
//		});
//		cbMod.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//			ModificationChoice cmod = (ModificationChoice)cbMod.getUserData();
//			if (n!=null && n!=model.getDecision(cmod)) {
//				logger.log(Level.DEBUG, "Switch ModificationChoice selection to "+n);
//				model.decide(cmod, n);
//				parentCharGen.runProcessors();
//			}
//		});
		

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.log(Level.DEBUG, "drag started for "+this.getItem());
		if (this.getItem()==null)
			return;

		//		logger.log(Level.DEBUG, "canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.log(Level.DEBUG, "canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();
		logger.log(Level.DEBUG, "drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "skill:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = "qualityval:"+this.getItem().getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MetamagicOrEchoValue item, boolean empty) {
		super.updateItem(item, empty);

		if (empty || item==null) {
			setText(null);
			setGraphic(null);			
		} else {
			IShadowrunCharacterController ctrl = controlProvider.getCharacterController();
			ShadowrunCharacter<?, ?,?,?> model = (ShadowrunCharacter<?, ?,?,?>) ctrl.getModel();
			IMetamagicOrEchoController charGen = ctrl.getMetamagicOrEchoController();

			MetamagicOrEcho data = item.getModifyable();
			
			boolean isMentorSpirit = false; //data.getSelect()==ChoiceType.MENTOR_SPIRIT;
			
			
			name.setText(item.getModifyable().getName(Locale.getDefault()));
//			attrib.setText(attr.getShortName()+" "+charGen.getModel().getAttribute(attr).getModifiedValue());
			//			tfDescr.setText(item.getModifyable().getAttribute1().getName());
			tfDescr.setText(item.getDecisionString(Locale.getDefault()));
//			imgRecommended.setVisible(charGen.isRecommended(item.getModifyable()));
			Possible removeable = charGen.canBeDeselected(item);
//			if (data.isFreeSelectable()) {
//				btnEdit.setText("\uE1C2");
////				btnEdit.setTooltip(new Tooltip(UI.getString("skillvaluelistview.tooltip.special")));
//			}
//			tiles.setVisible(data.getMax()>1 && data.isFreeSelectable());
//			lblVal.setText(" "+String.valueOf(item.getModifiedValue())+" ");
			tiles.setVisible(data.getMax()>1);
			tiles.setManaged(data.getMax()>1);
			
			setGraphic(layout);
			btnDec.setDisable(!charGen.canBeDecreased(item).get());
			btnInc.setDisable(!charGen.canBeIncreased(item).get());
			
//			logger.log(Level.DEBUG, "btnEdit = "+btnEdit.getStyleClass());
//			if (btnEdit.getScene()!=null)
//				logger.log(Level.DEBUG, "CSS1 = "+btnEdit.getScene().getStylesheets());
//			if (getScene()!=null)
//				logger.log(Level.DEBUG, "CSS2 = "+getScene().getStylesheets());
		}
	}

	//-------------------------------------------------------------------
	private void editClicked(MetamagicOrEchoValue ref) {
		logger.log(Level.DEBUG, "editClicked");

	}

}