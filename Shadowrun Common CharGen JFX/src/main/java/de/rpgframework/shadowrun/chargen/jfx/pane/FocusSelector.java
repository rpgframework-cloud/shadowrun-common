package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.function.Function;

import org.prelle.javafx.ResponsiveControl;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.Selector;
import de.rpgframework.shadowrun.Focus;
import de.rpgframework.shadowrun.FocusValue;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.chargen.charctrl.IFocusController;
import de.rpgframework.shadowrun.chargen.jfx.listcell.FocusListCell;
import javafx.scene.layout.Pane;

/**
 * @author prelle
 *
 */
public class FocusSelector extends Selector<Focus, FocusValue> implements ResponsiveControl {

	//-------------------------------------------------------------------
	public FocusSelector(IFocusController ctrl, Function<Requirement,String> resolver, Function<Modification,String> modResolver, Quality.QualityType...types) {
		super(ctrl, resolver, modResolver, null);
		listPossible.setCellFactory( lv -> new FocusListCell(ctrl));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.Selector#getDescriptionNode(de.rpgframework.genericrpg.data.ComplexDataItem)
	 */
	@Override
	protected Pane getDescriptionNode(Focus selected) {
		genericDescr.setData(selected);
		return genericDescr;
	}

}
