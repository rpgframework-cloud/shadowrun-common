package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.GenericCore;
import de.rpgframework.shadowrun.Contact;
import de.rpgframework.shadowrun.ContactType;
import de.rpgframework.shadowrun.chargen.charctrl.IContactController;
import de.rpgframework.shadowrun.chargen.jfx.section.ContactSection;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class ContactDetailPane extends VBox {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(ContactSection.class.getPackageName()+".Section");

	private TextField tfName, tfType;
	public ChoiceBox<ContactType> cbType;
	private Label lbRating, lbLoyalty;
	private Button btnRatInc, btnRatDec, btnLoyInc, btnLoyDec;
	private TextArea taDesc;

	private Contact data;
	private IContactController ctrl;

	private Runnable changeCallback;

	//-------------------------------------------------------------------
	public ContactDetailPane(IContactController ctrl, Runnable callback) {
		super(5);
		this.changeCallback = callback;
		this.ctrl = ctrl;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		tfType = new TextField();
		tfType.setPromptText(ResourceI18N.get(RES, "contactsection.prompt.type"));
		cbType = new ChoiceBox<ContactType>();
		cbType.getItems().addAll(GenericCore.getItemList(ContactType.class));
		Collections.sort(cbType.getItems(), new Comparator<ContactType>() {
			public int compare(ContactType o1, ContactType o2) {
				String t1 = (o1!=null)?o1.getName(Locale.getDefault()):"-";
				String t2 = (o1!=null)?o2.getName(Locale.getDefault()):"-";
				return Collator.getInstance().compare(t1, t2);
			}
		});
		cbType.setConverter(new StringConverter<ContactType>() {
			public String toString(ContactType object) {return (object!=null)?object.getName(Locale.getDefault()):"-";}
			public ContactType fromString(String string) {return null;}
		});
		lbRating = new Label();
		lbLoyalty= new Label();
		btnRatDec = new Button("\uE0C9");
		btnRatInc = new Button("\uE0C8");
		btnLoyDec = new Button("\uE0C9");
		btnLoyInc = new Button("\uE0C8");
		btnRatDec.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnRatInc.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnLoyDec.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnLoyInc.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		taDesc = new TextArea();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdRating = new Label(ResourceI18N.get(RES, "contactsection.label.rating"));
		Label hdLoyalty = new Label(ResourceI18N.get(RES, "contactsection.label.loyalty"));
		GridPane values = new GridPane();
		values.setVgap(5);
		values.setHgap(5);
		values.add(hdRating , 0, 0);
		values.add(btnRatDec, 1, 0);
		values.add(lbRating , 2, 0);
		values.add(btnRatInc, 3, 0);
		values.add(hdLoyalty, 0, 1);
		values.add(btnLoyDec, 1, 1);
		values.add(lbLoyalty, 2, 1);
		values.add(btnLoyInc, 3, 1);

		Label hdName = new Label(ResourceI18N.get(RES, "contactsection.label.name"));
		Label hdType = new Label(ResourceI18N.get(RES, "contactsection.label.type"));

		GridPane nameAndType = new GridPane();
		nameAndType.setVgap(5);
		nameAndType.setHgap(5);
		nameAndType.add(hdName, 0,0);
		nameAndType.add(tfName, 1,0, 2,1);
		nameAndType.add(hdType, 0,1);
		nameAndType.add(cbType, 1,1);
		nameAndType.add(tfType, 2,1);
		getChildren().addAll(nameAndType, values, taDesc);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnRatInc.setOnAction(ev -> {ctrl.increaseRating(data); refresh();});
		btnRatDec.setOnAction(ev -> {ctrl.decreaseRating(data); refresh();});
		btnLoyInc.setOnAction(ev -> {ctrl.increaseLoyalty(data); refresh();});
		btnLoyDec.setOnAction(ev -> {ctrl.decreaseLoyalty(data); refresh();});
		tfName.textProperty().addListener( (ov,o,n) -> { data.setName(n); if (changeCallback!=null) changeCallback.run();});
		taDesc.textProperty().addListener((ov,o,n) -> { data.setDescription(n); if (changeCallback!=null) changeCallback.run();});
		cbType.valueProperty().addListener( (ov,o,n) -> { data.setType(n); if (changeCallback!=null) changeCallback.run();});
		tfType.textProperty().addListener( (ov,o,n) -> { data.setTypeName(n); if (changeCallback!=null) changeCallback.run();});
	}

	//-------------------------------------------------------------------
	public void setController(IContactController ctrl) {
		this.ctrl = ctrl;
	}

	//-------------------------------------------------------------------
	public void setData(Contact data) {
		if (data==null) throw new NullPointerException("Contact may not be null");
		this.data = data;
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		btnRatInc.setDisable(!ctrl.canIncreaseRating(data).get());
		btnRatDec.setDisable(!ctrl.canDecreaseRating(data).get());
		btnLoyInc.setDisable(!ctrl.canIncreaseLoyalty(data).get());
		btnLoyDec.setDisable(!ctrl.canDecreaseLoyalty(data).get());
		if (data==null) {
			tfName.setText(null);
			tfType.setText(null);
			cbType.getSelectionModel().clearSelection();
			taDesc.setText(null);
			lbRating.setText("?");
			lbLoyalty.setText("?");
			return;
		}
		tfName.setText(data.getName());
		tfType.setText(data.getTypeName());
		cbType.setValue(data.getType());
		taDesc.setText(data.getDescription());
		lbRating.setText(String.valueOf(data.getRating()));
		lbLoyalty.setText(String.valueOf(data.getLoyalty()));
	}

}
