package de.rpgframework.shadowrun.chargen.jfx.pages;

import java.io.File;
import java.io.FileInputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.ComponentElementView;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.items.AAvailableSlot;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.Hook;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * @author Stefan Prelle
 *
 */
@SuppressWarnings("rawtypes")
public abstract class ACarriedItemPage<T extends PieceOfGear<?,?,?,?>, H extends Hook, A extends AAvailableSlot<H,T>> extends Page {

	public static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(ACarriedItemPage.class.getPackageName()+".AllPages");

	private final static Logger logger = System.getLogger(ACarriedItemPage.class.getPackageName());

	private IShadowrunCharacterController control;

	private NavigButtonControl btnControl;

	protected ComponentElementView view;
	private TextField tfCustomName;
	private TextArea  taCustomDesc;
	private ValueInfo priceAndCount;

	private OptionalNodePane content;
	protected GenericDescriptionVBox description;
	protected CarriedItem<T> selectedItem;

	protected Map<H, Integer> positionCache;

	//--------------------------------------------------------------------
	public ACarriedItemPage(IShadowrunCharacterController ctrl, CarriedItem<T> data) {
		super(ResourceI18N.get(UI,"dialog.editcarrieditem.title"), null);
		this.control = ctrl;
		this.selectedItem    = data;
		this.positionCache = new HashMap<H, Integer>();
		btnControl = new NavigButtonControl();

		initCompoments();
		initLayout();
		initInteractivity();
		refresh();

		description.setData(data.getResolved());

//		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	protected abstract Function<CarriedItem,ItemAttributeNumericalValue> getNuyenResolver();

	//--------------------------------------------------------------------
	protected void initCompoments() {
		taCustomDesc = new TextArea();
		taCustomDesc.setPromptText(ResourceI18N.get(UI, "dialog.editcarrieditem.custom.description"));
		tfCustomName = new TextField();
		tfCustomName.setPromptText(selectedItem.getResolved().getName());

		priceAndCount = new ValueInfo(selectedItem, control, getNuyenResolver());

		view = new ComponentElementView();
		view.setTitle(selectedItem.getNameWithRating());
		//view.setSubtitle(selectedItem.getUsedAsSubType().getName());
		description = new GenericDescriptionVBox(null,null, selectedItem.getResolved());
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		// Layout
		content = new OptionalNodePane(view, description);
		content.setUseScrollPane(false);

		this.setMaxHeight(Double.MAX_VALUE);
		setContent(content);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		taCustomDesc.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()==0 || n.isBlank()) {
				selectedItem.setNotes(null);
			} else {
				selectedItem.setNotes(n);
			}
		});
		tfCustomName.textProperty().addListener( (ov,o,n) -> {
			if (n==null || n.length()==0 || n.isBlank()) {
				selectedItem.setCustomName(null);
			} else {
				selectedItem.setCustomName(n);
			}
		});

		// Requires update inn JavaFXExtension
//		for (int i=3; i<=12; i++) {
//			view.getListView(i);
//		}

		try {
			view.setOnEditAction(ev -> showImageDialog());
		} catch (Throwable e) {
			logger.log(Level.ERROR, "Cannot listen to image change requests: "+e);
		}

		setOnModeChangeAction(mode -> changedResponsiveMode(mode.getMode()));
	}

	//--------------------------------------------------------------------
	private void changedResponsiveMode(WindowMode value) {
		logger.log(Level.ERROR, "Mode changed {0}",value);
		ResponsiveControlManager.changeModeRecursivly(this, value);
		ResponsiveControlManager.changeModeRecursivly(content, value);
	}

	//--------------------------------------------------------------------
	protected int getIndexFor(A slot) {
		return -1;
	}

	//--------------------------------------------------------------------
	private String toString(float val) {
		if (  ((int)val) == val )
			return String.valueOf( (int)val);
		return String.format("%.1f",val);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void positionSlot(A slot) {
//		logger.log(Level.DEBUG, "positionSlot({0})", slot);
		boolean large = false;
		int index = getIndexFor(slot);

		// Consult cache, if necessary
		if (index<0 && positionCache.containsKey(slot.getHook())) {
			index = positionCache.get(slot.getHook());
		}

		logger.log(Level.DEBUG, "positionSlot({0}) returns {1}", slot, index);
		if (index<0) {
			logger.log(Level.ERROR, "No instruction where to position slot "+slot.getHook());
			boolean found = false;
			for (int i=2; i<=12; i++) {
				if (view.getName(i)==null) {
					logger.log(Level.ERROR, "Guess slot "+i+" for "+slot.getHook());
					if (slot.getHook().hasCapacity() && slot.getCapacity()!=99) {
						view.setName(i, slot.getHook().getName()+"  "+toString(slot.getFreeCapacity())+"/"+toString(slot.getCapacity()));
					} else {
						view.setName(i, slot.getHook().getName());  //
					}
					view.getList(i).setAll(slot.getAllEmbeddedItems());
					final int staticI = i;
					view.setCellFactory(i, (lv)-> new EditDialogCarriedItemListCell(control, view.getList(staticI), selectedItem, slot.getHook()) {
						public void editAccessory(CarriedItem accessoryToEdit) {
							ACarriedItemPage.this.editAccessory(accessoryToEdit);
						}});
					view.setOnAddAction(i, ev -> addClicked(slot.getHook()));
					positionCache.put(slot.getHook(), i);
					found = true;
					break;
				}
			}
			if (!found) {
				logger.log(Level.ERROR, "Could not display list for slot "+slot.getHook());
			}
		} else  {
			final int staticIndex = index;
			logger.log(Level.TRACE, " slot {0} has capacity = {1}", slot.getHook(), slot.getHook().hasCapacity());
			if (slot.getHook().hasCapacity() && slot.getCapacity()!=99) {
				view.setName(index, slot.getHook().getName()+"  "+toString(slot.getFreeCapacity())+"/"+toString(slot.getCapacity()));
			} else {
				view.setName(index, slot.getHook().getName());
			}
			view.getList(index).setAll(slot.getAllEmbeddedItems());
			view.setCellFactory(index, (lv)-> new EditDialogCarriedItemListCell(control, view.getList(staticIndex), selectedItem, slot.getHook()) {
				public void editAccessory(CarriedItem accessoryToEdit) {
					ACarriedItemPage.this.editAccessory(accessoryToEdit);
				}});
			view.setOnAddAction(index, ev -> addClicked(slot.getHook()));
		}
	}

	//--------------------------------------------------------------------
	protected abstract void editAccessory(CarriedItem<T> accessoryToEdit);

	//--------------------------------------------------------------------
	protected void updateImage() {
	}

	//--------------------------------------------------------------------
	public void refresh()  {

		logger.log(Level.INFO, "refresh");
		updateImage();
		description.setData(selectedItem);

		// Add fixed components
		positionNonSlots();
		positionSlots();


		tfCustomName.setText(selectedItem.getCustomName());
		taCustomDesc.setText(selectedItem.getNotes());
	}

	//-------------------------------------------------------------------
	/**
	 * Position all elements that are NOT AvailableSlots
	 */
	protected void positionNonSlots() {
		// Price/Count
		view.setOverrideComponent(0, priceAndCount);
		priceAndCount.refresh();

//		view.setOverrideComponent(1, ItemUtilJFX.getFlexItemInfoNode(selectedItem, control));

		// Custom
		Label hdName = new Label(ResourceI18N.get(UI, "dialog.editcarrieditem.custom.name"));
		tfCustomName.setMaxWidth(Double.MAX_VALUE);
		hdName.getStyleClass().add("base");
		HBox bxLine = new HBox(5, hdName, tfCustomName);
		HBox.setHgrow(tfCustomName, Priority.ALWAYS);
		bxLine.setAlignment(Pos.CENTER_LEFT);
		VBox bxCustom = new VBox(5, bxLine, taCustomDesc);
		// Don't show customization options for auto-created items
//		if (!selectedItem.isAutoAdded() ) {
			view.setOverrideComponent(7, bxCustom);
			view.setLargeComponent(7, true);
//		}
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	protected void positionSlots() {
		for (AAvailableSlot<? extends Hook,T> slot : selectedItem.getSlots()) {
			positionSlot((A) slot);
		}
	}

//	//--------------------------------------------------------------------
//	/**
//	 * @param event
//	 */
//	@Override
//	public void handleGenerationEvent(GenerationEvent event) {
//		logger.warn("RCV "+event+"-----------------------------------");
//		switch (event.getType()) {
//		case EQUIPMENT_ADDED:
//		case EQUIPMENT_CHANGED:
//		case EQUIPMENT_REMOVED:
//		case CHARACTER_CHANGED:
//			refresh();
//			break;
//		default:
//		}
//	}


	//-------------------------------------------------------------------
	private void afterTryingToAdd(T master, CarriedItem added) {
		if (added==null) {
			FlexibleApplication.getInstance().showAlertAndCall(AlertType.ERROR,
					ResourceI18N.get(UI, "selectiondialog.failed.title"), ResourceI18N.format(UI, "selectiondialog.failed.format",master.getName()));
		}
		logger.log(Level.INFO, "embedding "+master+" in "+selectedItem+" returned "+added);
		if (added!=null) {
			logger.log(Level.INFO, "explicitly call refresh independent from event");
			refresh();
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @param slot
	 * @return
	 */
	protected void addClicked(H slot) {
		logger.log(Level.INFO, "addClicked("+slot+")");

		List<T> data = control.getEquipmentController().getEmbeddableIn(selectedItem, slot);
		logger.log(Level.INFO, "getEmbeddableIn("+selectedItem+") returns "+data.size()+" elements");
//		SelectAccessoryDialog dialog = new SelectAccessoryDialog(
//				ResourceI18N.get(UI, "dialog.add.accessory.title"),
//				data, slot,
//				lv -> new ItemTemplateListCell2(control.getCharacter(), control.getEquipmentController(), true, selectedItem, ItemType.ACCESSORY),
//				CloseType.CANCEL, CloseType.OK);
//		CloseType closed = provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
//		logger.log(Level.INFO, "Closed via "+closed);
//		if (closed==CloseType.OK) {
//			for (T master : dialog.getSelection()) {
//     			logger.log(Level.DEBUG, "add accessory "+master+" to "+selectedItem+" in slot "+slot);
//     			logger.log(Level.DEBUG, "embed "+master+" in "+selectedItem);
//     			UseAs usage = master.getUsageFor(slot);
//     			logger.log(Level.INFO, "use for slot "+slot+" is "+usage);
//     			ItemType useAs = (usage==null)?master.getNonAccessoryType():usage.getType();
//     			logger.log(Level.INFO, "ItemType is "+useAs);
//     			List<SelectionOptionType> options = control.getEquipmentController().getOptions(master, usage);
//				try {
//					if (!options.isEmpty()) {
//						logger.log(Level.INFO, "ask options");
//						Platform.runLater( () -> {
//							SelectionOption[] opts = ItemUtilJFX.askOptionsFor( provider, control.getEquipmentController(), master, selectedItem, useAs, selectedItem.getSlot(slot).getFreeCapacity(), usage);
//							CarriedItem added = control.getEquipmentController().embed(selectedItem, master, slot, opts);
//							logger.log(Level.INFO, "After adding =============="+selectedItem.getSlot(slot));
//							afterTryingToAdd(master, added);
//						});
//					} else {
//						logger.log(Level.INFO, "dont ask options");
//	    				CarriedItem added = control.getEquipmentController().embed(selectedItem, master, slot);
//						afterTryingToAdd(master, added);
//					}
//				} catch (Exception e) {
//					logger.log(Level.ERROR, "Failed asking for Options",e);
//					StringWriter out = new StringWriter();
//					e.printStackTrace(new PrintWriter(out));
//					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE,2,out.toString());
//				}
//			}
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @param slot
	 * @return
	 */
	private void addModificationClicked() {
		logger.log(Level.INFO, "addModificationClicked");

//		List<ItemEnhancement> data = control.getEquipmentController().getAvailableEnhancementsFor(selectedItem);
//		SelectPluginDataDialog<ItemEnhancement> dialog = new SelectPluginDataDialog<ItemEnhancement>(
//				ResourceI18N.get(UI, "dialog.add.enhancement.title"),
//				data,
//				lv -> new ItemEnhancementListCell(control.getCharacter(), control.getEquipmentController(), selectedItem),
//				CloseType.CANCEL, CloseType.OK);
//		CloseType closed = provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
//		logger.log(Level.INFO, "Closed via "+closed);
//		if (closed==CloseType.OK) {
//			for (ItemEnhancement master : dialog.getSelection()) {
//     			logger.log(Level.DEBUG, "add modification "+master+" to "+selectedItem);
//     			ItemEnhancementValue result = control.getEquipmentController().modify(selectedItem, master);
//     			logger.log(Level.DEBUG, "embedding "+master+" in "+selectedItem+" returned "+result);
//     			if (result==null) {
//     				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 0, ResourceI18N.format(UI, "dialog.add.enhancement.fail", master.getName()));
//     			}
//			}
//		}
	}


	//-------------------------------------------------------------------
	private void showImageDialog() {
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Open Icon 300x200 Pixel max");
		FileChooser.ExtensionFilter imageFilter
	    = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
		chooser.setSelectedExtensionFilter(imageFilter);
		File file = chooser.showOpenDialog(getScene().getWindow());
		if (file!=null) {
			try {
				Image img = new Image(new FileInputStream(file));
				if (img.getWidth()>300 || img.getHeight()>200) {
					Alert alert = new Alert(Alert.AlertType.WARNING, "Max. 300x200 Pixel", ButtonType.CLOSE);
					alert.showAndWait();
					return;
				}
				logger.log(Level.INFO, "Custom image set");
				selectedItem.setImage(Files.readAllBytes(file.toPath()));
				view.setImage(img);
			} catch (Exception e) {
				logger.log(Level.ERROR, "Failed on image",e);
			}
		}
	}

}

abstract class EditDialogCarriedItemListCell<T extends PieceOfGear<?, ?, ?, ?>> extends ListCell<Object> {

	private static PropertyResourceBundle UI = ACarriedItemPage.UI;

	private final static Logger logger = System.getLogger(EditDialogCarriedItemListCell.class.getPackageName());

	private final static String NORMAL_STYLE = "carrieditem-cell";

	private IShadowrunCharacterController control;
	private ObservableList<Object> parent;
	private CarriedItem container;
	private Hook hook;

	private Label lbName;
	private Button btnDel;
	private Button btnEdit;
	private MenuItem menUndo;
	private MenuItem menSell;
	private MenuItem menDelete;

	private HBox layout;

	//-------------------------------------------------------------------
	public EditDialogCarriedItemListCell(IShadowrunCharacterController charGen, ObservableList<Object> parent, CarriedItem container, Hook hook) {
		this.control = charGen;
		this.container = container;
		this.hook   = hook;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		getStyleClass().add(NORMAL_STYLE);

		FontIcon icoPopup  = new FontIcon("\uE17E\uE107");
		btnDel = new Button(null, icoPopup);
		btnDel.getStyleClass().add("mini-button");
//		btnDel.setOnAction(ev -> {
//			logger.log(Level.DEBUG, "remove accessory "+item+" from "+container.getName());
//			if (control.remove(container, item)) {
//				slot.removeEmbeddedItem(item);
//				refresh();
//			}
//		});
		// Context menu
		FontIcon icoDelete = new FontIcon("\uE17E\uE107");
		FontIcon icoSell   = new FontIcon("\uE17E \u00A5");
		FontIcon icoUndo   = new FontIcon("\uE17E\uE10E");
		icoDelete.setStyle("-fx-padding: 3px");
		icoSell.setStyle("-fx-padding: 3px");
		icoUndo.setStyle("-fx-padding: 3px");
		menUndo = new MenuItem(ResourceI18N.get(UI, "label.removeitem.undo"), icoUndo);
		menSell = new MenuItem(ResourceI18N.get(UI, "label.removeitem.sell"), icoSell);
		menDelete = new MenuItem(ResourceI18N.get(UI, "label.removeitem.trash"), icoDelete);
		ContextMenu menu = new ContextMenu();
		menu.getItems().add(menUndo);
		menu.getItems().add(menSell);
		menu.getItems().add(menDelete);
		btnDel.setContextMenu(menu);

		btnDel.setOnAction(event -> menu.show(btnDel, Side.BOTTOM, 0, 0));

		// Edit button
		FontIcon icoEdit  = new FontIcon("\uE17E\uE104");
		btnEdit = new Button(null, icoEdit);
		btnEdit.getStyleClass().add("mini-button");
		btnEdit.setManaged(false);
		btnEdit.setVisible(false);


		lbName = new Label();
		lbName.setMaxWidth(Double.MAX_VALUE);
		lbName.setStyle("-fx-font-size: 120%");

		layout = new HBox(15, lbName, btnEdit, btnDel);
		layout.setAlignment(Pos.CENTER_LEFT);
		HBox.setHgrow(lbName, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	public abstract void editAccessory(CarriedItem<T> accessoryToEdit);

	//-------------------------------------------------------------------
	public void editAc2cessory(CarriedItem<T> accessoryToEdit) {
		logger.log(Level.WARNING, "edit accessory {0} in {1}", accessoryToEdit, container.getNameWithRating());
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	public void updateItem(Object oitem, boolean empty) {
		super.updateItem(oitem, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			CarriedItem<T> item = (CarriedItem<T>)oitem;
			lbName.setText(item.getNameWithRating());
			logger.log(Level.WARNING, "isAutoAdded( {0} ) = {1}", item, item.isAutoAdded());
			btnDel.setVisible(!item.isAutoAdded());
//			menDelete.setOnAction(event -> { logger.log(Level.INFO, "Trash "+item); control.getEquipmentController().sell(item, 0f); });
//			menSell  .setOnAction(event -> { logger.log(Level.INFO, "Sell  "+item); control.getEquipmentController().sell(item, 0.5f);});
			menUndo  .setOnAction(event -> {
				logger.log(Level.INFO, "Undo  "+item);
				boolean removed = false;
				if (container==null) {
					removed = control.getEquipmentController().deselect(item);
				} else {
					removed = control.getEquipmentController().removeEmbedded(container, hook, item).get();
				}
				if (removed) {
					getListView().getItems().remove(item);
				}
				});

			if (item.getSlots().size()>0) {
				btnEdit.setManaged(true);
				btnEdit.setVisible(true);
				btnEdit.setOnAction(ev -> {
					editAccessory(item);
				});
			}

			setGraphic(layout);
		}
	}
}

@SuppressWarnings("rawtypes")
class ValueInfo extends VBox {

	private final static Logger logger = System.getLogger(ValueInfo.class.getPackageName());

	private static PropertyResourceBundle RES = ACarriedItemPage.UI;

	private CarriedItem item;
	private IShadowrunCharacterController control;
	private Function<CarriedItem,ItemAttributeNumericalValue> nuyenResolver;

	private Button btnDec;
	private Button btnInc;
	private Label  lbCount;
	private Label  lbValue;

	//-------------------------------------------------------------------
	public ValueInfo(CarriedItem item, IShadowrunCharacterController control, Function<CarriedItem,ItemAttributeNumericalValue> nuyenResolver) {
		this.item = item;
		this.control = control;
		this.nuyenResolver = nuyenResolver;
		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	public void initComponents() {
		lbValue = new Label();
		lbValue.setStyle("-fx-font-weight: bold; -fx-font-size: 120%");

		lbCount = new Label();
		lbCount.setStyle("-fx-font-weight: bold; -fx-font-size: 120%");

		btnDec = new Button(null, new SymbolIcon("remove"));
		btnInc = new Button(null, new SymbolIcon("add"));
		btnDec.getStyleClass().addAll("mini-icon","extended-list-view-button");
		btnInc.getStyleClass().addAll("mini-icon","extended-list-view-button");
	}

	//-------------------------------------------------------------------
	public void initLayout() {
		setAlignment(Pos.TOP_CENTER);
		Label hdValue = new Label(ResourceI18N.get(RES, "dialog.editcarrieditem.value"));
		hdValue.getStyleClass().add("table-head");
		hdValue.setMaxWidth(Double.MAX_VALUE);
		hdValue.setAlignment(Pos.CENTER);
		getChildren().addAll(hdValue, lbValue);

		TilePane bxButtons = new TilePane();
		bxButtons.setVgap(5);
		bxButtons.getChildren().addAll(btnDec, lbCount, btnInc);
		bxButtons.setAlignment(Pos.CENTER);
		Label hdCount = new Label(ResourceI18N.get(RES, "dialog.editcarrieditem.count"));
		hdCount.getStyleClass().add("table-head");
		hdCount.setMaxWidth(Double.MAX_VALUE);
		hdCount.setAlignment(Pos.CENTER);
		getChildren().addAll(hdCount, bxButtons);

		VBox.setMargin(hdCount, new Insets(20, 0, 0, 0));
		VBox.setMargin(bxButtons, new Insets(5, 0, 0, 0));
	}

	//-------------------------------------------------------------------
	public void initInteractivity() {
		btnDec.setOnAction(ev -> { control.getEquipmentController().decrease(item); refresh(); });
		btnInc.setOnAction(ev -> { control.getEquipmentController().increase(item); refresh(); });
	}

	//-------------------------------------------------------------------
	public void refresh() {
		lbValue.setText(nuyenResolver.apply(item).getModifiedValue()+" Nuyen");
		lbCount.setText(String.valueOf(item.getCount()));
		btnDec.setDisable(!control.getEquipmentController().canChangeCount(item, item.getCount()-1));
		btnInc.setDisable(!control.getEquipmentController().canChangeCount(item, item.getCount()+1));
	}

}