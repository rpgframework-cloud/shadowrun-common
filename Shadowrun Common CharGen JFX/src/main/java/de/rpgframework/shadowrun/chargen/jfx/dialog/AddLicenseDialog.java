package de.rpgframework.shadowrun.chargen.jfx.dialog;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;

import de.rpgframework.ResourceI18N;
import de.rpgframework.shadowrun.SIN;
import de.rpgframework.shadowrun.SIN.FakeRating;
import de.rpgframework.shadowrun.chargen.charctrl.IShadowrunCharacterController;
import de.rpgframework.shadowrun.chargen.charctrl.SINController;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
public class AddLicenseDialog extends ManagedDialog {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(AddLicenseDialog.class.getPackageName()+".AllDialogs");

	class MyTitledComponent extends VBox {
		public MyTitledComponent(String title, Node node) {
			Label head = new Label(title);
			head.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
			getChildren().addAll(head, node);
		}
	}
	
	private IShadowrunCharacterController<?,?,?,?>  control;
	private SINController sinControl;
	private NavigButtonControl btnControl;

	private Label    lblNuyenFree, lblCost;
	
	private TextField tfLicenseName;
	private ChoiceBox<FakeRating> cbLicenseRating;
	private ChoiceBox<SIN> cbSIN;

	//-------------------------------------------------------------------
	public AddLicenseDialog(IShadowrunCharacterController<?,?,?,?> control) {
		super(ResourceI18N.get(RES, "dialog.license.title"), null, CloseType.OK, CloseType.CANCEL);
		this.control = control;
		this.sinControl = control.getSINController();
		btnControl = new NavigButtonControl();
		btnControl.initialize(FlexibleApplication.getInstance(), this);


		initComponents();
		initLayout();
		lblNuyenFree.setText(control.getModel().getNuyen()+" ");

		initInteractivity();
		cbLicenseRating.setValue(FakeRating.ANYONE);		
		updateAllCost();
		
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		cbSIN = new ChoiceBox<>();
		cbSIN.getItems().addAll(control.getModel().getSINs());
		cbSIN.setConverter(new StringConverter<SIN>() {
			public String toString(SIN value) {return (value!=null)?value.getName():"?";}
			public SIN fromString(String string) {return null;}
		});
		lblCost= new Label();

		lblNuyenFree = new Label();
		lblNuyenFree.setStyle("-fx-text-fill: accent");
		
		/* License column */
		tfLicenseName = new TextField();
		tfLicenseName.setPromptText(ResourceI18N.get(RES, "dialog.sin.license_prompt"));
		cbLicenseRating = new ChoiceBox<>();
		cbLicenseRating.getItems().addAll(FakeRating.getSelectableValues());
		cbLicenseRating.setValue(FakeRating.ANYONE);
		cbLicenseRating.setConverter(new StringConverter<SIN.FakeRating>() {
			public String toString(FakeRating value) {return value.getValue()+" - \u00A5 "+(value.getValue()*200);}
			public FakeRating fromString(String string) {return null;}
		});
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		MyTitledComponent name = new MyTitledComponent(ResourceI18N.get(RES, "label.name"), tfLicenseName);
		MyTitledComponent qual = new MyTitledComponent(ResourceI18N.get(RES, "label.quality"), cbLicenseRating);
		MyTitledComponent sin  = new MyTitledComponent(ResourceI18N.get(RES, "label.sin"), cbSIN);
		
		Label hdCost = new Label(ResourceI18N.get(RES, "label.cost")+": ");
		hdCost.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		HBox lastLine = new HBox(5, hdCost, lblCost, new Label("/"), lblNuyenFree);

		VBox layout = new VBox(5, name, qual, sin, lastLine);
		layout.setStyle("-fx-max-width: 25em; -fx-min-width: 20em");
		setContent(new VBox(5, layout, lastLine));
	}

	//-------------------------------------------------------------------
	private void updateOKButton() {
		boolean enoughText = tfLicenseName.getText()!=null && tfLicenseName.getText().length()>0;
		boolean canSelect  = sinControl.canCreateNewLicense(cbSIN.getValue(), cbLicenseRating.getValue());
		btnControl.setDisabled(CloseType.APPLY, !(enoughText && canSelect));
	}

	//-------------------------------------------------------------------
	private void updateAllCost() {
		int cost = cbLicenseRating.getValue().getValue()*200;
		lblCost.setText("\u00A5 "+ String.valueOf(cost));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbLicenseRating.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)-> {
			if (n!=null) {
				updateAllCost();
				updateOKButton();
			}
		});
		tfLicenseName.textProperty().addListener( (ov,o,n) -> {updateOKButton();});
	}

	//-------------------------------------------------------------------
	public Object[] getData() {
		return new Object[]{tfLicenseName.getText(), cbLicenseRating.getValue(), cbSIN.getValue()};
	}

	//-------------------------------------------------------------------
	private void refresh() {
		
		lblNuyenFree.setText(control.getModel().getNuyen()+" ");
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	public String getName() { return tfLicenseName.getText(); }
	public FakeRating getRating() { return cbLicenseRating.getValue(); }
	public SIN getSIN() { return cbSIN.getValue(); }
	
}
