package de.rpgframework.shadowrun.chargen.jfx.pane;

import java.util.function.Function;

import org.prelle.javafx.ResponsiveControl;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.Selector;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun.QualityValue;
import de.rpgframework.shadowrun.chargen.charctrl.IQualityController;
import de.rpgframework.shadowrun.chargen.jfx.listcell.QualityListCell;
import de.rpgframework.shadowrun.chargen.jfx.pages.FilterQualities;
import javafx.scene.layout.Pane;

/**
 * @author prelle
 *
 */
public class QualitySelector extends Selector<Quality, QualityValue> implements ResponsiveControl {

	//-------------------------------------------------------------------
	public QualitySelector(IQualityController ctrl, Function<Requirement,String> resolver, Function<Modification,String> modResolver) {
		super(ctrl, resolver, modResolver, new FilterQualities());
		listPossible.setCellFactory( lv -> new QualityListCell(ctrl, resolver));
	}

	//-------------------------------------------------------------------
	public QualitySelector(IQualityController ctrl, Function<Requirement,String> resolver, Function<Modification,String> modResolver, FilterQualities.PosNeg positiveOrNegative) {
		super(ctrl, resolver, modResolver, new FilterQualities(positiveOrNegative));
		listPossible.setCellFactory( lv -> new QualityListCell(ctrl, resolver));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.Selector#getDescriptionNode(de.rpgframework.genericrpg.data.ComplexDataItem)
	 */
	@Override
	protected Pane getDescriptionNode(Quality selected) {
		genericDescr.setData(selected);
		return genericDescr;
	}

}
