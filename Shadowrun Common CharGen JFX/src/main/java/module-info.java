open module shadowrun.common.chargen.jfx {
	exports de.rpgframework.shadowrun.chargen.jfx;
	exports de.rpgframework.shadowrun.chargen.jfx.dialog;
	exports de.rpgframework.shadowrun.chargen.jfx.fxml;
	exports de.rpgframework.shadowrun.chargen.jfx.listcell;
	exports de.rpgframework.shadowrun.chargen.jfx.pages;
	exports de.rpgframework.shadowrun.chargen.jfx.pane;
 	exports de.rpgframework.shadowrun.chargen.jfx.section;
 	exports de.rpgframework.shadowrun.chargen.jfx.wizard;

	requires transitive de.rpgframework.javafx;
	requires de.rpgframework.rules;
	requires javafx.base;
	requires javafx.controls;
	requires transitive javafx.extensions;
	requires javafx.graphics;
	requires transitive shadowrun.common;
	requires transitive shadowrun.common.chargen;
	requires javafx.fxml;
	requires org.controlsfx.controls;
	requires rpgframework.pdfviewer;
}