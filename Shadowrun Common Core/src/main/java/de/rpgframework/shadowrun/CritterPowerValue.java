package de.rpgframework.shadowrun;

import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableNumericalValue;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
@Root(name = "critterpowerval")
public class CritterPowerValue extends ComplexDataItemValue<CritterPower> implements ModifyableNumericalValue<CritterPower> {

	//-------------------------------------------------------------------
	public CritterPowerValue() {
	}

	//-------------------------------------------------------------------
	public CritterPowerValue(CritterPower data, int val) {
		super(data, val);
	}

	//-------------------------------------------------------------------
	public CritterPowerValue(CritterPower data) {
		super(data, 0);
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (incomingModifications.isEmpty())
			return String.format("%s",getName());
		return String.format("%s = %d (mod=%s)",
				getName(),
				value,
				String.valueOf(incomingModifications)
				);
	}


	//-------------------------------------------------------------------
	public String getName() {
		if (resolved==null)
			return "("+ref+" "+value+")";

		String ratingSuffix="";
		if (resolved.hasLevel())
			ratingSuffix=" "+getModifiedValue();

		return resolved.getName()+ratingSuffix;
	}

}
