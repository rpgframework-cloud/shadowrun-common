package de.rpgframework.shadowrun;

import java.util.UUID;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.shadowrun.SIN.FakeRating;

/**
 * @author prelle
 *
 */
public class LicenseValue {

	@Attribute(name="uniqueid")
	private UUID uniqueId;
	@Attribute(name="sin")
	private UUID sin;
	@Attribute
	private String name;
	@Attribute(required=true)
	private SIN.FakeRating rating;
	private transient Object injectedBy;

	//-------------------------------------------------------------------
	public LicenseValue() {
		uniqueId = UUID.randomUUID();
	}

	//-------------------------------------------------------------------
	public LicenseValue(String name, FakeRating rating) {
		this();
		this.rating = rating;
		this.name   = name;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "LicenseValue("+name+", "+rating+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the uniqueId
	 */
	public UUID getUniqueId() {
		return uniqueId;
	}

	//-------------------------------------------------------------------
	/**
	 * @param uniqueId the uniqueId to set
	 */
	public void setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
	}

	//-------------------------------------------------------------------
	public String getNameWithRating() {
		return name+" "+rating.getValue();
	}

	//-------------------------------------------------------------------
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sin
	 */
	public UUID getSIN() {
		return sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sin the sin to set
	 */
	public void setSIN(UUID sin) {
		this.sin = sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public SIN.FakeRating getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rating the rating to set
	 */
	public void setRating(SIN.FakeRating rating) {
		this.rating = rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the injectedBy
	 */
	public Object getInjectedBy() {
		return injectedBy;
	}

	//-------------------------------------------------------------------
	/**
	 * @param injectedBy the injectedBy to set
	 */
	public void setInjectedBy(Object injectedBy) {
		this.injectedBy = injectedBy;
	}

}
