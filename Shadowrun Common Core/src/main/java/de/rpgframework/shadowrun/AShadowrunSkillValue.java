package de.rpgframework.shadowrun;

import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.data.ASkillValue;
import de.rpgframework.genericrpg.modification.CheckModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
@Root(name = "skillval")
public abstract class AShadowrunSkillValue<T extends AShadowrunSkill> extends ASkillValue<T> implements NumericalValue<T> {

	protected transient int modifierCap;

	//-------------------------------------------------------------------
	public AShadowrunSkillValue() {
	}

	//-------------------------------------------------------------------
	public AShadowrunSkillValue(T skill, int val) {
		super(skill, val);
	}

	//-------------------------------------------------------------------
	public AShadowrunSkillValue(AShadowrunSkillValue<T> toClone) {
		this.resolved = toClone.resolved;
		this.value = toClone.value;
		incomingModifications.addAll(toClone.getIncomingModifications());
	}

	//-------------------------------------------------------------------
	@Deprecated
	public String getName() {
		return getName(Locale.getDefault());
	}

	//-------------------------------------------------------------------
	public String getName(Locale loc) {
		StringBuffer buf = new StringBuffer();
		if (resolved==null)
			buf.append("? ");
		else
			buf.append(getSkill().getName(loc)+" ");
		buf.append(String.valueOf(value));
		if (getModifier()>0)
			buf.append("("+getModifiedValue()+")");
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ComplexDataItemValue#getNameWithoutRating(java.util.Locale)
	 */
	@Override
	public String getNameWithoutRating(Locale loc) {
		if (resolved==null)
			return ref;
		if ("knowledge".equals(getKey()) || "language".equals(getKey())) {
			return getDecisionString(loc);
		}
		return resolved.getName(loc);
	}


	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public boolean equals(Object o) {
		if (o instanceof AShadowrunSkillValue) {
			AShadowrunSkillValue<T> other = (AShadowrunSkillValue<T>)o;
			if (ref==null) {
				return other.getModifyable()==null;
			}
			if (!ref.equals(other.getModifyable().getId())) return false;
			if (uuid!=null && other.getUuid()==null) return false;
			if (uuid==null && other.getUuid()!=null) return false;
			if (uuid!=null && other.getUuid()!=null && !uuid.equals(other.getUuid())) return false;

			if (value!=other.getDistributed()) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public abstract T getSkill();

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableNumericalValue#getModifier(de.rpgframework.genericrpg.ValueType)
	 */
	@Override
	public int getModifier(ValueType... typeArray) {
		int count = 0;
		int countEquip = 0;
		int countMagic = 0;
		List<ValueType> types = List.of(typeArray);
		for (Modification mod : getIncomingModifications()) {
			if (mod instanceof CheckModification) {
				continue;
			}
			if (mod instanceof ValueModification) {
				ValueModification vMod = (ValueModification)mod;
				if (types.contains( vMod.getSet() )) {
					count += vMod.getValue();
				}
			}
		}

		if (modifierCap>0) {
			count += Math.min(countMagic, modifierCap);
			count += Math.min(countEquip, modifierCap);
		} else {
			count += countMagic;
			count += countEquip;
		}

		return count;
	}

	//-------------------------------------------------------------------
	public void setModifierCap(int modifierCap) {
		this.modifierCap = modifierCap;
	}

}
