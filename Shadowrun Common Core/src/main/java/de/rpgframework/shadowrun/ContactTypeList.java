package de.rpgframework.shadowrun;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="contacttypes")
@ElementList(entry="contacttype",type=ContactType.class)
public class ContactTypeList extends ArrayList<ContactType> {

}
