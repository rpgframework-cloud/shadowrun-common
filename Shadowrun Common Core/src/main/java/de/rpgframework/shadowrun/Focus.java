package de.rpgframework.shadowrun;

import java.util.Locale;
import java.util.UUID;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author Stefan
 *
 */
@DataItemTypeKey(id = "focus")
public class Focus extends ComplexDataItem {

	@Attribute(name="avail")
	private int availabilityPlus;
	@Attribute(name="karma")
	private int bondingMultiplier;
	@Attribute(name="cost")
	private int nuyen;

	//--------------------------------------------------------------------
	public Focus() {
	}

	//--------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getNuyenCost() {
		return nuyen;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the availabilityPlus
	 */
	public int getAvailabilityPlus() {
		return availabilityPlus;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the bondingMultiplier
	 */
	public int getBondingMultiplier() {
		return bondingMultiplier;
	}

}
