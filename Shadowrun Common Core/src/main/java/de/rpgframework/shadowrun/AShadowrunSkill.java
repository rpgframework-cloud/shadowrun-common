/**
 *
 */
package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.OneAttributeSkill;
import de.rpgframework.genericrpg.data.SkillSpecialization;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="skill")
public abstract class AShadowrunSkill extends OneAttributeSkill<ShadowrunAttribute> {

	@org.prelle.simplepersist.Attribute(required=true)
	protected ShadowrunAttribute attr;
	@org.prelle.simplepersist.Attribute(required=false)
	protected SkillType type;
	@org.prelle.simplepersist.Attribute(name="restrict")
	protected boolean restricted;
	@ElementList(entry="skillspec",type=SkillSpecialization.class,inline=true)
	protected List<SkillSpecialization<?>> specializations;

	//-------------------------------------------------------------------
	/**
	 */
	public AShadowrunSkill() {
		type = SkillType.PHYSICAL;
		specializations = new ArrayList<SkillSpecialization<?>>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.OneAttributeSkill#getAttribute()
	 */
	@Override
	public ShadowrunAttribute getAttribute() {
		return attr;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public SkillType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(SkillType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ISkill#getSpecializations()
	 */
	public List<SkillSpecialization<?>> getSpecializations() {
		return specializations;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the masterships
	 */
	public SkillSpecialization getSpecialization(String id) {
		for (SkillSpecialization tmp : specializations)
			if (tmp.getId().equals(id))
				return tmp;
		return null;
	}

	//-------------------------------------------------------------------
	public void addSpecialization(SkillSpecialization spec) {
		specializations.add(spec);
		spec.setSkill(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the restricted
	 */
	public boolean isRestricted() {
		return restricted;
	}

	//-------------------------------------------------------------------
	public void assignToDataSet(DataSet set) {
		super.assignToDataSet(set);
		for (SkillSpecialization spec : specializations) {
			spec.setSkill(this);
			spec.assignToDataSet(set);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Used in deriving classes to perform validation checks on loading,
	 * if necessary
	 * @return Error message or NULL
	 */
	public void validate() throws DataErrorException {
		for (SkillSpecialization spec : specializations) {
			spec.getName();
		}
	}

}
