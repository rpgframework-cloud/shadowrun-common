package de.rpgframework.shadowrun;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class MetamagicOrEchoValue extends ComplexDataItemValue<MetamagicOrEcho> {

	//-------------------------------------------------------------------
	public MetamagicOrEchoValue() {
	}

	//-------------------------------------------------------------------
	public MetamagicOrEchoValue(MetamagicOrEcho power) {
		super(power);
		this.resolved = power;
	}

}
