package de.rpgframework.shadowrun.items;

import de.rpgframework.shadowrun.DamageElement;
import de.rpgframework.shadowrun.DamageType;

/**
 * @author prelle
 *
 */
public interface IDamage {

	//--------------------------------------------------------------------
	public int getValue();

	//--------------------------------------------------------------------
	public DamageType getType();
	
	//--------------------------------------------------------------------
	public DamageElement getElement();

}
