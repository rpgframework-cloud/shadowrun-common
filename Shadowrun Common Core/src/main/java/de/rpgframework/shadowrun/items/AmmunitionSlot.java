package de.rpgframework.shadowrun.items;

/**
 * @author prelle
 *
 */
public class AmmunitionSlot {

	private int amount;
	private AmmoSlotType type;
	
	//-------------------------------------------------------------------
	public AmmunitionSlot() {
	}
	
	//-------------------------------------------------------------------
	public AmmunitionSlot(int amount, AmmoSlotType type) {
		this.amount = amount;
		this.type   = type;
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		if (type==null)
			return String.valueOf(amount);
		else
			return String.valueOf(amount)+"("+type.toString()+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}

	//-------------------------------------------------------------------
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public AmmoSlotType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(AmmoSlotType type) {
		this.type = type;
	}

}
