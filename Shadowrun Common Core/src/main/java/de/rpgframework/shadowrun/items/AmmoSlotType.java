package de.rpgframework.shadowrun.items;

import java.util.NoSuchElementException;

import de.rpgframework.shadowrun.ShadowrunCore;

public enum AmmoSlotType {
	BREAK_ACTION("b"),
	CLIP("c"),
	DRUM("d"),
	MUZZLE_LOADER("ml"),
	MAGAZINE("m"),
	CYLINDER("cy"),
	BELT("belt"),
	SPECIAL("special")
	
	;
	String val;
	private AmmoSlotType(String val) {
		this.val = val;
	}
	public String getValue() {return val;}
	public static AmmoSlotType getByValue(String val) {
		for (AmmoSlotType mode : AmmoSlotType.values()) {
			if (mode.getValue().equalsIgnoreCase(val))
				return mode;
		}
		throw new NoSuchElementException(val);
	}
	public String toString() {
		return ShadowrunCore.getI18nResources().getString("ammoslottype."+name().toLowerCase());
	}
}

