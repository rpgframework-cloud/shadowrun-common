package de.rpgframework.shadowrun.items;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.items.AItemEnhancement;
import de.rpgframework.shadowrun.persist.AvailabilityConverter;

/**
 * @author prelle
 *
 */
public abstract class AShadowrunItemEnhancement extends AItemEnhancement {

	@Attribute(name="avail",required=false)
	@AttribConvert(AvailabilityConverter.class)
	private Availability availability;
	//-------------------------------------------------------------------
	/**
	 */
	public AShadowrunItemEnhancement() {
		super();
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the availability
	 */
	public Availability getAvailability() {
		return availability;
	}

	//-------------------------------------------------------------------
	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(Availability availability) {
		this.availability = availability;
	}

}
