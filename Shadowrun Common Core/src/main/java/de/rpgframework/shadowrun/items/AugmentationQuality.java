package de.rpgframework.shadowrun.items;

import java.util.Locale;

import de.rpgframework.shadowrun.ShadowrunCore;

/**
 * @author prelle
 *
 */
public enum AugmentationQuality {

	STANDARD,
	ALPHA,
	BETA,
	DELTA,
	USED,
	OMEGA,
	EXO,
	GAMMA
    ;

    public String getName() {
        return ShadowrunCore.getI18nResources().getString("augmentationquality."+name().toLowerCase());
    }

    public String getName(Locale loc) {
        return ShadowrunCore.getI18nResources().getString("augmentationquality."+name().toLowerCase(), loc);
    }

}
