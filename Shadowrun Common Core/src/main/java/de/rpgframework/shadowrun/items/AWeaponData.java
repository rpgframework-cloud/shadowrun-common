package de.rpgframework.shadowrun.items;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.SkillSpecialization;
import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.shadowrun.AShadowrunSkill;
import de.rpgframework.shadowrun.persist.AmmunitionConverter;
import de.rpgframework.shadowrun.persist.FireModesConverter;

/**
 * @author prelle
 *
 */
public abstract class AWeaponData<S extends AShadowrunSkill, D extends IDamage> implements IGearTypeData {

//	private transient ItemTemplate parent;
//	@Attribute(name="attack")
//	@AttribConvert(value=AttackRatingConverter.class)
//	private int[] attackRating;
	private int[] range = new int[] {3,50,250,500,1000};

//	@Attribute(name="dmg")
//	@AttribConvert(WeaponDamageConverter.class)
//	protected Damage damage;
	@Attribute(name="mode")
	@AttribConvert(FireModesConverter.class)
	private List<FireMode> mode;
	@AttribConvert(AmmunitionConverter.class)
	@Attribute(name="ammo")
	private List<AmmunitionSlot> ammo;
	@Attribute(name="nowifi")
	private boolean noWiFi;

	//-------------------------------------------------------------------
	/**
	 */
	public AWeaponData() {
//		ammo = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * Convenience for getSkillSpecialization().getSkill()
	 * @return the skill
	 */
	public abstract S getSkill();

	//--------------------------------------------------------------------
	/**
	 * @return the damage
	 */
	public abstract D getDamage();

	//-------------------------------------------------------------------
	/**
	 * @return the spec
	 */
	public abstract SkillSpecialization getSpecialization();

//	//--------------------------------------------------------------------
//	public ItemAttributeValue getRecoilCompensation(List<Modification> mods) {
//		return new ItemAttributeValue(ItemAttribute.RECOIL_COMPENSATION, recoilCompensation, mods);
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the mode
	 */
	public List<FireMode> getFireModes() {
		return mode;
	}

	//-------------------------------------------------------------------
	public List<String> getFireModeNames(Locale loc) {
		List<String> ret = new ArrayList<String>();
		if (mode!=null) {
			for (FireMode tmp : mode)
				ret.add(tmp.getName(loc));
		}
		return ret;
	}

//	//--------------------------------------------------------------------
//	public List<AmmunitionSlot> getAmmunition() {
//		return ammo;
//	}
//
//	//-------------------------------------------------------------------
//	public List<String> getAmmunitionNames() {
//		List<String> ret = new ArrayList<String>();
//		if (ammo!=null) {
//			for (AmmunitionSlot tmp : ammo)
//				ret.add(tmp.toString());
//		}
//		return ret;
//	}

	//--------------------------------------------------------------------
	public abstract boolean isMeleeWeapon() ;

	//--------------------------------------------------------------------
	public abstract boolean isRangedWeapon();

	//-------------------------------------------------------------------
	/**
	 * @param mode the mode to set
	 */
	public void setFireModes(List<FireMode> mode) {
		this.mode = mode;
	}

	//-------------------------------------------------------------------
	/**
	 * @param ammo the ammo to set
	 */
	public void setAmmunition(AmmunitionSlot ammo) {
		this.ammo = Collections.singletonList(ammo);
	}

	//-------------------------------------------------------------------
	/**
	 * @param ammo the ammo to set
	 */
	public void setAmmunition(List<AmmunitionSlot> ammo) {
		this.ammo = ammo;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the noWiFi
	 */
	public boolean hasNoWiFi() {
		return noWiFi;
	}

	//-------------------------------------------------------------------
	/**
	 * @param noWiFi the noWiFi to set
	 */
	public void setNoWiFi(boolean noWiFi) {
		this.noWiFi = noWiFi;
	}

}
