package de.rpgframework.shadowrun;

import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.shadowrun.generators.ShadowrunTaxonomy;

/**
 * @author prelle
 *
 */
public enum NPCType implements Classification<NPCType> {

	GRUNT,
	CONTACT,
	CRITTER,
	CRITTER_AWAKENED,
	SPIRIT,
	SPRITE;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return ShadowrunTaxonomy.NPCTYPE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public NPCType getValue() {
		return this;
	}

}
