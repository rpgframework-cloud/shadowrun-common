/**
 * 
 */
package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="lifestyles")
@ElementList(entry="lifestyle",type=LifestyleQuality.class,inline=true)
public class LifestyleQualityList extends ArrayList<LifestyleQuality> {

	private static final long serialVersionUID = 3865034028125164048L;

	//-------------------------------------------------------------------
	/**
	 */
	public LifestyleQualityList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public LifestyleQualityList(Collection<? extends LifestyleQuality> c) {
		super(c);
	}

}
