package de.rpgframework.shadowrun;

import java.text.Collator;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "tradition")
public class Tradition extends ComplexDataItem implements Comparable<Tradition> {
	
	@Attribute
	private String id;
	@Attribute(name="drain1")
	private ShadowrunAttribute drainAttribute1;
	@Attribute(name="drain2")
	private ShadowrunAttribute drainAttribute2;

	//-------------------------------------------------------------------
	public Tradition() {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the drainAttribute1
	 */
	public ShadowrunAttribute getDrainAttribute1() {
		return drainAttribute1;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the drainAttribute2
	 */
	public ShadowrunAttribute getTraditionAttribute() {
		return drainAttribute2;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Tradition o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

}
