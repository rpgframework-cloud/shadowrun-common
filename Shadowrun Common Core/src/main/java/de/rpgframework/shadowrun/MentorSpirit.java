package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationList;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="mentorspirit")
public class MentorSpirit extends ComplexDataItem {

	public enum Type {
		MENTOR_SPIRIT,
		PARAGON
	}

	@Attribute
	protected Type type = Type.MENTOR_SPIRIT;
	@ElementList(entry = "similar", inline = false, type = String.class)
	protected List<String> similarNames;
	@Element
	protected ModificationList magician;
	@Element
	protected ModificationList adept;

	//-------------------------------------------------------------------
	public MentorSpirit() {
		similarNames  = new ArrayList<>();
		magician = new ModificationList();
		adept    = new ModificationList();
	}

	//-------------------------------------------------------------------
	public String getDisadvantage(Locale loc) {
		String key = getTypeString()+"."+id.toLowerCase()+".disadvantage";
		if (parentSet==null) {
			System.err.println("No parent dataset for "+getTypeString()+":"+id);
			return key;
		}
		return getLocalizedString(loc, key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ComplexDataItem#validate()
	 */
	@Override
	public void validate() {
		getDisadvantage(Locale.getDefault());

		for (String key : similarNames) {
			if (key.isBlank()) throw new DataErrorException(this, "<similiar> contains blank text");
		}
	}

	//-------------------------------------------------------------------
	public List<Modification> getAdeptModifications() {
		return adept;
	}

	//-------------------------------------------------------------------
	public List<Modification> getMagicianModifications() {
		return magician;
	}

	//-------------------------------------------------------------------
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	public List<String> getSimilarNameList(Locale loc) {
		List<String> ret = new ArrayList<>();

		DataSet set = getFirstParent(loc);
		if (set==null)
			ret.add("?No Parent Set?");
		else {
			similarNames.forEach(i18n -> ret.add(set.getResourceString( getTypeString()+"."+id.toLowerCase()+".alias."+i18n, loc)));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public String getSimilarNames(Locale loc) {
		return String.join(", ", getSimilarNameList(loc));
	}

}
