package de.rpgframework.shadowrun.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modification.Origin;
import de.rpgframework.shadowrun.CritterPowerValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public class GetModificationsFromCritterPowers implements ProcessingStep {

	protected static final Logger logger = System.getLogger(GetModificationsFromCritterPowers.class.getPackageName()+".critter");

	private ShadowrunCharacter<?,?,?,?> model;

	//-------------------------------------------------------------------
	public GetModificationsFromCritterPowers(ShadowrunCharacter<?,?,?,?> model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.log(Level.TRACE, "ENTER: process");
		try {
			for (CritterPowerValue val : model.getCritterPowers()) {
				val.updateOutgoingModificiations(model);
				for (Modification mod : val.getOutgoingModifications()) {
					logger.log(Level.DEBUG, "Add {0} from {1}", mod, val);
					mod.setOrigin(Origin.OUTSIDE);
					unprocessed.add(mod);
				}
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
