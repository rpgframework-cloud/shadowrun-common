package de.rpgframework.shadowrun.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.shadowrun.FocusValue;
import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public class GetModificationsFromFoci implements ProcessingStep {

	protected static final Logger logger = System.getLogger(GetModificationsFromFoci.class.getPackageName()+".quality");

	private ShadowrunCharacter<?,?,?,?> model;

	//-------------------------------------------------------------------
	public GetModificationsFromFoci(ShadowrunCharacter<?,?,?,?> model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.log(Level.TRACE, "ENTER: process");
		try {

			for (FocusValue val : model.getFoci()) {
				val.setCharacter(model);
				val.updateOutgoingModificiations(model);
				List<Modification> modList = val.getOutgoingModifications();
				logger.log(Level.DEBUG, "Add modifications from quality {0} = {1}", val.getNameWithRating(), modList);
				unprocessed.addAll(modList);
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
