package de.rpgframework.shadowrun.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modification.Origin;
import de.rpgframework.shadowrun.MetamagicOrEcho;
import de.rpgframework.shadowrun.MetamagicOrEchoValue;
import de.rpgframework.shadowrun.ShadowrunAttribute;
import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public class GetModificationsFromMetaEchoes implements ProcessingStep {

	protected static final Logger logger = System.getLogger(GetModificationsFromMetaEchoes.class.getPackageName()+".metaecho");

	private ShadowrunCharacter<?,?,?,?> model;

	//-------------------------------------------------------------------
	public GetModificationsFromMetaEchoes(ShadowrunCharacter<?,?,?,?> model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.log(Level.TRACE, "ENTER: process");
		try {
			int initGrade = 0;
			int submGrade = 0;
			int transGrade= 0;
			int dracoGrade= 0;
			int neumoGrade= 0;

			for (MetamagicOrEchoValue val : model.getMetamagicOrEchoes()) {
				if (val.isAutoAdded()) continue;
				MetamagicOrEcho tmp = val.getModifyable();
				if (tmp==null) {
					logger.log(Level.ERROR, "Found an unknown MetamagicOrEchoValue:  {0}", val.getKey());
					continue;
				}

				switch (tmp.getType()) {
				case METAMAGIC:
				case METAMAGIC_ADEPT:
					initGrade += tmp.hasLevel()?val.getDistributed():1;
					break;
				case ECHO:
					submGrade += tmp.hasLevel()?val.getDistributed():1;
					break;
				case DRACOGENESIS_POWER:
					dracoGrade += tmp.hasLevel()?val.getDistributed():1;
					break;
				case TRANSHUMANISM:
					transGrade += tmp.hasLevel()?val.getDistributed():1;
					break;
				case NEUROMORPHISM:
					neumoGrade += tmp.hasLevel()?val.getDistributed():1;
					break;
				}

				val.updateOutgoingModificiations(model);
				List<Modification> modList = val.getOutgoingModifications();
				modList.forEach(mod -> mod.setOrigin(Origin.OUTSIDE));
				logger.log(Level.DEBUG, "Apply modifications from metaecho {0} = {1}", val.getNameWithRating(), modList);
				for (Modification mod : modList) {
					unprocessed.add(mod);
				}

				model.setAttribute(new AttributeValue<ShadowrunAttribute>(ShadowrunAttribute.INITIATION_RANK, initGrade));
				model.setAttribute(new AttributeValue<ShadowrunAttribute>(ShadowrunAttribute.SUBMERSION_RANK, submGrade));
				model.setAttribute(new AttributeValue<ShadowrunAttribute>(ShadowrunAttribute.TRANSHUMANISM_RANK, transGrade));
				model.setAttribute(new AttributeValue<ShadowrunAttribute>(ShadowrunAttribute.DRACOGENESIS_RANK, dracoGrade));
				model.setAttribute(new AttributeValue<ShadowrunAttribute>(ShadowrunAttribute.NEUROMORPISHM_RANK, neumoGrade));
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
