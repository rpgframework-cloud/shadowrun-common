package de.rpgframework.shadowrun.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modification.Origin;
import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public class GetModificationsFromMetaType implements ProcessingStep {

	protected static final Logger logger = System.getLogger(GetModificationsFromMetaType.class.getPackageName()+".meta");

	private ShadowrunCharacter<?,?,?,?> model;

	//-------------------------------------------------------------------
	public GetModificationsFromMetaType(ShadowrunCharacter<?,?,?,?> model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.log(Level.TRACE, "ENTER: process");
		try {
			// Apply modifications by metatype
			if (model.getMetatype()!=null) {
				model.getMetatype().getOutgoingModifications().forEach(mod -> mod.setOrigin(Origin.OUTSIDE));
				logger.log(Level.DEBUG, "1. Apply modifications from metatype {0} = {1}",model.getMetatype().getId(),model.getMetatype().getOutgoingModifications());
				unprocessed.addAll(model.getMetatype().getOutgoingModifications());
			} else {
				logger.log(Level.WARNING, "Metatype not selected yet");
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE : process() ends with {0} modifications still to process",unprocessed.size());
		}
		return unprocessed;
	}

}
