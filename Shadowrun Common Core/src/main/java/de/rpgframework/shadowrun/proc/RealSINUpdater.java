package de.rpgframework.shadowrun.proc;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.shadowrun.SIN;
import de.rpgframework.shadowrun.SIN.FakeRating;
import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public class RealSINUpdater implements ProcessingStep {

	protected static final Logger logger = System.getLogger(RealSINUpdater.class.getPackageName()+".sin");

	private ShadowrunCharacter<?,?,?,?> model;

	//-------------------------------------------------------------------
	public RealSINUpdater(ShadowrunCharacter<?,?,?,?> model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.ProcessingStep#process(java.util.List)
	 */
	@Override
	public List<Modification> process(List<Modification> unprocessed) {
		SIN realSIN = null;
		for (SIN sin : model.getSINs()) {
			if (sin.getQuality()==FakeRating.REAL_SIN) {
				realSIN = sin;
				break;
			}
		}
		if (realSIN==null)
			return unprocessed;

		logger.log(Level.INFO, "Found real SIN {0}", realSIN.getUniqueId());
		realSIN.setName(model.getRealName());

		return unprocessed;
	}

}
