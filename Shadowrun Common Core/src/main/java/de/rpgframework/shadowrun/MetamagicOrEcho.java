package de.rpgframework.shadowrun;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "metaecho")
public class MetamagicOrEcho extends ComplexDataItem {

	public static enum Type {
		ECHO,
		METAMAGIC,
		METAMAGIC_ADEPT,
		TRANSHUMANISM,
		NEUROMORPHISM,
		DRACOGENESIS_POWER
	}

	@Attribute
	private Type type;
	@Attribute(name="max")
	private int max;
	@Attribute(name="multi")
	private boolean multi;

	//-------------------------------------------------------------------
	/**
	 */
	public MetamagicOrEcho() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	//-------------------------------------------------------------------
	public boolean hasLevel() {
		return max>1;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the multi
	 */
	public boolean isMulti() {
		return multi;
	}

	//-------------------------------------------------------------------
	/**
	 * @param multi the multi to set
	 */
	public void setMulti(boolean multi) {
		this.multi = multi;
	}

}
