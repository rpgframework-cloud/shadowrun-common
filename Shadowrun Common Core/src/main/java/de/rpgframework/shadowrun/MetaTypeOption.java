package de.rpgframework.shadowrun;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author Stefan
 *
 */
@Root(name="metaopt")
public class MetaTypeOption extends PriorityOption<MetaType> {
	
	@Attribute(name="spec")
	private int specialAttributePoints;
	@Attribute(name="karma")
	private int additionalKarmaKost;

	//--------------------------------------------------------------------
	public MetaTypeOption() {
		
	}

	//--------------------------------------------------------------------
	public MetaTypeOption(MetaType type) {
		this.id = type.getId();
		this.resolved = type;
	}

	//--------------------------------------------------------------------
	public MetaTypeOption(MetaType type, int karma) {
		this(type);
		this.additionalKarmaKost = karma;
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (id==null)
			return "Empty-MetaTypeOption";
		if (additionalKarmaKost==0)
			return id+" ("+specialAttributePoints+")";
		return id+" ("+specialAttributePoints+")  "+additionalKarmaKost+" karma";		
	}

	//--------------------------------------------------------------------
    public int compareTo(PriorityOption o) {
    	if (o instanceof MetaTypeOption) {
    		MetaTypeOption other = (MetaTypeOption)o;
    		int cmp = ((Integer)specialAttributePoints).compareTo(other.getSpecialAttributePoints());
    		if (cmp!=0) return -cmp;
    		return id.compareTo(other.getId());
    	}
    	return 0;
    }

	//--------------------------------------------------------------------
	/**
	 * @return the specialAttributePoints
	 */
	public int getSpecialAttributePoints() {
		return specialAttributePoints;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the additionalKarmaKost
	 */
	public int getAdditionalKarmaKost() {
		return additionalKarmaKost;
	}

	//-------------------------------------------------------------------
	/**
	 * @param specialAttributePoints the specialAttributePoints to set
	 */
	public void setSpecialAttributePoints(int specialAttributePoints) {
		this.specialAttributePoints = specialAttributePoints;
	}

}
