package de.rpgframework.shadowrun;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.modification.Modifyable;

/**
 * @author prelle
 *
 */
public class ComplexFormValue extends ComplexDataItemValue<ComplexForm> implements Modifyable {

	//-------------------------------------------------------------------
	public ComplexFormValue() {
	}

	//-------------------------------------------------------------------
	public ComplexFormValue(ComplexForm data) {
		super(data,0);
	}

//	//-------------------------------------------------------------------
//	public String getName() {
//		if (complexForm.needsChoice()) {
//			if (choice!=null) {
//				switch (complexForm.getChoice()) {
//				case ATTRIBUTE:
//				case MATRIX_ATTRIBUTE:
//					return complexForm.getName()+" ("+((org.prelle.shadowrun6.Attribute)choice).getName()+")";
//				case PROGRAM:
//					return complexForm.getName()+" ("+((ItemTemplate)choice).getName()+")";
//				default:
//					System.err.println("Error: ComplexFormValue.getName() for type "+complexForm.getChoice());
//					return complexForm.getName()+"(?"+choiceReference+"?)";
//				}
//			}
//			return complexForm.getName()+" (Choice not set)";
//		}
//		return complexForm.getName();
//	}

}
