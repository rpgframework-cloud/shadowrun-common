package de.rpgframework.shadowrun;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.shadowrun.persist.FadingConverter;

/**
 * @author Stefan
 *
 */
@DataItemTypeKey(id="complexform")
public class ComplexForm extends ComplexDataItem {
	
	public final static int FADING_HITS = -1;
	public final static int FADING_RATING = -2;
	public final static int FADING_VARIABLE = -3;

	public enum Duration {
		INSTANTANEOUS,
		SUSTAINED,
		PERMANENT,
		VARIES,
		;
		public String getName() { return ShadowrunCore.getI18nResources().getString("cplxform.duration."+name().toLowerCase()); }
		public String getShortName() { return ShadowrunCore.getI18nResources().getString("cplxform.duration."+name().toLowerCase()+".short"); }
	}

	@Attribute(required=true)
	private String id;
	@Attribute(name="fad",required=true)
	@AttribConvert(FadingConverter.class)
	private int fading;
	@Attribute(name="dur",required=true)
	private Duration duration;
	@Attribute(name="mul")
	private boolean multipleSelectable;
	@Attribute(name="selectable")
	private boolean freeSelectable = true;

	//--------------------------------------------------------------------
	/**
	 */
	public ComplexForm() {
	}

	//--------------------------------------------------------------------
	/**
	 * @return the fading
	 */
	public int getFading() {
		return fading;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the multipleSelectable
	 */
	public boolean isMultipleSelectable() {
		return multipleSelectable;
	}

	//-------------------------------------------------------------------
	/**
	 * @param multipleSelectable the multipleSelectable to set
	 */
	public void setMultipleSelectable(boolean multipleSelectable) {
		this.multipleSelectable = multipleSelectable;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the duration
	 */
	public Duration getDuration() {
		return duration;
	}

	//-------------------------------------------------------------------
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the freeSelectable
	 */
	public boolean isFreeSelectable() {
		return freeSelectable;
	}
	//-------------------------------------------------------------------
	/**
	 * @param freeSelectable the freeSelectable to set
	 */
	public void setFreeSelectable(boolean freeSelectable) {
		this.freeSelectable = freeSelectable;
	}

}
