package de.rpgframework.shadowrun;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class SpellValue<T extends ASpell> extends ComplexDataItemValue<T> {

	//-------------------------------------------------------------------
	public SpellValue() {
	}

	//-------------------------------------------------------------------
	public SpellValue(T data) {
		super(data);
	}

}
