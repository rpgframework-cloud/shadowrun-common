package de.rpgframework.shadowrun;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "lifestyle")
public class LifestyleQuality extends ComplexDataItem {
	
	@Attribute
	private int cost;
	@Attribute
	private int lp;

	//-------------------------------------------------------------------
	/**
	 */
	public LifestyleQuality() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	//-------------------------------------------------------------------
	public int getLifestylePoints() {
		return lp;
	}

}
