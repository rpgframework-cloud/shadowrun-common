package de.rpgframework.shadowrun.vehicle;

/**
 * @author prelle
 *
 */
public enum VehicleOperationMode {

	DRIVEN,
	RCC,
	RIGGED,
	AUTONOMOUS
	
}
