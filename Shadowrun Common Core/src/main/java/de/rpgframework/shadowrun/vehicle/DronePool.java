package de.rpgframework.shadowrun.vehicle;

/**
 * Pools or values dependent on the VehicleOperationMode.
 * Meant to be implemented by Enums of SR5 and SR6
 * @author prelle
 *
 */
public interface DronePool<T> {
	
	//-------------------------------------------------------------------
	/**
	 * @return
	 */
	public T[] getValidValues();
	
}
