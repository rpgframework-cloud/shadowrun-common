package de.rpgframework.shadowrun.vehicle;

import de.rpgframework.genericrpg.CalculatedValue;

/**
 * @author stefa
 *
 */
public class OpModePools {
	
	public CalculatedValue driven;
	private CalculatedValue rcc;
	private CalculatedValue rigged;
	private CalculatedValue autonomous;

	//-------------------------------------------------------------------
	public OpModePools() {
		driven = new CalculatedValue();
		rcc = new CalculatedValue();
		rigged = new CalculatedValue();
		autonomous = new CalculatedValue();
	}

	//-------------------------------------------------------------------
	public CalculatedValue get(VehicleOperationMode mode) {
		switch (mode) {
		case AUTONOMOUS: return autonomous;
		case DRIVEN    : return driven;
		case RCC       : return rcc;
		case RIGGED    : return rigged;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public int getInt(VehicleOperationMode mode) {
		switch (mode) {
		case AUTONOMOUS: return autonomous.getAsInt();
		case DRIVEN    : return driven.getAsInt();
		case RCC       : return rcc.getAsInt();
		case RIGGED    : return rigged.getAsInt();
		}
		return 0;
	}

}
