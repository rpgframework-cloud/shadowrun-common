package de.rpgframework.shadowrun;

import java.lang.System.Logger.Level;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public abstract class ShadowrunCharacter<S extends AShadowrunSkill, V extends AShadowrunSkillValue<S>, T extends PieceOfGear, Z extends ASpell>
	extends CommonCharacter<ShadowrunAttribute,S,V,T>
	implements IShadowrunLifeform<ShadowrunAttribute,S,V> {

	protected static Gson GSON = (new GsonBuilder()).create();
	public final static UUID UUID_UNUSED_SOFTWARE_DEVICE = UUID.fromString("8fc8c01e-3023-4ba6-9d02-99ba6fcd6979");

	@Element
	protected String ruleProfile;
    @Element(name="chargenSettings")
    protected String chargenSettingsJSON;
    protected transient Object chargenSettingsObject;

	@Attribute(name="karmaI")
	private int karmaInvested;
	@Attribute(name="karmaF")
	private int karmaFree;
	@Attribute(name="nuyen")
	private int nuyen;
	@Attribute(name="debt")
	private int debt;
	@Attribute(name="debtRate")
	private int debtRate;
	@Attribute(name="aspskill")
	private String aspectSkill;
	/**
	 * The maximum essence character can reach, multiplied by 1000.
	 * 6000 at chargen, reduced by every augmentation added not caught by artificial essence hole
	 */
	@Attribute(name="essMax")
	@Deprecated
	private int essenceMaximum;
	/**
	 * How much of the essence hole is unsed?
	 */
	@Attribute(name="essHole")
	private int essenceHoleUnused;

	@Attribute(name="meta")
	protected String metatype = "human";
    @Element(name="realName")
    protected String realName;
	@Element(name="type")
	protected String bodyType = BodyType.METAHUMAN.name();

	@Element(name="mortype")
	protected String magicOrResonance;
	@Element
	protected String tradition;
	@ElementList(entry="quality", type = QualityValue.class, inline = false)
	private List<QualityValue> qualities;
    @ElementList(type = SpellValue.class, entry = "spell")
    protected List<SpellValue<Z>> spells;
    @ElementList(type = AdeptPowerValue.class, entry = "adeptpower")
    protected List<AdeptPowerValue> adeptPowers;
	@ElementList(entry="ritual", type = RitualValue.class, inline = false)
	private List<RitualValue> rituals;
    @ElementList(type = ComplexFormValue.class, entry = "complexforms")
    protected List<ComplexFormValue> complexforms;
    @ElementList(type = MetamagicOrEchoValue.class, entry = "metaEcho")
    protected List<MetamagicOrEchoValue> metaEchoes;
    @ElementList(type = SIN.class, entry = "sin")
    protected List<SIN> sins;
	@ElementList(entry="licenses", type=LicenseValue.class)
	protected List<LicenseValue> licenses;
    @ElementList(type = Contact.class, entry = "contact")
    protected List<Contact> contacts;
	@ElementList(entry="focus", type=FocusValue.class)
	protected List<FocusValue> foci;
    @ElementList(type = CritterPowerValue.class, entry = "critterpower")
    protected List<CritterPowerValue> critterPowers;

	protected transient List<Modification> globalItemModifications;
	/**
	 * Elements that must be ignored for calculations
	 * e.g. Dermal deposits when Dermal plating is present
	 */
	protected transient List<Object> forbiddenSources;
	private transient int essenceCost;

	private transient List<BodyForm> bodies;
//	private transient CarriedItem<T> persona;
	protected transient List<AdeptPowerValue> autoPowers;
	protected transient List<MetamagicOrEchoValue> autoMetaEchoes;

	//-------------------------------------------------------------------
	public ShadowrunCharacter() {
		qualities = new ArrayList<>();
		spells    = new ArrayList<>();
		adeptPowers = new ArrayList<>();
		rituals     = new ArrayList<>();
		complexforms= new ArrayList<>();
		metaEchoes  = new ArrayList<>();
		sins        = new ArrayList<>();
		licenses    = new ArrayList<>();
		contacts    = new ArrayList<>();
		foci        = new ArrayList<>();
		critterPowers = new ArrayList<>();
		bodies      = new ArrayList<>();

		globalItemModifications = new ArrayList<>();
		forbiddenSources = new ArrayList<>();

		autoPowers = new ArrayList<>();
		autoMetaEchoes = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getShortDescription()
	 */
	@Override
	public String getShortDescription() {
		StringBuffer buf = new StringBuffer();
		if (gender!=null) {
			buf.append(gender.getName(Locale.getDefault())+" ");
		}
		if (metatype!=null) {
			buf.append(metatype+" ");
		}
		if (magicOrResonance!=null) {
			buf.append(magicOrResonance+"x");
		}
		return buf.toString();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the karmaInvested
	 */
	public int getKarmaInvested() {
		return karmaInvested;
	}

	//--------------------------------------------------------------------
	/**
	 * @param karmaInvested the karmaInvested to set
	 */
	public void setKarmaInvested(int karmaInvested) {
		this.karmaInvested = karmaInvested;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the karmaFree
	 */
	public int getKarmaFree() {
		return karmaFree;
	}

	//--------------------------------------------------------------------
	/**
	 * @param karmaFree the karmaFree to set
	 */
	public void setKarmaFree(int karmaFree) {
		this.karmaFree = karmaFree;
	}

	//-------------------------------------------------------------------
	public void setNuyen(int value) { this.nuyen = value; }
	public int getNuyen() { return this.nuyen; }

	//-------------------------------------------------------------------
	public void setMetatype(MetaType value) {
		if (value!=null)
			metatype = value.getId();
		else
			metatype = null;
	}

	//-------------------------------------------------------------------
	public abstract <M extends MetaType> M getMetatype() ;

	//-------------------------------------------------------------------
	public void setBodytype(BodyType value) {
		if (value!=null)
			bodyType = value.name();
		else
			bodyType = null;
	}

	//-------------------------------------------------------------------
	public BodyType getBodytype() {
		if (bodyType==null) return BodyType.METAHUMAN;
		try {
			return BodyType.valueOf(bodyType);
		} catch (Exception e) {
			return BodyType.METAHUMAN;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.Lifeform#getAttribute(A)
	 */
	@Override
	public AttributeValue<ShadowrunAttribute> getAttribute(String key) {
		for (AttributeValue<ShadowrunAttribute> val : getAttributes()) {
			if (val.getModifyable().name().equals(key))
				return val;
		}
		for (Entry<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>> entry : derivedAttributes.entrySet()) {
			if (entry.getKey().name().equals(key))
				return entry.getValue();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the qualities
	 */
	public List<QualityValue> getQualities() {
		return qualities;
	}

	//-------------------------------------------------------------------
	public boolean hasQuality(String id) {
		for (QualityValue ref : qualities) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public QualityValue getQuality(String id) {
		for (QualityValue ref : getQualities()) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addQuality(QualityValue value) {
		if (!qualities.contains(value))
			qualities.add(value);
	}

	//-------------------------------------------------------------------
	public void removeQuality(QualityValue value) {
		qualities.remove(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the qualities
	 */
	public List<MetamagicOrEchoValue> getMetamagicOrEchoes() {
		List<MetamagicOrEchoValue> ret = new ArrayList<>(autoMetaEchoes);
		ret.addAll(metaEchoes);
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasMetamagicOrEcho(String id) {
		for (MetamagicOrEchoValue ref : metaEchoes) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public MetamagicOrEchoValue getMetamagicOrEcho(String id) {
		for (MetamagicOrEchoValue ref : metaEchoes) {
			if (ref.getModifyable()==null) {
				System.getLogger(ShadowrunCharacter.class.getPackageName()).log(Level.ERROR, "MetamagicOrEchoValue with unknown reference: "+ref.getKey());
				continue;
			}
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addMetamagicOrEcho(MetamagicOrEchoValue value) {
		if (!metaEchoes.contains(value))
			metaEchoes.add(value);
	}

	//-------------------------------------------------------------------
	public void removeMetamagicOrEcho(MetamagicOrEchoValue value) {
		metaEchoes.remove(value);
	}

	//-------------------------------------------------------------------
	public void addAutoMetamagicOrEchoe(MetamagicOrEchoValue ref) {
		if (!autoMetaEchoes.contains(ref))
			autoMetaEchoes.add(ref);
	}

	//-------------------------------------------------------------------
	public List<MetamagicOrEchoValue> getAutoMetamagicOrEchoes() {
		return new ArrayList<>(autoMetaEchoes);
	}

	//-------------------------------------------------------------------
	public void clearAutoMetamagicOrEchoes() {
		autoMetaEchoes.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the realName
	 */
	public String getRealName() {
		return realName;
	}

	//-------------------------------------------------------------------
	/**
	 * @param realName the realName to set
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	//-------------------------------------------------------------------
	public List<SpellValue<Z>> getSpells() {
		return spells;
	}

	//-------------------------------------------------------------------
	public void addSpell(SpellValue<Z> value) {
		if (!spells.contains(value))
			spells.add(value);
	}

	//-------------------------------------------------------------------
	public void removeSpell(SpellValue<Z> value) {
		spells.remove(value);
	}

	//-------------------------------------------------------------------
	public List<RitualValue> getRituals() {
		return rituals;
	}

	//-------------------------------------------------------------------
	public void addRitual(RitualValue value) {
		if (!rituals.contains(value))
			rituals.add(value);
	}

	//-------------------------------------------------------------------
	public void removeRitual(RitualValue value) {
		rituals.remove(value);
	}

	//-------------------------------------------------------------------
	public List<V> getSkillValues(SkillType... skill) {
		return skills.stream().filter(s -> List.of(skill).contains( s.getModifyable().getType())).collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the chargenSettingsJSON
	 */
	public String getChargenSettingsJSON() {
		return chargenSettingsJSON;
	}

	//-------------------------------------------------------------------
	/**
	 * @param chargenSettingsJSON the chargenSettingsJSON to set
	 */
	public void setChargenSettingsJSON(String chargenSettingsJSON) {
		this.chargenSettingsJSON = chargenSettingsJSON;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the chargenSettingsObject
	 */
	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> T getCharGenSettings(Class<T> cls) {
		return (T)chargenSettingsObject;
	}

	//-------------------------------------------------------------------
	public boolean hasCharGenSettings(Class<? extends ACommonCharacterSettings> cls) {
		return chargenSettingsObject!=null && cls.isAssignableFrom(chargenSettingsObject.getClass());
	}

	//-------------------------------------------------------------------
	/**
	 * @param chargenSettingsObject the chargenSettingsObject to set
	 */
	public void setCharGenSettings(ACommonCharacterSettings chargenSettingsObject) {
//		try {
//			throw new RuntimeException("Trace: old="+this.chargenSettingsObject);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		//if (this.chargenSettingsObject!=null) throw new IllegalStateException("Already have settings");

		this.chargenSettingsObject = chargenSettingsObject;
		System.err.println("ShadowrunCharacter.setCharGenSettings: overwrite with "+chargenSettingsObject);
		System.getLogger(ShadowrunCharacter.class.getPackageName()).log(Level.ERROR,"overwrite with "+chargenSettingsObject);
		chargenSettingsJSON = GSON.toJson(chargenSettingsObject);
	}

	//-------------------------------------------------------------------
	public void readCharGenSettings(Class<?> clazz) {
		if (clazz==null) throw new NullPointerException("Class to decode JSON chargen settings not set");
		if (chargenSettingsObject!=null && clazz.isAssignableFrom(chargenSettingsObject.getClass()))
			return;
		try {
			if (chargenSettingsJSON!=null)
				chargenSettingsObject = GSON.fromJson(chargenSettingsJSON, clazz);
			if (chargenSettingsObject==null) {
				try {
					chargenSettingsObject = clazz.getConstructor().newInstance();
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | NoSuchMethodException | SecurityException e) {
					System.getLogger(getClass().getPackageName()).log(Level.ERROR, "Failed creating settings object",e);
				}
			}
			System.out.println("ShadowrunCharacter.readCharGenSettings ----> settings = "+chargenSettingsObject);
		} catch (JsonSyntaxException e) {
			System.getLogger(getClass().getPackageName()).log(Level.ERROR, "Error decoding JSON\n"+chargenSettingsJSON,e);
			throw e;
		}
	}

	//-------------------------------------------------------------------
	public abstract MagicOrResonanceType getMagicOrResonanceType();
	public void setMagicOrResonanceType(MagicOrResonanceType value) {
		magicOrResonance = value.getId();
	}

	//-------------------------------------------------------------------
	public abstract Tradition getTradition();
	public void setTradition(Tradition value) {
		tradition = value.getId();
	}

	//-------------------------------------------------------------------
	public List<AdeptPowerValue> getUserSelectedAdeptPowers() {
		return new ArrayList<>(adeptPowers);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the qualities
	 */
	public List<AdeptPowerValue> getAdeptPowers() {
		List<AdeptPowerValue> ret = new ArrayList<>(autoPowers);
		ret.addAll(adeptPowers);
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasAdeptPower(String id) {
		return getAdeptPower(id)!=null;
	}

	//-------------------------------------------------------------------
	public AdeptPowerValue getAdeptPower(String id) {
		for (AdeptPowerValue ref : getAdeptPowers()) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addAdeptPower(AdeptPowerValue value) {
		if (!adeptPowers.contains(value))
			adeptPowers.add(value);
	}

	//-------------------------------------------------------------------
	public void removeAdeptPower(AdeptPowerValue value) {
		adeptPowers.remove(value);
	}

	//-------------------------------------------------------------------
	public void addAutoAdeptPower(AdeptPowerValue ref) {
		if (!autoPowers.contains(ref))
			autoPowers.add(ref);
	}

	//-------------------------------------------------------------------
	public List<AdeptPowerValue> getAutoAdeptPowers() {
		return new ArrayList<>(autoPowers);
	}

	//-------------------------------------------------------------------
	public void clearAutoAdeptPower() {
		autoPowers.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the qualities
	 */
	public List<CritterPowerValue> getCritterPowers() {
		return critterPowers;
	}

	//-------------------------------------------------------------------
	public boolean hasCritterPower(String id) {
		for (CritterPowerValue ref : critterPowers) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public CritterPowerValue getCritterPower(String id) {
		for (CritterPowerValue ref : critterPowers) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addCritterPower(CritterPowerValue value) {
		if (!critterPowers.contains(value))
			critterPowers.add(value);
	}

	//-------------------------------------------------------------------
	public void removeCritterPower(CritterPowerValue value) {
		critterPowers.remove(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the qualities
	 */
	public List<ComplexFormValue> getComplexForms() {
		return complexforms;
	}

	//-------------------------------------------------------------------
	public boolean hasComplexForm(String id) {
		for (ComplexFormValue ref : complexforms) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public ComplexFormValue getComplexForm(String id) {
		for (ComplexFormValue ref : complexforms) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addComplexForm(ComplexFormValue value) {
		if (!complexforms.contains(value))
			complexforms.add(value);
	}

	//-------------------------------------------------------------------
	public void removeComplexForm(ComplexFormValue value) {
		complexforms.remove(value);
	}

	//-------------------------------------------------------------------
	public List<Contact> getContacts() {
		return contacts;
	}

	//-------------------------------------------------------------------
	public Contact getContact(UUID uuid) {
		return contacts.stream().filter(c -> c.getId().equals(uuid)).findFirst().orElse(null);
	}

	//-------------------------------------------------------------------
	public void addContact(Contact contact) {
		if (!contacts.contains(contact))
			contacts.add(contact);
	}

	//-------------------------------------------------------------------
	public boolean removeContact(Contact contact) {
		return contacts.remove(contact);
	}

	//-------------------------------------------------------------------
	public List<SIN> getSINs() {
		return sins;
	}

	//-------------------------------------------------------------------
	public SIN getSIN(UUID id) {
		for (SIN sin : sins) {
			if (sin.getUniqueId().equals(id))
				return sin;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addSIN(SIN value) {
		if (!sins.contains(value))
			sins.add(value);
	}

	//-------------------------------------------------------------------
	public void removeSIN(SIN value) {
//		try {
//			throw new RuntimeException("Trace");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		sins.remove(value);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LicenseValue> getLicenses() {
		return new ArrayList<>(licenses);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LicenseValue> getLicenses(SIN sin) {
		List<LicenseValue> ret = new ArrayList<>();
		for (LicenseValue tmp : licenses) {
			if (sin.getUniqueId().equals(tmp.getSIN()))
				ret.add(tmp);
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public void addLicense(LicenseValue val) {
		if (!licenses.contains(val))
			licenses.add(val);
	}

	//--------------------------------------------------------------------
	public boolean removeLicense(LicenseValue val) {
		return licenses.remove(val);
	}

	//-------------------------------------------------------------------
	public int getEssenceCost() {
		return essenceCost;
	}

	//-------------------------------------------------------------------
	public void setEssenceCost(int value) {
		this.essenceCost = value;
	}

	//-------------------------------------------------------------------
	public abstract <E extends Lifestyle> List<E> getLifestyles();


	//-------------------------------------------------------------------
	/**
	 * @return the initiateSubmersionLevel
	 */
	public int getInitiateSubmersionLevel() {
		int count = 0;
		for (MetamagicOrEchoValue ref : metaEchoes) {
			if (ref.getModifyable().hasLevel())
				count+=ref.getDistributed();
			else
				count++;
		}
		return count;
	}

	//-------------------------------------------------------------------
	public List<FocusValue> getFoci() {
		ArrayList<FocusValue> ret = new ArrayList<>(foci);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addFocus(FocusValue ref) {
		if (!foci.contains(ref)) {
			ref.setCharacter(this);
			foci.add(ref);
		}
	}

	//-------------------------------------------------------------------
	public void removeFocus(FocusValue ref) {
		foci.remove(ref);
	}

	//-------------------------------------------------------------------
	public void addItemModification(Modification value) {
		if (!globalItemModifications.contains(value))
			globalItemModifications.add(value);
	}

	//-------------------------------------------------------------------
	public void removeItemModification(Modification value) {
		globalItemModifications.remove(value);
	}

	//-------------------------------------------------------------------
	public void clearItemModifications() {
		globalItemModifications.clear();
	}

	//-------------------------------------------------------------------
	public List<Modification> getItemModifications() {
		return new ArrayList<>(globalItemModifications);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the essenceMaximum
	 */
	@Deprecated
	public int getEssenceMaximum() {
		return essenceMaximum;
	}

	//-------------------------------------------------------------------
	/**
	 * @param essenceMaximum the essenceMaximum to set
	 */
	@Deprecated
	public void setEssenceMaximum(int essenceMaximum) {
		this.essenceMaximum = essenceMaximum;
	}

	//-------------------------------------------------------------------
	public int getEssenceHoleUnused() {
		return essenceHoleUnused;
	}

	//-------------------------------------------------------------------
	/**
	 * @param essenceMaximum the essenceMaximum to set
	 */
	public void setEssenceHoleUnsed(int value) {
		this.essenceHoleUnused = value;
	}

//	//-------------------------------------------------------------------
//	public void setPersona(CarriedItem<T> persona) {
//		this.persona = persona;
//	}
//	//-------------------------------------------------------------------
//	public CarriedItem<T> getPersona() {
//		return persona;
//	}

	//-------------------------------------------------------------------
	public CarriedItem<T> getSoftwareLibrary() {
		return getCarriedItem(UUID_UNUSED_SOFTWARE_DEVICE);
	}

	//-------------------------------------------------------------------
	public String getAspectSkillId() {
		return aspectSkill;
	}
	public abstract S getAspectSkill();

	//-------------------------------------------------------------------
	public <X extends AShadowrunSkill> void setAspectSkill(X skill) {
		this.aspectSkill = skill.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun.IShadowrunLifeform#getBodyForms()
	 */
	@Override
	public List<BodyForm> getBodyForms() {
		return bodies;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun.IShadowrunLifeform#getBodyForm(de.rpgframework.shadowrun.BodyType)
	 */
	@Override
	public BodyForm getBodyForm(BodyType type) {
		for (BodyForm m : bodies) {
			if (m.getType()==type) {
				return m;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun.IShadowrunLifeform#clearBodyForms()
	 */
	@Override
	public void clearBodyForms() {
		bodies.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun.IShadowrunLifeform#addBodyForm(de.rpgframework.shadowrun.BodyForm)
	 */
	@Override
	public void addBodyForm(BodyForm value) {
		for (BodyForm m : bodies) {
			if (m.getType()==value.getType()) {
				int index = bodies.indexOf(m);
				bodies.remove(m);
				bodies.add(index, value);
				return;
			}
		}

		bodies.add(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the debt
	 */
	public int getDebt() {
		return debt;
	}

	//-------------------------------------------------------------------
	/**
	 * @param debt the debt to set
	 */
	public void setDebt(int debt) {
		this.debt = debt;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the debtRate
	 */
	public int getDebtRate() {
		return debtRate;
	}

	//-------------------------------------------------------------------
	/**
	 * @param debtRate the debtRate to set
	 */
	public void setDebtRate(int debtRate) {
		this.debtRate = debtRate;
	}

	//-------------------------------------------------------------------
	public void clearForbiddenSources() { forbiddenSources.clear(); }
	public void addForbiddenSource(Object data) { if (!forbiddenSources.contains(data)) forbiddenSources.add(data); }
	public boolean isForbiddenSource(Object data) { return forbiddenSources.contains(data); }

}
