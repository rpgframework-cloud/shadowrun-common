/**
 * 
 */
package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="critterpowers")
@ElementList(entry="critterpower",type=CritterPower.class,inline=true)
public class CritterPowerList extends ArrayList<CritterPower> {

	private static final long serialVersionUID = -3997028647463180304L;

	//-------------------------------------------------------------------
	/**
	 */
	public CritterPowerList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public CritterPowerList(Collection<? extends CritterPower> c) {
		super(c);
	}

}
