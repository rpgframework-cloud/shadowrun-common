package de.rpgframework.shadowrun;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;

/**
 * @author prelle
 *
 */
public class ShadowrunCore {

	static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(ShadowrunCore.class, Locale.GERMAN, Locale.ENGLISH, Locale.FRENCH);

	//-------------------------------------------------------------------
	public static MultiLanguageResourceBundle getI18nResources() {
		return RES;
	}

}
