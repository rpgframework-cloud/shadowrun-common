package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.data.GenericCore;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="ritual")
public class Ritual extends ComplexDataItem {
	
	@ElementList(entry="ritualfeature",type=RitualFeatureReference.class, inline=true)
	private List<RitualFeatureReference> features;

	/** Only needed in SR6 */
	@Attribute(name="thr")
	private int threshold;

	//-------------------------------------------------------------------
	/**
	 */
	public Ritual() {
		features = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the features
	 */
	public List<RitualFeatureReference> getFeatures() {
		return features;
	}

	//-------------------------------------------------------------------
	/**
	 * @param features the features to set
	 */
	public void setFeatures(List<RitualFeatureReference> features) {
		this.features = features;
	}

	//-------------------------------------------------------------------
	/**
	 * Used in deriving classes to perform validation checks on loading,
	 * if necessary
	 * @return Error message or NULL
	 */
	public void validate() throws DataErrorException {
		for (RitualFeatureReference ref : features) {
			if (ref.getResolved()==null) {
				RitualFeature feat = GenericCore.getItem(RitualFeature.class, ref.getKey());
				ref.setResolved(feat);
				if (feat==null)
					throw new DataErrorException(this, "Unresolved ritual feature: "+ref.getKey());
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return the threshold
	 */
	public int getThreshold() {
		return threshold;
	}

}
