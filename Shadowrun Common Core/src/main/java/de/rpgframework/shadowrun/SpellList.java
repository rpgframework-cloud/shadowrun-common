/**
 * 
 */
package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="spells")
@ElementList(entry="spell",type=ASpell.class,inline=true)
public class SpellList extends ArrayList<ASpell> {

	private static final long serialVersionUID = -1947492169452643981L;

	//-------------------------------------------------------------------
	/**
	 */
	public SpellList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public SpellList(Collection<? extends ASpell> c) {
		super(c);
	}

}
