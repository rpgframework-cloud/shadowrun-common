package de.rpgframework.shadowrun;

import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="metatype")
@DataItemTypeKey(id="metatype")
public abstract class MetaType extends ComplexDataItem {

	@Attribute(name="variantof")
	protected String variantOf;
	@Attribute
	protected int weight;
	@Attribute
	protected int size;
	@Attribute
	protected int karma;
	/** Is this metatype considered metahuman? */
	@Attribute(name="human")
	protected boolean metahuman = true;
	@Attribute(name="ai")
	protected boolean ai = false;

	//-------------------------------------------------------------------
	public MetaType() {
	}

	//-------------------------------------------------------------------
	public MetaType(String id) {
		this.id = id;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the variantOf
	 */
	public abstract MetaType getVariantOf();

	//--------------------------------------------------------------------
//	public abstract boolean isSpecialRacialAttribute(ShadowrunAttribute key) ;

	//-------------------------------------------------------------------
	public abstract List<Modification> getAttributeModifications();

	//-------------------------------------------------------------------
	public abstract List<Modification> getNonAttributeModifications();

	//-------------------------------------------------------------------
	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the karma
	 */
	public int getKarma() {
		return karma;
	}

	//-------------------------------------------------------------------
	/**
	 * @param karma the karma to set
	 */
	public void setKarma(int karma) {
		this.karma = karma;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the metahuman
	 */
	public boolean isMetahuman() {
		return metahuman;
	}

	//-------------------------------------------------------------------
	public boolean isAI() {
		return ai;
	}

}
