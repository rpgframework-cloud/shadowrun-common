package de.rpgframework.shadowrun;

/**
 * @author prelle
 *
 */
public enum ShadowrunFlags {
	
	PRIMARY_RANGED_WEAPON,
	PRIMARY_MELEE_WEAPON,

}
