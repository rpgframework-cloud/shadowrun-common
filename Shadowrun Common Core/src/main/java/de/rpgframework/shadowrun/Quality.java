package de.rpgframework.shadowrun;

import java.util.Locale;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="quality")
public class Quality extends ComplexDataItem {

	static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(Quality.class, Locale.GERMAN, Locale.ENGLISH);

	public enum QualityType {
		NORMAL,
		QUALITY_PATH,
		METAGENIC,
		INFECTED,
		LIFESTYLE,
		VIRTUAL_LIFE,
		ADEPT_WAY,
		STREAM,
		TRANSGENIC,
		SPECIAL,
		SHIFTER
		;
		public String getName(Locale loc) { return RES.getString("qualitysorttype."+name().toLowerCase(), loc); }
		public String getName() { return getName(Locale.getDefault()); }
		public static String getAllName(Locale loc) { return RES.getString("qualitysorttype.all", loc); }
	}

	public enum QualityCategory {
		ANIMAL_PELAGE,
		BENEFICIAL_SECRETIONS,
		COMPLEX_TRAITS,
		DERMAL_ALTERATION,
		METAGENIC_SENSES,
		METAGENIC_MORPHISMS,
		AHD,
		BIOLOGICAL_DYSMORPHIA,
		METAGENIC_ANOMALIES,
		METAGENIC_DEFECTS,
		METAGENIC_THROWBACKS,
		NEUROPATHIC_ADJUSTED_STATES,
		;
		public String getName(Locale loc) { return RES.getString("qualitysubsorttype."+name().toLowerCase(), loc); }
		public String getName() { return getName(Locale.getDefault()); }
		public static String getAllName(Locale loc) { return RES.getString("qualitysubsorttype.all", loc); }
	}

	@Attribute
	private int karma;
	@Attribute(name = "pos", required=true)
	private boolean positive;
	@Attribute
	private QualityType type;
	@Attribute
	private QualityCategory cat;
	@Attribute
	private int max;
	@Attribute(name="multi")
	private boolean multi;
	@Attribute(name="selectable")
	private boolean freeSelectable = true;
	@Attribute(name="nodouble")
	private boolean noDouble = true;

	//-------------------------------------------------------------------
	public int getKarmaCost() {
		return karma;
	}

	//-------------------------------------------------------------------
	public boolean isPositive() {
		return positive;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public QualityType getType() {
		return type;
	}
	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(QualityType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}
	//-------------------------------------------------------------------
	/**
	 * @param max the max to set
	 */
	public void setMax(int max) {
		this.max = max;
	}

	//-------------------------------------------------------------------
	public boolean hasLevel() {
		return super.hasLevel() || max>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the multi
	 */
	public boolean isMulti() {
		return multi;
	}
	//-------------------------------------------------------------------
	/**
	 * @param multi the multi to set
	 */
	public void setMulti(boolean multi) {
		this.multi = multi;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the freeSelectable
	 */
	public boolean isFreeSelectable() {
		return freeSelectable;
	}

	//-------------------------------------------------------------------
	/**
	 * @param freeSelectable the freeSelectable to set
	 */
	public void setFreeSelectable(boolean freeSelectable) {
		this.freeSelectable = freeSelectable;
	}

	//-------------------------------------------------------------------
	/**
	 * Return a shortend version of the name if present, otherwise the normal
	 * name
	 */
	public String getShortName() {
//		if (i18n==null)
//			return id;
//		String key = "quality."+id+".short";
//		if (i18n.containsKey(key))
//			return i18n.getString(key);
//		else
			return getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ComplexDataItem#validate()
	 */
	public void validate() throws DataErrorException {
		super.validate();
		if (type==null) throw new DataErrorException(this, "type= not set");
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cat
	 */
	public QualityCategory getCategory() {
		return cat;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the noDouble
	 */
	public boolean isNoDouble() {
		return noDouble;
	}


}
