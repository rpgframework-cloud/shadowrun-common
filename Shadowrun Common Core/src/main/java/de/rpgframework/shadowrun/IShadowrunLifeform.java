package de.rpgframework.shadowrun;

import java.util.List;

import de.rpgframework.genericrpg.data.ASkillValue;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.data.ISkill;
import de.rpgframework.genericrpg.data.Lifeform;

/**
 * @author prelle
 *
 */
public interface IShadowrunLifeform<A extends IAttribute, S extends ISkill, V extends ASkillValue<S>> extends Lifeform<A, S, V> {

	public List<BodyForm> getBodyForms();

	public BodyForm getBodyForm(BodyType type);

	public void clearBodyForms();

	public void addBodyForm(BodyForm value);

}
