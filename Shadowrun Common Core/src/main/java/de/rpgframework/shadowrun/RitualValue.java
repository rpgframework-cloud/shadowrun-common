package de.rpgframework.shadowrun;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class RitualValue extends ComplexDataItemValue<Ritual> {

	//-------------------------------------------------------------------
	public RitualValue() {
	}

	//-------------------------------------------------------------------
	public RitualValue(Ritual ritual) {
		super(ritual);
	}

}
