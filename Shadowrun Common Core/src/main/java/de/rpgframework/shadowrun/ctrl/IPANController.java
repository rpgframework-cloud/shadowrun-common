package de.rpgframework.shadowrun.ctrl;

import java.util.List;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.shadowrun.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public interface IPANController<T extends PieceOfGear> extends ProcessingStep {
	
	//-------------------------------------------------------------------
	public <C extends ShadowrunCharacter<?,?,?,?>> C getModel();

	//-------------------------------------------------------------------
	public List<CarriedItem<T>> getAvailableAccessDevices();
	
	//-------------------------------------------------------------------
	public CarriedItem<T> getUsedAccessDevice();
	public void setUsedAccessDevice(CarriedItem<T> data);
	
	public List<CarriedItem<T>> getDevicesInPAN();
	
}
