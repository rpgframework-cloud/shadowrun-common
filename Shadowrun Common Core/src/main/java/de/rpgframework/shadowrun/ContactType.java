package de.rpgframework.shadowrun;

import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.shadowrun.generators.ShadowrunTaxonomy;

/**
 * From: Sixth World Companion
 * @author prelle
 *
 */
@DataItemTypeKey(id = "contacttype")
public class ContactType extends DataItem implements Classification<ContactType> {

	public static final String CRIMINAL = "CRIMINAL";
	public static final String CORPORATE = "CORPORATE";
	public static final String GOVERNMENT = "GOVERNMENT";
	public static final String MAGIC   = "MAGIC";
	public static final String MATRIX  = "MATRIX";
	public static final String MEDICAL = "MEDICAL";
	public static final String STREET  = "STREET";

	@Override
	public ClassificationType getType() {
		return ShadowrunTaxonomy.CONTACT;
	}

	@Override
	public ContactType getValue() {
		return this;
	}

}
