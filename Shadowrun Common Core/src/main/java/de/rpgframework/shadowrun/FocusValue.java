package de.rpgframework.shadowrun;

import java.util.UUID;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.modification.Modifyable;

/**
 * @author prelle
 *
 */
public class FocusValue extends ComplexDataItemValue<Focus> implements Modifyable {

	@Attribute(name="name")
	private String name;
	
	@Attribute(name="itemUUID")
	private UUID linkedItem;

	//-------------------------------------------------------------------
	public FocusValue() {
	}

	//-------------------------------------------------------------------
	public FocusValue(Focus focus, int rating) {
		super(focus,rating);
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Focus("+resolved+",lvl="+value+",mods="+incomingModifications+")";
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (name!=null)
			return name;
		return resolved.getName();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return value;
	}

	//--------------------------------------------------------------------
	public int getCostNuyen() {
		return getLevel()*resolved.getNuyenCost();
	}

	//--------------------------------------------------------------------
	public int getCostKarma() {
		return getLevel()*resolved.getBondingMultiplier();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the linkedItem
	 */
	public UUID getLinkedItem() {
		return linkedItem;
	}

	//-------------------------------------------------------------------
	/**
	 * @param linkedItem the linkedItem to set
	 */
	public void setLinkedItem(UUID linkedItem) {
		this.linkedItem = linkedItem;
	}

}
