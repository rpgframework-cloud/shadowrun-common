package de.rpgframework.shadowrun;

import java.util.Locale;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="adeptpower")
public class AdeptPower extends ComplexDataItem {

	static MultiLanguageResourceBundle RES = ShadowrunCore.RES;
	
	public enum Activation {
		PASSIVE,
		FREE_ACTION,
		MINOR_ACTION,
		MAJOR_ACTION,
		SIMPLE_ACTION
		;
		public String getName(Locale loc) { return RES.getString("adeptpower.activation."+name().toLowerCase(), loc); }
		public String getName() { return getName(Locale.getDefault()); }
		public static String getAllName(Locale loc) { return RES.getString("adeptpower.activation.all", loc); }
	}
	
	@Attribute(name="multi")
	private boolean multi;
	@Attribute(name="base")
	private Double basecost;
	@Attribute(required=true)
	private float cost;
	@Attribute(name="max")
	private int maxLevel;
	@Attribute(name="act")
	private Activation activation;

	//-------------------------------------------------------------------
	/**
	 * @return the multi
	 */
	public boolean isMulti() {
		return multi;
	}
	//-------------------------------------------------------------------
	/**
	 * @param multi the multi to set
	 */
	public void setMulti(boolean multi) {
		this.multi = multi;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public float getCost() {
		return cost;
	}

	//--------------------------------------------------------------------
	/**
	 * @param level A value of 1 or higher
	 */
	public float getCostForLevel(int level) {
		if (basecost!=null)
			return (float) ( basecost + level*cost );
		return (float) (level*cost);
	}

	//--------------------------------------------------------------------
	public String getCostString(Locale loc) {
		try {
			if (!hasLevel) {
				System.out.println("cost="+cost+"  ::"+RES.getString("adeptpower.cost.format.plain", loc));
				return String.format(RES.getString("adeptpower.cost.format.plain", loc), cost);
			} else if (basecost==null) {
				System.out.println("cost="+cost+"  ::"+RES.getString("adeptpower.cost.format.level", loc));
				return String.format(RES.getString("adeptpower.cost.format.level", loc), cost);
			} else {
				System.out.println("basecost="+basecost+"   cost="+cost+"  ::"+RES.getString("adeptpower.cost.format.complex", loc));
				return String.format(RES.getString("adeptpower.cost.format.complex", loc), basecost, cost);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return the maxLevel
	 */
	public int getMaxLevel() {
		return maxLevel;
	}

	//-------------------------------------------------------------------
	/**
	 * @param maxLevel the maxLevel to set
	 */
	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the activation
	 */
	public Activation getActivation() {
		return activation;
	}

}
