package de.rpgframework.shadowrun.generators;

import java.util.MissingResourceException;

import org.prelle.simplepersist.Root;

import de.rpgframework.RPGFrameworkConstants;
import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;

/**
 * @author prelle
 *
 */
@Root(name="culture")
public enum FilterCulture implements Classification<FilterCulture> {

	ADL,
	ORZET,
	SPERETHIEL,
	UCAS
	;

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return RPGFrameworkConstants.RES.getString("filter.culture");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "filter.culture";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return ShadowrunTaxonomy.CULTURE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public FilterCulture getValue() {
		return this;
	}

}
