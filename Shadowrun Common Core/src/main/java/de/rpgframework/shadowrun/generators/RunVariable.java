package de.rpgframework.shadowrun.generators;

import de.rpgframework.random.GeneratorVariable;

/**
 * @author prelle
 *
 */
public enum RunVariable implements GeneratorVariable {

	DIFFICULTY(true),
	DIFFICULTY_REAL,
	JOHNSON_META,
	PAYMENT,
	PROF_EXPECTED_MAX(true),
	PROF_EXPECTED_MIN(true),
	REWARD_GROUP(true),
	REWARD_PERSON(true),
	;

	boolean integer;

	RunVariable() {
	}

	RunVariable(boolean integer) {
		this.integer = integer;
	}

	@Override
	public boolean isInteger() {
		return integer;
	}

}
