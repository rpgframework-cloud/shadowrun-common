package de.rpgframework.shadowrun.generators;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import org.prelle.simplepersist.Persister;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.classification.GenericClassificationType;
import de.rpgframework.classification.Genre;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.random.DataType;
import de.rpgframework.random.GeneratorType;
import de.rpgframework.random.GeneratorVariable;
import de.rpgframework.random.Plot;
import de.rpgframework.random.RandomGenerator;
import de.rpgframework.random.RollTable;
import de.rpgframework.random.RollTableGenerator;
import de.rpgframework.random.VariableHolderNode;
import de.rpgframework.shadowrun.NPCType;

/**
 * @author prelle
 *
 */
public class RunGenerator extends RollTableGenerator implements RandomGenerator, GenericClassificationType, ShadowrunTaxonomy {

	private final static Logger logger = System.getLogger("shadowrun.rungen");

//	protected List<Classification<?>> classifier = new ArrayList<>();
//	protected Map<GeneratorVariable,Integer> variables = new HashMap<>();

	//-------------------------------------------------------------------
	public RunGenerator() {
		super(new MultiLanguageResourceBundle(RunGenerator.class.getName().toString(), Locale.ENGLISH, Locale.GERMAN));
		super.loadTables(getClass().getResourceAsStream("runGenTables.xml"));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getId()
	 */
	@Override
	public String getId() {
		return "SRRun";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getType()
	 */
	@Override
	public GeneratorType getType() {
		return GeneratorType.RUN;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.random.RandomGenerator#withHints(java.util.List)
//	 */
//	@Override
//	public RandomGenerator withHints(List<Classification<?>> hints) {
//		for (Classification<?> hint : hints)
//			classifier.add(hint);
//		return this;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.random.RandomGenerator#withVariables(java.util.Map)
//	 */
//	@Override
//	public RandomGenerator withVariables(Map<GeneratorVariable, Integer> variables) {
//		for (Entry<GeneratorVariable, Integer> entry : variables.entrySet())
//			this.variables.put(entry.getKey(), entry.getValue());
//		return this;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getRequiredVariables()
	 */
	@Override
	public Collection<ClassificationType> getRequiredVariables() {
		return List.of(GenericClassificationType.GENRE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#matchesFilter(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean matchesFilter(Classification<?> filter) {
		if (filter.getType()==GENRE) {
			return Genre.CYBERPUNK == filter.getValue();
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#understandsHint(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean understandsHint(ClassificationType filter) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RollTableGenerator#resolveModifier(java.lang.String)
	 */
	@Override
	protected GeneratorVariable resolveModifier(String name) {
		return RunVariable.valueOf(name);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#generate(de.rpgframework.classification.Classification[])
	 */
	@Override
	public Object generate(VariableHolderNode context) {
		Plot plot = new Plot();
		// Copy global data
		for (Classification<?> tmp : context.getHints()) plot.setHint(tmp);

		RollTable table = tables.get("firstContact");
		if (table==null) {
			logger.log(Level.ERROR,"No table found for firstContact");
			return null;
		}
		logger.log(Level.DEBUG,"Roll with classifier "+plot.getHints());
		super.roll(table, plot, Locale.getDefault());

//		System.out.println("**Die Anwerbung**");
//		rollFirstContact(plot);
//		System.out.println("\n**Der Auftrag**");
//		rollTheJob(plot);
//		rollTarget(plot);
//		rollDifficulty(plot);
//		rollPayment(plot);
//		System.out.println("\n**Der Ablauf**");
//		rollComplication(plot);
//		rollDaumenschrauben(plot);
		return plot;
	}
//
//	//-------------------------------------------------------------------
//	private void rollFirstContact(Plot plot) {
//		PlotNode introduction = new PlotNode(Type.EXPOSITION);
//		plot.addNode(introduction);
//
//		switch (rollD6(2)) {
//		case 2:
//			System.out.println("Ein Gruppenmitglied wird zum Auftraggeber, zum Beispiel weil es von der eigenen Vergangenheit eingeholt wird");
//			break;
//		case 3:
//			System.out.println("Schmidt selbst meldet sich bei der Gruppe.");
//			rollMeetingMrJohnson();
//			rollWhoIsMrJohnson(plot);
//			break;
//		case 11:
//			System.out.println("Ein renommierterer Schieber hat von den Runnern gehört und meldet sich. Scheint so, als ginge es aufwärts");
//			rollMeetingMrJohnson();
//			rollWhoIsMrJohnson(plot);
//			break;
//		case 12:
//			System.out.println("Von wegen Anruf! Die Runner stolpern ungewollt direkt in einen Run.");
//			rollMittenInsGetümmel(plot);
//			rollWhoIsMrJohnson(plot);
//			break;
//		default:
//			System.out.println("Der übliche Schieber kontaktiert die Runner.");
//			rollMeetingMrJohnson();
//			rollWhoIsMrJohnson(plot);
//			break;
//		}
//	}
//
//	//-------------------------------------------------------------------
//	private void rollMittenInsGetümmel(Plot plot) {
//		switch (rollD6(1)) {
//		case 1:
//			System.out.println("Die Runner werden auf der Straße von der Fluchtwagenfahrerin eines fremden Runnerteams erkannt und angesprochen. Ihr Team ist in Schwierigkeiten geraten, und sie bietet ihnen einen Teil der Bezahlung/einen Gefallen, wenn sie es raushauen.");
//			break;
//		case 2:
//			System.out.println("Der Fluchtversuch eines anderen Runnerteams endet in unmittelbarer Nähe der Runner mit einem üblen Crash. Der einzige Überlebende bittet die Runner um Hilfe – und falls sie ihn vor den anrückenden Verfolgern schützen, weiht er sie in seinen Auftrag ein.");
//			break;
//		case 3:
//			System.out.println("Die Runner werden Zeugen eines geheimen und höchst illegalen Deals – zum Beispiel zwischen einem Sternschutz-Gerichtsmediziner und einer Gruppe Ghule –, und werden bemerkt. Wenn sie sich nicht mit beiden beteiligten Parteien an legen wollen, müssen sie sich mit einem Gefallen deren Vertrauen verdienen.");
//			break;
//		case 4:
//			System.out.println("Die Runner erfahren durch Zufall von einer unmittelbar bevorstehenden Gelegenheit, Zugriff auf ein wertvolles Ziel zu erhalten, etwa weil ein Fahrer eines Transporters vor Kollegen damit angibt, was er nach der Mittagspause transportieren soll. Sie wissen, dass einer ihrer Kontakte an diesem Ziel sehr interessiert wäre, können ihn aber gerade nicht errei chen – und der Fahrer winkt schon nach der Rechnung.");
//			break;
//		case 5:
//			System.out.println("Die Runner geraten ungewollt in ein ungleiches Kreuzfeuer, etwa zwischen dem Türwächter eines Wohnblocks und einer lokalen Gang, und die unterlegene Seite versucht, sie anzuheuern – für das lau fende Gefecht, und um längerfristig für Ruhe zu sorgen.");
//			break;
//		case 6:
//			System.out.println("Die Runner werden plötzlich das Ziel der Aufmerksamkeit einer gefährlichen Gruppierung, etwa weil sie von Konzerngardisten als gesuchte Verbrecher erkannt wurden und ausgeschaltet – oder angeworben – werden sollen.");
//			break;
//		}
//	}
//
//	//-------------------------------------------------------------------
//	private void rollMeetingMrJohnson() {
//		// Zeitpunkt
//		switch (rollD6(1)) {
//		case 1:
//			System.out.println("Das Treffen mit dem Schmidt findet sofort statt, die Runner müssen unverzüglich aufbrechen.");
//			break;
//		case 6:
//			System.out.println("Die Runner haben ein großes Zeitfenster bis zum Treffen mit dem Schmidt. Man könnte schon mal Informationen einholen.");
//			break;
//		default:
//			System.out.println("Das Treffen mit dem Schmidt findet innerhalb weniger Stunden statt. Die Runner können sich normal vorbereiten.");
//			break;
//		}
//
//		// Ort des Treffens
//		switch (rollD6(2)) {
//		case 2: case 3:
//			System.out.println("Treffpunkt ist in der Matrix");
//			break;
//		case 4:
//			System.out.println("Treffpunkt ist ein einsamer Ort in der Wildnis (z.B. ein Waldweg, eine Berghütte ...)");
//			break;
//		case 5: case 6:
//			System.out.println("Treffpunkt ist ein einsamer Ort in der Stadt (z.B. ein nächtliches Gewerbegebiet, ausgestorbene Seitenstraße)");
//			break;
//		case 7: case 8:
//			System.out.println("Der Treffpunkt ist völlig durchschnittlich (z.B. eine Hotellobby, eine Bar)");
//			break;
//		case 9:
//			System.out.println("Der Treffpunkt ist was besonderes (ein Luxusrestaurant, ein Goldplatz, eine Dachterrasse)");
//			break;
//		case 10:
//			System.out.println("Der Treffpunkt ist belebt (eine Mall, ein Konzert, ein Sportstadion)");
//			break;
//		case 11:
//			System.out.println("Der Treffpunkt ist mobil (eine Limousine, ein Zug)");
//			break;
//		case 12:
//			System.out.println("Der Treffpunkt ist Ort im Astralraum");
//			break;
//		}
//
//		// Komplikationen beim Treffens
//		switch (rollD6(2)) {
//		case 2:
//			System.out.println("Das Ziel des Runds wurde informiert und hat starke Kräfte geschickt um die Runner abzufangen");
//			break;
//		case 3: case 4:
//			System.out.println("Eine lokale Gruppierung macht den Runnern Schwierigkeiten (eine Gang, ein Junggesellinnenabschied oder ein Rudel wilder Tiere.)");
//			break;
//		case 10: case 11:
//			System.out.println("Die Runner geraten in eine Polizeikontrolle. Hoffentlich haben alle ordentliche SINs und Lizenzen dabei …");
//			break;
//		case 12:
//			System.out.println("Fahnder haben sich an die Fersen der gesuchten Runner geheftet und nutzen die Gelegenheit für einen Zugriffsversuch. Vielleicht haben sie sie auch nur verwechselt …");
//			break;
//		default:
//			System.out.println("Alles läuft glatt. Es gibt keine Komplikationen.");
//			break;
//		}
//	}
//
//	//-------------------------------------------------------------------
//	private void rollWhoIsMrJohnson(Plot plot) {
//		System.out.print("Der Auftraggeber ist ");
//
//		switch (rollD6(2)) {
//		case 2:
//			System.out.println("ein Geheimbund – der sich nur deswegen den Runnern anvertraut, weil er nicht erwartet, dass sie später davon erzählen können");
//			plot.addVariable(RunVariable.DIFFICULTY, 2);
//			break;
//		case 3:
//			System.out.println("eine offen auftretende politische Gruppe.");
//			plot.addVariable(RunVariable.PAYMENT, -1);
//			break;
//		case 4:
//			System.out.println("eine staatliche oder öffentiche Institution");
//			break;
//		case 5: case 6:
//			System.out.println("ein Konzern von regionaler oder nationaler Bedeutung");
//			break;
//		case 7: case 8:
//			System.out.println("ein Megakon - der freilich für alltägliche Probleme eigene Ressourcen hat");
//			plot.addVariable(RunVariable.DIFFICULTY, 1);
//			plot.addVariable(RunVariable.PAYMENT, 1);
//			break;
//		case 9: case 10:
//			System.out.println("eine illegale Gruppierung");
//			break;
//		case 11:
//			System.out.println("ein zwielichtiges Individuum - was selten etwas Gutes bedeutet.");
//			plot.addVariable(RunVariable.DIFFICULTY, 1);
//			break;
//		case 12:
//			System.out.println("eine magische Gruppierung - die nur bei wirklich herausfordernden Fällen Hilfe braucht");
//			plot.addVariable(RunVariable.DIFFICULTY, 2);
//			plot.addVariable(RunVariable.PAYMENT, 1);
//			break;
//		}
//
//	}
//
//	//-------------------------------------------------------------------
//	private void rollTheJob(Plot plot) {
//		System.out.print("Die Aufgabe ist ");
//
//		switch (rollD6(1)) {
//		case 1:
//			System.out.print("die Beschaffung ");
//			switch (rollD6(1)) {
//			case 1: System.out.println("von Informationen (Beschattung, Datendiebstahl)"); break;
//			case 2: System.out.println("eines leicht zu verbergenden Objekts"); break;
//			case 3: System.out.println("einer Lebensform"); break;
//			case 4: System.out.println("eines besonders großen und/oder schweren Objects"); break;
//			case 5: System.out.println("eines magischen Objects"); break;
//			case 6: System.out.println("eines Fahrzeugs"); break;
//			}
//			break;
//		case 2:
//			System.out.print("die Anwerbung ");
//			switch (rollD6(1)) {
//			case 1: System.out.println("eines freiwilligen, vorbereiteten Ziels"); break;
//			case 2: System.out.println("eines bereitwilligen, überraschten Ziels"); break;
//			case 3: System.out.println("eines bereitwilligen Ziels, das Bedingungen stellt (etwa die Mitnahme einer weiteren Person)"); break;
//			case 4: System.out.println("eines unwilligen Ziels"); break;
//			case 5: System.out.println("eines wehrhaften unwilligen Ziels"); break;
//			case 6: System.out.println("eines wehrhaften unwilligen Ziels, das sich eher etwas antut, als sich entführen zu lassen"); break;
//			}
//			break;
//		case 3:
//			System.out.print("die Zerstörung ");
//			switch (rollD6(1)) {
//			case 1: System.out.println("eines Datensatzes"); break;
//			case 2: System.out.println("eines kleinen Objektes"); break;
//			case 3: System.out.println("eines großen Ziels (etwa ein Gebäude)"); break;
//			case 4: System.out.println("eines beweglichen Ziels (z.B. ein Drohnentruck)"); break;
//			case 5: System.out.println("eines Ziels, das von mehreren Personen frequentiert wird (etwa eine Mall)"); break;
//			case 6: System.out.println("einer Zielperson"); break;
//			}
//			break;
//		case 4:
//			System.out.print("die Infiltration ");
//			switch (rollD6(1)) {
//			case 1: System.out.println("eines kleinen Unternehmens"); break;
//			case 2: System.out.println("eines nationalen Unternehmens"); break;
//			case 3: System.out.println("eines Megakons"); break;
//			case 4: System.out.println("einer öffentlichen Stelle"); break;
//			case 5: System.out.println("einer privaten Gruppierung (z.B. ein Policlub)"); break;
//			case 6: System.out.println("eines Geheimbundes"); break;
//			}
//			break;
//		case 5:
//			System.out.print("der Schutz ");
//			switch (rollD6(1)) {
//			case 1: System.out.println("eines kleinen Objektes"); break;
//			case 2: System.out.println("einer Einrichtung"); break;
//			case 3: System.out.println("einer Person"); break;
//			case 4: System.out.println("einer Personengruppe"); break;
//			case 5: System.out.println("eines Ortes"); break;
//			case 6: System.out.println("eines Fahrzeugs in Bewegung"); break;
//			}
//			break;
//		case 6:
//			System.out.print("der Transport ");
//			switch (rollD6(1)) {
//			case 1: System.out.println("von Daten"); break;
//			case 2: System.out.println("eines kleineren Objektes"); break;
//			case 3: System.out.println("einer größeren Warenmenge"); break;
//			case 4: System.out.println("eines gefährlichen Objektes (Sprengstoffe, Krankheitserreger, unbekanntes magisches Artefakt)"); break;
//			case 5: System.out.println("eines einzelnen (un)willigen Lebewesens"); break;
//			case 6: System.out.println("einer Gruppe (un)williger Lebewesen"); break;
//			}
//			break;
//		}
//	}
//
//	//-------------------------------------------------------------------
//	private void rollTarget(Plot plot) {
//		System.out.print("Das Ziel, sofern es nicht bereits durch den Job vorgegeben ist, ");
//
//		switch (rollD6(2)) {
//		case 2: case 3:
//			System.out.println("ist eine abgelegene Gegend mitten in der Wildnis.");
//			break;
//		case 4: case 5:
//			System.out.println("ist ein ländliches, aber bevölkertes Gebiet.");
//			break;
//		case 6: case 7:
//			System.out.println("ist ein abgelegenes, städtisches Gebiet (ein Gewerbegebiet, ein Vorort, ein Slum)");
//			break;
//		case 8: case 9:
//			System.out.println("ist ein Innenstadtbereich.");
//			break;
//		case 10:
//			System.out.println("ist ein zugangsbeschränktes Gebiet (ein Flughafen, ein Fabrikgelände, eine Mülldeponie, ein Zoo)");
//			break;
//		case 11: case 12:
//			System.out.println("ist eine bewachte Sperrzone (ein Truppenübungsplatz, ein Gefängnis, eine Arkologie)");
//			plot.addVariable(RunVariable.DIFFICULTY, 2);
//			break;
//		}
//	}
//
//	//-------------------------------------------------------------------
//	private void rollDifficulty(Plot plot) {
//		System.out.print("\nDer Run ist ");
//		int modifier = plot.getVariable(RunVariable.DIFFICULTY);
//
//		int difficulty = rollD6(2)+modifier;
//		plot.setGenericVariable(DIFFICULTY, difficulty);
//		switch (difficulty) {
//		case 2:
//			System.out.println("ein Milkrun! ");
//			plot.setGenericVariable(PROFESSIONAL_MIN, 0);
//			plot.setGenericVariable(PROFESSIONAL_MAX, 1);
//			break;
//		case 3: case 4:
//			System.out.println("ein entspannter Auftrag. ");
//			plot.setGenericVariable(PROFESSIONAL_MIN, 1);
//			plot.setGenericVariable(PROFESSIONAL_MAX, 3);
//			break;
//		case 5: case 6: case 7:
//			System.out.println("ein alltäglicher Job. ");
//			plot.setGenericVariable(PROFESSIONAL_MIN, 3);
//			plot.setGenericVariable(PROFESSIONAL_MAX, 5);
//			break;
//		case 8: case 9:
//			System.out.println("eine Aufgabe für erfahrene Runner");
//			plot.setGenericVariable(PROFESSIONAL_MIN, 4);
//			plot.setGenericVariable(PROFESSIONAL_MAX, 6);
//			break;
//		case 10: case 11:
//			System.out.println("eine echte Herausforderung.");
//			plot.setGenericVariable(PROFESSIONAL_MIN, 6);
//			plot.setGenericVariable(PROFESSIONAL_MAX, 8);
//			break;
//		case 12: case 13:
//			System.out.println("ein Himmelfahrtskommando. ");
//			plot.setGenericVariable(PROFESSIONAL_MIN, 7);
//			plot.setGenericVariable(PROFESSIONAL_MAX, 9);
//			break;
//		default:
//			System.out.println("eine Erklärung dafür, woher der Ausspruch 'Lass ab von Drachen' kommt.");
//			plot.setGenericVariable(PROFESSIONAL_MIN, 10);
//			plot.setGenericVariable(PROFESSIONAL_MAX, 10);
//			break;
//		}
//		System.out.println("Professionalitätsstufe: "+plot.getGenericVariable(PROFESSIONAL_MIN)+"-"+plot.getGenericVariable(PROFESSIONAL_MAX));
//	}
//
//	//-------------------------------------------------------------------
//	private void rollComplication(Plot plot) {
//		System.out.println("\n*Komplikation*");
//		switch (rollD6(2)) {
//		case 2:
//			System.out.println("Schmidt versucht die Runner in die Pfanne zu hauen.");
//			break;
//		case 3: case 4:
//			System.out.println("Die Runner dienen als Ablenkung für einen anderen Run.");
//			break;
//		case 5: case 6:
//			System.out.println("Die Runner haben Konkurrenz, von der sie zuvor nichts wussten.");
//			break;
//		case 7:
//			System.out.println("Es kommt zu keinen Komplikationen.");
//			break;
//		case 8:
//			System.out.println("Zu einem Zeitpunkt, der den Runnern völlig ungelegen kommt, werden sie in eine unvorhergesehene Situation geworfen.");
//			break;
//		case 9:
//			System.out.println("Das Zielobjekt stellt sich als etwas anderes heraus, als die Runner erwartet hatten.");
//			break;
//		case 10: case 11:
//			System.out.println("Um den Run absolvieren zu können, muss die Gruppe ein besonderes Objekt in ihren Besitz bringen.");
//			break;
//		default:
//			System.out.println("Das Ziel der Gruppe wird kurz vor Beginn des Runs an einen stärker gesicherten ort verlegt.");
//			break;
//		}
//	}
//
//	//-------------------------------------------------------------------
//	private void rollDaumenschrauben(Plot plot) {
//		System.out.println("\n*Daumenschrauben*");
//		switch (rollD6(1)) {
//		case 1:
//			System.out.println("Eine unerwartete Patrouille der für die Umgebung zuständigen Sicherheitskräfte erscheint (Anzahl 1W6).");
//			break;
//		case 2:
//			System.out.println("Die Sicherheit des Ziels wird von einer erfahreneren Gruppe der dort eingesetzten Kräfte überprüft, die unangekündigt auftaucht (Anzahl 3W6, Professionalitätsstufe +2).");
//			break;
//		case 3:
//			System.out.println("Die Informationen der Runner über die Sicherheit des Ziels waren falsch, tatsächlich sind die Kräfte stärker (Professionalitätsstufe +2).");
//			break;
//		case 4:
//			System.out.println("Die Sicherheitskräfte befinden sich bereits im Alarmzustand, sie sind aufmerksam und halten ihre Waffen im Anschlag");
//			break;
//		case 5:
//			System.out.println("Die Runner sind nicht die Einzigen, die es auf das Ziel abgesehen haben. Eine konkurrierende Gruppe sorgt für Zeitdruck - oder löst gar Alarm aus und ruft Verstärkung auf den Plan.");
//			break;
//		case 6:
//			System.out.println("Es brennt – oder jemand mit einem teuren BuMoNA-Vertrag ist in Schwierigkeiten. Auf jeden Fall tauchen gut bewaffnete Rettungskräfte auf, die zum gleichen Ort wollen wie die Runner.");
//			break;
//		}
//	}
//
//	//-------------------------------------------------------------------
//	private void rollPayment(Plot plot) {
//		System.out.print("Die Bezahlung ");
//		int difficulty = (Integer)plot.getGenericVariable(DIFFICULTY);
//		int modifier = plot.getVariable(RunVariable.PAYMENT);
//
//		switch (rollD6(2)+modifier) {
//		case 0: case 1: case 2:
//			System.out.println("gitb es nicht. Schmidt versucht entweder die Runner zu betrügen oder ist pleite.");
//			break;
//		case 3: case 4:
//			System.out.println("ist ein echt mieses Geschaft. Die Gruppe erhält "+(1000*difficulty)+" Euro.");
//			break;
//		case 5: case 6:
//			System.out.println("ist nicht gerade üppig. Die Gruppe erhält "+(3000*difficulty)+" Euro.");
//			break;
//		case 7: case 8: case 9:
//			System.out.println("ist ein angemessener Lohn. Die Gruppe erhält "+(5000*difficulty)+" Euro.");
//			break;
//		case 10: case 11:
//			System.out.println("ist fair, wirklich fair. Jeder Runner erhält "+(1000*difficulty)+" Euro.");
//			break;
//		case 12: case 13:
//			System.out.println("ist großzügig. Jeder Runner erhält "+(2000*difficulty)+" Euro.");
//			break;
//		default:
//			System.out.println("ist herausragend. Jeder Runner erhält "+(5000*difficulty)+" Euro.");
//			break;
//		}
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RollTableGenerator#parseVariables(java.lang.String)
	 */
	@Override
	protected List<Classification<?>> parseVariables(String line) {
		List<Classification<?>> ret = super.parseVariables(line);
		StringTokenizer tok = new StringTokenizer(line, ",; ");
		while (tok.hasMoreTokens()) {
			String keyValue = tok.nextToken();
			String[] pair = keyValue.split(":");
			try {
				switch (pair[0]) {
//				case "CONTACT":
//					Classification<?> clsf = GenericCore.getItem(ContactType.class, pair[1]);
//					if (clsf==null) {
//						logger.log(Level.ERROR, "No contact type {0} known in {1}", pair[1], GenericCore.getItemList(ContactType.class));
//					}
//					ret.add(clsf); break;
				case "CULTURE": ret.add(FilterCulture.valueOf(pair[1])); break;
				case "META"   : ret.add(FilterMetatype.valueOf(pair[1].toUpperCase())); break;
				case "NPCTYPE": ret.add(NPCType.valueOf(pair[1])); break;
				default:
					logger.log(Level.WARNING,"Unknown classification: "+pair[0]);
				}
			} catch (IllegalArgumentException iae) {
				logger.log(Level.ERROR,"Invalid value '"+pair[1]+"' for classification '"+pair[1]);
//				System.exit(1);
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getProvidedData()
	 */
	@Override
	public Collection<DataType> getProvidedData() {
		return List.of(DataType.PLOT_DETAIL);
	}

}
