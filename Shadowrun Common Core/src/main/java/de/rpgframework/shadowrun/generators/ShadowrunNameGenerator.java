package de.rpgframework.shadowrun.generators;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.classification.Gender;
import de.rpgframework.classification.GenericClassificationType;
import de.rpgframework.classification.Genre;
import de.rpgframework.random.DataType;
import de.rpgframework.random.GeneratorType;
import de.rpgframework.random.RandomGenerator;
import de.rpgframework.random.VariableHolderNode;

/**
 * @author prelle
 *
 */
public class ShadowrunNameGenerator implements RandomGenerator, GenericClassificationType, ShadowrunTaxonomy {

	private enum Nametype {
		MALE,
		FEMALE,
		FAMILIY
	}

	private final static Logger logger = System.getLogger(ShadowrunNameGenerator.class.getPackageName());
	private final static Random RANDOM = new Random();

	private Map<FilterCulture,Map<Nametype,List<String>>> names;

	//-------------------------------------------------------------------
	public ShadowrunNameGenerator() {
		names   = new HashMap<>();

		loadNames(FilterCulture.ADL, Nametype.MALE   , "names_male_de.txt");
		loadNames(FilterCulture.ADL, Nametype.FEMALE , "names_female_de.txt");
		loadNames(FilterCulture.ADL, Nametype.FAMILIY, "familynames_de.txt");
		loadNames(FilterCulture.ORZET, Nametype.MALE   , "names_male_orzet.txt");
		loadNames(FilterCulture.ORZET, Nametype.FEMALE , "names_female_orzet.txt");
		loadNames(FilterCulture.SPERETHIEL, Nametype.MALE   , "names_male_sperethiel.txt");
		loadNames(FilterCulture.SPERETHIEL, Nametype.FEMALE , "names_female_sperethiel.txt");
		loadNames(FilterCulture.UCAS, Nametype.MALE   , "names_male_en.txt");
		loadNames(FilterCulture.UCAS, Nametype.FEMALE , "names_female_en.txt");
		loadNames(FilterCulture.UCAS, Nametype.FAMILIY, "familynames_en.txt");
	}

	//-------------------------------------------------------------------
	private void loadNames(FilterCulture cult, Nametype type, String resourceName) {
		Map<Nametype, List<String>> listByType = names.get(cult);
		if (listByType==null) {
			listByType = new HashMap<>();
			names.put(cult, listByType);
		}
		// Now the list
		List<String> list = listByType.get(type);
		if (list==null) {
			list = new ArrayList<>();
			listByType.put(type, list);
		}


		InputStream ins = ShadowrunNameGenerator.class.getResourceAsStream(resourceName);
		if (ins!=null) {
			try {
				BufferedReader read = new BufferedReader(new InputStreamReader(ins));
				List<String> lines = read.lines().collect(Collectors.toList());
				for (String line : lines) {
					if (line.isBlank()) continue;
					if (line.startsWith("#")) continue;
					String[] split = line.split(",");
					for (String tmp : split) {
						list.add(tmp.trim());
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//System.out.println("Loaded "+list.size()+" names from "+resourceName);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getId()
	 */
	@Override
	public String getId() {
		return "SRNames";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getType()
	 */
	@Override
	public GeneratorType getType() {
		return GeneratorType.NAME_PERSON;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getRequiredVariables()
	 */
	@Override
	public Collection<ClassificationType> getRequiredVariables() {
		return List.of(GenericClassificationType.GENRE, CULTURE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#matchesFilter(de.rpgframework.classification.Classification)
	 */
	@SuppressWarnings("incomplete-switch")
	@Override
	public boolean matchesFilter(Classification<?> filter) {
		if (filter.getType()==CULTURE) {
			return names.containsKey( (FilterCulture) filter.getValue());
		}
		if (filter.getType()==GENRE) {
			switch( (Genre) filter.getValue()) {
			case CYBERPUNK:
			case TODAY:
				return true;
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#understandsHint(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean understandsHint(ClassificationType filter) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#generate(de.rpgframework.random.VariableHolderNode)
	 */
	@Override
	public Object generate(VariableHolderNode context) {
		List<Classification<?>> list = new ArrayList<Classification<?>>(context.getHints());
		logger.log(Level.INFO, "Generate name for "+list);
		boolean male = RANDOM.nextBoolean();
		if (RandomGenerator.contains(list, GENDER)) {
			Gender gender =  RandomGenerator.getValueOf(list, GENDER);
			if (gender==Gender.MALE) male=true;
			if (gender==Gender.FEMALE) male=false;
		}

		StringBuffer buf = new StringBuffer();

		FilterCulture cult = RandomGenerator.getValueOf(list, ShadowrunTaxonomy.CULTURE);
		if (cult==null) cult = FilterCulture.UCAS;
		logger.log(Level.INFO, "use tables for "+cult);
		Map<Nametype, List<String>> map = names.get(cult);
		List<String> namelist = null;
		if (male) {
			namelist = map.get(Nametype.MALE);
		} else {
			namelist = map.get(Nametype.FEMALE);
		}
		// Maybe pick a different nametable for metatype
		FilterMetatype meta = RandomGenerator.getValueOf(list, ShadowrunTaxonomy.META);
		if (meta!=null && RANDOM.nextInt(10)>7) {
			// In 20% of all cases, pick custom table
			switch (meta) {
			case ELF:
				Map<Nametype, List<String>> map2 = names.get(FilterCulture.SPERETHIEL);
				if (male) {
					namelist = map2.get(Nametype.MALE);
				} else {
					namelist = map2.get(Nametype.FEMALE);
				}
				break;
			case ORK:
				map2 = names.get(FilterCulture.ORZET);
				if (male) {
					namelist = map2.get(Nametype.MALE);
				} else {
					namelist = map2.get(Nametype.FEMALE);
				}
				break;
			}
		}


		buf.append(namelist.get(RANDOM.nextInt(namelist.size())));
		buf.append(" ");
		buf.append(map.get(Nametype.FAMILIY).get(RANDOM.nextInt((map.get(Nametype.FAMILIY).size()))));
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getProvidedData()
	 */
	@Override
	public Collection<DataType> getProvidedData() {
		return List.of(DataType.ACTOR_BASEDATA);
	}

}
