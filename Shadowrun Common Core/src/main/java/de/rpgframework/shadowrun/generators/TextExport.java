package de.rpgframework.shadowrun.generators;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.Map.Entry;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.random.Actor;
import de.rpgframework.random.GeneratorVariable;
import de.rpgframework.random.Plot;
import de.rpgframework.random.PlotNode;
import de.rpgframework.random.ResultExport;
import de.rpgframework.random.TextLine;

/**
 * @author prelle
 *
 */
public class TextExport implements ResultExport {

	private final static Logger logger = System.getLogger(TextExport.class.getPackageName());

	private MultiLanguageResourceBundle PLOT_RES = new MultiLanguageResourceBundle(
			RunGenerator.class.getName().toString(), Locale.ENGLISH, Locale.GERMAN);

	// -------------------------------------------------------------------
	/**
	 */
	public TextExport() {
		// TODO Auto-generated constructor stub
	}

	// -------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.ResultExport#export(java.lang.Object,
	 *      java.io.OutputStream)
	 */
	@Override
	public void export(Object toExport, OutputStream out, Locale loc) {
		if (toExport instanceof Plot) {
			export((Plot) toExport, out, loc);
		}

	}

	// -------------------------------------------------------------------
	private void export(Plot plot, OutputStream stream, Locale loc) {
		logger.log(Level.INFO, "Export plot");
		PrintWriter out = new PrintWriter(stream);
		for (PlotNode section : plot.getChildNodes()) {
			String title = PLOT_RES.getString("section." + section.getType().name().toLowerCase(), loc);
			out.println("\n" + title);
			out.println(new String(new char[title.length()]).replace("\0", "="));

			for (TextLine line : section.getLines()) {
				StringBuffer buf = new StringBuffer();
				for (String key : line.getKeys()) {
					String trans = PLOT_RES.getString(key, loc).trim();
					buf.append(trans + " ");
				}
				if (!buf.toString().trim().endsWith(".")) {
					out.println(buf.toString().trim() + ".");
				} else {
					out.println(buf.toString());
				}
			}
		}
		out.flush();

		out.println("\nPersonen\n===========");
		for (Actor actor : plot.getActors()) {
			out.flush();
			export(actor, stream, loc);
		}

		out.println("\nVariables\n===========");
		for (Entry<GeneratorVariable, Integer> entry : plot.getVariables().entrySet()) {
			export(entry, stream, loc);
		}
		out.flush();

	}

	// -------------------------------------------------------------------
	private void export(Actor actor, OutputStream stream, Locale loc) {
		logger.log(Level.INFO, "Export actor");
		PrintWriter out = new PrintWriter(stream);
		logger.log(Level.INFO, "Actor " + actor.getId());
		out.println(actor.getRole() + ": " + actor.getName()+", "+actor.getGender().name());
		out.flush();
	}

	// -------------------------------------------------------------------
	private void export(Entry<GeneratorVariable, Integer> variable, OutputStream stream, Locale loc) {
		logger.log(Level.INFO, "Export variable");
		PrintWriter out = new PrintWriter(stream);
		logger.log(Level.INFO, "Variable " + variable.getKey()+"="+variable.getValue());
		out.println(variable.getKey() + ": " + variable.getValue());
		out.flush();
	}

}
