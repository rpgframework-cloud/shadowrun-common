package de.rpgframework.shadowrun.generators;

import java.util.List;

import de.rpgframework.random.GeneratorInitializer;
import de.rpgframework.random.RandomGenerator;

/**
 *
 */
public class ShadowrunGeneratorInitializer implements GeneratorInitializer {

	private static List<RandomGenerator> generators;

	//-------------------------------------------------------------------
	public ShadowrunGeneratorInitializer() {
		generators = List.of(
			new ShadowrunNameGenerator(),
			new ShadowrunNSCGenerator(),
			new RunGenerator()
			);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.GeneratorInitializer#getGeneratorsToRegister()
	 */
	@Override
	public List<RandomGenerator> getGeneratorsToRegister() {
		return generators;
	}

}
