package de.rpgframework.shadowrun.generators;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.shadowrun.ShadowrunCore;

/**
 * @author prelle
 *
 */
public interface ShadowrunTaxonomy {

	final static MultiLanguageResourceBundle RES = ShadowrunCore.getI18nResources();

	public final static ClassificationType CULTURE = new ClassificationType("CULTURE", RES);
	public final static ClassificationType META    = new ClassificationType("META", RES);
	public final static ClassificationType CONTACT = new ClassificationType("CONTACT", RES);
	public final static ClassificationType NPCTYPE = new ClassificationType("NPCTYPE", RES);

}
