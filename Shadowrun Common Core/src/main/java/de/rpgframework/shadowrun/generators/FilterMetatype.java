/**
 * 
 */
package de.rpgframework.shadowrun.generators;

import java.util.MissingResourceException;

import org.prelle.simplepersist.Root;

import de.rpgframework.RPGFrameworkConstants;
import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;

/**
 * @author prelle
 *
 */
@Root(name="meta")
public enum FilterMetatype implements Classification<FilterMetatype> {
	
	HUMAN,
	DWARF,
	TROLL,
	ELF,
	ORK
	;

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return RPGFrameworkConstants.RES.getString("filter.metatype");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "filter.metatype";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return ShadowrunTaxonomy.META;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public FilterMetatype getValue() {
		return this;
	}
	
}
