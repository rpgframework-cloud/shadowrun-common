package de.rpgframework.shadowrun.generators;

import de.rpgframework.classification.Classification;
import de.rpgframework.classification.Genre;
import de.rpgframework.random.NSCGenerator;

/**
 * @author prelle
 *
 */
public class ShadowrunNSCGenerator extends NSCGenerator {

	//-------------------------------------------------------------------
	/**
	 */
	public ShadowrunNSCGenerator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#matchesFilter(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean matchesFilter(Classification<?> filter) {
		if (filter==Genre.CYBERPUNK) return true;
		return false;
	}

}
