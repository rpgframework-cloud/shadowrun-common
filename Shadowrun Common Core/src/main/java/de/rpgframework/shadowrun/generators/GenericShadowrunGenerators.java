package de.rpgframework.shadowrun.generators;

import de.rpgframework.classification.Taxonomy;
import de.rpgframework.random.RandomGeneratorRegistry;

/**
 * @author prelle
 *
 */
public class GenericShadowrunGenerators {

	//-------------------------------------------------------------------
	public static void initialize() {
		RandomGeneratorRegistry.register(new ShadowrunNameGenerator());
		RandomGeneratorRegistry.register(new RunGenerator());
	}

}
