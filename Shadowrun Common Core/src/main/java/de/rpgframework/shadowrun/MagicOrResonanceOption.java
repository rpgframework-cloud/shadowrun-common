package de.rpgframework.shadowrun;

import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@Root(name="magicopt")
@DataItemTypeKey(id="magicopt")
public class MagicOrResonanceOption extends PriorityOption<MagicOrResonanceType> {

	@Attribute
	private int value;
	@ElementList(type = Choice.class, entry = "choice")
	protected List<Choice> choices;

	//-------------------------------------------------------------------
	public MagicOrResonanceOption() {
	}

	//-------------------------------------------------------------------
	public MagicOrResonanceOption(MagicOrResonanceType type, int value) {
		this.id = type.getId();
		this.value  = value;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (id!=null)
			return id;
		return super.toString();
	}

	//-------------------------------------------------------------------
	public String getType() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(PriorityOption<MagicOrResonanceType> other) {
     		return id.compareTo(other.getId());
	}

	//--------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

}
