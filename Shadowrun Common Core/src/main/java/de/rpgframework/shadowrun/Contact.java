/**
 *
 */
package de.rpgframework.shadowrun;

import java.util.UUID;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.data.GenericCore;

/**
 * @author Stefan
 *
 */
public class Contact {

	@Attribute(name="id")
	private UUID id;
	@Attribute(name="rat")
	private int rating;
	@Attribute(name="loy")
	private int loyalty;
	@Attribute
	private String name;
	@Attribute
	private String type;
	@Attribute
	private String typename;
	@Element
	private String description;
	@Attribute
	private int favors;

	//--------------------------------------------------------------------
	public Contact() {
		id = UUID.randomUUID();
		loyalty = 1;
		rating  = 1;
		name    = "Unnamed";
	}

	//--------------------------------------------------------------------
	public Contact(String name, ContactType type, String typename, int infl, int loy) {
		this.name = name;
		this.type     = type.getId();
		this.typename = typename;
		rating    = infl;
		loyalty   = loy;
		id = UUID.randomUUID();
	}

	//--------------------------------------------------------------------
	public String toString() {
		return name+" (type="+type+", job="+typename+", rat="+rating+", loy="+loyalty+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the loyalty
	 */
	public int getLoyalty() {
		return loyalty;
	}

	//--------------------------------------------------------------------
	/**
	 * @param loyalty the loyalty to set
	 */
	public void setLoyalty(int loyalty) {
		this.loyalty = loyalty;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//--------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name==null)
			this.name = name;
		else
			this.name = name.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//--------------------------------------------------------------------
	/**
	 * @return the influence
	 */
	public int getRating() {
		return rating;
	}

	//--------------------------------------------------------------------
	/**
	 * @param influence the influence to set
	 */
	public void setRating(int influence) {
		this.rating = influence;
	}

	//-------------------------------------------------------------------
	public String getTypeName(){
		return typename;
	}

	//--------------------------------------------------------------------
	public void setTypeName(String type) {
		if (type==null)
			this.typename = type;
		else
			this.typename = type.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		if (description!=null)
			this.description = description.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the favors
	 */
	public int getFavors() {
		return favors;
	}

	//-------------------------------------------------------------------
	/**
	 * @param favors the favors to set
	 */
	public void setFavors(int favors) {
		this.favors = favors;
	}

	//-------------------------------------------------------------------
	public ContactType getType() {
		return GenericCore.getItem(ContactType.class, type);
	}

	//-------------------------------------------------------------------
	public void setType(String id) {
		this.type = id;
	}

	//-------------------------------------------------------------------
	public void setType(ContactType data) {
		if (data==null)
			this.type=null;
		else
			this.type = data.getId();
	}

}
