package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.shadowrun.Movement.MovementType;

/**
 * Representation of an additional
 * @author prelle
 *
 */
public class BodyForm {

	@Attribute
	private BodyType type;
	@ElementList(entry = "movement", type = Movement.class)
	private List<Movement> movements;
	@ElementList(type = AttributeValue.class, entry = "attributes")
    protected List<AttributeValue<ShadowrunAttribute>> attributes;
	@ElementList(entry="quality", type = QualityValue.class, inline = false)
	private List<QualityValue> qualities;
    @ElementList(type = CritterPowerValue.class, entry = "critterpower")
    protected List<CritterPowerValue> critterPowers;

	//-------------------------------------------------------------------
	public BodyForm() {
		movements = new ArrayList<>();
		qualities = new ArrayList<>();
		critterPowers = new ArrayList<>();
		attributes = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public BodyForm(BodyType type) {
		this();
		this.type = type;
	}

	//-------------------------------------------------------------------
	public List<Movement> getMovements() {
		return movements;
	}

	//-------------------------------------------------------------------
	public Movement getMovement(MovementType type) {
		for (Movement m : movements) {
			if (m.getType()==type)
				return m;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void clearMovements() {
		movements.clear();
	}

	//-------------------------------------------------------------------
	public void addMovement(Movement value) {
		Movement old = getMovement(value.getType());
		if (old==null)
			movements.add(value);
		else {
			old.setRun( old.getRun() + value.getRun() );
			old.setSprint( old.getSprint() + value.getSprint() );
			old.setMeterPerHit( old.getMeterPerHit() + value.getMeterPerHit() );
		}
	}

	//-------------------------------------------------------------------
	public boolean canWalk() {
		return movements.stream().filter(m -> m.getType()==MovementType.GROUND).findFirst().isPresent();
	}

	//-------------------------------------------------------------------
	public boolean canFly() {
		return movements.stream().filter(m -> m.getType()==MovementType.AIR).findFirst().isPresent();
	}

	//-------------------------------------------------------------------
	public boolean canSwim() {
		return movements.stream().filter(m -> m.getType()==MovementType.WATER).findFirst().isPresent();
	}

	//-------------------------------------------------------------------
	public Movement getMovementGround() {
		for (Movement m : movements) {
			if (m.getType()==MovementType.GROUND) return m;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public Movement getMovementAir() {
		for (Movement m : movements) {
			if (m.getType()==MovementType.AIR) return m;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public Movement getMovementWater() {
		for (Movement m : movements) {
			if (m.getType()==MovementType.WATER) return m;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public BodyType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(BodyType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the qualities
	 */
	public List<CritterPowerValue> getCritterPowers() {
		return critterPowers;
	}

	//-------------------------------------------------------------------
	public boolean hasCritterPower(String id) {
		for (CritterPowerValue ref : critterPowers) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public CritterPowerValue getCritterPower(String id) {
		for (CritterPowerValue ref : critterPowers) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addCritterPower(CritterPowerValue value) {
		if (!critterPowers.contains(value))
			critterPowers.add(value);
	}

	//-------------------------------------------------------------------
	public void removeCritterPower(CritterPowerValue value) {
		critterPowers.remove(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the qualities
	 */
	public List<QualityValue> getQualities() {
		return qualities;
	}

	//-------------------------------------------------------------------
	public boolean hasQuality(String id) {
		for (QualityValue ref : qualities) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public QualityValue getQuality(String id) {
		for (QualityValue ref : getQualities()) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addQuality(QualityValue value) {
		if (!qualities.contains(value))
			qualities.add(value);
	}

	//-------------------------------------------------------------------
	public void removeQuality(QualityValue value) {
		qualities.remove(value);
	}

	//-------------------------------------------------------------------
	public List<AttributeValue<ShadowrunAttribute>> getAttributeValues() {
		return attributes;
	}

	//-------------------------------------------------------------------
	public AttributeValue<ShadowrunAttribute> getAttributeValue(ShadowrunAttribute attr) {
		for (AttributeValue<ShadowrunAttribute> aVal : attributes) {
			if (aVal.getModifyable()==attr)
				return aVal;
		}
		return null;
	}

}
