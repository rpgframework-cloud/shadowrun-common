package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author Stefan
 *
 */
@Root(name="priotableentry")
@DataItemTypeKey(id="priotableentry")
public class PriorityTableEntry extends ComplexDataItem {

	@Attribute(required=true)
	protected PriorityType type;
	@Attribute(name="prio",required=true)
	protected Priority priority;
	@Attribute(name="adj")
	protected int adjustmentPoints;
	@ElementListUnion({
		@ElementList(entry="metaopt", type=MetaTypeOption.class),
		@ElementList(entry="magicopt", type=MagicOrResonanceOption.class)
})
	protected List<PriorityOption> options;

	//--------------------------------------------------------------------
	public PriorityTableEntry() {
		options = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	public PriorityTableEntry(PriorityType type, Priority prio) {
		this();
		this.type = type;
		this.priority = prio;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public PriorityType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the priority
	 */
	public Priority getPriority() {
		return priority;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the options
	 */
	public List<PriorityOption> getOptions() {
		return options;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the adjustmentPoints
	 */
	public int getAdjustmentPoints() {
		return adjustmentPoints;
	}

	//-------------------------------------------------------------------
	public void mergeFrom(PriorityTableEntry tmp) {
		options.addAll(tmp.getOptions());
		modifications.addAll(tmp.getOutgoingModifications());
		if (id==null) id=tmp.getId();
//		Collections.sort(this);
		adjustmentPoints = tmp.getAdjustmentPoints();
	}

}
