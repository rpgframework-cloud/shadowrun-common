package de.rpgframework.shadowrun;

import java.lang.System.Logger.Level;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.HasName;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.persist.StringArrayConverter;
import de.rpgframework.shadowrun.persist.AccessConverter;

/**
 * @author Stefan
 *
 */
public class ShadowrunAction extends DataItem implements Comparable<ShadowrunAction> {

	public static enum Category {
		ANY,
		BOOST,
		COMBAT,
		DRIVING,
		POSITION,
		MATRIX,
		MAGIC,
		RESONANCE,
		SOCIAL,
		OTHER
		;
		public String getName() { return ShadowrunCore.getI18nResources().getString("action.category."+name().toLowerCase()); }
	}

	public static enum Type {
		MINOR_NO_COST,
		MINOR,
		MAJOR,
		SPECIAL,
		EDGE,
		BOOST
		;
		public String getNameLong() { return ShadowrunCore.getI18nResources().getString("action.type."+name().toLowerCase(), Locale.getDefault()); }
		public String getNameShort() { return ShadowrunCore.getI18nResources().getString("action.type."+name().toLowerCase()+".short", Locale.getDefault()); }
	}

	public static enum Time {
		INITIATIVE,
		ANYTIME,
		/** Pre dice roll */
		PRE,
		/** Post dice roll */
		POST
		;
		public String getNameLong() { return ShadowrunCore.getI18nResources().getString("action.time."+name().toLowerCase()); }
		public String getNameShort() { return ShadowrunCore.getI18nResources().getString("action.time."+name().toLowerCase()+".short"); }
	}

	public static enum Access implements HasName {
		SELF,
		OUTSIDER,
		USER,
		ADMIN;

		@Override
		public String getName(Locale loc) {return ShadowrunCore.getI18nResources().getString("action.access."+name().toLowerCase()); }

		@Override
		public String getId() { return name().toLowerCase(); }
	}

	public static enum Situation {
		MELEE,
		RANGED,
		ATTACK,
		SKILL,
		ACTION,
		MATRIX,
		MATRIX_COMBAT,
		VEHICLE,
		CHASE,
		OTHER
	}

	@Attribute(name="cat",required=false)
	protected Category category;
	@Attribute(name="type",required=true)
	protected Type type;
	@Attribute
	private Time time;
	@Attribute
	@AttribConvert(StringArrayConverter.class)
	private String[] skill;
	@Attribute
	private String action;

	// For edge actions
	@Attribute
	private int cost;
	@Attribute(name="apply")
	private Situation apply;

	// For matrix actions
	@Attribute
	private boolean illegal;
	@Attribute
	@AttribConvert(AccessConverter.class)
	private List<Access> access;
	@Attribute(name="attr")
 	private ShadowrunAttribute attribute;
//	@Attribute(name="iattr")
//	private ItemAttribute itemAttribute;
	@Attribute(name="opt")
	private Boolean optional;

	@Attribute(name="skillSpec")
	private String skillSpecialID;

	//--------------------------------------------------------------------
	public ShadowrunAction() {
		time = Time.INITIATIVE;
	}

	//--------------------------------------------------------------------
	public ShadowrunAction(String id) {
		this.id = id;
		time = Time.INITIATIVE;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ShadowrunAction other) {
		int cmp = ((Integer)type.ordinal()).compareTo(other.getType().ordinal());
		if (cmp!=0)
			return cmp;
		Integer c1 = cost;
		Integer c2 = other.getCost();
		cmp = c1.compareTo(c2);

		if (cmp!=0)
			return cmp;
		return getName().compareTo(other.getName());
	}

	//--------------------------------------------------------------------
	public Category getCategory() {
		return category;
	}

	//--------------------------------------------------------------------
	public Type getType() {
		return type;
	}

	//--------------------------------------------------------------------
	public Time getTime() {
		return time;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

//	public String getApplyString() {
//		if (apply==null)
//			return "";
//		switch (apply) {
//		case MELEE:
//			return Resource.get(getResourceBundle(), "actiontype.edge.apply.melee");
//		case RANGED:
//			return Resource.get(getResourceBundle(), "actiontype.edge.apply.ranged");
//		case ATTACK:
//			return Resource.get(getResourceBundle(), "actiontype.edge.apply.attack");
//		case SKILL:
//			if (skill==null)
//				return Resource.get(getResourceBundle(), "actiontype.edge.apply.skill");
//			return skill.getName();
//		case ACTION:
//			return action.getName();
//		case MATRIX:
//			return "";
////			return Resource.get(getResourceBundle(), "actiontype.edge.apply.matrix");
//		}
//		return "?"+apply+"?";
//	}

	//--------------------------------------------------------------------
	/**
	 * @return the apply
	 */
	public Situation getApply() {
		return apply;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	//--------------------------------------------------------------------
	public String[] getSkills() {
		return skill;
	}
	//--------------------------------------------------------------------
	public String getSkill() {
		if (skill==null) return null;
		return skill[0];
	}

	//--------------------------------------------------------------------
	/**
	 * @return the illegal
	 */
	public boolean isIllegal() {
		return illegal;
	}

//	//--------------------------------------------------------------------
//	/**
//	 * @return the access
//	 */
//	public List<Access> getAccess() {
//		return access;
//	}

	//--------------------------------------------------------------------
	/**
	 * @return the attribute
	 */
	public ShadowrunAttribute getAttribute() {
		return attribute;
	}

//	//--------------------------------------------------------------------
//	/**
//	 * @return the itemAttribute
//	 */
//	public ItemAttribute getItemAttribute() {
//		return itemAttribute;
//	}

	//--------------------------------------------------------------------
	public String getShortDescription(Locale locale) {
		if (id==null) return "id not set";
		String key = getTypeString()+"."+id.toLowerCase()+".shortdesc";
		if (parentSet==null) {
			System.err.println("No parent dataset for "+getTypeString()+":"+id);
			return key;
		}
		if (parentItem!=null) {
			key = parentItem.getTypeString()+"."+parentItem.getId().toLowerCase()+"."+key;
		}

		// Check license
		DataSet set = null;
		int where = 0;
		try {
			set = getFirstParent(locale);
			if (set==null)
				return "?No ParentSet?";
			where++;
			String foo = set.getResourceString(key,locale);
			return foo;
		} catch (MissingResourceException mre) {
			mre.printStackTrace();
			if (where==0)
				logger.log(Level.ERROR, mre.toString());
			else
				logger.log(Level.ERROR, "Missing resource  "+mre.getKey()+"\t  for locale "+locale+" in "+set.getBaseBundleName());
			return id;
		}
	}

	//--------------------------------------------------------------------
	/**
	 * Use getDescription(Locale)
	 */
	public String getShortDescription() {
		return getShortDescription(Locale.getDefault());
	}

	//-------------------------------------------------------------------
	public List<Access> getAccessLevels() {
		return access;
	}

	//-------------------------------------------------------------------
	public Access getMinimalAccess() {
		Access lowest = null;
		if (access==null) return null;
		for (Access tmp : access) {
			if (lowest==null || tmp.ordinal()<lowest.ordinal())
				lowest = tmp;
		}
		return lowest;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the optional
	 */
	public Boolean isOptional() {
		if (optional==null) return false;
		return optional;
	}
}
