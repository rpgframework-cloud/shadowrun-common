package de.rpgframework.shadowrun;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.chargen.Rule;
import de.rpgframework.genericrpg.chargen.Rule.EffectOn;

/**
 * @author prelle
 *
 */
public interface ShadowrunRules {

	static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(ShadowrunRules.class,
			Locale.ENGLISH, Locale.GERMAN);

	public static Rule CHARGEN_ALLOW_INITIATION = new Rule(EffectOn.CHARGEN,"CHARGEN_ALLOW_INITIATION", Rule.Type.BOOLEAN, RES, "true");
	public static Rule CHARGEN_MAX_KARMA_REMAIN = new Rule(EffectOn.CHARGEN,"CHARGEN_MAX_KARMA_REMAIN", Rule.Type.INTEGER, RES, "5");
	public static Rule CHARGEN_MAX_NUYEN_REMAIN = new Rule(EffectOn.CHARGEN,"CHARGEN_MAX_NUYEN_REMAIN", Rule.Type.INTEGER, RES, "5000");
	public static Rule CHARGEN_NEGATIVE_NUYEN   = new Rule(EffectOn.CHARGEN,"CHARGEN_NEGATIVE_NUYEN"  , Rule.Type.BOOLEAN, RES, "false");

	public static Rule CAREER_PAY_GEAR          = new Rule(EffectOn.CAREER,"CAREER_PAY_GEAR", Rule.Type.BOOLEAN, RES, "true");
	public static Rule CAREER_UNDO_FROM_CAREER  = new Rule(EffectOn.CAREER,"CAREER_UNDO_FROM_CAREER", Rule.Type.BOOLEAN, RES, "true");
	public static Rule CAREER_UNDO_FROM_CHARGEN = new Rule(EffectOn.CAREER,"CAREER_UNDO_FROM_CHARGEN", Rule.Type.BOOLEAN, RES, "false");

	public static Rule IGNORE_GEAR_REQUIREMENTS = new Rule(EffectOn.COMMON,"IGNORE_GEAR_REQUIREMENTS", Rule.Type.BOOLEAN, RES, "false");
	public static Rule ALWAYS_ASK_FOR_FLAGS     = new Rule(EffectOn.UI,"ALWAYS_ASK_FOR_FLAGS", Rule.Type.BOOLEAN, RES, "false");
	public static Rule ALLOW_USED_CULTURED_BIOWARE = new Rule(EffectOn.COMMON,"ALLOW_USED_CULTURED_BIOWARE", Rule.Type.BOOLEAN, RES, "true");

	//-------------------------------------------------------------------
	public static Rule[] values() {
		return new Rule[] {
				CHARGEN_ALLOW_INITIATION,
				CHARGEN_MAX_KARMA_REMAIN,
				CHARGEN_MAX_NUYEN_REMAIN,
				CHARGEN_NEGATIVE_NUYEN,

				CAREER_PAY_GEAR,
				CAREER_UNDO_FROM_CAREER,
				CAREER_UNDO_FROM_CHARGEN,
				IGNORE_GEAR_REQUIREMENTS,
				ALWAYS_ASK_FOR_FLAGS,
				ALLOW_USED_CULTURED_BIOWARE
		};
	}

}
