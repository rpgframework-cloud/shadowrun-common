package de.rpgframework.shadowrun;

import java.util.Locale;
import java.util.MissingResourceException;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.persist.IntegerConverter;
import de.rpgframework.shadowrun.persist.MovementConverter;

/**
 * @author prelle
 *
 */
public enum ShadowrunAttribute implements IAttribute {

	BODY,
	AGILITY,
	REACTION,
	STRENGTH,
	WILLPOWER,
	LOGIC,
	INTUITION,
	CHARISMA,
	EDGE,
	MAGIC,
	RESONANCE,
	ESSENCE_HOLE,
	ESSENCE,
	SPARK, // AI version of essence

	COMPOSURE,
	JUDGE_INTENTIONS,
	MEMORY,
	LIFT_CARRY,

	POWER_POINTS,
	PHYSICAL_MONITOR,
	STUN_MONITOR,
	MONITOR_MATRIX,
	DAMAGE_OVERFLOW,
	MOVEMENT(new MovementConverter()),

	// Defense Pools
	/* REA + INT */
	DEFENSE_POOL_PHYSICAL,
	DEFENSE_POOL_COMBAT_DIRECT,  // WIL + INT
	DEFENSE_POOL_COMBAT_INDIRECT, // REA + WIL
	DEFENSE_POOL_ASTRAL, // INT + LOG
	DEFENSE_POOL_MATRIX, // D+F
	// Full defense
	FULL_DEFENSE_POOL_MATRIX, // D+F+F

	DAMAGE_REDUCTION_PHYSICAL,

	// Soak
	RESIST_DAMAGE, // BOD
	RESIST_DAMAGE_ASTRAL, // WIL
	RESIST_TOXIN,  // BOD + WIL
	RESIST_DRAIN,  // WIL + Trad
	RESIST_DRAIN_ADEPT, // BOD + WIL
	RESIST_FADING, // WIL + LOG
	RESIST_DAMAGE_MATRIX, // FIR
	RESIST_DAMAGE_MATRIX_BIOFEED, // WIL

	INITIATIVE_PHYSICAL,
	INITIATIVE_MATRIX,
	INITIATIVE_MATRIX_VR_COLD,
	INITIATIVE_MATRIX_VR_HOT,
	INITIATIVE_ASTRAL,
	INITIATIVE_DICE_PHYSICAL,
	INITIATIVE_DICE_MATRIX,
	INITIATIVE_DICE_MATRIX_VR_COLD,
	INITIATIVE_DICE_MATRIX_VR_HOT,
	INITIATIVE_DICE_ASTRAL,


	/* Extra cost in addition to lifestyle */
	MONTHLY_COST,

	// SR6 Specific
	HEAT,
	REPUTATION,
	DEFENSE_RATING_PHYSICAL,
	DEFENSE_RATING_ASTRAL,
	DEFENSE_RATING_MATRIX,
	DEFENSE_RATING_SOCIAL,
	ATTACK_RATING_PHYSICAL,
	ATTACK_RATING_ASTRAL,
	ATTACK_RATING_MATRIX,
	MINOR_ACTION_PHYSICAL,
	MINOR_ACTION_ASTRAL,
	MINOR_ACTION_MATRIX,

	INITIATION_RANK,
	SUBMERSION_RANK,
	TRANSHUMANISM_RANK,
	DRACOGENESIS_RANK,
	NEUROMORPISHM_RANK,


	AUGMENTATION_INDEX,
	PERCEPTION,
	DEFENSE_PHYSICAL,
	DEFENSE_ASTRAL,
	DEFENSE_SOCIAL,
	DEFENSE_MATRIX,


	;

	private static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(ShadowrunAttribute.class, Locale.GERMAN, Locale.ENGLISH, Locale.FRENCH, Locale.forLanguageTag("pt"));

	private StringValueConverter<? extends Object> converter;

	//-------------------------------------------------------------------
	private ShadowrunAttribute() {
		// Having a default int converter breaks ENUMs
		//converter = new IntegerConverter();
	}

	//-------------------------------------------------------------------
	private ShadowrunAttribute(StringValueConverter<? extends Object> conv) {
		converter = conv;
	}

     //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.genericrpg.data.IAttribute#getName(java.util.Locale)
     */
    public String getName(Locale locale) {
        try {
        	return ResourceI18N.get(RES, locale, "attribute."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RES.getBaseBundleName());
			return e.getKey();
		}
    }

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.genericrpg.data.IAttribute#getName()
     */
    public String getName() {
       return getName(Locale.getDefault());
    }

    //-------------------------------------------------------------------
   public String getShortName(Locale locale) {
       try {
       	return ResourceI18N.get(RES, locale, "attribute."+this.name().toLowerCase()+".short");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RES.getBaseBundleName());
			return e.getKey();
		}
   }

   //-------------------------------------------------------------------
    public String getShortName() {
      return getShortName(Locale.getDefault());
   }

	//-------------------------------------------------------------------
	public static ShadowrunAttribute[] primaryValues() {
		return new ShadowrunAttribute[]{BODY,AGILITY,REACTION,STRENGTH, WILLPOWER,LOGIC,INTUITION,CHARISMA};
	}

	//-------------------------------------------------------------------
	public static ShadowrunAttribute[] primaryValuesPlusEdge() {
		return new ShadowrunAttribute[]{BODY,AGILITY,REACTION,STRENGTH, WILLPOWER,LOGIC,INTUITION,CHARISMA, EDGE};
	}

	//-------------------------------------------------------------------
	public static ShadowrunAttribute[] primaryAndSpecialValues() {
		return new ShadowrunAttribute[]{BODY,AGILITY,REACTION,STRENGTH, WILLPOWER,LOGIC,INTUITION,CHARISMA,EDGE,MAGIC,RESONANCE};
	}

	//-------------------------------------------------------------------
	public static ShadowrunAttribute[] specialAttributes() {
		return new ShadowrunAttribute[]{EDGE,MAGIC,RESONANCE, POWER_POINTS};
	}

	//-------------------------------------------------------------------
	public boolean isPrimary() {
		for (ShadowrunAttribute key : primaryValues())
			if (this==key) return true;
		return false;
	}

	//-------------------------------------------------------------------
	public boolean isSpecial() {
		for (ShadowrunAttribute key : specialAttributes())
			if (this==key) return true;
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.IAttribute#isDerived()
	 */
	@Override
	public boolean isDerived() {
		return ! (isPrimary() || isSpecial());
	}

	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.items.IItemAttribute#getConverter()
//	 */
//	@Override
	public <T> StringValueConverter<T> getConverter() {
		return (StringValueConverter<T>) converter;
	}

}
