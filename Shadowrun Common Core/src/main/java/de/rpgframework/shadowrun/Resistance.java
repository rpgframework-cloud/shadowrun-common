package de.rpgframework.shadowrun;

/**
 * @author stefa
 *
 */
public enum Resistance {
	
	DAMAGE,
	TOXIN_POOL,
	TOXIN_THRESHOLD

}
