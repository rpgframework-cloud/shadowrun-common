/**
 *
 */
package de.rpgframework.shadowrun;

import java.util.Locale;

/**
 * @author prelle
 *
 */
public enum DamageType {
	PHYSICAL,
	STUN,
	PHYSICAL_SPECIAL,
	STUN_SPECIAL,
	NO_CHANGE,
	SPECIAL
	;
	public String getName()           { return getName(Locale.getDefault()); }
	public String getShortName()      { return getShortName(Locale.getDefault()); }
	public String getName(Locale loc) { return ShadowrunCore.RES.getString("damagetype."+name().toLowerCase(), loc); }
	public String getShortName(Locale loc) { return ShadowrunCore.RES.getString("damagetype."+name().toLowerCase()+".short", loc); }

}
