package de.rpgframework.shadowrun;

/**
 * @author prelle
 *
 */
public interface IRecurringCost {

	//-------------------------------------------------------------------
	public int getCostPerMonth();

	//-------------------------------------------------------------------
	public int getCost();

	//-------------------------------------------------------------------
	/**
	 * Get the months paid
	 */
	public int getDistributed();

}
