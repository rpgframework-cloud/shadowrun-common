package de.rpgframework.shadowrun;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="ritualfeature")
public class RitualFeature extends DataItem {

}
