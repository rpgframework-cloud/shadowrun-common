package de.rpgframework.shadowrun;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="magicresons")
@ElementList(entry="magicreson",type=MagicOrResonanceType.class)
public class MagicOrResonanceTypeList extends ArrayList<MagicOrResonanceType> {

}
