package de.rpgframework.shadowrun;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableNumericalValue;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ChoiceOption;
import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
@Root(name = "qualityval")
public class QualityValue extends ComplexDataItemValue<Quality> implements ModifyableNumericalValue<Quality> {

	public final static UUID MENTOR_SPIRIT_ADVANTAGES = UUID.fromString("76e784ae-1787-4f96-a463-94f30efe9e20");

	private final static Logger logger = System.getLogger(ComplexDataItemValue.class.getPackageName());

	@Element
	private String description;

	/**
	 * Identifier of a quality path, if bought from here
	 */
	@Attribute(name="qpath")
	private String qualityPath;
	@Attribute(name="karma")
	private Integer karmaOverride;

	//-------------------------------------------------------------------
	public QualityValue() {
	}

	//-------------------------------------------------------------------
	public QualityValue(Quality data, int val) {
		super(data, val);
	}

	//-------------------------------------------------------------------
	public String getNameWithRating(Locale loc) {
		if (resolved==null)
			return ref+" "+value;
		if (getModifiedValue()==0) return resolved.getName(loc);
		return resolved.getName(loc)+" "+getModifiedValue();
	}

	//-------------------------------------------------------------------
	public String toString() {
//		if (modifications.isEmpty())
//			return String.format("%s",getName()+"//"+description);
		return String.format("%s = %d (mod=%s  mod=%d  dist=%d)",
				getName(),
				value,
				String.valueOf(incomingModifications),
				getModifier(),
				getDistributed()
				);
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (resolved==null)
			return "("+ref+" "+value+")";

		String ratingSuffix="";
		if (resolved.hasLevel())
			ratingSuffix=" "+getModifiedValue();

		return resolved.getName()+ratingSuffix;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		if (description==null) {
			this.description = null;
		} else
			this.description = description.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	/**
	 * Check if the quality value only exists because its modifications
	 * @return
	 */
	public boolean isRemoveOnReset() {
		if (resolved==null)
			System.err.println("QualityValue: resolved=null for "+ref);
		if (resolved.hasLevel()) {
			return value==0;
		}

		if (incomingModifications.isEmpty()) return false;
		return true;
	}

	//-------------------------------------------------------------------
	public int getKarmaCost() {
		if (karmaOverride!=null)
			return karmaOverride;
		
		int cost = resolved.getKarmaCost();
		if (resolved.hasLevel())
			cost *= getDistributed();
		else if (isAutoAdded()) {
			cost = 0;
			return 0;
		}


		for (Choice choice : resolved.getChoices()) {
			if (choice.getSubOptions()!=null && !choice.getSubOptions().isEmpty()) {
				Decision dec = getDecision(choice.getUUID());
				if (dec!=null) {
					ChoiceOption opt = choice.getSubOption(dec.getValue());
					if (opt!=null && opt.getCost()!=0) {
						cost += (int)opt.getCost();
					}
				}
			}
		}

//		if (resolved.isPositive())
			return cost;
//		return -cost;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ComplexDataItemValue#getChoiceMapRecursivly()
	 */
	@Override
	public Map<UUID,Choice> getChoiceMapRecursivly(CommonCharacter<?, ?, ?, ?> model) {
		Map<UUID, Choice> allChoices = new HashMap<>();
		resolved.getChoices().forEach(c -> allChoices.put(c.getUUID(), c));
		// Make preparations for hardcoded effects choice for mentor spirits
		Choice adeptOrMagicianChoice = null;
		if (getKey().equals("mentor_spirit")) {
			Class<? extends ModifiedObjectType> clz = (Class<? extends ModifiedObjectType>) Persister.getKey(Persister.PREFIX_KEY_INTERFACE+"."+ModifiedObjectType.class.getName());
			ModifiedObjectType found = null;
			for (ModifiedObjectType tmp : clz.getEnumConstants()) {
				if (tmp.toString().equals("MENTOR_SPIRIT_EFFECTS")) {
					found = tmp; break;
				}
			}
			adeptOrMagicianChoice = new Choice(MENTOR_SPIRIT_ADVANTAGES, found);
			allChoices.put(adeptOrMagicianChoice.getUUID(), adeptOrMagicianChoice);
		}

		for (Decision dec : decisions) {
			logger.log(Level.DEBUG, "getDecisionString: dec=" + dec);
			Choice choice = allChoices.get(dec.getChoiceUUID());
//			if (choice==null && getKey().equals("mentor_spirit") && (dec.getValue().equals("adept") || dec.getValue().endsWith("magician"))) {
//				choice = new Choice(UUID.fromString("76e784ae-1787-4f96-a463-94f30efe9e20"), null);
//			}
			if (choice == null && dec.getChoiceUUID().toString().equals("c2d17c87-1cfe-4355-9877-a20fe09c170d")) {
				continue;
			}
			if (choice == null) {
				logger.log(Level.WARNING, "No choice found for decision " + dec + " of " + this);
				continue;
			}

			if (choice.getChooseFrom()==null) {
				logger.log(Level.ERROR, "Quality {0} has decision with unknown choice {1}", this.getKey(), dec.getChoiceUUID());
				continue;
			}

			Object obj = null;
			if (choice.getChooseFrom().toString().equals("SUBSELECT")) {
				obj = choice.getSubOption(dec.getValue());
//			} else if (choice.getChooseFrom().toString().equals("TEXT")) {
//				obj = getResolved().getLocalizedString(Locale.getDefault(), getKey()+".choice."+dec.getChoiceUUID());
			} else if (choice.getChooseFrom().toString().equals("MENTOR_SPIRIT_EFFECTS")) {
				continue;
			} else if (choice.getChooseFrom().toString().equals("CONTACT")) {
				continue;
			} else {
				obj = choice.getChooseFrom().resolve(dec.getValue());
			}
			if (logger.isLoggable(Level.DEBUG))
				logger.log(Level.DEBUG, "obj="+obj+" and "+choice.getChooseFrom());

			DataItem item = null;
			if (obj instanceof DataItem) {
				item = (DataItem) obj;
				if (logger.isLoggable(Level.DEBUG))
					logger.log(Level.DEBUG, "resolved {0} to {1}", dec.getValue(), item);
				if (item != null && item instanceof ComplexDataItem) {
					// Add choices from resolved
					if (logger.isLoggable(Level.DEBUG))
						logger.log(Level.DEBUG, "Choices from {0} are {1}", dec.getValue(),
							((ComplexDataItem) item).getChoices());
					((ComplexDataItem) item).getChoices().forEach(c -> {
						if (logger.isLoggable(Level.DEBUG))
							logger.log(Level.DEBUG, "Add choice " + c + " for resolution");
						allChoices.put(c.getUUID(), c);
					});
					if (item instanceof MentorSpirit) {
						MentorSpirit mentor = (MentorSpirit) item;
						resolveModifications(allChoices, mentor.getAdeptModifications());
						resolveModifications(allChoices, mentor.getMagicianModifications());
					}
				}
			} else if (obj instanceof String) {
			} else if (obj instanceof ShadowrunAttribute) {

			} else {
				logger.log(Level.WARNING, "Don't know to map choice type: " + obj + " / " + choice.getChooseFrom());
			}
		}
		return allChoices;
	}
	@SuppressWarnings("rawtypes")
	public void updateOutgoingModificiations(CommonCharacter<?,?,?,?> model) {
		super.updateOutgoingModificiations(model);

		if (getKey().equals("mentor_spirit")) {
			Choice choice = resolved.getChoices().get(0);
			Decision dec = getDecision( choice.getUUID() );
			if (dec==null) {
				logger.log(Level.ERROR, "No decision made for Mentor Spirit");
			} else {
				MentorSpirit mentor = choice.getChooseFrom().resolve(dec.getValue());
				if (mentor==null) {
					logger.log(Level.ERROR, "Unknown Mentor Spirit ''{0}''", dec.getValue());
				} else {
					for (Modification tmp : mentor.getOutgoingModifications()) {
						if (tmp.getReferenceType()==null) {
							outgoingModifications.add(tmp);
						} else {
							Modification mod = tmp.getReferenceType().instantiateModification(tmp, this, getModifiedValue(), model);
							mod.setSource(mentor);
							logger.log(Level.DEBUG, "add modification {0} from {1}",mod,mentor);
							outgoingModifications.add(mod);
						}
					}
					// Adept and Magician modifications are pulled from SR6 special GetModificationsFromMentorSpirits
				}
			}
		}
	}

	// -------------------------------------------------------------------
	private void resolveModifications(Map<UUID, Choice> allChoices, List<Modification> list) {
		list.stream().filter(m -> m instanceof DataItemModification).forEach(m -> {
			DataItemModification mod = (DataItemModification)m;
			Object resolved2 = mod.getReferenceType().resolve(mod.getKey());
			logger.log(Level.DEBUG,"   resolved {0} of type {2} to {1}", mod.getKey(), resolved2, mod.getReferenceType());
			if (resolved2!=null && resolved2 instanceof ComplexDataItem) {
				((ComplexDataItem) resolved2).getChoices().forEach(c -> {
					logger.log(Level.DEBUG, "Add choice " + c + " for resolution");
					allChoices.put(c.getUUID(), c);
				});
			}
		});
	}

//	//-------------------------------------------------------------------
//	public void clearModifications() {
//		System.err.println("QualityValue; ClearModifications of "+getKey());
//		modifications.clear();
//	}

	//-------------------------------------------------------------------
	public String getShortName() {
		if (resolved==null)
			return "null";

		String ratingSuffix="";
		if (resolved.getMax()>0)
			ratingSuffix=" "+value;

		return resolved.getShortName()+ratingSuffix;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the qualityPath
	 */
	public String getQualityPath() {
		return qualityPath;
	}

	//-------------------------------------------------------------------
	/**
	 * @param qualityPath the qualityPath to set
	 */
	public void setQualityPath(String qualityPath) {
		this.qualityPath = qualityPath;
	}

	//-------------------------------------------------------------------
	/**
	 * @param karmaOverride the karmaOverride to set
	 */
	public void setKarmaOverride(Integer karmaOverride) {
		this.karmaOverride = karmaOverride;
	}

}
