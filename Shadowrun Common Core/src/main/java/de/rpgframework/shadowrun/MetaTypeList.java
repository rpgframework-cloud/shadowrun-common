/**
 * 
 */
package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="metatypes")
@ElementList(entry="metatype",type=MetaType.class,inline=true)
public class MetaTypeList extends ArrayList<MetaType> {

	private static final long serialVersionUID = 3867563481428087973L;

	//-------------------------------------------------------------------
	/**
	 */
	public MetaTypeList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public MetaTypeList(Collection<? extends MetaType> c) {
		super(c);
	}

}
