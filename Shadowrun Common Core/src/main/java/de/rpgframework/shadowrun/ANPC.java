package de.rpgframework.shadowrun;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.ASkillValue;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.data.GenericCore;
import de.rpgframework.genericrpg.data.ISkill;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeDefinition;

/**
 * @author prelle
 *
 */
@Root(name="npc")
@DataItemTypeKey(id="npc")
public abstract class ANPC<S extends ISkill, V extends ASkillValue<S>, P extends ASpell> extends DataItem
	implements IShadowrunLifeform<ShadowrunAttribute, S, V>{

	@Attribute
	private int rating;
	@Attribute
	private NPCType type;

	@ElementList(type = AttributeValue.class, entry = "attribute")
    protected List<AttributeValue<ShadowrunAttribute>> attributes;
	@ElementList(entry = "attrdef", type = ItemAttributeDefinition.class)
	protected List<ItemAttributeDefinition> itemattr;
    @ElementList(type = ASkillValue.class, entry = "skillval")
    protected List<V> skills;
	@ElementList(entry="quality", type = QualityValue.class, inline = false)
	private List<QualityValue> qualities;
    @ElementList(type = SpellValue.class, entry = "spellval")
    protected List<SpellValue<P>> spells;
	@ElementList(entry="adeptpowerval", type = AdeptPowerValue.class, inline = false)
	protected List<AdeptPowerValue> adeptpowers;
	@ElementList(entry="critterpowerval", type = CritterPowerValue.class, inline = false)
	protected List<CritterPowerValue> critterpowers;
    @ElementList(type = CarriedItem.class, entry = "item")
    protected List<CarriedItem> gear;
    @Element
    protected String types;
    @ElementList(type = BodyForm.class, entry = "bodyform")
    protected List<BodyForm> bodies;

	//-------------------------------------------------------------------
	public ANPC() {
		attributes  = new ArrayList<>();
		skills      = new ArrayList<>();
		spells      = new ArrayList<>();
		adeptpowers = new ArrayList<>();
		critterpowers = new ArrayList<>();
		gear        = new ArrayList<>();
		qualities   = new ArrayList<>();
		bodies      = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public void addAttribute(AttributeValue<ShadowrunAttribute> value) {
		if (getAttribute(value.getModifyable())!=null)
			throw new IllegalStateException("Attribute "+value.getModifyable()+" already exists");
		attributes.add(value);
	}

	//-------------------------------------------------------------------
	public AttributeValue<ShadowrunAttribute> getAttribute(ShadowrunAttribute key) {
		for (AttributeValue<ShadowrunAttribute> val : attributes) {
			if (val.getModifyable()==key)
				return val;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.Lifeform#getAttribute(A)
	 */
	@Override
	public AttributeValue<ShadowrunAttribute> getAttribute(String key) {
		for (AttributeValue<ShadowrunAttribute> val : attributes) {
			if (val.getModifyable().name().equals(key))
				return val;
		}
//		for (Entry<ShadowrunAttribute, AttributeValue<ShadowrunAttribute>> entry : derivedAttributes.entrySet()) {
//			if (entry.getKey().name().equals(key))
//				return entry.getValue();
//		}
		return null;
	}

	//-------------------------------------------------------------------
	public List<AttributeValue<ShadowrunAttribute>> getAttributes() {
		return new ArrayList<AttributeValue<ShadowrunAttribute>>(attributes);
	}

	//-------------------------------------------------------------------
	public List<V> getSkillValues() {
		return skills;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getSkillValue(de.rpgframework.genericrpg.data.ISkill)
	 */
	public V getSkillValue(S skill) {
		for (V val : skills) {
			if (val.getModifyable()==skill || val.getModifyable().getId().equals(skill.getId())) {
				return val;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void removeSkillValue(V value) {
		for (V val : skills) {
			if (val.equals(value)) {
				skills.remove(val);
				return;
			}
		}
	}

	//-------------------------------------------------------------------
	public V addSkillValue(V value) {
		if (skills.contains(value))
			return value;
		removeSkillValue(value);
		skills.add(value);
		return value;
	}

	//-------------------------------------------------------------------
	public List<CritterPowerValue> getCritterPowers() {
		return critterpowers;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getSkillValue(de.rpgframework.genericrpg.data.ISkill)
	 */
	public CritterPowerValue getCritterPower(String id) {
		for (CritterPowerValue val : critterpowers) {
			if (val.getKey().equals(id)) {
				return val;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void removeCritterPower(CritterPowerValue value) {
		for (CritterPowerValue val : critterpowers) {
			if (val.equals(value)) {
				critterpowers.remove(val);
				return;
			}
		}
	}

	//-------------------------------------------------------------------
	public CritterPowerValue addCritterPower(CritterPowerValue value) {
		if (critterpowers.contains(value))
			return value;
		removeCritterPower(value);
		critterpowers.add(value);
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public int getRating() {
		return rating;
	}

	// -------------------------------------------------------------------
	public List<ContactType> getTypes() {
		List<ContactType> ret = new ArrayList<>();
		if (types != null) {
			for (String tmp : types.split(",")) {
				ret.add(GenericCore.getItem(ContactType.class, tmp));
			}
		}
		return ret;
	}

	// -------------------------------------------------------------------
	public boolean isContactType(ContactType value) {
		return getTypes().contains(value);
	}

	//-------------------------------------------------------------------
	public NPCType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	public void setType(NPCType value) {
		this.type = value;;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getGear() {
		return gear;
	}

	//-------------------------------------------------------------------
	public List<QualityValue> getQualities() {
		return qualities;
	}
	//-------------------------------------------------------------------
	public List<SpellValue<P>> getSpells() {
		return spells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getSkillValue(de.rpgframework.genericrpg.data.ISkill)
	 */
	public AdeptPowerValue getAdeptPower(String id) {
		for (AdeptPowerValue val : adeptpowers) {
			if (val.getKey().equals(id)) {
				return val;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void removeAdeptPower(AdeptPowerValue value) {
		for (AdeptPowerValue val : adeptpowers) {
			if (val.equals(value)) {
				adeptpowers.remove(val);
				return;
			}
		}
	}

	//-------------------------------------------------------------------
	public AdeptPowerValue addAdeptPower(AdeptPowerValue value) {
		if (adeptpowers.contains(value))
			return value;
		removeAdeptPower(value);
		adeptpowers.add(value);
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun.IShadowrunLifeform#getBodyForms()
	 */
	@Override
	public List<BodyForm> getBodyForms() {
		return bodies;
	}
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun.IShadowrunLifeform#getBodyForm(de.rpgframework.shadowrun.BodyType)
	 */
	@Override
	public BodyForm getBodyForm(BodyType type) {
		for (BodyForm m : bodies) {
			if (m.getType()==type) {
				return m;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun.IShadowrunLifeform#clearBodyForms()
	 */
	@Override
	public void clearBodyForms() {
		bodies.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.shadowrun.IShadowrunLifeform#addBodyForm(de.rpgframework.shadowrun.BodyForm)
	 */
	@Override
	public void addBodyForm(BodyForm value) {
		for (BodyForm m : bodies) {
			if (m.getType()==value.getType()) {
				int index = bodies.indexOf(m);
				bodies.remove(m);
				bodies.add(index, value);
				return;
			}
		}
	}

}
