package de.rpgframework.shadowrun.persist;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.genericrpg.data.ReferenceException;
import de.rpgframework.shadowrun.ShadowrunAttribute;

/**
 * @author prelle
 *
 */
public class AttributeConverter implements StringValueConverter<ShadowrunAttribute> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(ShadowrunAttribute value) throws Exception {
		return value.name();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public ShadowrunAttribute read(String idref) throws Exception {
		try {
			return ShadowrunAttribute.valueOf(idref.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new ReferenceException(null, idref);
		}
	}

}
