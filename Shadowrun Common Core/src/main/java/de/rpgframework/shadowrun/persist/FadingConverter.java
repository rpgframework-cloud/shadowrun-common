package de.rpgframework.shadowrun.persist;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.shadowrun.ComplexForm;

public class FadingConverter implements StringValueConverter<Integer> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Integer read(String v) throws Exception {
		v = v.trim();
		if ("HITS".equals(v)) return ComplexForm.FADING_HITS;
		if ("RATING".equals(v)) return ComplexForm.FADING_RATING;
		if ("VARIES".equals(v)) return ComplexForm.FADING_VARIABLE;
		return Integer.parseInt(v);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Integer v) throws Exception {
		if (v==ComplexForm.FADING_HITS) return "HITS";
		if (v==ComplexForm.FADING_RATING) return "RATING";
		if (v==ComplexForm.FADING_VARIABLE) return "VARIES";
		
		return String.valueOf(v);
	}
	
}