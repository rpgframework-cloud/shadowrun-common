package de.rpgframework.shadowrun.persist;

import java.util.StringTokenizer;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.shadowrun.Movement;
import de.rpgframework.shadowrun.Movement.MovementType;

/**
 *
 */
public class MovementConverter implements StringValueConverter<Movement> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Movement value) throws Exception {
		if (value.getType() == MovementType.GROUND)
			return String.format("%d/%d/%d", value.getRun(), value.getSprint(), value.getMeterPerHit());
		return String.format("%d/%d/%d/%s", value.getRun(), value.getSprint(), value.getMeterPerHit(), value.getType());

	}

	//-------------------------------------------------------------------
	public static Movement convert(String v) {
		v = v.trim();

		Movement move = new Movement();
		StringTokenizer tok = new StringTokenizer(v,"/");
		if (tok.hasMoreTokens()) {
			move.setRun(Integer.parseInt(tok.nextToken()));
		}
		if (tok.hasMoreTokens()) {
			move.setSprint(Integer.parseInt(tok.nextToken()));
		}
		if (tok.hasMoreTokens()) {
			move.setMeterPerHit(Integer.parseInt(tok.nextToken()));
		}
		if (tok.hasMoreTokens()) {
			move.setType(MovementType.valueOf(tok.nextToken().toString().trim()));
		}

		return move;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Movement read(String v) {
		return convert(v);
	}

}
