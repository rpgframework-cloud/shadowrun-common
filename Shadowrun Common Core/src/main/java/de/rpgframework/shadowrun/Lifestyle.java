package de.rpgframework.shadowrun;

import java.util.UUID;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class Lifestyle extends ComplexDataItemValue<LifestyleQuality> implements NumericalValue<LifestyleQuality>, IRecurringCost {

	@Attribute(name="name")
	private String name;
	@Attribute(name="sin")
	private UUID sin;
	@Element
	private String description;
	@Attribute(name="prim")
	private boolean primary;

	//-------------------------------------------------------------------
	public Lifestyle() {}

	//-------------------------------------------------------------------
	public Lifestyle(LifestyleQuality value) {
		super(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sin
	 */
	public UUID getSIN() {
		return sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sin the sin to set
	 */
	public void setSIN(UUID sin) {
		this.sin = sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		if (description!=null)
			this.description = description.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name==null)
			this.name = name;
		else
			this.name = name.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	/**
	 * @return the primary
	 */
	public boolean isPrimary() {
		return primary;
	}

	//-------------------------------------------------------------------
	/**
	 * @param primary the primary to set
	 */
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	//-------------------------------------------------------------------
	public int getCostPerMonth() {
		int ret = resolved.getCost();
		return ret;
	}

	//-------------------------------------------------------------------
	public int getCost() {
		return getCostPerMonth() * getDistributed();
	}

}
