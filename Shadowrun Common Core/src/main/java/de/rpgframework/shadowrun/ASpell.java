package de.rpgframework.shadowrun;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataErrorException;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.data.GenericCore;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="spell")
public abstract class ASpell extends ComplexDataItem implements Comparable<ASpell> {

	static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(ASpell.class, Locale.ENGLISH, Locale.GERMAN);

	@DataItemTypeKey(id="spellcategory")
	public enum Category {
		COMBAT,
		DETECTION,
		HEALTH,
		ILLUSION,
		MANIPULATION
		;
		public String getName() { return getName(Locale.getDefault()); }
		public String getShortName() { return getShortName( Locale.getDefault()); }
		public String getName(Locale loc) { return RES.getString("spellcategory."+name().toLowerCase(), loc); }
		public String getShortName(Locale loc) { return RES.getString("spellcategory."+name().toLowerCase()+".short", loc); }
	}

	public enum Type {
		PHYSICAL,
		MANA,
		RESONANCE
		;
		public String getName() { return RES.getString("spelltype."+name().toLowerCase()); }
		public String getShortName() { return RES.getString("spelltype."+name().toLowerCase()+".short"); }
		public String getName(Locale loc) { return RES.getString("spelltype."+name().toLowerCase(), loc); }
		public String getShortName(Locale loc) { return RES.getString("spelltype."+name().toLowerCase()+".short", loc); }
	}

	public enum Range {
		LINE_OF_SIGHT,
		LINE_OF_SIGHT_AREA,
		TOUCH,
		SELF,
		SELF_AREA,
		SPECIAL
		;
		public String getName() { return RES.getString("spellrange."+name().toLowerCase()); }
		public String getShortName() { return RES.getString("spellrange."+name().toLowerCase()+".short"); }
		public String getName(Locale loc) { return RES.getString("spellrange."+name().toLowerCase(), loc); }
		public String getShortName(Locale loc) { return RES.getString("spellrange."+name().toLowerCase()+".short", loc); }
	}

	public enum Duration {
		INSTANTANEOUS,
		IMMEDIATE,
		SUSTAINED,
		PERMANENT,
		LIMITED,
		SPECIAL,
		ALWAYS
		;
		public String getName() { return RES.getString("spellduration."+name().toLowerCase()); }
		public String getShortName() { return RES.getString("spellduration."+name().toLowerCase()+".short"); }
		public String getName(Locale loc) { return RES.getString("spellduration."+name().toLowerCase(), loc); }
		public String getShortName(Locale loc) { return RES.getString("spellduration."+name().toLowerCase()+".short", loc); }
	}

//	public enum Damage {
//		PHYSICAL,
//		STUN,
//		SPECIAL
//		;
//		public String getName() { return RES.getString("spelldamage."+name().toLowerCase()); }
//		public String getShortName() { return RES.getString("spelldamage."+name().toLowerCase()+".short"); }
//		public String getName(Locale loc) { return RES.getString("spelldamage."+name().toLowerCase(), loc); }
//		public String getShortName(Locale loc) { return RES.getString("spelldamage."+name().toLowerCase()+".short", loc); }
//	}

	@Attribute(name="cat")
	private Category category;
	@Attribute
	private Type   type;
	@Attribute
	private Range  range;
	@Attribute(name="dmg")
	private DamageType damage;
	@Attribute(name="dur")
	private Duration duration;
	@Attribute
	private int  drain;
	@Attribute
	private boolean opposed;
	@Attribute
	private boolean essence;
	@Attribute(name="multi")
	private boolean multiple;

	@ElementList(entry="spellfeature",type=SpellFeatureReference.class, inline=true)
	private List<SpellFeatureReference> features;

	//-------------------------------------------------------------------
	public ASpell() {
		features = new ArrayList<SpellFeatureReference>();
	}

	//-------------------------------------------------------------------
	public ASpell(String id) {
		this();
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append(id+"\n");
		buf.append(features+"\n");
		buf.append("Type:"+type+"  \tRange:"+range+"   Damage:"+damage+"\n");
		buf.append("Duration:"+duration+"  \tDrain:F"+drain);

		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ASpell o) {
		int foo = category.compareTo(o.getCategory());
		if (foo!=0)
			return foo;
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	//-------------------------------------------------------------------
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the range
	 */
	public Range getRange() {
		return range;
	}

	//-------------------------------------------------------------------
	/**
	 * @param range the range to set
	 */
	public void setRange(Range range) {
		this.range = range;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the damage
	 */
	public DamageType getDamage() {
		return damage;
	}

	//-------------------------------------------------------------------
	/**
	 * @param damage the damage to set
	 */
	public void setDamage(DamageType damage) {
		this.damage = damage;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the duration
	 */
	public Duration getDuration() {
		return duration;
	}

	//-------------------------------------------------------------------
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the drain
	 */
	public int getDrain() {
		return drain;
	}

	//-------------------------------------------------------------------
	/**
	 * @param drain the drain to set
	 */
	public void setDrain(int drain) {
		this.drain = drain;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the features
	 */
	public List<SpellFeatureReference> getFeatures() {
		return features;
	}

	//-------------------------------------------------------------------
	/**
	 * @param features the features to set
	 */
	public void setFeatures(List<SpellFeatureReference> features) {
		this.features = features;
	}

	//-------------------------------------------------------------------
	/**
	 * Used in deriving classes to perform validation checks on loading,
	 * if necessary
	 * @return Error message or NULL
	 */
	public void validate() throws DataErrorException {
		for (SpellFeatureReference ref : features) {
			if (ref.getFeature()==null) {
				SpellFeature feat = GenericCore.getItem(SpellFeature.class, ref.getKey());
				ref.setResolved(feat);
				if (feat==null)
					throw new DataErrorException(this, "Unresolved spell feature: "+ref.getKey());
			}
		}
	}

	//-------------------------------------------------------------------
	public boolean isOpposed() {
		return opposed;
	}

	//-------------------------------------------------------------------
	public void setOpposed(boolean opposed) {
		this.opposed = opposed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the essence
	 */
	public boolean isEssence() {
		return essence;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the multiple
	 */
	public boolean isMultiple() {
		return multiple;
	}

	//-------------------------------------------------------------------
	/**
	 * @param multiple the multiple to set
	 */
	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

}
