package de.rpgframework.shadowrun;

public enum PriorityType {
	METATYPE,
	ATTRIBUTE,
	MAGIC,
	SKILLS,
	RESOURCES,
	KARMA
}