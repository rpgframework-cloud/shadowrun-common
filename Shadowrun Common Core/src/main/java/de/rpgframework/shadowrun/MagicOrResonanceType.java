package de.rpgframework.shadowrun;

import java.text.Collator;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="mor")
public class MagicOrResonanceType extends ComplexDataItem implements Comparable<MagicOrResonanceType> {

	@Attribute
	private boolean magic;
	@Attribute
	private boolean resonance;
	@Attribute
	private boolean spells;
	@Attribute
	private boolean powers;
	@Attribute
	private boolean paysPowers;
	@Attribute
	private boolean aspected;
	@Attribute
	private int cost;

	//-------------------------------------------------------------------
	public MagicOrResonanceType() {
	}

	//-------------------------------------------------------------------
	public MagicOrResonanceType(String id) {
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MagicOrResonanceType o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//-------------------------------------------------------------------
	public boolean usesMagic() {
		return magic;
	}

	//-------------------------------------------------------------------
	public boolean usesResonance() {
		return resonance;
	}

	//-------------------------------------------------------------------
	public boolean usesSpells() {
		return spells;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the powers
	 */
	public boolean usesPowers() {
		return powers;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the paysPowers
	 */
	public boolean paysPowers() {
		return paysPowers;
	}

	//-------------------------------------------------------------------
	public int getCost() {
		return cost;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the aspected
	 */
	public boolean isAspected() {
		return aspected;
	}

}
