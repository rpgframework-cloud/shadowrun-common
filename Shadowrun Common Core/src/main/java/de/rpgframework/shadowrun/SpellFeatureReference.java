/**
 * 
 */
package de.rpgframework.shadowrun;

import de.rpgframework.genericrpg.data.DataItemValue;

/**
 * @author prelle
 *
 */

public class SpellFeatureReference extends DataItemValue<SpellFeature> {

	//-------------------------------------------------------------------
	public SpellFeatureReference() {	
	}

	//-------------------------------------------------------------------
	public SpellFeatureReference(SpellFeature feat) {
		super(feat);
	}

	//-------------------------------------------------------------------
	public SpellFeature getFeature() {
		return getModifyable();
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.data.ResolvableDataItem#validateAndResolve()
//	 */
//	@Override
//	public void validateAndResolve() {
//		resolved = ShadowrunReference.SPELLFEATURE.resolveAsDataItem(ref);
//		if (resolved==null)
//			throw new IllegalArgumentException("Unknown SpellFeature '"+ref+"'");
//	}

}
