package de.rpgframework.shadowrun;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.AttributeValue;

/**
 * @author prelle
 *
 */
@Root(name="attributes")
@ElementList(entry="attr",type=AttributeValue.class)
public class Attributes extends ArrayList<AttributeValue> {

	private static final long serialVersionUID = 1L;

	private final static Logger logger = System.getLogger("shadowrun");

	private transient Map<ShadowrunAttribute, AttributeValue> secondary;

	//-------------------------------------------------------------------
	/**
	 */
	public Attributes() {
		secondary = new HashMap<ShadowrunAttribute, AttributeValue>();
		for (ShadowrunAttribute attr : ShadowrunAttribute.values()) {
			AttributeValue toAdd = new AttributeValue(attr, 0);
			add(toAdd);
		}
	}

	//-------------------------------------------------------------------
	public boolean containsAttribute(ShadowrunAttribute key) {
		for (AttributeValue pair : this) {
			if (pair.getModifyable()==key) {
				return true;
			}
		}
		return secondary.containsKey(key);
	}

	//-------------------------------------------------------------------
	@Override
	public boolean add(AttributeValue val) {
		ShadowrunAttribute attr = (ShadowrunAttribute)val.getModifyable();
		if (attr.isPrimary() || attr.isSpecial()) {
			for (AttributeValue tmp : new ArrayList<AttributeValue>(this)) {
				if (tmp.getModifyable()==val.getModifyable()) {
					super.remove(tmp);
				}
			}
			return super.add(val);
		} else {
			if (secondary.containsKey(val.getModifyable()))
				throw new IllegalStateException("Already exists");
			secondary.put(attr, val);
			return true;
		}

	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		for (AttributeValue tmp : this)
			buf.append("\n "+tmp.getModifyable()+" \t "+tmp);
//		for (ShadowrunAttribute tmp : ShadowrunAttribute.secondaryValues())
//			buf.append("\n "+tmp+" \t "+get(tmp));

		return buf.toString();
	}

	//-------------------------------------------------------------------
	public int getValue(ShadowrunAttribute key) {
		return get(key).getModifiedValue();
	}

	//-------------------------------------------------------------------
	public AttributeValue get(ShadowrunAttribute key) {
		if (key==null)
			throw new NullPointerException("Attribute may not be null");
		for (AttributeValue pair : this) {
			if (pair.getModifyable()==key) {
				return pair;
			}
		}
		AttributeValue pair = secondary.get(key);
		if (pair!=null) {
			return pair;
		}

		logger.log(Level.ERROR,"Something accessed an unset attribute: "+key+"\nsecondary="+secondary);
		throw new NoSuchElementException(String.valueOf(key));
	}

//	//--------------------------------------------------------------------
//	public void addModification(ValueModification mod) {
//		get(mod.getAttribute()).addModification(mod);
//	}
//
//	//--------------------------------------------------------------------
//	public void removeModification(AttributeModification mod) {
//		get(mod.getAttribute()).removeModification(mod);
//	}

}
