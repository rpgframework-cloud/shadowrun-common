package de.rpgframework.shadowrun;

import java.util.Locale;

/**
 * @author prelle
 *
 */
public enum SkillType {
	
	COMBAT,
	PHYSICAL,
	SOCIAL,
	MAGIC,
	RESONANCE,
	TECHNICAL,
	VEHICLE,
	ACTION,
	LANGUAGE,
	KNOWLEDGE,
	NOT_SET,
	;

	public String getName(Locale loc) {
		return ShadowrunCore.getI18nResources().getString("skill.type."+name().toLowerCase(), loc);
	}
	public static SkillType[] regularValues() {
		return new SkillType[]{COMBAT, PHYSICAL, SOCIAL, MAGIC, RESONANCE, TECHNICAL, VEHICLE};
	}
	public static SkillType[] individualValues() {
		return new SkillType[]{LANGUAGE, KNOWLEDGE};
	}

}
