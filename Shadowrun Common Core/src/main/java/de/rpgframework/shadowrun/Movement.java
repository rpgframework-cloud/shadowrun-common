package de.rpgframework.shadowrun;

import java.util.Locale;

/**
 * @author prelle
 *
 */
public class Movement {

	public enum MovementType {
		GROUND,
		AIR,
		WATER
		;
		public String getShortName(Locale loc) {
			return ShadowrunCore.RES.getString("movement."+name().toLowerCase()+".short",loc);
		}
	}

	private MovementType type = MovementType.GROUND;
	/** Meters */
	private int run;
	/** Meters per hit on sprint test */
	private int sprint;
	/** Meters */
	private int hits;

	//-------------------------------------------------------------------
	public Movement() {
	}

	//-------------------------------------------------------------------
	public Movement(MovementType type, int run, int sprint, int hits) {
		this.type   = type;
		this.sprint = sprint;
		this.run    = run;
		this.hits   = hits;
	}

	//-------------------------------------------------------------------
	public Movement(Movement move) {
		this.type = move.type;
		this.hits = move.hits;
		this.run  = move.run;
		this.sprint = move.sprint;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Move("+type+", "+run+", "+sprint+", "+hits+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public MovementType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(MovementType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	public int getMeterPerHit() {
		return hits;
	}

	//-------------------------------------------------------------------
	public void setMeterPerHit(int value) {
		this.hits = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the run
	 */
	public int getRun() {
		return run;
	}

	//-------------------------------------------------------------------
	/**
	 * @param run the run to set
	 */
	public void setRun(int run) {
		this.run = run;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sprint
	 */
	public int getSprint() {
		return sprint;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sprint the sprint to set
	 */
	public void setSprint(int sprint) {
		this.sprint = sprint;
	}

}
