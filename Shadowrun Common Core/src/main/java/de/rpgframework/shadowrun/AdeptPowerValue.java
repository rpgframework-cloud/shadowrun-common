package de.rpgframework.shadowrun;

import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableNumericalValue;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
@Root(name = "adeptpowerval")
public class AdeptPowerValue extends ComplexDataItemValue<AdeptPower> implements ModifyableNumericalValue<AdeptPower> {

	//-------------------------------------------------------------------
	public AdeptPowerValue() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param data
	 */
	public AdeptPowerValue(AdeptPower data) {
		super(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @param data
	 * @param val
	 */
	public AdeptPowerValue(AdeptPower data, int val) {
		super(data, val);
	}

	//-------------------------------------------------------------------
	public float getCost() {
		if (resolved.hasLevel()) {
			return resolved.getCostForLevel( getModifiedValue() );
		}
		return resolved.getCost();
	}

}
