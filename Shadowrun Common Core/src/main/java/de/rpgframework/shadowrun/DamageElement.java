/**
 *
 */
package de.rpgframework.shadowrun;

import java.util.Locale;

import de.rpgframework.HasName;

/**
 * @author prelle
 *
 */
public enum DamageElement implements HasName {

	REGULAR,
	FIRE,
	COLD,
	ELECTRICITY,
	CHEMICAL
	;
	public String getName()           { return getName(Locale.getDefault()); }
	public String getShortName()      { return getShortName(Locale.getDefault()); }
	public String getName(Locale loc) { return ShadowrunCore.RES.getString("damageelement."+name().toLowerCase(), loc); }
	public String getShortName(Locale loc) { return ShadowrunCore.RES.getString("damageelement."+name().toLowerCase()+".short", loc); }
	public String getId()             { return name().toLowerCase(); }

}
