package de.rpgframework.shadowrun;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="critterpower")
public class CritterPower extends ComplexDataItem {

	public static enum Action {
		MINOR,
		MAJOR,
		AUTO,
		SPECIAL
		;
		public String getNameLong() { return ShadowrunCore.getI18nResources().getString("action.type."+name().toLowerCase()); }
		public String getNameShort() { return ShadowrunCore.getI18nResources().getString("action.type."+name().toLowerCase()+".short"); }
	}

	@Attribute
	private ASpell.Type type;
	@Attribute
	private Action action;
	@Attribute
	private ASpell.Range range;
	@Attribute(name="dur")
	private ASpell.Duration duration;
	@Attribute
	private boolean level;

	//-------------------------------------------------------------------
	/**
	 */
	public CritterPower() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ASpell.Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the action
	 */
	public Action getAction() {
		return action;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the range
	 */
	public ASpell.Range getRange() {
		return range;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the duration
	 */
	public ASpell.Duration getDuration() {
		return duration;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public boolean hasLevel() {
		return level;
	}

}
