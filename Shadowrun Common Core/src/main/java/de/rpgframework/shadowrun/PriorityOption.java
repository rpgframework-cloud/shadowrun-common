package de.rpgframework.shadowrun;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.ModificationList;

/**
 * @author Stefan
 *
 */
public abstract class PriorityOption<T extends DataItem> extends ComplexDataItem implements Comparable<PriorityOption<T>> {
	
	protected transient T resolved;
	
	//--------------------------------------------------------------------
	protected PriorityOption() {
		this.modifications = new ModificationList();
	}
	
	//--------------------------------------------------------------------
	public T getResolved() { return resolved; }

}
