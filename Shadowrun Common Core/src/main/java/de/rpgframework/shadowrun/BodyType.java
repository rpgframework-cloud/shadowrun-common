package de.rpgframework.shadowrun;

/**
 * @author prelle
 *
 */
public enum BodyType {
	
	/** Normal metatype/metavariant/metasapient - may have metagenic qualities */
	METAHUMAN,
	/** Animal form and metahuman form */
	SHAPESHIFTER,
	/** Requires metahuman - not allowed for metasapients */
	DRAKE,
	DRAKE_LATENT,

}
