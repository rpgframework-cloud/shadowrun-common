package de.rpgframework.shadowrun;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 *
 */
@DataItemTypeKey(id="language")
public class Language extends DataItem {

	public static enum Type {
		COMMON,
		TRIBAL,
		RACIAL,
		LINGO,
		SPECIAL,
		DEAD
	}

	@Attribute
	private Type type;

	//-------------------------------------------------------------------
	public Language() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

}
