/**
 *
 */
package de.rpgframework.shadowrun;

import java.util.Locale;

import de.rpgframework.HasName;

/**
 * @author prelle
 *
 */
public enum ShadowrunElement implements HasName {

	FIRE,
	WATER,
	AIR,
	EARTH
	;
	public String getName()           { return getName(Locale.getDefault()); }
	public String getShortName()      { return getShortName(Locale.getDefault()); }
	public String getName(Locale loc) { return ShadowrunCore.RES.getString("element."+name().toLowerCase(), loc); }
	public String getShortName(Locale loc) { return ShadowrunCore.RES.getString("element."+name().toLowerCase()+".short", loc); }
	public String getId()             { return name().toLowerCase(); }

}
