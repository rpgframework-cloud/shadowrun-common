open module shadowrun.common {
	exports de.rpgframework.shadowrun;
	exports de.rpgframework.shadowrun.ctrl;
	exports de.rpgframework.shadowrun.generators;
	exports de.rpgframework.shadowrun.items;
	exports de.rpgframework.shadowrun.persist;
	exports de.rpgframework.shadowrun.proc;
	exports de.rpgframework.shadowrun.vehicle;

	provides de.rpgframework.random.GeneratorInitializer with de.rpgframework.shadowrun.generators.ShadowrunGeneratorInitializer;

	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.rules;
	requires transitive de.rpgframework.generator;
	requires java.xml;
	requires simple.persist;
	requires com.google.gson;

}