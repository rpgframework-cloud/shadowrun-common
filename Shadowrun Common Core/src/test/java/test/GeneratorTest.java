package test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prelle.simplepersist.Persister;

import de.rpgframework.classification.Gender;
import de.rpgframework.classification.Genre;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.random.GeneratorType;
import de.rpgframework.random.Plot;
import de.rpgframework.random.RandomGeneratorRegistry;
import de.rpgframework.shadowrun.generators.GenericShadowrunGenerators;
import de.rpgframework.shadowrun.generators.FilterCulture;
import de.rpgframework.shadowrun.generators.TextExport;

/**
 * @author prelle
 *
 */
class GeneratorTest {
	
	private static Persister persist = new Persister();

	//-------------------------------------------------------------------
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		GenericShadowrunGenerators.initialize();
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() throws Exception {
		Object gen =RandomGeneratorRegistry.generate(GeneratorType.NAME_PERSON, List.of(FilterCulture.ADL), List.of(Gender.FEMALE), Map.of());
		System.out.println("Generated name: "+gen);
		assertNotNull(gen);
		
		gen =RandomGeneratorRegistry.generate(GeneratorType.RUN, List.of(Genre.CYBERPUNK), List.of(FilterCulture.ADL), Map.of());
		System.out.println("Generated plot: "+gen);
		assertNotNull(gen);
		persist.write( (Plot)gen, System.out);
		
		TextExport txtExport = new TextExport();
		txtExport.export(gen, System.out, Locale.GERMAN);
		System.out.println("Plot export done");
	}

}
